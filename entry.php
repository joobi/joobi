<?php
if ( ( defined('_JEXEC') ) && !defined('JOOBI_SECURE') ) define( 'JOOBI_SECURE', true );
defined('JOOBI_SECURE') or die('J....');

### Copyright (c) 2006-2013 Joobi Limited. All rights reserved.
### license GNU GPLv3 , link http://www.joobi.co

//include the right bridge
if(!defined('JOOBI_FRAMEWORK')) {
	if ( !defined('DS')) define('DS', DIRECTORY_SEPARATOR );
	//search for the CMS and define JOOBI_FRAMEWORK
	//this line is used by joobiplex when not yet packaged
	require( 'node' . DS . 'install' . DS . 'install' . DS . 'files' . DS . 'discover.php' );
	WDiscoverEntry::discover();

	if(!defined('JOOBI_FRAMEWORK')) {
		echo 'The entry point file did not provide a CMS name. Please contact the support.';
		exit();
	}//endif
}//endif



if(!defined('JOOBI_FOLDER')) define('JOOBI_FOLDER', 'joobi' );
if(!defined('JOOBI_DS_ROOT')) define( 'JOOBI_DS_ROOT', dirname(dirname((__FILE__))) . DS );
if(!defined('JOOBI_DS_CONFIG')) define( 'JOOBI_DS_CONFIG', dirname(__FILE__) . DS );
if (!defined('JOOBI_DS_NODE')) define( 'JOOBI_DS_NODE' , dirname(__FILE__) . DS .'node' . DS ); // where library is located for the present version

// Joobi library path
if ( !defined('JOOBI_LIB_CORE') ) define( 'JOOBI_LIB_CORE' , JOOBI_DS_NODE . 'library' . DS ); // where library is located for the present version
// JOOBI_LIB_CORE


//load the api for the cms
require_once( JOOBI_DS_NODE . 'api' . DS . 'addon' . DS . JOOBI_FRAMEWORK . DS . JOOBI_FRAMEWORK . '.php' );
$appName = 'WApplication_'. JOOBI_FRAMEWORK;

if ( class_exists($appName) ) {
	if ( !isset($params) ) $params=null;
	if ( isset($module) ) $params->module = $module;
	$app = new $appName();
	if ( !isset($joobiEntryPoint) ) $joobiEntryPoint='';
	$html = $app->make($joobiEntryPoint,$params);
} else {
	echo 'JOOBI ERROR 56399. Please contact support.';	// this is a code error to not reveal information to hackers
	exit();
}//endif

// echo results
if ( !empty($html) ) echo $html;

