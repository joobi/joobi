<?php
if ( defined('_JEXEC') && !defined('JOOBI_SECURE') ) define( 'JOOBI_SECURE', true );
defined('_JEXEC') or die;
/**
* @link http://www.joobi.co
* @copyright Copyright (C) 2006-2013 Joobi Limited All rights reserved.
* This file is released under the LGPL
*/
function JmarketBuildRoute(&$query){
if(!class_exists("WPage")){
$status=include(JPATH_ROOT.DIRECTORY_SEPARATOR."joobi".DIRECTORY_SEPARATOR."entry.php");
if(!$status) return;
}
return WPage::buildURL($query);
}//endfct

function JmarketParseRoute($segments){
if(!class_exists("WPage")){
$status=include(JPATH_ROOT.DIRECTORY_SEPARATOR."joobi".DIRECTORY_SEPARATOR."entry.php");
if(!$status) return;
}
return WPage::interpretURL($segments);
}//endfct
