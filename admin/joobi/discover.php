<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('JOOBI_SECURE') or defined('_JEXEC') or die('J...');

### Copyright (c) 2006-2012 Joobi Limited. All rights reserved.
### license GNU GPLv3 , link http://www.joobi.co





class WDiscoverEntry {






	public static function discover() {
		
		if ( defined('JVERSION') ) {
			$I1TP = explode( '_', JVERSION );
			if ( version_compare( $I1TP[0], '3.0.0', '>='  ) ) {
				define('JOOBI_FRAMEWORK', 'joomla30' );
				return true;
			} elseif ( version_compare( JVERSION,'1.6.0','>=') ) {
				define('JOOBI_FRAMEWORK', 'joomla16' );
				return true;
			} elseif ( version_compare(JVERSION,'1.5.0','>=') ) {
				define('JOOBI_FRAMEWORK', 'joomla15' );
				return true;
			}		}
		
				if(!defined('JOOBI_FRAMEWORK')){
			die( 'unknown Framework' );
		}
		return false;
	}

}