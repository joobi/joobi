<?php
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Joobi_Config{

var $model = array(

'tablename'=>'#__model_node'

);

var $table = array(

'tablename'=>'#__dataset_tables'

);

var $db = array(

'tablename'=>'#__dataset_node'

);

var $multiDB = false;

}//endclass