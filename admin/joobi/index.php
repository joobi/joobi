<?php
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;
$joobiEntryPoint = __FILE__ ;
define('JOOBI_SECURE', 1);
define('JOOBI_FRAMEWORK_CONFIG_FILE','configuration.php');
define('JOOBI_FRAMEWORK_OVERRIDE','joomla16');
$protocol=isset( $_REQUEST["netcom"] ) ? $_REQUEST["netcom"] :"";
if (!empty($protocol)) define("JOOBI_FRAMEWORK","netcom");
else define("JOOBI_FRAMEWORK","joobi");
require( dirname(__FILE__). DIRECTORY_SEPARATOR  . 'entry.php' );