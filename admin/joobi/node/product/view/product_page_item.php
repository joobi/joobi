<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'item.view.item_page_item' , JOOBI_DS_NODE );
class Product_Product_page_item_view extends Item_Item_page_item_view {


	protected function prepareView() {



		parent::prepareView();



		$this->_catalogItemPageC = WClass::get( 'catalog.itempage' );



		$priceType = $this->getValue( 'type', 'product.price' );	


		
		WGlobals::set( 'itemShowQuantity', $this->pageParams->showQuantity, 'joobi' );



		
		$targettotal = $this->getValue('targettotal');

		if ( !empty( $targettotal ) ) {

			$target = $this->getValue('target');

			$progressbarC = WClass::get( 'main.progressbar' );

			$progressbarC->targetTotal = $targettotal;

			$progressbarC->target = $target;

			$this->setValue('progressBar', $progressbarC->createProgressBarHTML() );

		}


		
		
		if ( $priceType == 110 ) {

			$priceToShow = 'itemtype';

			$showPriceType = true;

		} else {

			$priceToShow = 'itemprice';

			$showPriceType = false;

		}


		$this->setValue( 'pricetoshow', $priceToShow );

		$this->setValue( 'showpricetype', $showPriceType );

		$this->setValue( 'pricetype', $priceType );

		$price = $this->getValue( 'price' );



		
		$myGroupPrice = true;

		if ( $this->getValue( 'type', 'product.price' ) == 17 ) {

			$shoppersPricesC = WClass::get( 'shoppers.prices', null, 'class', false );

			if ( !empty($shoppersPricesC) ) {

				$myGroupPrice = $shoppersPricesC->getPrice( $this->getValue( 'pid' ), $this->vendid );

				if ( $myGroupPrice !==false ) $price = $myGroupPrice;

			}
		}




		if ( $myGroupPrice ) {



			
			$discountRate = $this->getValue( 'discountrate' );

			$discountValue = $this->getValue( 'discountvalue' );

			$showDiscount = false;

			if ( ( $discountRate > 0 || $discountValue > 0 ) && $price > 0 ) {

				$this->setValue( 'priceDiscountClass', ' class="priceStrikeOut"');	
				$showDiscount = true;

				if ( $discountRate > 0 ) {

					$price = $price * ( 100 - $discountRate ) / 100;

				}
				if ( $discountValue > 0 ) {

					$price = $price - $discountValue;

					if ( $price < 0 ) $price = 0;

				}


				$this->setValue( 'onSaleProduct', true );

				$this->setValue( 'onSaleProductText', TR1320230249NPSI );



			}


			$curid = $this->getValue( 'curid' );

			if ( WGlobals::checkCandy(25) ) {

				
				WPref::load( 'PCATALOG_NODE_USETAX' );

				
				$productTaxC = WObject::get( 'product.tax' );

								$pricetaxtype = $productTaxC->definePriceTaxType( $this->getValue( 'pricetaxtype' ) );


				$productTaxC->setVendorID( $productTaxC->getVendorID( $this->vendid ) );

				$productTaxC->setPrice( $price, $pricetaxtype, $this->getValue( 'prodtypid' ) );

				$price = $productTaxC->renderPrice();



			}


			$usedCurrency = WUser::get('curid');

			if ( $usedCurrency != $curid ) {

				
				$currencyConvertC = WClass::get('currency.convert',null,'class',false );

				$convertedPrice = $currencyConvertC->currencyConvert( $curid, $usedCurrency, $price );

			} else {

				$convertedPrice = $price;

			}


			$discountPrice = WTools::format( $convertedPrice, 'money', $usedCurrency );

			if ($showDiscount) $this->setValue( 'discountPrice', $discountPrice );



			
			if ( PCATALOG_NODE_USETAX && PCATALOG_NODE_SHOWTAX ) {

				$discountPrice .= '<div id="productTax">' . $productTaxC->renderTaxMessage( $usedCurrency, true, $curid ) . '</div>';

			}


			
			if ( WGlobals::checkCandy(50) ) {



				
				$rolidviewprice = $this->getValue( 'rolidviewprice' );



				if ( empty( $rolidviewprice ) || $rolidviewprice == 1 ) {

					if ( !empty($this->itemTypeInfoO->rolidviewprice) ) $rolidviewprice = $this->itemTypeInfoO->rolidviewprice;

					else $rolidviewprice = 0;



					
					if ( empty($rolidviewprice) || $rolidviewprice == 1  ) {

						WPref::load('PPRODUCT_NODE_ROLIDVIEWPRICE');

						$rolidviewprice = PPRODUCT_NODE_ROLIDVIEWPRICE;

					}
				}


				$itemAccessC = WClass::get( 'item.access' );

				if ( empty($rolidviewprice) ) $hasRole = true;

				else $hasRole = $itemAccessC->haveAccess( $rolidviewprice );



				WGlobals::set( 'rolidViewPrice', $hasRole, 'joobi' );



				if ( $hasRole ) $this->setValue( 'priceTotal', $discountPrice );

				else {

					WGlobals::set( 'noItemOptions', true, 'joobi' );

					$roleC = WRole::get();

					$ROLENAME = $roleC->getRole( $rolidviewprice, 'name' ) ;

					$this->setValue( 'priceTotal', str_replace(array('$ROLENAME'),array($ROLENAME),TR1329161769FGFT) );

					$this->setTitle('itemtotal' , '' );

				}


			}


		}


		$electronic = $this->getValue( 'electronic' );

		$deliveryType = WPref::load('PPRODUCT_NODE_DELIVERYTYPE');

		
		$shippingMethod = $this->getValue( 'shippingmethod' );

		if ( !empty($shippingMethod) && !( $electronic || $deliveryType==1 ) ) {

			$this->_catalogItemPageC->setShippingMethod( $this, $shippingMethod );

		}


		
		if ( !empty($this->pageParams->showStock) ) {

			
			$stock = $this->getValue( 'stock' );

			if ( $stock == -1 ) {

				$stockHTML = TR1345593052CDGP;

				$stockClass = 'inStock';

			} elseif ( $stock <= 0 ) {

				$stockHTML = TR1227580983RNJM;

				$stockClass = 'outStock';

			} else {

				$stockClass = 'inStock';

				$stockHTML = $stock . ' ' . TR1345593052CDGQ;

			}
			$stockHTML = '<span class="'.$stockClass.'">' . $stockHTML . '</span>';



			$this->setValue( 'stockInformation', $stockHTML );

		}


		return true;



	}
}