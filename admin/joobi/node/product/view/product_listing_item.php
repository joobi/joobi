<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'item.view.item_listing_item' , JOOBI_DS_NODE );
class Product_Product_listing_item_view extends Item_Item_listing_item_view {
	function prepareView() {


		$allowvendorbundle = WPref::load( 'PVENDORS_NODE_VENDORBUNDLEALLOW' );
		if ( !IS_ADMIN && !$allowvendorbundle ) $this->removeMenus( array('item_listing_product_bundle', 'item_listing_product_divider' ) );


		return parent::prepareView();



	}}