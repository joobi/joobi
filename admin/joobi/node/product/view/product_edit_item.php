<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

WLoadFile( 'item.view.item_edit_item' , JOOBI_DS_NODE );
class Product_product_edit_item_view extends Item_Item_edit_item_view {








protected function prepareView() {



	$status = parent::prepareView();



	
	if ( !defined('PPRODUCT_NODE_DELIVERYTYPE') ) WPref::get('product.node');

	$deliveryType = PPRODUCT_NODE_DELIVERYTYPE;

	if ( empty( $deliveryType ) ) {

		$electronic = $this->getValue( 'electronic', $this->_modelName );

		if ( $electronic == 1 ) $this->removeElements( array($this->_modelName . '_edit_item_tangible_goods_tab', $this->_modelName . '_edit_item_tangible_goods_stock', $this->_modelName . '_edit_item_tangible_goods_weight', $this->_modelName . '_edit_item_tangible_goods_unit', $this->_modelName . '_edit_item_shipping_method', $this->_modelName . '_edit_item_shipping_price' ) );

		else {

			
			$allAvaillableA = explode( ',', PPRODUCT_NODE_AVAILABLESHIPPINGMETHOD );

			if ( !in_array( 9, $allAvaillableA ) ) $this->removeElements( $this->_modelName . '_edit_item_shipping_price' );

			
			if ( $electronic == 3 ) $this->removeElements( $this->_modelName . '_edit_item_electronic' );

		}


		if ( !empty($electronic) ) {

			$this->removeElements( $this->_modelName . '_edit_item_delivery_type_pref', false );

		} else {

			$this->removeElements( $this->_modelName . '_edit_item_delivery_electronic_hidden', false );

		}


	} else {

		
		$this->removeElements( $this->_modelName . '_edit_item_delivery_type_pref', false );

		if ( $deliveryType == 1 ) $this->removeElements( array($this->_modelName . '_edit_item_tangible_goods_tab', $this->_modelName . '_edit_item_tangible_goods_stock', $this->_modelName . '_edit_item_tangible_goods_weight', $this->_modelName . '_edit_item_tangible_goods_unit', $this->_modelName . '_edit_item_shipping_method', $this->_modelName . '_edit_item_shipping_price' ) );

		else {

			
			if ( $deliveryType == 3 ) {

				$this->removeElements( $this->_modelName .  '_edit_item_electronic' );

			}


			
			$allAvaillableA = explode( ',', PPRODUCT_NODE_AVAILABLESHIPPINGMETHOD );

			if ( !in_array( 9, $allAvaillableA ) ) $this->removeElements( $this->_modelName . '_edit_item_shipping_price' );

		}


	}




	
	$productPricetypeC = WClass::get( 'product.pricetype' );

	$priceid = $this->getValue( 'priceid', $this->_modelName );

	if ( empty($priceid) ) {

		
		$prodPriceM = WModel::get( 'product.price' );

		$prodPriceM->whereE( 'publish', 1 );

		$prodPriceM->orderBy( 'ordering', 'ASC' );

		$prodPriceM->checkAccess();

		$priceid = $prodPriceM->load( 'lr', 'priceid' );

	}


	if ( $productPricetypeC->removePrice( $priceid ) ) {

		$this->removeElements( array( $this->_modelName . '_edit_item_price', $this->_modelName . '_edit_item_tax', $this->_modelName . '_edit_item_' . $this->_modelName . '_curid', $this->_modelName . '_edit_item_discountrate', $this->_modelName . '_edit_item_discountvalue') );

	}
	if ( $productPricetypeC->removePrice( $priceid, 20 ) ) $this->removeElements( array( $this->_modelName . '_edit_item_tax', $this->_modelName . '_edit_item_discountrate', $this->_modelName . '_edit_item_discountvalue') );

	if ( $productPricetypeC->removePrice( $priceid, 17 ) ) {

		$this->removeElements( array( $this->_modelName . '_edit_item_price', $this->_modelName . '_edit_item_' . $this->_modelName . '_curid') );

		if ( !defined('PVENDORS_NODE_SHOPPERS_SAME_PRICE') ) WPref::get( 'vendors.node' );

		$samePriceForAllProducts = PVENDORS_NODE_SHOPPERS_SAME_PRICE;	
		if ( $samePriceForAllProducts ) $this->removeElements( array( $this->_modelName . '_edit_item_discountrate', $this->_modelName . '_edit_item_discountvalue' ) );



	} else {

		$this->removeElements( $this->_modelName . '_edit_item_price_shoppers' );

	}


	
	if ( !$productPricetypeC->removePrice( $priceid, 40 ) ) $this->removeElements( array( $this->_modelName . '_edit_item_buy_now_link' ) );



	
	if ( !IS_ADMIN ) {

		
		if ( !PPRODUCT_NODE_VENDORDELIVERYALLOW ) $this->removeElements( $this->_modelName . '_edit_item_delivery_tab' );	
		if ( !PPRODUCT_NODE_VENDORALLOWDISCOUNT ) $this->removeElements( array( $this->_modelName . '_edit_item_discountvalue', $this->_modelName . '_edit_item_discountrate' ) );

	}


	return $status;



}
}