<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;


class Product_Showvend_listing extends WListings_standard {






function create () 

{

	if(!defined('PPRODUCT_NODE_SHOWVEND')) WPref::get('product');

	if( PPRODUCT_NODE_SHOWVEND ) 

	{

		
		if ( !isset($vendorC) ) $vendorC = WClass::get('vendor.helper',null,'class',false);	

		$vendor = $vendorC->showVendName ($this->getValue('vendid' , 'product'), $this->getValue('uid' , 'product'), true);

	

		$this->content = $vendor;

		return true;

	}

	else return false;

}}