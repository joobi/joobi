<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Product_Product_priceconv_listing extends WListings_standard {






function create() {



	$curid = $this->getValue( 'curid', 'product' );

	$price = $this->getValue( 'price', 'product' );

	$display = WTools::format( $price, 'money', $curid);



	
	if ( WGlobals::checkCandy(50) && ( !empty($price) && ($price > 0) ) ) {



		WPref::load( 'PCURRENCY_NODE_MULTICUR' );

	        if ( PCURRENCY_NODE_MULTICUR )

	        {

	                if ( !defined('CURRENCY_USED') )

	                {

	                        $currencyFormatC = WClass::get('currency.format',null,'class',false);

	                        $currencyFormatC->set();

	                }
	

	                
	                if ( $curid != CURRENCY_USED )

	                {

	                        static $currencyC=null;

	                        if ( !isset($currencyC) ) $currencyC = WClass::get( 'currency.convert',null,'class',false);

	

	                        $object = new stdClass;

	                        $object->currencyFrom = $curid;                
	                        $object->currencyTo = CURRENCY_USED;        
	                        $object->amount = $this->getValue( 'price', 'product' );                
	                        $object->formatAmount = true;                
	                        $object->decimalPlace = 2;                
	

	                        $display = $currencyC->convertTo( $object );

	                }
	        }
	

	}else

	{

		$display = ( empty($price) || ( $price <= 0 ) ) ? TR1256629168HWSC : WTools::format( $price, 'money', $curid);

	}


	$this->content = $display;

	return true;

}}