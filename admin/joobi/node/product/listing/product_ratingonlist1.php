<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Product_ratingonlist1_listing extends WListings_standard {














function create() {

	





	if ( !defined('PCATALOG_NODE_PAGEPRDSHOWRATING') ) WPref::get( 'product.node' );

	if ( !PCATALOG_NODE_PAGEPRDSHOWRATING ) return false;



	
	if ($this->value == 1) {





		
		static $ratingC=null;

		$rate = $this->getValue('score' , 'product');



		if ( !isset($ratingC) ) $ratingC = WClass::get('output.rating');

		$this->_ratingC = $ratingC;

		$this->_ratingC->rating = ( ( isset($this->rating->rating) ) ) ? ( !isset($this->rating->rating) ) : $rate;

		$this->_ratingC->restriction = ( ( isset($this->rating->restriction) ) ) ? ( !isset($this->rating->restriction) ) : 1;

		if ( !defined( 'PPRODUCT_NODE_PRODRATECOLOR' ) ) WPref::get( 'product.node' );

		$this->rating->colorPref = PPRODUCT_NODE_PRODRATECOLOR;

		if ( !empty($this->rating->colorPref) ) {

				$this->_ratingC->colorPref = $this->rating->colorPref;

		} else {

			if (!defined('PAPPS_NODE_STARCOLOR')) WPref::get('apps.node');

			$this->_ratingC->colorPref = PAPPS_NODE_STARCOLOR;

		}


		$this->_ratingC->rateController = ( ( isset($this->rating->rateController) ) ) ? ( !isset($this->rating->rateController) ) : '';

		$this->_ratingC->type = ( ( isset($this->rating->type) ) ) ? ( !isset($this->rating->type) ) : 0;



		
		$ratingHTML = '<div style="float:left;" >'.$this->_ratingC->createHTMLRating($this).'</div>';

		


		
		
		$etid		= $this->getValue('pid');		
		
		
		


		
		
		
		
		


		$route=WPage::routeURL('controller=catalog&task=show&eid='.$etid.'');



		
		$nbreviews= $this->getValue('nbreviews' , 'product');

		$reviewsText = (empty($nbreviews) || $nbreviews == 0 || $nbreviews == 1) ? TR1256629165ITHU : TR1256629165ITHV;



		if ( !defined( 'PCATALOG_NODE_PAGEPRDSHOWREVIEW' ) ) WPref::get( 'product.node' );

		if ( PCATALOG_NODE_PAGEPRDSHOWREVIEW ) $commentHTML=''.$ratingHTML.'  (<a href="'.$route.'">'.$nbreviews.$reviewsText.'</a>)';

		else $commentHTML=''.$ratingHTML;



		$this->content = $commentHTML;



	}else

	{

		return false;

	}


	return true;





}}