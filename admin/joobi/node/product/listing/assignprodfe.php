<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Product_Assignprodfe_listing extends WListings_standard {








function create()

{

	
	$uid = WGlobals::getEID();

	$catid = $this->getValue( 'catid' );

	$name = $this->getValue( 'name' );

	$namekey = $this->getValue( 'namekey' );

	$vendid = WGlobals::get( 'id' );

	if ( empty($vendid) ) $vendid = $this->getValue( 'vendid' );

	

	if ( $namekey != 'other_vendors' )

	{	

		
		$link = WPage::routeURL( 'controller=item-category-vendor-assign&task=listing&id='. $vendid .'&catid='. $catid .'&titleheader='. $name );

	

		$numOfProd = $this->value;

		$this->content = '<a href="'. $link .'">Products (<span style="color:red;">'. $numOfProd .'</span>)</a>';

	} 

	else $this->content = '';

	return true;

}}