<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Pricedisplay_listing extends WListings_standard {












function create() {



	$HTML = '';



	
	static $getPref = true;



	if ( $getPref ) {

		WPref::get( 'product.node' );

		$getPref = false;

	}


	
	if (WGlobals::checkCandy(25)) {

		$uid = WUser::get('uid');

		if ( PPRODUCT_NODE_LOGPRICE && $uid== 0 ) {

			return '';

		}


	}


	if ( $this->value==0 ) {

		$price = TR1206961944PEUR;

	} else {



		$curid = $this->getValue( 'curid' );

		$origprice = WTools::format( $this->value, 'money', $curid );

		$price = $origprice;



		WPref::load( 'PCURRENCY_NODE_MULTICUR' );



		if ( PCURRENCY_NODE_MULTICUR && ( WGlobals::checkCandy(50) ) )

		{

			if ( !defined( 'PPRODUCT_NODE_SHOWNPRICEFORMAT' ) ) WPref::get( 'product.node' );

			if ( !defined( 'PPRODUCT_NODE_SHOWNPRICECOLOR' ) ) WPref::get( 'product.node' );

			$color = PPRODUCT_NODE_SHOWNPRICECOLOR;

			if ( empty($color) ) $color='blue';



			if ( !defined('CURRENCY_USED') ) {

				$currencyFormatC = WClass::get('currency.format',null,'class',false);

				$currencyFormatC->set();

			}


			if ( $curid != CURRENCY_USED )

			{

				
				$vendid = 1;

				static $vendorC=null;

				if (empty($vendorC)) $vendorC = WClass::get('vendor.helper',null,'class',false);

				$vendcurid = $vendorC->getCurrency( $vendid );



				
				static $currencyC=null;

				if ( empty($currencyC) ) $currencyC = WClass::get( 'currency.convert',null,'class',false);

				if ( $curid == $vendcurid ) $revCon = false;

				else $revCon = true;

				$convprice = $currencyC->currencyConvert( $curid, CURRENCY_USED, $this->value, $revCon );



				if ( is_numeric($convprice) ) $convprice = WTools::format( $convprice, 'money', CURRENCY_USED );



				if ( PPRODUCT_NODE_SHOWNPRICEFORMAT ) $price .= '<br><span style="float:right;color:'. $color .';">'. $convprice .'</span>';

				else $price = $convprice;

			}
		}
	}


	$HTML .= '<span style="font-size:120%;padding-right:10px;">'.$price.'</span>';



	
	if ( !defined( 'PPRODUCT_NODE_SHOWCARTBT' ) ) WPref::get( 'product.node' );

	if ( WGlobals::checkCandy(50) && PCATALOG_NODE_PAGEPRDCARTBTN ) {

		$HTML .= '<div class="jprdmaincartbtn">'. $this->_addtoCartBtn( $this->value ) .'</div>';

	}


	$this->content = $HTML;





	return true;







}


function _addtoCartBtn($pid)

{



	$pricetype = $this->getValue( 'pricetype' );

	$price = $this->getValue( 'price' );



	
	if ( !defined( 'PPRODUCT_NODE_NOCHECKOUTDL' ) ) WPref::get( 'product.node' );



	$display = '';

	if ( PPRODUCT_NODE_NOCHECKOUTDL && ( $pricetype == 1 ) && ( $price <= 0 ) ) {


		$link = 'controller=catalog&task=download&eid='. $pid;
		$link = WPage::routeURL( $link );



		$HTML = '<a href="'. $link .'" class="tabcartbtn"><span><div id="tabcarttxt">'.TR1206961905BHAV.'</div></span></a>';

	} else {

		$link = 'controller=basket&task=addbasket&eid='. $pid;

		$link = WPage::routeURL( $link );



		$HTML = '<a href="'. $link .'" class="tabcartbtn"><span><div id="tabcarttxt">'.TR1263480669ALUN .'</div></span></a>';

	}


	return $HTML;

}}