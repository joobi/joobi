<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'main.listing.moneycurrency' , JOOBI_DS_NODE );
class Product_Optionprice_listing extends WListings_moneycurrency {


function create() {



	$type = $this->getValue( 'type' );

	if ( $type == 2 ) {
		$this->content = round( $this->value, 2 ) . '%';
		return true;
	}



	return parent::create();

}}