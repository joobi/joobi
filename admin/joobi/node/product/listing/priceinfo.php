<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'main.listing.moneycurrency' , JOOBI_DS_NODE );
class Product_Priceinfo_listing extends WListings_moneycurrency {


	function create() {

		static $productPricetypeC = null;


		$priceid = $this->getValue( 'priceid' );

		if ( empty($priceid) ) return false;


		
		$prodtypid = $this->getValue( 'prodtypid' );

		$type = $this->getValue( 'type' );


		if ( $type > 30 ) return false;


		if ( !isset($productPricetypeC) ) $productPricetypeC = WClass::get( 'product.pricetype' );

		if ( !$productPricetypeC->removePrice( $priceid, array( 10, 20 ) ) ) {

			$this->content = $productPricetypeC->getPriceTypeName( $priceid );

			return true;

		}


		return parent::create();



	}

}