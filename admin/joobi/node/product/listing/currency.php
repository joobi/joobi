<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Currency_listing extends WListings_standard {
function create() {

	
	$curid = $this->getValue('curid');

	$price = $this->value;



	if ( WGlobals::checkCandy(50) && ( $price > 0 ) )

	{

		WPref::load( 'PCURRENCY_NODE_MULTICUR' );

		if ( PCURRENCY_NODE_MULTICUR )

		{

			if ( !defined('CURRENCY_USED') )

			{

			    $currencyFormatC = WClass::get('currency.format',null,'class',false);

			    $currencyFormatC->set();

			}


	                
	                if ( $curid != CURRENCY_USED )

	                {

	                	static $currencyC=null;

	                        if ( !isset($currencyC) ) $currencyC = WClass::get( 'currency.convert' );

	                        $object = new stdClass;

	                        $object->currencyFrom = $curid;

	                        $object->currencyTo = CURRENCY_USED;

	                        $object->amount = $this->value;

	                        $object->formatAmount = true;

	                        $object->decimalPlace = 2;



	                        $price = $currencyC->convertTo( $object );

	                 }
	         }
	}

	else

	{
		if ( empty($price) || $price == 0 ) {
			return false;
		} else {
			$price = WTools::format( $price, 'money', $curid );			}

	}


	$this->content = '<strong>'.$price.'</strong>';

	return true;

}}