<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'listing.yesno' , JOOBI_LIB_HTML );
class Product_Customproductblock_listing extends WListings_yesno {
function create() {

	$this->value = !$this->value;

	if ( IS_ADMIN ) return parent::create();

	else {

		$roleC = WRole::get();
		$isStoreManger = $roleC->hasRole( 'storemanager' );
				

 		if ( empty( $isStoreManger ) ) $this->element->infonly = 1;

	

		return parent::create();
		

	}
	

	return true;

}
}