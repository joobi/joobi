<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Numraffle_listing extends WListings_standard {




function create()

{

	
	static $currentDate = null;

	if (!isset($currentDate)) $currentDate = time();



	$raffleUsed = $this->getValue( 'useraffle' );

	$deadline = $this->getValue( 'deadline' );



	if ( !empty($raffleUsed) )

	{ 

		$rafid = $this->getValue( 'rafid' );

	

		static $prodRafC=null;

		if ( !isset($prodRafC) ) $prodRafC = WClass::get( 'product.raffle' );

		$numOfRaffles = $prodRafC->numAvailableRaffles( $rafid );

	

		if ( empty($numOfRaffles) || ( $numOfRaffles <= 0 ) ) $display = '<b>'. TR1254294898PHIV .'</b>';

		else

		{

			if ( ( $deadline == 0 ) || $currentDate < $deadline ) $display = ' [<span style="color:red;"> '. (int)$this->value .' </span>]';

			else $display = '<b>'. TR1254294898PHIW .'</b>';

		}
	}

	else $display = '';

	

	$this->content = $display;

	return true;

}}