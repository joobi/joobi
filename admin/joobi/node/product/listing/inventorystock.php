<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'main.listing.dyninput' , JOOBI_DS_NODE );
class Product_Inventorystock_listing extends WListings_dyninput {











	function create() {

		static $deliveryType = null;



		if ( !isset($deliveryType) ) {

			$deliveryType = WPref::load('PPRODUCT_NODE_DELIVERYTYPE');

		}


		if ( !empty( $deliveryType ) && $deliveryType == 1 ) return false;



		if ($this->value > 0) {

	
			return parent::create();

		}elseif ( $this->value < 0 ) {

	
			$this->content = TR1206961954EAMU;

		} else {

			$this->content = '<b><blink><font style="color:red">'.TR1227580983RNJM.'</font></b></blink>';

		}
		return true;



	}}