<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Progressbar_listing extends WListings_standard {


	var $targetTotal = ''; 	  	 	var $target = '';		 	var $labelstyle = ''; 		 	var $labelmsg = '';      	 	var $completestyle = ''; 	 	var $completemsg = '';       	var $zerostyle = ''; 		 	var $zeromsg = '';     		 	var $percentage = 0;         	var $height = '';     			var $width = ''; 				var $_progressbarC = null;  

function create() {
	$price = $this->value;
	$target = $this->getValue('targettotal');
	if (!is_numeric($price) || !is_numeric($target)) return false;
	$result= ($price / $target)* 100;

	$this->percentage = round($result, 1);


				static $progressbarC=null;
		if ( !isset($progressbarC) ) $progressbarC = WClass::get('main.progressbar');
		$this->_progressbarC = $progressbarC;

		if( empty($this->percentage) ) {
						if(empty($this->targetTotal)){
				if( empty($this->value))return false;
				else $this->targetTotal = $this->value;
			}
						if(empty($this->target)){
				$target = $this->getValue('target');
				if(empty($target)) $this->target = 0;
				else $this->target = 	$target;
			}		}

		$this->_progressbarC->percentage = $this->percentage;
		$this->_progressbarC->targetTotal = $this->targetTotal;
		$this->_progressbarC->target = $this->target;
		$this->_progressbarC->labelstyle = (!empty($this->labelstyle))? $this->labelstyle:'';
		$this->_progressbarC->labelmsg = $this->labelmsg;
		$this->_progressbarC->completestyle = $this->completestyle;
		$this->_progressbarC->completemsg = $this->completemsg;
		$this->_progressbarC->zerostyle = (!empty($this->zerostyle))? $this->zerostyle: 'color: white; text-shadow: 1px 1px 0 #B6BCC6; margin: 6px 35% 0 0;';
		$this->_progressbarC->zeromsg = $this->zeromsg;
		$this->_progressbarC->height = (!empty($this->height))? $this->height: '25px';
		$this->_progressbarC->width = (!empty($this->width))? $this->width: '90%';
		$progressbar=$this->_progressbarC->createProgressBarHTML() ;

		$this->content =$progressbar;
		return true;

	}

}