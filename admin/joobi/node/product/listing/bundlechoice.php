<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Bundlechoice_listing extends WListings_standard {





function create() {

	

	
	$eid = WGlobals::get( 'eid' );

	$prodtypid = WGlobals::get( 'prodtypid' );

	$titleheader = WGlobals::get( 'titleheader' );
	
	$pidToUse = $this->getValue( 'pid', 'product' );
	

	$pid = $this->value;



	if ( !empty($pid) ) { 

		$related = 1; 
		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/yes.png';

		$display = '<img src="'. $src .'"/>';

	} else {

		$related = '0'; 
		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/cancel.png';

		$display = '<img src="'. $src .'"/>';

	}
	

	$link = WPage::routeURL( 'controller=product-bundle&task=add&relpid='. $pidToUse .'&rel='. $related .'&eid='. $eid .'&prodtypid='. $prodtypid .'&titleheader='. $titleheader );



 	$this->content = '<a href="'. $link .'" >'.$display.'</a>';

	return true;

}}