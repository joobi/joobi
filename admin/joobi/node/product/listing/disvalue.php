<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_Disvalue_listing extends WListings_standard {








function create()

{

	
	$curid = $this->getValue( 'curid' );

	

	
	static $mapID = null;

	if(empty($mapID)) $mapID = WModel::get( 'discount', 'sid' );

	$disvaltype = 'disvaltype_'. $mapID;



	
	if ( $this->data->$disvaltype == 1 ) $this->content = (real)$this->value .' %'; 
	else $this->content = WTools::format( $this->value, 'money', $curid );	
	return true;

}}