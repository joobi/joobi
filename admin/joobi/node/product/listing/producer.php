<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Producer_listing extends WListings_standard {
function create() {



$pid = $this->value;



$orderM = WModel::get('order');

$orderM->makeLJ('order.products', 'oid', 'oid');

$orderM->whereE('pid', $pid, 1);

$orderO = $orderM->load('ol', array('oid','uid'));



$producerA = array();



foreach($orderO as $order) {



    $producerA[$order->uid]=true;



}


$this->content = ''.count($producerA).'';



return true;

}}