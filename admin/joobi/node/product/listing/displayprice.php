<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_Displayprice_listing extends WListings_standard {










function create() {

	
	static $getPref = true;



	if ( $getPref ) {

		WPref::get( 'product.node' );

		$getPref = false;

	}


	
	if(WGlobals::checkCandy(25)) {

		$uid = WUser::get('uid');

		if( PPRODUCT_NODE_LOGPRICE && $uid== 0 ) {

			return '';


		}


	}
	

	if ( $this->value==0 ) {

		$price = TR1206961944PEUR;

	} else {

		

		$curid = $this->getValue( 'curid' );

		$origprice = WTools::format( $this->value, 'money', $curid );

		$price = $origprice;

		

		WPref::load( 'PCURRENCY_NODE_MULTICUR' );

		

		if( PCURRENCY_NODE_MULTICUR && ( WGlobals::checkCandy(50) ) )

		{

			if( !defined( 'PPRODUCT_NODE_SHOWNPRICEFORMAT' ) ) WPref::get( 'product.node' );

			if( !defined( 'PPRODUCT_NODE_SHOWNPRICECOLOR' ) ) WPref::get( 'product.node' );

			$color = PPRODUCT_NODE_SHOWNPRICECOLOR;

			if( empty($color) ) $color='blue';

		

			if ( !defined('CURRENCY_USED') ) {

				$currencyFormatC = WClass::get('currency.format',null,'class',false);

				$currencyFormatC->set();

			}
			

			if( $curid != CURRENCY_USED )

			{

				static $currencyC=null;

				if( !isset($currencyC) ) $currencyC = WClass::get( 'currency.convert',null,'class',false);



				$object = new stdClass;

				$object->currencyFrom = $curid;

				$object->currencyTo = CURRENCY_USED;

				$object->amount = $this->value;

				$object->formatAmount = true;

				$object->decimalPlace = 4;

				$convprice = $currencyC->convertTo( $object );

			

				if( PPRODUCT_NODE_SHOWNPRICEFORMAT ) $price .= '<br><span style="float:right;color:'. $color .';">'. $convprice .'</span>';

				else $price = $convprice;

			}
		}
	}


	$this->content = $price;

	

	return true;





}

}