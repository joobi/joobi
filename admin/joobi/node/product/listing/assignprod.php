<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Assignprod_listing extends WListings_standard {
function create()  {

	
	$option = WGlobals::get( 'option' );

	if ( !empty($option) ) $option = substr( $option, 4 );



	
	
	switch ( $option ) {

		case 'jsubscription': 

			$text = TR1218029673NSUZ;

			break;

		case 'jmarket': 

		case 'jstore': 

		case 'jauction': 

			$text = TR1206961902CIFE;

			break;

		default: 

			$text = TR1233642085PNTA;

			break;

			

	}
	

	$display = $text .' (<span style="color:red;">'. $this->value .'</span>)';

	

	
	$this->content = ($this->value > 0 ) ? $display : '<b>'. $display .'</b>';

	return true;

	

}}