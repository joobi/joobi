<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Rating_listing extends WListings_standard {


function create() {
	$HTML = '';

		if (!defined('PCATALOG_NODE_PAGEPRDSHOWRATING')) WPref::get('product');
	if ( PCATALOG_NODE_PAGEPRDSHOWRATING ) {
		if (!defined('PPRODUCT_NODE_PRODRATECOLOR')) WPref::get('product');
		$getRatingColor = PPRODUCT_NODE_PRODRATECOLOR;
		$HTML .= '<div class="jprodhomerate">'. $this->_rateHTML( $this->value, $getRatingColor ) .'</div>';
	}
	$this->content = $HTML;

	return true;

}




function _rateHTML($pid,$color=null,$showReview=false) {

		
		$rate = $this->getValue ('score' , 'product');		
		$nbreviews= $this->getValue ('nbreviews' , 'product');	


		static $ratingC=null;

		if ( !isset($ratingC) ) $ratingC = WClass::get('output.rating');



		$this->_ratingC = $ratingC;

		$this->_ratingC->rating = ( ( isset($this->rating->rating) ) ) ? ( !isset($this->rating->rating) ) : $rate;

		$this->_ratingC->restriction = ( ( isset($this->rating->restriction) ) ) ? ( !isset($this->rating->restriction) ) : 1;



		if ( isset($this->rating->colorPref) ) $this->_ratingC->colorPref = $this->rating->colorPref;

		else {

			if (!defined('PAPPS_NODE_STARCOLOR')) WPref::get('apps.node');

			$this->_ratingC->colorPref = PAPPS_NODE_STARCOLOR;

		}


		$this->_ratingC->rateController = ( ( isset($this->rating->rateController) ) ) ? ( !isset($this->rating->rateController) ) : '';

		$this->_ratingC->type = ( ( isset($this->rating->type) ) ) ? ( !isset($this->rating->type) ) : 0;



		
		$ratingHTML = $this->_ratingC->createHTMLRating($this);



		
		
		
		$mapPID     = $pid;

		$etid		= $pid;				
		$returnId 	= WView::getURI();					
		$realVal 	= base64_encode($returnId);			
		$option		= WGlobals::get('option');		


		static $comNameC = null;

		if (!isset($comNameC )) $comNameC = WClass::get('comment.commenttype');

		$commenttype = $comNameC->comValue($option);



		
		
		$route = 'controller=catalog&task=show&eid='. $pid;

		$route = WPage::routeURL($route) .'#comment';



		
		$reviewsText = (empty($nbreviews) || $nbreviews == 0 || $nbreviews == 1) ? TR1256629165ITHU : TR1256629165ITHV;



		if ( !defined( 'PCATALOG_NODE_PAGEPRDSHOWREVIEW' ) ) WPref::get( 'product.node' );

		if ( PCATALOG_NODE_PAGEPRDSHOWREVIEW && $showReview ) $HTML = $ratingHTML .' (<a href="'.$route.'">'. $nbreviews.$reviewsText .'</a>)';

		else $HTML = $ratingHTML;



		return $HTML;

}}