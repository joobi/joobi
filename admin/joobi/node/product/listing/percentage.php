<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Percentage_listing extends WListings_standard {
function create() {

	$price = $this->value;	

	$target = $this->getValue('targettotal');	

	if (!is_numeric($price) || !is_numeric($target)) return false;

	$result= ($price / $target)* 100;

	$percentage = round($result,1);

	$this->content= $percentage.'% overall value';

	return true;

}}