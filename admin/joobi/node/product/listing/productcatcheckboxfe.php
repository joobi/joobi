<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

WLoadFile( 'listing.checkbox' , JOOBI_LIB_HTML );
class Product_Productcatcheckboxfe_listing extends WListings_checkbox {
function create() {

	if ( IS_ADMIN ) return parent::create();

	else {

		
		

		$username = WUser::get( 'username' );

		$uid = WUser::get( 'uid' );

		

		$namekey = $this->getValue( 'namekey' );

		$userKey = $username .'_'. $uid;

		

		if ( $userKey == $namekey ) return false;

		else return parent::create();

	}
}}