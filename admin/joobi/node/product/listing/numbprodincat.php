<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Product_Numbprodincat_listing extends WListings_standard {








function create() {

	

	$option = WGlobals::get( 'option' );

	if ( !empty($option) ) $option = substr( $option, 4 );

	

	
	if ( $option == 'jsubscription' ) $text = (($this->value) > 1) ? 'Subscriptions' : 'Subscription';

	else $text = (($this->value) > 1) ? 'Products' : 'Product';



	
	$catid 	= $this->getValue('catid' , 'product.category');	

	$link 	= WPage::routeURL('controller=item-category&task=show&eid='.$catid );



	$this->content = $text.' (<span class="numpid"><a href="'.$link.'">'.$this->value.'</a></span>)';



	return true;

}}