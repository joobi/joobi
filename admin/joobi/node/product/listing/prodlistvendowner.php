<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

WLoadFile( 'listing.text' , JOOBI_LIB_HTML );
class Product_Prodlistvendowner_listing extends WListings_text {
function create() {

	$storeName = $this->getValue( 'name', 'vendor' );

	if ( !empty( $storeName ) ) $display = $storeName;

	else {

		$vendUID = $this->getValue( 'uid', 'vendor' );

		$display = WUser::get( 'name', $vendUID );

	}


	$this->content = $display;

	return true;

}}