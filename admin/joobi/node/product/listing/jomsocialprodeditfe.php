<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

WLoadFile( 'listing.butedit' , JOOBI_LIB_HTML );
class Product_Jomsocialprodeditfe_listing extends WListings_butedit {
function create() {

	$uid = WUser::get('uid');

	$jomID = WUser::get( 'id', $uid );

	$userid = WGlobals::get( 'userid' );

	
	if ( ( $jomID != $userid || empty( $jomID ) ) || ( empty( $uid ) || empty( $userid ) ) ) return false;

	

	static $vendidA = null;

	
	if ( !isset( $vendidA[ $uid ] ) && !empty( $uid ) ) {

		$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

		$vendidA[ $uid ] = $vendorHelperC->getVendorID( $uid );

	}
	

	if ( empty( $vendidA[ $uid ] ) ) return false;

	else return parent::create();

	

	return true;

}}