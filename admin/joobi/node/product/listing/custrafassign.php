<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'listing.text' , JOOBI_LIB_HTML );
class Product_Custrafassign_listing extends WListings_text {


function create() {

	$uid = $this->getValue( 'uid' );



	if( !empty( $uid ) && ( $uid > 0 ) ) {

		$display = $this->value;

	} else {


		$rafid = $this->getValue( 'rafid' );

		$rafno = $this->getValue( 'rafno' );

		$titleheader = WGlobals::get( 'titleheader' );



		$link = WPage::routeURL( 'controller=product-rafflemembers-assign&task=listing&rafid='. $rafid .'&rafno='. $rafno .'&titleheader='. $titleheader );

		$display = '<a href="'. $link .'" style="color:red;">'. TR1269921565CKGK .'</a>';


	}


	$this->content = $display;

	return true;


}
}