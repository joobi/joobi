<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_Cancelraffle_listing extends WListings_standard {


function create() {

	$uid = $this->getValue( 'uid' );



	if( !empty($uid) && ( $uid > 0 ) ) {
		

		$cancelIMG = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/cancel.png';

		$rafid = $this->getValue( 'rafid' );

		$rafno = $this->getValue( 'rafno' );

		$pid = WGlobals::get( 'eid' );

		$titleheader = WGlobals::get( 'titleheader' );

		

		
		$ACTION = TR1254123959HMAD;

		$extras = 'onclick="return confirm(\''.str_replace(array('$ACTION'),array($ACTION),TR1233626551NWXV).'\')"';

		$link = WPage::routeURL( 'controller=product-rafflemembers&task=cancelraffle&rafid='. $rafid .'&rafno='. $rafno .'&pid='. $pid .'&titleheader='. $titleheader .'&uid='. $uid );

		

		$this->content = '<a href="'. $link .'" '. $extras .'><img src="'. $cancelIMG .'"></a>';

	}

	else $this->content = '';
	

	return true;

}
}