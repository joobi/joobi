<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Addtoorder_listing extends WListings_standard {
function create() {



	$oid = WGlobals::get('eid');

	$link = WPage::routeURL( 'controller=order-products&task=addtoorderproducts&oid='.$oid.'&pid=' . $this->value );

	$this->content = '<a href="'.$link.'"> '.TR1369750916CWAY .'</a>';



	return true;



}}