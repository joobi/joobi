<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Addtocartbtnsubfe_listing extends WListings_standard {


function create() {

	
	
	$pricetype = $this->getValue( 'pricetype' );

	$price = $this->getValue( 'price' );



	
	WPref::load( 'PPRODUCT_NODE_NOCHECKOUTDL' );



	$display = '';

	$itemid = WGlobals::get( 'Itemid' );

	$itemid = ( !empty($itemid) ) ? $itemid : 0;
	$itemId = CMSAPIPage::getSpecificItemId( 'basket' );



	if( PPRODUCT_NODE_NOCHECKOUTDL && ( $pricetype == 1 ) && ( $price <= 0 ) ) {

		$link = ( !empty($itemid) ) ? 'controller=catalog&task=download&eid='. $this->value .'&Itemid='. $itemid : 'controller=catalog&task=download&eid='. $this->value;
		$link = WPage::routeURL( $link );



		$display = '<a href="'. $link .'" class="bigCartLeft">';
		$display .= '<span class="bigCartRight"><span class="bigCartCenter">'. TR1206961905BHAV .'</span></span></a>';


	} else {

		$link = ( !empty($itemid) ) ? 'controller=basket&task=addbasket&eid='. $this->value .'&Itemid='. $itemid : 'controller=basket&task=addbasket&eid='. $this->value;

		$link = WPage::routeURL( $link );



		$display = '<a href="'. $link .'" class="bigCartLeft">';
		$display .= '<span class="bigCartRight"><span class="bigCartCenter">'. TR1263480669ALUN .'</span></span></a>';

	}


	$this->content = $display;

	return true;


}
}