<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Shares_listing extends WListings_standard {
function create() {

	$value = round($this->value);

	$text = ($value > 1) ? TR1304945985LEYS : TR1305103892NKOI;

	$this->content = $value .' '. $text;

	return true;

}}