<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Avatar_listing extends WListings_standard {
function create() {



	if ( !defined('PUSERS_NODE_FRAMEWORK_FE') ) WPref::get( 'users.node', false, true, false );	

	$usersAddon = WAddon::get( 'users.'. PUSERS_NODE_FRAMEWORK_FE );

	$img = $usersAddon->getAvatar($this->value);	

	$uid = $this->getValue('uid');

	$link = WPage::routeURL('controller=users&task=show&eid='.$uid );

	$contentImg='<a href="'.$link.'"><center><img src="'.$img .'" height = "36px"/></center></a>';

	$this->content = $contentImg;

	return true;



}}