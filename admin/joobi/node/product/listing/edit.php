<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Edit_listing extends WListings_standard {










function create() {

	$sidDesc = null;



	$shared = $this->getValue( 'share' );

	if ( $shared && !IS_ADMIN ) return false;



	if ( !isset($sidDesc) ) {

		$sidDesc = 'name_'. WModel::get('product.optiontrans','sid');

	}


	$name = $this->data->$sidDesc;



	$pkeyMap = $this->pkeyMap;

	$link = '<a href="'.WPage::routeURL($this->element->lien.'&opid='.$this->data->$pkeyMap . '&titleheader='.urlencode($name)).'">';


	$link .= TR1276181285MQBX;

	$link .= '</a>';




	$this->content = $link;

	return true;

}}