<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Expiredate_listing extends WListings_standard {


function create() {

	

	static $currentDate = null;

	if (!isset($currentDate)) $currentDate = time();



	if ($this->value > 0) {

		if ( $currentDate < $this->value ) $this->content = date( JOOBI_DATE5, $this->value);

		else $this->content = '<span style="color:red;">'. date( JOOBI_DATE5, $this->value) .'</span>';

	} else {

		return false;

		$this->content = TR1237967968ABUO;

	}
	return true;

}}