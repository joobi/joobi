<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Assigns_listing extends WListings_standard {
function create() {

	




	$pid = $this->value;

	$catid = WGlobals::get('catid');
	$productPID = $this->getValue( 'pid', 'product' );



	$prodtypid = WGlobals::get( 'prodtypid' );

	$titleheader = WGlobals::get( 'titleheader' );



	if ( !empty($pid) ) { 

		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/yes.png';

		$display = '<img src="'. $src .'"/>';

	} else {

		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/cancel.png';

		$display = '<img src="'. $src .'"/>';

	}


	$link = 'controller=item-assign&task=assigncategories&catid='. $catid .'&prodtypid='. $prodtypid;

	$link .= '&pid='. $productPID .'&titleheader='. $titleheader;
	$this->content = '<a href="'. WPage::routeURL( $link ) .'">'. $display .'</a>';
	

	return true;

}}