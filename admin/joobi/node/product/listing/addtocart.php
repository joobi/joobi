<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Product_Addtocart_listing extends WListings_standard {












function create() {



	static $productID=null;

	if(empty($productID)) $productID = $this->modelID;



	$pid = WGlobals::get( 'pid' );

	if(empty($pid))

	{

		$modelid = WModel::get( 'product.categoryproduct', 'sid' );

		$id = 'pid_'. $modelid;

		$pid = $this->data->$id;

	}

	static $deliveryType = null;

	if ( !isset($deliveryType) ) {
		if ( !defined('PPRODUCT_NODE_DELIVERYTYPE') ) WPref::get('product.node');
		$deliveryType = PPRODUCT_NODE_DELIVERYTYPE;
	}

	$stock =  'stock_' . $productID;

	$available = 'available_' . $productID;

	if( $this->data->$stock == 0 && ( empty( $deliveryType ) || $deliveryType != 1 ) ) {

		 $this->content = '<blink><b>' . TR1227580983RNJM . '</b></blink>';

		 return true;

	}




	if( isset($this->data->$available) && !empty($this->data->$available) )

	{

		if( time() >= $this->data->$available )

		{


			$this->content = '<a href="'. WPage::routeURL('controller=basket&task=addbasket&eid='. $pid ) .'"> <input type="button" size="35" value="'. TR1263480669ALUN.'"> </a>';

			return true;

		}

		else

		{

			$this->content .= '<blink><b>' . TR1206961954EAMY . date('d',  $this->data->$available )  . '</b></blink>';

			return true;

		}
	}

	else

	{


		$this->content = '<a href="'. WPage::routeURL('controller=basket&task=addbasket&eid='. $pid ) .'"> <input type="button" size="35" value="'. TR1263480669ALUN.'"> </a>';

		return true;

	}


}

}