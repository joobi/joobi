<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_Vendprodpricefe_listing extends WListings_standard {
function create()

{

	
	$curid = $this->getValue('curid');

	$price = $this->value;

	

	$price = (empty($price) || $price == 0) ? TR1256629168HWSC : WTools::format( $price, 'money', $curid);

	

	if( WGlobals::checkCandy(50) && ( $this->value > 0 ) )

	{

		WPref::load( 'PCURRENCY_NODE_MULTICUR' );

		if( PCURRENCY_NODE_MULTICUR )

		{

			$vendid = $this->getValue( 'vendid' );

		

			static $vendorHelperC=null;

			if( !isset($vendorHelperC) ) $vendorHelperC = WClass::get('vendor.helper',null,'class',false);

			$vendorCurid = $vendorHelperC->getCurrency( $vendid );

		

	                
	                if( $curid != $vendorCurid ) 

	                {

	                	static $currencyC=null;

	                        if( !isset($currencyC) ) $currencyC = WClass::get( 'currency.convert',null,'class',false);

	                        $object = new stdClass;

	                        $object->currencyFrom = $curid;

	                        $object->currencyTo = $vendorCurid;

	                        $object->amount = $this->value;

	                        $object->formatAmount = true;

	                        $object->decimalPlace = 2;

	

	                        $price = $currencyC->convertTo( $object );       

	                 }
	         }
	}
	

	$this->content = '<strong>'.$price.'</strong>';

	return true;

}}