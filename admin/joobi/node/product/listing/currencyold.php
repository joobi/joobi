<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_Currencyold_listing extends WListings_standard {




function create()

{

	$curid = $this->getValue( 'curid' );

	$price = ( empty($this->value) || ( $this->value <= 0 ) ) ? TR1206961944PEUR : WTools::format( $this->value, 'money', $curid );

	

	if( !empty($this->value) && ( $this->value > 0 ) )

	{

		if( WGlobals::checkCandy(50) )

		{

			WPref::load( 'PCURRENCY_NODE_MULTICUR' );

			if( PCURRENCY_NODE_MULTICUR )

			{

				$vendid = $this->getValue( 'vendid' );

			

				
				static $vendorC=null;

				if( empty($vendorC) ) $vendorC = WClass::get('vendor.helper',null,'class',false);

				$vendcurid = $vendorC->getCurrency( $vendid );

				

				
				if( empty($vendcurid) )

				{

					if( !defined( 'PCURRENCY_NODE_PREMIUM' ) ) WPref::get( 'currency.node' );

					$vendcurid = PCURRENCY_NODE_PREMIUM;

				}
									

				if ( !defined('CURRENCY_USED') ) 

				{

				    $currencyFormatC = WClass::get('currency.format',null,'class',false);

				    $currencyFormatC->set();

				}
				

				$choosedCurrency = CURRENCY_USED;

			

				
				static $curconC=null;

				if( !isset($curconC)) $curconC = WClass::get( 'currency.convert',null,'class',false);

				$revCon = ( $curid == $vendcurid ) ? false : true;

				$priceconv = $curconC->currencyConvert( $curid, $choosedCurrency, $this->value, $revCon );

			

				$price = ( is_numeric($priceconv) ) ? WTools::format( $priceconv, 'money', $choosedCurrency ) : $priceconv;

			}
		}
	}


	$this->content = '<strong>'.$price.'</strong>';

	return true;

}}