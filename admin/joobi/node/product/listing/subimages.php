<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;












class Product_Subimages_listing extends WListings_image{
	





	function create() {
		if ( $this->value<1 ) return '';

		$image = WModel::get('files');
				$img = $image->getInfo($this->value );

		if ( isset($img) ) {

			$img->path=$image->convertPath($img->path);

						$url= JOOBI_URL_MEDIA.$img->path.'/';
						if(isset($this->element->thumb)){
				if(file_exists(JOOBI_DS_MEDIA.$img->path.DS.'thumbnails'.DS.$img->name.'.'.$img->type))
					$url.='thumbnails'.DS;
				$width=$img->twidth;
				$heigth=$img->theight;
			}else{
				$width=$img->width;
				$heigth=$img->height;
			}			$heigth = $heigth/2;
			$width = $width/2;
						$url.=$img->name.'.'.$img->type;

			$contentImg='<img src="'.$url.'" width="'.$width.'px" height="'.$heigth.'px" border="0" id="pic'.$this->name.$this->line.'" alt="'.$img->name.'.'.$img->type.'" />';
			$class = ( isset( $this->data->classes ) ) ? $this->data->classes : 'a';
			if(!isset($this->element->lien) && !isset($this->element->imgfull)){
				$this->content =	$contentImg.'<br />';
			}elseif(!isset($this->element->lien) && isset($this->element->imgfull)){
								
				$this->content .= WPage::createPopUpLink( JOOBI_URL_MEDIA.$img->path.'/'.$img->name.'.'.$img->type, $contentImg, $img->width, $img->height, $class, 'ln'.$this->name.$this->line );
			}else{
				$this->content = $contentImg;
			}
			if(isset($this->element->pname)){
				$this->content .= '<small>'.$img->name.'.'.$img->type.'</small></a>';
			}
		}
		return $this->content;

	}}