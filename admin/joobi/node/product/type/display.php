<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Product_display_type extends WTypes {
	var $display = array(
		'standard' => 'Default',
		'carrousel' => 'Carrousel',
		'slider' => 'Slider',
		'scroller' => 'Scroller',
		'accordion-horizontal' => 'Horizontal Accordion',
		'accordion-vertical' => 'Vertical Accordion',
		'vertical' => 'Vertical',
		'horizontal' => 'Horizontal'
	 );

}