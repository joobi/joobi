<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Unittype_type extends WTypes {
var $unittype = array(

	'1' => 'Gram (g)',

	'2' => 'Kilogram (kg)'

);
}