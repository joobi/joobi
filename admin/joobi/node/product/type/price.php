<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Price_type extends WTypes {
	
	
var $price = array(
	'10' => 'Price',

	'17' => 'Shoppers',
	'20' => 'Donation',

	'40' => 'Link',		'50' => 'ContactUs',

	'250' => 'Free'
);
}