<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Product_export_type_type extends WTypes {
var $product_export_type = array(

	1=>'Google Merchant Feed',

	2=>'Csv File',

);
}