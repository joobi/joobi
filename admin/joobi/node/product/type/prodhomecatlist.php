<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Prodhomecatlist_type extends WTypes {


var $prodhomecatlist = array(

	'1'=>'List 1',

	'2'=>'List 2',

	'3'=>'List 3'

);
}