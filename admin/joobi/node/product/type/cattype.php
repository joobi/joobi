<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Product_Cattype_type extends WTypes {

	var $cattype = array (
		'list' => 'Normal listing',
		'div' => 'Div listing'
	);
}