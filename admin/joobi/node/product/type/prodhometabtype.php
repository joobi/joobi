<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Prodhometabtype_type extends WTypes {


var $prodhometabtype = array(

	'1'=>'Single Tab',

	'2'=>'Multiple Tabs',

	'3'=>'Slider'

);
}