<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Reqtype_type extends WTypes {


var $reqtype = array(

	'0' => 'Quantity',

	'1' => 'Price'

);
}