<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

 defined('_JEXEC') or die;











class Product_Weightmeasure_type extends WTypes {



var $weightmeasure = array(

		'1' => 'Grams',

		'2' => 'Kilograms (kg)'

	);
}