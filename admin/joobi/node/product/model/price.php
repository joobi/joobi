<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Price_model extends WModel {

	function validate() {


		$linkValue = ( !empty($this->link) ) ? strtolower( $this->link ) : null;

		if ( !empty($linkValue) && ( $linkValue == 'vendor contact page' ) ) $this->link = null;


		return true;


	}
}