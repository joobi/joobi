<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'item.model.node', JOOBI_DS_NODE );
class Product_Node_model extends Item_Node_model {

	public $priceid = 0;




	function addValidate() {

				$deliveryType = WPref::load('PPRODUCT_NODE_DELIVERYTYPE');
		if ( !empty( $deliveryType ) ) {
			$this->electronic = $deliveryType;
		}
				if ( empty( $this->priceid ) ) {
						$productPricetypeC = WClass::get( 'product.pricetype' );
			$priceid = $productPricetypeC->getPriceTypeID( 'price' );
			if ( is_array( $priceid ) ) $this->priceid = $priceid[0];
			else $this->priceid = $priceid;
		}
		return parent::addValidate();

	}




function editValidate() {

	$productPricetypeC = WClass::get( 'product.pricetype' );
	$freePriceIDA = $productPricetypeC->getPriceTypeID( 'free' );

	


	if ( !empty($this->priceid) && in_array( $this->priceid, $freePriceIDA ) ) {
		$this->price = 0;
	}
	if ( !empty($this->discountrate) ) {
				if ( $this->discountrate > 100 ) {
			$this->discountrate = 100;
		}				if ( $this->discountrate < 0 ) {
			$this->discountrate = $this->discountrate * -1;
		}
	}
	if ( !empty($this->discountvalue) && !empty($this->price) ) {
		if ( $this->discountvalue > $this->price ) $this->discountvalue = $this->price;
		if ( $this->discountvalue < 0 ) $this->discountvalue = $this->discountvalue * -1;

	}
	return parent::editValidate();

}










function validate() {

	parent::validate();

		if ( empty($this->curid) ) {
		$this->curid = WPref::load( 'PCURRENCY_NODE_PREMIUM' );

				if ( empty($this->curid) ) $this->curid = 1;
	}

	$defaultItemType = $this->getItemType();

			if ( 'product' == $defaultItemType ) {
		$checkMembe = $this->getChild( $defaultItemType . '.poptions', 'opid' );
		if ( !empty( $checkMembe ) ) {
			$key = array_search( 0, $this->getChild( $defaultItemType . '.poptions', 'opid') );
			if ( is_numeric($key) ) $this->unsetChild( $defaultItemType . '.poptions', 'opid', $key );
		}
	}

	
	$currencyFormatC = WClass::get( 'currency.format', null, 'class', false );

	if ( !empty($this->price) ) $this->price = $currencyFormatC->SQLizePrice( $this->price );

	
	if ( !isset($this->stock) || ( !empty($this->electronic) && $this->electronic==1 ) ) $this->stock = '-1';




		if ( !IS_ADMIN && !empty($this->price) ) {
		$minimunPrice = WPref::load('PPRODUCT_NODE_PRICE_MIN');

		if ( !empty($minimunPrice) && $minimunPrice > 0 && $this->price < $minimunPrice ) {
			$PRICE = $minimunPrice;
			$this->price = $minimunPrice;
			$message = WMessage::get();
			$message->userN('1340153118LQXB',array('$PRICE'=>$PRICE));
		}
		$maximumPrice = PPRODUCT_NODE_PRICE_MAX;
		if ( !empty($maximumPrice) && $maximumPrice > 0 && $this->price > $maximumPrice ) {
			$PRICE = $maximumPrice;
			$this->price = $maximumPrice;
			$message = WMessage::get();
			$message->userN('1340153118LQXC',array('$PRICE'=>$PRICE));
		}
	}
		if ( empty($this->p['shippingmethod']) ) {
		if ( !defined('PPRODUCT_NODE_AVAILABLESHIPPINGMETHOD') ) WPref::get('product.node');
		$allAvaillableA = explode( ',', PPRODUCT_NODE_AVAILABLESHIPPINGMETHOD );
		if ( count( $allAvaillableA ) == 1 ) $this->p['shippingmethod'] = $allAvaillableA[0];
	}
	return true;


}



function extra() {

	$AVAILABLESHIPPINGMETHOD = WPref::load('PPRODUCT_NODE_AVAILABLESHIPPINGMETHOD');

	$allAvaillableA = explode( ',', $AVAILABLESHIPPINGMETHOD );
		if ( in_array( 9, $allAvaillableA ) ) {
				if ( !empty( $this->_p['shippingprice'] ) && !empty( $this->_p['shippingmethod'] ) && $this->_p['shippingmethod'] == 9 ) {

						$productShippingoptionC = WClass::get( 'product.shippingoption' );
			$name = $this->getChild( 'producttrans', 'name');
			$pricetaxtypeParam = ( !empty($this->_p['pricetaxtype']) ? $this->_p['pricetaxtype'] : 0 );
			if ( empty($this->vendid) ) {
								$itemM = WModel::get( 'item' );
				$itemM->whereE( 'pid', $this->pid );
				$this->vendid = $itemM->load( 'lr', 'vendid' );
			}			$productShippingoptionC->createShippingOption( $this->pid, $this->_p['shippingprice'], $this->curid, $this->vendid, $name, $pricetaxtypeParam );

		} else {											$productShippingoptionC = WClass::get( 'product.shippingoption' );
			$productShippingoptionC->deleteShippingOption( $this->pid );

		}
	}

	return parent::extra();

}















	protected function setDefaultPreferences($processO) {


		$processO = new stdClass;
		$processO->bundle = true;
		$processO->related = true;
		return parent::setDefaultPreferences( $processO );

	}
}