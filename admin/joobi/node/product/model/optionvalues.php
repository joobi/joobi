<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Optionvalues_model extends WModel {

function validate() {



	$opid = $this->opid;

	



	$productOptionC = WClass::get('product.option');

	$opttype = $productOptionC->getOptionType( $opid );

	if ( empty( $this->groupkey ) ) {

		$message = WMessage::get();

		$message->userW('1369750942JUCR');

	}


	if ( $opttype==2 ) { 
		if ( !is_numeric($this->groupkey) ) {

			$message = WMessage::get();

			$message->historyE('1260501529MKQV');

		}
	}




	return true;



}}