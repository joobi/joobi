<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Option_model extends WModel {

	function validate() {


	if ( empty($this->alias) ) {

		$this->alias = $this->getChild( 'product.optiontrans', 'name' );

	}

	return true;



}


function addValidate() {



	if ( empty( $this->vendid ) ) {

		$uid = WUser::get( 'uid' );

		$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

		$this->vendid = $vendorHelperC->getVendorID( $uid, true );

	}

	if ( empty( $this->namekey ) ) {
		$this->genNamekey();
	} else {
	
				$productOptionM = WModel::get( 'product.option' );
		$productOptionM->whereE( 'namekey', $this->namekey );
		$exits = $productOptionM->load( 'lr', 'opid');
		
		if ( !empty($exits) ) {
			$message = WMessage::get();
			$message->historyE('1330653576QNLB');
		}		
	}	

	return true;



}}