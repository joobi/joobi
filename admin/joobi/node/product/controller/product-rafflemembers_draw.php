<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_rafflemembers_draw_controller extends WController {




function draw()

{

	

	
	$pid = WGlobals::getEID();

	$titleheader = WGlobals::get( 'titleheader' );



	
	$prodRafC = WClass::get( 'product.raffle', null, 'class', false );

	if( !empty($prodRafC) )

	{ 

		$rafid = $prodRafC->getRaffleID( $pid );

	

		
		$prodRafMemM = WModel::get( 'product.rafflemembers' );	

		$prodRafMemM->whereE( 'rafid', $rafid );

		$prodRafMemM->where( 'uid', '>', 0 );

		$rafflesA = $prodRafMemM->load( 'lra', 'rafno' );

		

		if( empty($rafflesA) )

		{

			
			$random = array_rand( $rafflesA );

			$winNum = $rafflesA[$random];		

		

			
			$prodRafMemM->whereE( 'rafid', $rafid );

			$prodRafMemM->whereE( 'rafno', $winNum );

			$uid = $prodRafMemM->load( 'lr', 'uid' );

		} 

		else

		{

			$uid = 0;

			$winNum = 0;

		}
	}

	else

	{

		$message = WMessage::get();

		$message->adminW( 'Error in using the draw function for raffles. Class product.raffle does not exist, trace controller product-rafflemembers.draw' );

		WPage::redirect( 'controller=product-rafflemembers' );

		return true;

	}


	
	WGlobals::set( 'eid', $uid, 'joobi' );

	WGlobals::set( 'winnum', $winNum, 'joobi' );

	



	return true;

}}