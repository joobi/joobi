<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_export_controller extends WController {
	function export() {
		
		$trucs = WGlobals::get('trucs');
		$productType = WGlobals::get('producttype');
		if ( empty($productType) ) {
			$myClassName = get_class( $this );
			$myClassNameA = explode( '_', $myClassName );
			$productType = strtolower( $myClassNameA[0] );
		}		
		$exporttype = $trucs['x']['exporttype'];
	
		if ($exporttype == 1) {
			$googleFeed = WClass::get( 'product.merchantfeed' );
			$googleFeed->makeFeedFile( );
			
			$message = WMessage::get();
			$message->userS('1312180347QOGF');
			
			return false;
			
		} else {
			
			WPage::routeURL('controller='.$productType.'&task=export&producttype='.$productType );
			
		}		
		return true;
		
	}}