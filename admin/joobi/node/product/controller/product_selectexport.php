<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_selectexport_controller extends WController {
function selectexport() {



if ( !IS_ADMIN ) WPage::redirect( 'controller=product&task=export&producttype=product');



return true;

}
}