<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_assigncat_controller extends WController {




function assigncat() {

	$pid = WGlobals::getEID();

	$titleheader = WGlobals::get( 'titleheader' );



	$prodSID = WModel::get( 'product', 'sid' );

	$trucs = WGlobals::get( 'trucs' );

	$prodtypid = isset( $trucs[ $prodSID ][ 'prodtypid' ] ) ? $trucs[ $prodSID ][ 'prodtypid' ] : WGlobals::get( 'prodtypid' );

	

	if (empty($titleheader))

	{

		$productM = WModel::get( 'producttrans' );

		$productM->whereE( 'pid', $pid );

		$titleheader = $productM->load( 'r', 'name');

	}


	WPage::redirect( 'item=item-category-assign&task=listing&eid='. $pid .'&prodtypid='. $prodtypid .'&titleheader='. $titleheader );

	return true;

}}