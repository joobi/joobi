<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_blockproduct_controller extends WController {
function blockproduct() {
	$pid = WGlobals::get('eid');
	$block= WGlobals::get('block');

	$productM=WModel::get('product');
	$productM->whereE('pid', $pid);
	$productM->setVal('block', $block);	
	$productM->update();	
	
	if ( WExtension::exist( 'vendors.node' ) ) {
		$vendid = WGlobals::get( 'vendid' );	
		
		if ( !isset($vendorC) ) $vendorC = WClass::Get('vendor.helper');
		$vendorO = $vendorC->getVendor( $vendid );
		if ( !empty($vendorO) ) {

						$prodtransM = WModel::get( 'producttrans' );
			$prodtransM->whereE( 'pid', $pid );
			$prodName = $prodtransM->load( 'lr', 'name' );
		
						$vendorsEmailC = WClass::get( 'vendors.email' );
			$uidStoreManager = $vendorsEmailC->getStoreMangerContact( 'email' );
						
						$param = new stdClass;
			if ( $block == 0 ) $param->status = TR1246518570RHDZ;
			else $param->status = TR1310529911CSAV;
			$user->email = ( !empty($vendorO->email) ) ? $vendorO->email : ( !empty($vendorO->contactemail) ? $vendorO->contactemail : '' );
			if ( empty($user->email) ) {
				$user->email = WUser::get( 'email', $vendorO->uid );
			}			$param->vendname = ( !empty($vendorO->name) ) ? $vendorO->name : WUser::get('name', $vendorO->uid); 
			$param->prodname = $prodName;
			$param->prodtype = 'product';
			$param->adminemail = $uidStoreManager;
			$param->sitename = JOOBI_SITE_NAME;
			$param->sitelink = '<a href="'. JOOBI_SITE .'">'. JOOBI_SITE .'</a>';
			$emailNamekey = 'vendor_prod_status_notification';
			
			$vendMemC = WClass::get( 'vendors.email' );
			$vendMemC->sendNotification( $user, $emailNamekey, $param );
				
		} else {
			$message = WMessage::get();
			$message->userE('1309242713RQCC');
		}	
	}	
	$controller = WGlobals::get( 'tocontroller' );	
	WPage::redirect('controller='.$controller.'&task=show&eid='.$pid);
	return true;
	
}}