<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_virtuemartimport_controller extends WController {

	function virtuemartimport() {

		$vmInstalled = WApplication::isEnabled( 'com_virtuemart', false );

		$message = WMessage::get();

		

		

		if ($vmInstalled) {

			$virtuemartC = WClass::get( 'product.virtuemart' );			

			$importCatC = WClass::get( 'item.category' );

					

			$categories = $virtuemartC->getCategories(); 
			$products = $virtuemartC->getProducts(); 
			$virtuemartC->copyVMcatImages(); 
			$virtuemartC->copyVMprodImages(); 
					

			if (!empty($categories)) {

				foreach($categories as $oneCategory) {

					$importCatC->insertCategory( $oneCategory, 'store', 'virtuemart' );

				}

			}
					

			if (!empty($products)) {
				static $itemInsertC = null;
				if ( !isset($itemInsertC) ) $itemInsertC = WClass::get('item.insert');
				

				foreach($products as $product) {


					$itemInsertC->insertItem( $product, false, 'virtuemart', 'product' );
				}

			}
					

			
			$catProds = $virtuemartC->getCategoryProducts();

			$importCatC->insertCatItem($catProds); 
					

			$message->userS('1308652783LDDK');

		} else {

			$message->userN('1311306414GHWY');

		}	

			

		return true;

	}}