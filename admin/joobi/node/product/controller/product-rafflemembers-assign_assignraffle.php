<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_rafflemembers_assign_assignraffle_controller extends WController {




function assignraffle()

{

	
	$uid = WGlobals::getEID();

	$rafid = WGlobals::get( 'rafid' );

	$rafno = WGlobals::get( 'rafno' );

	$titleheader = WGlobals::get( 'titleheader' );

	$pid = 0;

	

	$prodRafC = WClass::get( 'product.raffle', null, 'class', false );

	if( !empty($prodRafC) )

	{

		
		$prodRafC->assignRaffle( $rafid, $rafno, $uid );

	

		
		$raffleObj = $prodRafC->getRaffle( $rafid, $rafno );

		$pid = $raffleObj->pid;

		$html = '<table border="0">';

		$html .= '<tr><td> '. TR1254292157HDGC .': '. WApplication::date( JOOBI_DATE5, $raffleObj->datepurchased ) .'</td></tr>';

		

		$html .= '<tr>';

		

		$html .= '<td> '. TR1254123961COUV .' # '. $rafno .'</td>';



		$html .= '</tr>';

		$html .= '</table>';

		

		
		$param = new stdClass;

		$param->raffleno = $rafno;

		$param->customerName = WUser::get( 'name', $uid );

		$param->customerEmail = WUser::get( 'email', $uid );

		$link = WPage::routeURL( 'controller=product-rafflemembers&task=listing&eid='. $pid .'&titleheader='. $titleheader, 'admin' );

		$param->textlink = '<a href="'. $link .'">here</a>';

		$param->sitename = JOOBI_SITE_NAME;

		$param->raffleHTML = $html;

	

		
		$prodRafC->sendRaffleEmail( 'customer_raffle_ticketno', $uid, $param, true );



	}

	else

	{

		
		$message = WMessage::get();

		$message->adminW( 'Failed in assigning the raffle ticket. Class does not exist, trace controller product-rafflemembers-assign.assignraffle' );

	}
	

	WPage::redirect( 'controller=product-rafflemembers&task=listing&eid='. $pid .'&titleheader='. $titleheader );

	return true;

}}