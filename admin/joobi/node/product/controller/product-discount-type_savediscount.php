<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_discount_type_savediscount_controller extends WController {




function savediscount() {


	$trucs = WGlobals::get( 'trucs' );

	$pids = explode('011', $trucs['x']['id']);



	$discountMID = WModel::get( 'discount', 'sid' );

	$formValues = $trucs[$discountMID];

	$discounttype = $trucs['x']['discounttype'];



	if (!empty($pids) && !empty($formValues))

	{

		if ($formValues['startdate'] > 0) $formValues['startdate'] = strtotime($formValues['startdate']);

		else $formValues['startdate'] = time();

		if ($formValues['enddate'] > 0) $formValues['enddate'] = strtotime($formValues['enddate']);

		else $formValues['enddate'] = 0;



		
		$discountM = WModel::get( 'discount' );

		$discountM->setVal( 'distype', $discounttype );

		$discountM->setVal( 'disvaltype', $formValues['disvaltype'] );

		$discountM->setVal( 'disvalue', $formValues['disvalue'] );

		$discountM->setVal( 'startdate', $formValues['startdate'] );

		$discountM->setVal( 'enddate', $formValues['enddate'] );

		$discountM->setVal( 'rolid', $formValues['rolid'] );

		if (isset($formValues['reqtype'])) $discountM->setVal( 'reqtype', $formValues['reqtype'] );

		if (isset($formValues['reqvalue'])) $discountM->setVal( 'reqvalue', $formValues['reqvalue'] );

		$discountM->insert();



		
		$discountM->whereE( 'distype', $discounttype );

		$discountM->whereE( 'disvaltype', $formValues['disvaltype'] );

		$discountM->whereE( 'disvalue', $formValues['disvalue'] );

		$discountM->whereE( 'startdate', $formValues['startdate'] );

		$discountM->whereE( 'enddate', $formValues['enddate'] );

		$discountM->whereE( 'rolid', $formValues['rolid'] );

		if (isset($formValues['reqtype'])) $discountM->whereE( 'reqtype', $formValues['reqtype'] );

		if (isset($formValues['reqvalue'])) $discountM->whereE( 'reqvalue', $formValues['reqvalue'] );

		$disid = $discountM->load( 'r', 'disid');



		$productM = WModel::get( 'product' );

		$productdiscountM = WModel::get( 'product.discount' );

		foreach( $pids as $pid )

		{

			
			$productdiscountM->setVal( 'pid', $pid );

			$productdiscountM->setVal( 'disid', $disid );

			$productdiscountM->insert();



			
			$productM->updatePlus( 'numdiscount', 1 );

			$productM->whereE( 'pid', $pid );

			$productM->update();

		}


		$message = WMessage::get();

		$message->userS('1275579115ARZM');

		WPage::redirect( 'controller=product' );

	}

	else

	{

		$message = WMessage::get();

		$message->userE('1240389043FKGS');

	}


	return true;

}}