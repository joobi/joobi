<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_publishproduct_controller extends WController {
	
function publishproduct() {
	$pid = WGlobals::get('eid');
	$publish= WGlobals::get('publish');
	
	$productM=WModel::get('product');
	$productM->whereE('pid', $pid);
	$productM->setVal('publish', $publish);
	if (!$publish) $productM->setVal( 'featured', 0 ); 	$productM->update();	

	if ( WExtension::exist( 'vendors.node' ) ) {
		$vendid = WGlobals::get( 'vendid' );	
		
		if ( !isset($vendorC) ) $vendorC = WClass::get('vendor.helper');
		$vendorO = $vendorC->getVendor($vendid);
		
				$prodtransM = WModel::get( 'producttrans' );
		$prodtransM->whereE( 'pid', $pid );
		$prodName = $prodtransM->load( 'lr', 'name' );
	
				$vendorsEmailC = WClass::get( 'vendors.email' );
		$uidStoreManager = $vendorsEmailC->getStoreMangerContact( 'email' );
				
				$param = new stdClass;
		if ( $publish == 0 ) $param->status = 'Unpublished';
		else $param->status = 'Published';
		$user->email = (!empty($vendorO->email))? $vendorO->email : $vendorO->contactemail;
		$param->vendname = (!empty($vendorO->name))? $vendorO->name : WUser::get('name', $vendorO->uid); 
		$param->prodname = $prodName;
		$param->prodtype = 'product';
		$param->adminemail = $uidStoreManager;
		$param->sitename = JOOBI_SITE_NAME;
		$param->sitelink = '<a href="'. JOOBI_SITE .'">'. JOOBI_SITE .'</a>';
		$emailNamekey = 'vendor_prod_status_notification';
		
		$vendMemC = WClass::get( 'vendors.email' );
		$vendMemC->sendNotification( $user, $emailNamekey, $param );
	
	}	
	$controller = WGlobals::get( 'tocontroller' );	
	WPage::redirect('controller='.$controller.'&task=show&eid='.$pid);
	return true;
}}