<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_vendor_verification_controller extends WController {




function verification() {
	

	
	$roleC = WRole::get();

	$status = $roleC->hasRole( 'sadmin' );



	if ( $status ) {
		

		
		$pid = WGlobals::getEID();

		$uid = WGlobals::get( 'id' );

		

		$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

		$vendid = $vendorHelperC->getVendorID( $uid );

	

		
		$producttransM = WModel::get( 'producttrans' );

		$producttransM->whereE( 'pid', $pid );

		$prodName = $producttransM->load( 'r', 'name' );

		

		
		$name = WUser::get( 'name', $uid );

	

		
		$productM = WModel::get( 'product' );

		$productM->setVal( 'block', 0 );

		$productM->whereE( 'pid', $pid );

		$productM->whereE( 'vendid', $vendid );

		$productM->update();

		

		
		$param = new stdClass;


		$param->itemName = $prodName;

		$param->name = $name;	

		

		
		$emailNamekey = 'vendor_item_approval_notification';

		

		$vendMemC = WClass::get( 'vendors.email', null, 'class', false );

		if ( !empty($vendMemC) ) $vendMemC->sendNotification( $uid, $emailNamekey, $param );

		

		$message = WMessage::get();

		$message->adminS( 'Successfully approved item!' );



	}


	WPage::redirect( 'controller=item&search=' . $pid );
	

	return true;

}
}