<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_rafflemembers_assign_back_controller extends WController {




function back()

{

	$rafid = WGlobals::get( 'rafid' );

	$titleheader = WGlobals::get( 'titleheader' );



	$prodRafC = WClass::get( 'product.raffle' );

	$raffleObj = $prodRafC->getRaffle( $rafid );

	$pid = $raffleObj->pid;



	WPage::redirect( 'controller=product-rafflemembers&task=listing&eid='. $pid .'&titleheader='. $titleheader );

	return true;

}}