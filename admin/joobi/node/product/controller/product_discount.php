<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_discount_controller extends WController {




function discount()

{



	
	$modelID = WModel::getID( 'product' );

	$pids = WGlobals::get( 'pid_'. $modelID );





	if ( empty($pids) )

	{

		$pids = WGlobals::getEID( true );

		if ( !empty($pids) && !is_array($pids) )

		{

			$pidA = array();

			$pidA[] = $pids;

			$pids = $pidA;

		}
	}


	if ( !empty($pids) )

	{ 

		
		
		$productM = WModel::get( 'product' );

		$productM->makeLJ( 'product.price', 'priceid' );

		$productM->whereIN( 'pid', $pids, 0 );

		$productM->where( 'type', '!=', 20, 1 ); 
		$productM->select( 'pid', 0 );

		$products = $productM->load( 'lra' );

	

		
		if ( !empty($products) ) WPage::redirect( 'controller=product-discount-type&task=listing&id='. implode('011', $products ) );

		else

		{

			
			
			$message = WMessage::get();

			$message->userN('1254123954INPN');

		}
	}

	else

	{

		
		$message = WMessage::get();

		$message->adminW( 'Problem of getting the ids of the selected products, trace controller product_discount.' );	

	}


	WPage::redirect( 'controller=product' );

	return true;

}}