<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_option_listing_controller extends WController {
function listing() {



	if ( !IS_ADMIN ) {

		
		$productHelperC = WClass::get( 'item.restriction', null, 'class', false);

		if ( $productHelperC ) $result = $productHelperC->filterRestriction();

		else $result = false;

  

		if ( !$result ) {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 62' );

		}
	}
   

	return true;

}}