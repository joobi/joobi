<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_bundle_controller extends WController {


function bundle() {



	$pids = WGlobals::getEID( true );

	$count = count($pids);



	$message = WMessage::get();

	$productM = WModel::get( 'product' );




	if ( !empty($pids) && ( $count > 1 ) ) {








		




		$productM->select( array( 'bundle', 'alias' ) );

		$productM->select( 'price' );

		$productM->whereIn( 'pid', $pids );

		$allProductA = $productM->load('ol');



		$alias = '';

		$price = 0;

		$i = 0;

		
		foreach( $allProductA as $product ) {



			if ( !empty($product) ) {








					$i++;

					
					$price += $product->price;

					$alias .= '   ' . $i . ': ' . $product->alias;



			}
		}
		$alias = trim($alias);



		









			
			$productProducttypeC = WClass::get('item.type');

			$productTypeID = $productProducttypeC->getDefaultItemType( 'product' );



			$productPriceM = WModel::get( 'product.price' );

			$productPriceM->whereE( 'type', 10 );

			$productPriceM->whereE( 'publish', 1 );

			$productPriceM->orderBy( 'priceid', 'ASC');

			$rpductPriceID = $productPriceM->load( 'lr', 'priceid');



			

			$productM->namekey = $productM->genNamekey( '', 20, 'bundle_' );

			$productM->setChild( 'producttrans', 'name', 'Bundle: ' . $alias );

			$productM->setChild( 'producttrans', 'introduction', 'This is a bundle product' );

			$productM->setChild( 'producttrans', 'description',  'All Bundled items: ' . $alias );

			$productM->alias = $alias;

			$productM->prodtypid = $productTypeID;

			$productM->priceid = $rpductPriceID;

			$productM->price = $price;

			$productM->publish = 1;

			$productM->created = time();

			$productM->modified = time();

			$productM->publishstart = time();

			$productM->bundle = $count;

			$productM->publishend = 0;

			$productM->returnId();

			$productM->save();



			$bundlePID = $productM->pid;



			$dataA = array();

			foreach( $pids as $pid ) {

				$dataA[] = array( $bundlePID, $pid );

			}


			
			$productBundleM = WModel::get( 'product.bundle' );


			$productBundleM->insertMany( array( 'pid', 'ref_pid'), $dataA );










			WPage::redirect( 'controller=product&task=edit&eid='. $bundlePID );




	} elseif ( $count < 2 ) {

		$message->userN('1369750933GZPK');

	} else {

		$message->userN('1237967966PVLY');

	}


	return true;

}}