<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_rafflemembers_cancelraffle_controller extends WController {




function cancelraffle()

{

	
	$rafid = WGlobals::get( 'rafid' );

	$rafno = WGlobals::get( 'rafno' );

	$pid = WGlobals::get( 'pid' );

	$uid = WGlobals::get( 'uid' );

	$titleheader = WGlobals::get( 'titleheader' );

	

	
	$prodRafC = WClass::get( 'product.raffle', null, 'class', false );

	if( !empty($prodRafC) )

	{

		$prodRafC->cancelRaffle( $rafid, $rafno );

		

		
		$param = new stdClass;

		$param->raffleno = $rafno;

		$param->customerName = WUser::get( 'name', $uid );

		$param->customerEmail = WUser::get( 'email', $uid );

		$link = WPage::routeURL( 'controller=product-rafflemembers&task=listing&eid='. $pid .'&titleheader='. $titleheader, 'admin' );

		$param->textlink = '<a href="'. $link .'">here</a>';

		$param->sitename = JOOBI_SITE_NAME;

	

		
		$prodRafC->sendRaffleEmail( 'customer_cancelraffle', $uid, $param, true );

	} 

	else

	{

		
		$message = WMessage::get();

		$message->adminW( 'Failed in cancelling the raffle ticket. Class does not exist, trace controller product-rafflemembers.cancelraffle' );

	}
	

	
	WPage::redirect( 'controller=product-rafflemembers&task=listing&eid='. $pid .'&titleheader='. $titleheader );

	return true;

}}