<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_optionvalues_listing_controller extends WController {

function listing() {
	

	if ( !IS_ADMIN ) {

		$opid = WGlobals::get( 'opid' );

		

		if ( empty( $opid ) ) {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 98' );

		}
 

		
		$productHelperC = WClass::get( 'item.restriction', null, 'class', false);

		if ( $productHelperC ) $result = $productHelperC->filterRestriction( 'query', 'product.option', 'opid', $opid );

		else $result = false;

  

		if ( !$result ) {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 97' );

		}
	}
	

	return true;

}}