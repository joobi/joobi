<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Product_categories_assignproducts_controller extends WController {




function assignproducts()

{

	$pid = WGlobals::get( 'pid' );

	$catids = WGlobals::getEID( true );

	$titleheader = WGlobals::get( 'titleheader' );



	if( !empty($pid) && !empty($catids) )

	{

		static $categoryproductM = null;

		static $categoryM = null;

		static $productM = null;



		foreach( $catids as $catid )

		{

			
			if( !isset($categoryproductM)) $categoryproductM = WModel::get( 'product.categoryproduct' );

			$categoryproductM->whereE( 'catid', $catid );

			$categoryproductM->whereE( 'pid', $pid );

			$categoryproduct = $categoryproductM->load( 'r', 'pid' );



			if(empty($categoryproduct)) {
				

				
				if( !isset($categoryproductM)) $categoryproductM = WModel::get( 'product.categoryproduct' );

				$categoryproductM->setVal( 'catid', $catid );

				$categoryproductM->setVal( 'pid', $pid );

				$categoryproductM->insert();
				

			} else {
				

				
				if( !isset($categoryproductM)) $categoryproductM = WModel::get( 'product.categoryproduct' );

				$categoryproductM->whereE( 'catid', $catid );

				$categoryproductM->whereE( 'pid', $pid );

				$categoryproductM->delete();

			}
									$prodHelperC = WClass::get('item.helper',null,'class',false);

						$obj = new stdClass;
			$obj->tablename = 'product.categoryproduct';
			$obj->column = 'pid';
			$obj->groupBy = 'catid';
			$obj->filterCol1 = 'catid';
			$obj->filterVal1 = $catid;
			$numOfProd = $prodHelperC->noOfItem( $obj );

						if( !empty($numOfProd) && is_numeric($numOfProd) )
			{
				if( !isset($categoryM) ) $categoryM = WModel::get( 'product.category' );
				$categoryM->setVal( 'numpid', $numOfProd );
				$categoryM->whereE('catid', $catid);
				$categoryM->update();
			}
						$obj = new stdClass;
			$obj->tablename = 'product.categoryproduct';
			$obj->column = 'catid';
			$obj->groupBy = 'pid';
			$obj->filterCol1 = 'pid';
			$obj->filterVal1 = $pid;
			$numOfCat = $prodHelperC->noOfItem( $obj );

						if( !empty($numOfCat) && is_numeric($numOfCat) )
			{
				if( !isset($productM) ) $productM = WModel::get( 'product' );
				$productM->setVal( 'numcat', $numOfCat );
				$productM->whereE('pid', $pid);
				$productM->update();
			}

		}		

	} else {
		

		$message = WMessage::get();

		$message->adminE( 'problem with the pass url values, trace -> controller product-categories.assignproducts' );

	}


	WPage::redirect( 'controller=product-categories&task=listing&pid='. $pid .'&titleheader='. $titleheader );

	return true;
	

}















}