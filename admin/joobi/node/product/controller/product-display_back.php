<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_display_back_controller extends WController {


function back() {

	
	
	$catid = WGlobals::get( 'catid' );

	if( empty($catid) ) {

		$trucs = WGlobals::get( 'trucs' );

		$catid = $trucs['x']['catid'];

	}


	if (!empty($catid)) $link = 'controller=item-category&task=show&catid='. $catid;

	else {
		$link = WGlobals::getSession( 'pageLocation', 'lastPage', 'controller=catalog' );		
	}
	

	WPage::redirect( $link, true );

	return true;

}}