<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Product_report_graph_controller extends WController {






















	function graph(){



		$x_name = 'name';

		$y_name = 'hit';



		$chart = null;

		$chart->graph->type = 'graph';



		
		$chart->definition->title->content = 'Content';



		$product = WModel::get('product');

		$product->select('pid',0,'count');

		$xmax = $product->load('lr');

		$chart->definition->x->legend = $x_name;

		$chart->definition->x->max = $xmax;



		$product->select('hit',0,'count');

		$ymax = $product->load('lr');

		$chart->definition->y->legend = $y_name;

		$chart->definition->y->max = $ymax+($ymax*10/100);





		
		$data[1] = null;

		$data[1]->type = 'bar_glass';

		
		$data[1]->legend->content = $y_name;

		$data[1]->legend->size = 10;

		
		$data[1]->tooltip->text = $y_name;

		$data[1]->tooltip->unit = 'views';



		$product->makeLJ('producttrans');

		$product->where($x_name,'!=',null,1);

		$product->select($x_name,1);

		$product->select($y_name,0);

		$tabl = $product->load('ol');

		$finaltabl = array();

		foreach($tabl as $item){

			$finaltabl[$item->$x_name] = $item->$y_name;

		}
		$data[1]->values = $finaltabl;

		$chart->data = $data;

		$graph = new WGraph;

		$this->content = $graph->create($chart);

		return true;

	}}