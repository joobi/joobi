<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_vendor_save_controller extends WController {


function save() {



	
	$eid = WGlobals::getEID();



	$productM = null;

	$uid = WUser::get( 'uid' );



	$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

	$vendid = $vendorHelperC->getVendorID( $uid );



	
	if ( isset( $this->_model->vendid ) && empty($this->_model->vendid) ) {

		if ( empty($eid) ) $eid = $this->_model->pid;

		if ( !isset($productM) ) $productM = WModel::get( 'product' );

		$productM->setVal( 'vendid', $vendid );

		$productM->whereE( 'pid', $eid );

		$productM->update();

	}




	WPage::redirect( 'controller=product&task=edit&eid='. $eid );

	return true;

}}