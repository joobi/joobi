<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_discount_deletediscount_controller extends WController {




function deletediscount()

{

	$eid = WGlobals::getEID();

	$titleheader = WGlobals::get( 'titleheader' );

	$discountSID = WModel::get( 'discount', 'sid' );

	$disids = WGlobals::get( 'disid_'. $discountSID );



	if (!empty($eid) && !empty($disids))

	{

		static $discountM = null;

		$productdiscountM = WModel::get( 'product.discount' );

		$cnt = 0;

		$productdiscount = 0;



		foreach( $disids as $disid )

		{

			
			$productdiscountM->whereE( 'disid', $disid );

			$productdiscount = count($productdiscountM->load( 'lr', 'pid' ));



			
			$productdiscountM->whereE( 'pid', $eid );

			$productdiscountM->whereE( 'disid', $disid );

			$productdiscountM->deleteAll();



			
			if ( $productdiscount < 2 )

			{

				if (empty($discountM)) $discountM = WModel::get( 'discount' );

				$discountM->whereE( 'disid', $disid );

				$discountM->deleteAll();

			}


			$cnt += 1;

		}


		
		$productM = WModel::get( 'product' );

		$productM->updatePlus( 'numdiscount', '-'.$cnt);

		$productM->whereE( 'pid', $eid );

		$productM->update();



		WPage::redirect( 'controller=product-discount&task=listing&eid='. $eid.'&titleheader='. $titleheader );

	}

	else

	{

		WPage::redirect( 'controller=product' );

	}


	return true;

}}