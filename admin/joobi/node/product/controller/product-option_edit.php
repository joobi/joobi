<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_option_edit_controller extends WController {


function edit() {



	if ( !IS_ADMIN ) {

		$opid = WGlobals::getEID();



		if ( empty( $opid ) ) {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 25' );

		}


		
		$productHelperC = WClass::get( 'item.restriction', null, 'class', false);

		if ( $productHelperC ) $result = $productHelperC->filterRestriction( 'query', 'product.option', 'opid', $opid );

		else $result = false;



		if ( !$result ) {

			$message = WMessage::get();
			$message->userE('1373216508GIIF');
			WPage::redirect( 'controller=product-option' );


		}
	}
}}