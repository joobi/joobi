<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;
 class Product_Archive_controller extends WController {





	function archive(){
		$pid = WGlobals::GetEID();
		$model = WModel::get('product','object');
		$model->whereIn('pid',$pid);
		$model->publish = '-1';
		$model->update();

		return true;
	}
}