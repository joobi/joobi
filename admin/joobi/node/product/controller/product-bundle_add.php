<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_bundle_add_controller extends WController {



function add() {


	
	$prevpid = WGlobals::getEID();

	$pid = WGlobals::get('relpid');

	$prodtypid = WGlobals::get( 'prodtypid' );

	$titleheader = WGlobals::get( 'titleheader' );

	$rel = WGlobals::get( 'rel' );



	$productRelatedM = WModel::get('product.bundle');
	if ( !empty($rel) ) {

		$productRelatedM->whereE('pid', $prevpid);

		$productRelatedM->whereE('ref_pid', $pid );

		$productRelatedM->delete();

		$valueToAdd = -1;
	} else {

		$productRelatedM->setVal('pid', $prevpid);

		$productRelatedM->setVal('ref_pid', $pid);

		$productRelatedM->insert();
		$valueToAdd = 1;


	}



		if ( !isset($productM) ) $productM = WModel::get( 'product' );

		$productM->updatePlus( 'bundle', $valueToAdd );

		$productM->whereE('pid', $prevpid);

		$productM->update();




	return true;


}


}