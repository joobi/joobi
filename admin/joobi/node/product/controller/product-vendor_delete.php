<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_vendor_delete_controller extends WController {




function delete()

{



	$uid = WUser::get( 'uid' );

	$modelID = WModel::getID( 'product' );

	$map = 'pid_'. $modelID;

	$pid = WGlobals::get( $map );

	if( is_array($pid) ) $pid = $pid[0];

	

	$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

	$vendid = $vendorHelperC->getVendorID( $uid );



	if( !empty( $pid ) )

	{

		$productM = WModel::get( 'product' );

		$productM->makeLJ( 'producttrans', 'pid', 'pid', 0, 1 );

		$productM->select( 'name', 1);

		$productM->select( 'publish', 0);

		$productM->whereE( 'pid', $pid );

		$productM->whereE( 'vendid', $vendid );

		$prod = $productM->load( 'o' );

		

		if( !empty($prod) )

		{

			
			
			if( isset( $prod->publish ) && ( $prod->publish == 1 ) )

			{

				$name = WUser::get( 'name' );

				$email = WUser::get( 'email' );

				$prodName = $prod->name;

				$text = TR1206961841DNYS;

			

				$itemid = WGlobals::get( 'Itemid' );

			

								$vendorsEmailC = WClass::get( 'vendors.email' );
				$uidStoreManager = $vendorsEmailC->getStoreMangerContact();
				
			

				
				$param = new stdClass;

				$param->vendName = $name;

				$param->vendEmail = $email;

				$param->prodName = $prodName;

				$param->type = $text;

				$param->dellink = WPage::routeURL( 'controller=product-vendor&task=verdelete&eid='. $pid .'&id='. $uid, 'admin', false, false, $itemid, 'jmarket' );

			

				$mail = WMail::get();

				$mail->setParameters( $param );

				$mail->sendNow( $uidStoreManager, 'vendor_product_delete', false );

			

				$message = WMessage::get();

				$message->userS('1329254297FMEP');

			

			} else {
				


			

				
				$productM->whereE( 'pid', $pid );

				$productM->whereE( 'vendid', $vendid );

				$productM->delete();

				

				
				$prodtransM = WModel::get( 'producttrans' );

				$prodtransM->whereE( 'pid', $pid );

				$prodtransM->delete();

				

				
				$prodCatM = WModel::get( 'product.categoryproduct' );

				$prodCatM->whereE( 'pid', $pid );

				$categories = $prodCatM->load( 'lra', 'catid' );

				

				
				if( !empty($categories) )

				{

					$categoryM = WModel::get( 'product.category' );

					foreach( $categories as $category )

					{

						
						$categoryM->whereE( 'catid', $category );

						$categoryM->updatePlus( 'numpid', '-1' );

						$categoryM->update();

						

						
						$prodCatM->whereE( 'pid', $pid );

						$prodCatM->whereE( 'catid', $category );

						$prodCatM->delete();

					}
				}
			}
		}

		else

		{

		}
	}

	else

	{

		$message = WMessage::get();

		$message->adminE( 'Cant find product ID. trace -> controller product-vendor_delete' );

		$message->userN('1251709409AKQJ');

	}


	WPage::redirect( 'controller=product-vendor&task=listing&id='. $vendid, true );

	return true;

}}