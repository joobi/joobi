<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Product_discount_type_next_controller extends WController {






function next()

{

	$trucs = WGlobals::get( 'trucs' );

	$type = $trucs['x']['discounttype'];

	$id = $trucs['x']['id'];



	switch ( $type )

	{

		case 1 : WView::updateElement( 'product_discount_dv', 'form', '1', 'publish' );

			 WView::updateElement( 'product_discount_reqtype', 'form', '1', 'publish' );

			 WView::updateElement( 'product_discount_reqvalue', 'form', '1', 'publish' );			 

			 break;

		case 2 : WView::updateElement( 'product_discount_dv', 'form', '0', 'publish' );

			 WView::updateElement( 'product_discount_reqtype', 'form', '0', 'publish' );

			 WView::updateElement( 'product_discount_reqvalue', 'form', '0', 'publish' );

			 break;

		default :  break;

	}
	

	WGlobals::set( 'id', $id );

	WGlobals::set( 'discounttype', $type );

	



	return true;

}}