<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_option_delete_controller extends WController {
function delete() {

	$modelID = WModel::getID( 'product.option' );

	$map = 'opid_'. $modelID;

	$opidsA = WGlobals::get( $map );



	if ( !IS_ADMIN ) {

		$opid = is_array( $opidsA ) ? $opidsA [0] : $opidsA;

		

		if ( empty( $opid ) ) {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 12' );

		}
	

		
		$productHelperC = WClass::get( 'item.restriction', null, 'class', false);

 		if ( $productHelperC ) $result = $productHelperC->filterRestriction( 'query', 'product.option', 'opid', $opid );

		else $result = false;

		

		if ( !$result ) {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 13' );

		}
	}
	

	parent::delete();

}}