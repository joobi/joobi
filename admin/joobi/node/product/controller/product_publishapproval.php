<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Product_publishapproval_controller extends WController {

	function publishapproval() {



	$productID = WModel::getID('product');

	$pids= WGlobals::get('pid_'.$productID );	

	
	$itemApprovalC = WClass::get( 'item.approval' );
	return $itemApprovalC->emailRequestApproval( $pids, 'admin_publish_approval' );
	
	

	}}