<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_vendor_cancel_controller extends WController {




function cancel()

{

	parent::cancel();

	

	$uid = WUser::get( 'uid' );

	

	$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

	$vendid = $vendorHelperC->getVendorID( $uid );

	

	WPage::redirect( 'controller=product-vendor&task=listing&id='. $vendid, true );

	return true;

}}