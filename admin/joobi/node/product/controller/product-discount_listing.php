<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_discount_listing_controller extends WController {
function listing() {



	if ( !IS_ADMIN ) {

		$pid = WGlobals::getEID();

	

		if ( empty( $pid ) ) {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 89' );

		}
	

		
		$productHelperC = WClass::get( 'item.restriction', null, 'class', false);

 		if ( $productHelperC ) $result = $productHelperC->filterRestriction( 'query', 'item', 'pid', $pid );

		else $result = false;

		

		if ( !$result ) {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 74' );

		}
	}
}}