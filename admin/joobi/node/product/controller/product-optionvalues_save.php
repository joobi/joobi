<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_optionvalues_save_controller extends WController {




function save() {



	$trucs=WGlobals::get('trucs');

	$poptionModelID=WModel::getID('product.option');

	$opid=$trucs['x']['opid'];

	$titleheader=$trucs['x']['titleheader'];




	$optionM=WModel::get( 'product.option' );

	$optionM->whereE('opid', $opid);

	$opttype=$optionM->load('lr', 'opttype' );




	$poptionValueModelID = WModel::getID( 'product.optionvalues' );

	$groupkey = $trucs[$poptionValueModelID]['groupkey'];




	if ( $opttype==5 ) {


		if ( !is_numeric($groupkey) ) {

			$message= WMessage::get();

			$message->historyE('1330653564LQYB');


			return true;

		}
	}


	parent::save();

	
	WPage::redirect( 'controller=product-optionvalues&opid='. $opid .'&titleheader='. $titleheader );



	return true;



}}