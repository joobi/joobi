<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */


defined('_JEXEC') or die;













class Product_Productcompleted_Productcompleted_action {



function create($status,$order) {



	$this->_manageStock(  $order->general->orderID );



	$this->_addTarget( $status, $order );



	return true;

}




function _manageStock($oid){

	if ( empty( $oid ) ) return false;

	static $orderProductM = null;

	if ( empty( $orderProductM ) ) $orderProductM = WModel::get( 'order.products' );

	$orderProductM->whereE( 'oid', $oid );

	$orderProductA = $orderProductM->load( 'ol', array( 'pid', 'quantity' ) );



	if ( !empty( $orderProductA ) ) {

		$productHelperC = WClass::get('product.helper',null,'class',false);

		foreach( $orderProductA as $product ) {

			$productHelperC->manageStock($product->pid, $product->quantity, true);

		}
	}


	return true;



	}







function _addTarget($status,$order) {



		$orderProducts = array();

		$orderProductsBundles = array();



		$oid = $order->general->orderID;

		$uid = $order->general->uid;



		
		$orderproductM = WModel::get( 'order.products' );	
		$orderproductM->makeLJ( 'product', 'pid' );

		$orderproductM->whereE( 'oid', $oid );

		$orderproductM->where( 'targettotal', '>', 0, 1);

		$orderproductM->where( 'price', '>', 0, 1);

		$orderproductM->select( 'pid', 1);

		$orderproductM->select( 'priceref', 0);

	
		$orderProducts = $orderproductM->load( 'ol' );

		$aproductM = WModel::get( 'product' );



		
		if ( !empty($order->bundles) ) {

			$myBundles = array();



			foreach( $order->bundles as $pid => $eachProduct ) {

				foreach( $eachProduct as $eachBundle => $quantity ) {

					$myBundles[$eachBundle] = $eachBundle;

				}

			}

			$aproductM->whereIn( 'pid', $myBundles );

			$aproductM->where( 'targettotal', '>', 0);

			$aproductM->where( 'price', '>', 0);

			$aproductM->select( 'pid');

			$aproductM->select( 'price');

		
			$orderProductsBundles = $aproductM->load( 'ol' );

		}


		$productA = array_merge($orderProducts, $orderProductsBundles);



		if ( !empty($productA) ) {

			foreach( $productA as $product ) {

				$price=(empty($product->price))? $product->priceref : $product->price;

				$aproductM = WModel::get( 'product' );	
				$aproductM->whereE( 'pid', $product->pid );

				$aproductM->updatePlus( 'target', $price);

		
				$aproductM->update();



			}
		}


	return true;

}


}