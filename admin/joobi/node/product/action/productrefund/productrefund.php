<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */


defined('_JEXEC') or die;













class Product_Productrefund_Productrefund_action {



function create($status,$order) {

	$productHelperC = WClass::get('product.helper',null,'class',false);

	$productHelperC->manageStock($order->general->orderID, false);



	$this->_deductTarget( $order );



	return true;

}










function _deductTarget($order) {



	if ( $order && isset($order->details) ) {

		if ( !empty($order->details) ) {

			$aproductM = WModel::get( 'product' );

			foreach( $order->details as $product ) {

				$aproductM->whereE( 'pid', $product->pid );

				$aproductM->where( 'targettotal', '>', 0);

				$aproductM->where( 'price', '>', 0);

				$target = 0-$product->priceRef;

				$aproductM->updatePlus( 'target', $target );

				$aproductM->update();

			}
		}


		if ( !empty($order->bundles) ) {

			$myBundles = array();



			foreach( $order->bundles as $pid => $eachProduct ) {

				foreach( $eachProduct as $eachBundle => $quantity ) {

					$myBundles[$eachBundle] = $eachBundle;

				}

			}

			$aproductM->whereIn( 'pid', $myBundles );

			$aproductM->where( 'targettotal', '>', 0);

			$aproductM->where( 'price', '>', 0);

			$aproductM->select( 'pid');

			$aproductM->select( 'price');

			$orderProductsBundles = $aproductM->load( 'ol' );

		}


		if ( !empty($orderProductsBundles) ) {

			foreach( $orderProductsBundles as $product ) {

				$aproductM->whereE( 'pid', $product->pid );

				$aproductM->where( 'targettotal', '>', 0);

				$aproductM->where( 'price', '>', 0);

				$target = 0-$product->price;

				$aproductM->updatePlus( 'target', $target );


				$aproductM->update();



			}
		}


	}
	return true;

}


}