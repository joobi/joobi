<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Product_Merchantfeed_scheduler extends Scheduler_Parent_class  {

	function process() {
		
		$googleFeed = WClass::get( 'product.merchantfeed' );
		$googleFeed->makeFeedFile();
		
		return true;
	}	
}