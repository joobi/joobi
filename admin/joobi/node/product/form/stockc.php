<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Stockc_form extends WForms_standard {














function show() {

	static $deliveryType = null;
	
	if ( !isset($deliveryType) ) {
		$deliveryType = WPref::load('PPRODUCT_NODE_DELIVERYTYPE');;		
	}	if ( !empty( $deliveryType ) && $deliveryType == 1 ) return false; 
	
	if ( $this->value > 0 ) {

		$this->content = $this->value.' '.TR1206961954EAMT;

	}elseif (($this->value < 0)) {

		$this->content = TR1206961954EAMU;

	} else {

		$this->content = '<b><blink>'.TR1227580983RNJM.'</b></blink>';

	}

	return true;

}
}