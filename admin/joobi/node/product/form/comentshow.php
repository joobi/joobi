<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'form.layout' , JOOBI_LIB_HTML );
class Product_Comentshow_form extends WForm_layout {

function show() {

	$status = parent::create();
	
	$commentButtonC = WClass::get( 'comment.button', null, 'class', false );
	if ( empty($commentButtonC) ) return true;

	if ( empty($list) ) { 	

		$this->content  .= $commentButtonC->addComment( 'first' );			

	} else {
		$this->content  .= $commentButtonC->addComment( 'addcomment' );			
	}
	return true;

}
}