<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'form.text' , JOOBI_LIB_HTML );
class Product_Optpercent_form extends WForm_text {


function create() {



	$opid = $this->getValue( 'opid' );

	if ( empty($opid) ) $opid = WGlobals::get( 'opid' );

	
	

	$productOptionC = WClass::get('product.option');

	$type = $productOptionC->getType($opid);

	if ( 2 != $type ) {

								$this->element->notitle = 1;			$this->content = '';		

		return true;	

	}


	$this->value = round( $this->value, 2 );

	parent::create();

	

	$this->content .= '%';

	return true; 

}}