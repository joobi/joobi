<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Add_form extends WForms_standard {








function show() {



	WPref::load('PPRODUCT_NODE_CATALOGUE');



	
	$pid = WGlobals::getEID();

	$pid = !empty( $pid ) ? $pid : $this->getValue( 'pid' );

	
	$type = $this->getValue( 'type', 'product.type' );

	$price = $this->getValue( 'price' );

	$stock =  $this->getValue( 'stock' );

	$vendid =  $this->getValue( 'vendid' );

	$electronic =  $this->getValue( 'electronic' );

	$prodtypid =  $this->getValue( 'prodtypid' );

	$pricetype = $this->getValue( 'type', 'product.price' );

	$filid = $this->getValue( 'filid', 'product' );

	$rolid_buy = $this->getValue( 'rolid_buy', 'product' );



	$AddCArtParam = array();

	$AddCArtParam['validation'] = true;



	
	if ( $pricetype == 17 ) {

		$shoppersPricesC = WClass::get( 'shoppers.prices', null, 'class', false );

		if ( !empty($shoppersPricesC) ) {

			$myGroupPrice = $shoppersPricesC->getPrice( $pid, $vendid );

			if ( $myGroupPrice !==false ) $price = $myGroupPrice;

			else return false;

		}
	}




	
	if ( WGlobals::checkCandy(25) && ( empty($stock)

	 || ( $stock < 1 && $stock != -1 ) ) && ( !empty($electronic) && $electronic > 1 ) ) {	


		WPref::load('PPRODUCT_NODE_DELIVERYTYPE');

		$deliveryType = PPRODUCT_NODE_DELIVERYTYPE;

		if ( empty( $deliveryType ) || $deliveryType != 1 ) {

			$this->content = '<blink><b>' . TR1227580983RNJM . '</b></blink>';

			return true;

		}


	} else {	


		
		$hasRole = WGlobals::get( 'rolidViewPrice', true, 'joobi' );

		if ( !$hasRole ) return false;



		if ( WGlobals::checkCandy(50) ) {

			if ( empty( $rolid_buy ) || $rolid_buy == 1 ) {

				
				$itemTypeC = WClass::get( 'item.type' );

				$rolid_buy = $itemTypeC->loadData( $prodtypid, 'rolid_buy' );

				if ( empty($rolid_buy) || $rolid_buy == 1 ) {

					WPref::load('PPRODUCT_NODE_ROLID_BUY');

					$rolid_buy = PPRODUCT_NODE_ROLID_BUY;

				}
			}


			if ( !empty($rolid_buy) ) {

				$itemAccessC = WClass::get( 'item.access' );

				$hasRole = $itemAccessC->haveAccess( $rolid_buy );

			} else $hasRole = true;



		}


		
		if ( !$hasRole ) { 
			$roleC = WRole::get();

			$ROLENAME = $roleC->getRole( $rolid_buy, 'name' ) ;

			$this->content = '<br><span>'.str_replace(array('$ROLENAME'),array($ROLENAME),TR1329161760PFPP);

			return true;



		} else { 


			
			if ( $type == 5 ) {	


				
				if ( $price <= 0 ) {

					$SubscribeAction = 'subscribe';

				} else {

					$SubscribeAction = 'addbasket';

				}
				$subscriptionButton = ( $this->_checkLoginRequired() ) ? TR1306322993GCMS : TR1220872966GYHA;



				$joobiRun = WView::addJSAction( $SubscribeAction, $this->formName, $AddCArtParam );

				$this->content = $this->_displayStandardButton( TR1306322993GCMS, TR1220872966GYHA, $joobiRun );



				return true;



			}


						$integrate = WPref::load( 'PCATALOG_NODE_SUBSCRIPTION_INTEGRATION' );
			if ( $integrate ) {
				if ( WExtension::exist( 'subscription.node' ) ) {
					$subscriptionCheckC = WObject::get( 'subscription.check' );
					$subscriptionCheckC->dontDeductCredits();
					$subscriptionCheckC->restriction( 'product_free' );

					if ( $subscriptionCheckC->restrictionExist() && $subscriptionCheckC->getStatus( false ) ) {
												$pricetype = 250;
											}				}			}



			
			$elementMap = $this->element->map;

			switch ( $pricetype ) {

				case 50:	
					$prodPricesC = WClass::get( 'product.prices', null, 'class', false );

					$this->content = $prodPricesC->showContactUs( $this->getValue( 'vendid' ) );

					break;

				case 40:	
					$this->content = '<a class="bigCartLeft" href="'. $this->getValue( 'buynowlink' ) .'" target="_blank"><span class="bigCartRight"><span class="bigCartCenter">'. TR1298350960NBMB .'</span></span></a>';

					break;

				case 250:	
					if ( PPRODUCT_NODE_NOCHECKOUTDL ) {

						$type = 141;	
						$this->_downloadButton( $type, $electronic, $filid, $elementMap, $AddCArtParam );

						break;

					}
					break;

				case 20:	
					$joobiRun = WView::addJSAction( substr($elementMap, 2, strlen($elementMap) - 3 ), $this->formName, $AddCArtParam );

					$this->content = $this->_displayStandardButton( TR1306322992BCSH, TR1301973197AJAW, $joobiRun );

					break;

				case 17:	
					$joobiRun = WView::addJSAction(substr($elementMap, 2, strlen($elementMap) - 3 ), $this->formName, $AddCArtParam );

					$this->content = $this->_displayStandardButton( TR1306322993GCMQ, TR1263480669ALUN, $joobiRun );

					break;

				case 10:	


					
					if ( PPRODUCT_NODE_NOCHECKOUTDL && $price <= 0 ) {

						$type = 141;	
						$this->_downloadButton( $type, $electronic, $filid, $elementMap, $AddCArtParam );

						break;

					} else {

						$joobiRun = WView::addJSAction(substr($elementMap, 2, strlen($elementMap) - 3 ), $this->formName, $AddCArtParam );

						$this->content = $this->_displayStandardButton( TR1306322993GCMQ, TR1263480669ALUN, $joobiRun );

						break;

					}


				default:	


					
					$this->_downloadButton( $type, $electronic, $filid, $elementMap, $AddCArtParam );

					break;



			}


		}


	}


	return true;



}






















private function _downloadButton($type,$electronic,$filid,$elementMap,$AddCArtParam) {



	
	if ( $type == 141 || ( $electronic == 1 && !empty( $filid ) ) && $type != 5 ) {

		$joobiRun = WView::addJSAction( 'download' , $this->formName, $AddCArtParam );
		$this->content = $this->_displayStandardButton( TR1306322993GCMR, TR1206961905BHAV, $joobiRun );

	} else {

		$joobiRun = WView::addJSAction( substr($elementMap, 2, strlen($elementMap) - 3), $this->formName, $AddCArtParam );

		$this->content = $this->_displayStandardButton( $this->element->name, $this->element->name, $joobiRun );

	}


}











private function _displayStandardButton($needLoginText,$noLoginText,$joobiRun) {



	if ( $this->_checkLoginRequired() ) {

		$buttonText = $needLoginText;

		$img = '<a id="addtocartAnchor" target="_blank" href="' . WPage::link( 'controller=users&task=login') . '" class="bigCartLeft">';

	} else {

		$buttonText = $noLoginText;

		$img = '<a id="addtocartAnchor" target="_blank" href="" onClick="return '.$joobiRun.'" class="bigCartLeft">';

	}
	$img .= '<span class="bigCartRight"><span class="bigCartCenter">'. $buttonText .'</span></span>';

	$img .= '</a>';





	
	$availability = WGlobals::get( 'productAvailable', null, 'joobi' );

	switch ( $availability ) {

		case 'avail':	$display = $img;

				break;

		case 'unavail':	$availableStartDate = $this->getValue( 'availablestart' );

				$display = '<center><strong>' . TR1339084889MCJS .'<br/><br/>'. WApplication::date( JOOBI_DATE105, $availableStartDate )  . '</strong></center>';

				break;

		case 'expired':	$availableEndDate = $this->getValue( 'availableend' );

				$text = TR1298350952HBRR;

				$display = '<center><strong>' . TR1339084889MCJT .'<br/><br/>'. WApplication::date( JOOBI_DATE105, $availableEndDate )  . '<br/><br/>'. $text .'</strong></center>';

				break;

		default : $display = $img;

			break;

	}


	return $display;



}













private function _checkLoginRequired() {



	$login = false;

	WPref::load( 'PBASKET_NODE_LOGINBEFORECHECKOUT' );

	if ( PBASKET_NODE_LOGINBEFORECHECKOUT ) {

		
		$login = ! WUser::isRegistered();

	}


	return $login;



}
}