<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

WLoadFile( 'form.yesno' , JOOBI_LIB_HTML );
class Product_Approvedform_form extends WForm_yesno {
function show() {

	$this->value = !$this->value;		

	return parent::show();

}}