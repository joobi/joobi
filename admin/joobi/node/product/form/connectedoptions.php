<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Connectedoptions_form extends WForms_standard {














function show() {



	
	$showOptions = WGlobals::get( 'noItemOptions', false, 'joobi' );

	if ( $showOptions ) return false;



	$display = null;

	if ( !WGlobals::checkCandy(25) ) return true;



	$user = WUser::get('uid');

	$pid = WGlobals::getEID();

	$curid = $this->getValue( 'curid' );



	
	static $optA = array();

	if ( empty( $optA[$pid] ) ) {

		static $poptionM = null;

		if ( empty( $poptionM ) ) $poptionM = WModel::get('product.poptions');

		$poptionM->makeLJ( 'product.option', 'opid', 'opid', 0, 1);

		$poptionM->makeLJ( 'product.optiontrans', 'opid', 'opid', 1, 2);

		$poptionM->whereE('pid', $pid );

		$poptionM->whereE('publish', 1);

		$poptionM->orderBy('ordering');

		$poptionM->whereE('publish', 1, 1);

		$poptionM->orderBy('ordering', 'asc', 1);

		$poptionM->whereLanguage(2);

		$poptionM->select( array('namekey', 'opid', 'type', 'required' ), 1 );

		$poptionM->select( 'name', 2 );

		$productOptionsA = $poptionM->load( 'ol' );

	}




	if ( empty( $productOptionsA ) ) return true;





	
	static $allOptionA = array();

	if ( empty( $allOptionA[$pid] ) ) {

		
		$myOptions = array();

		foreach( $productOptionsA as $optionV ) {

			$myOptions[] = $optionV->opid;

		}


		static $getoptM = null;

		if ( empty( $getoptM ) ) $getoptM = WModel::get( 'product.optionvalues' );


		$getoptM->whereIn('opid', $myOptions );

		$getoptM->makeLJ( 'product.optionvaluestrans', 'opvid', 'opvid');

		$getoptM->whereLanguage(1);

		$getoptM->whereE('publish', 1 );

		$getoptM->select( array( 'groupkey', 'price', 'opvid', 'opid', 'curid', 'pricetaxtype' ) );

		$getoptM->select( 'name', 1 );

		$getoptM->orderBy( 'opid' );

		$getoptM->orderBy( 'ordering' );

		$allOptionA[$pid] = $getoptM->load('ol');

	}


	if ( empty( $allOptionA[$pid] ) ) return true;



	$allOptions = $allOptionA[$pid];



	static $productHelperC = null;

	if ( empty( $productHelperC ) ) $productHelperC = WClass::get('product.helper',null,'class',false);



	foreach( $productOptionsA as $myOption ) {

		$drops = array();



		
		$optionFound = WGlobals::get( $myOption->namekey );



		
		if (!empty($optionFound)) {

			foreach( $allOptions as $key=>$options ) {

				if ( $optionFound == $options->groupkey ) {

					if ( $key != 0 ) {

						$getOption = $allOptions[0];

						$allOptions[0] = $allOptions[$key];

						$allOptions[$key] = $getOption;

					}
				}
			}
		}

		foreach( $allOptions as $optionValue ) {



			if ( $optionValue->opid != $myOption->opid ) continue;



			
			if ( !empty($myOption->required) && empty($optionValue->groupkey) ) {

				$optionValue->opvid = 0;

			}


			if ( $optionValue->price==0 ) {



				$drops[] = WSelect::option( $optionValue->opvid, $optionValue->name );



			} else {


				$optCurid = !empty($optionValue->curid) ? $optionValue->curid : $curid;

				static $productTaxC=null;

				if ( !isset( $productTaxC ) ) $productTaxC = WObject::get( 'product.tax' );

				$prodtypeID = $this->getValue( 'prodtypid' );

				$vendid = $this->getValue( 'vendid' );



				if ( $myOption->type == 2 ) {	


					




					

					





					$productPrice = $this->getValue( 'price' );

					$opprice = ( $productPrice * $optionValue->price ) / 100;

					$productTaxC->setVendorID( $productTaxC->getVendorID( $vendid ) );

					$productTaxC->setPrice( $opprice, $optionValue->pricetaxtype, $prodtypeID );

					$opprice = $productTaxC->renderPrice();



					$optionPrices = $productHelperC->price( $opprice, $curid );

					$drops[] = WSelect::option( $optionValue->opvid, $optionValue->name . ' ' . $optionPrices->money  );



				} else {	
					$productTaxC->setVendorID( $productTaxC->getVendorID( $vendid ) );

					$productTaxC->setPrice( $optionValue->price, $optionValue->pricetaxtype, $prodtypeID );

					$opprice = $productTaxC->renderPrice();



					$prices = $productHelperC->price( $opprice, $optCurid );

					$drops[] = WSelect::option( $optionValue->opvid, $optionValue->name . ' ' . $prices->money );



				}
			}


		}


		$HTMLDrop = new WSelect();

		$picklistName = 'trucs[' . WModel::getID('product.poptions') . '][pid_' . $pid . '][opid][' . $myOption->opid . ']';

		$htmlOutPut = $HTMLDrop->create( $drops, $picklistName, null, 'value', 'text', $this->formName, null, false, false, null );



		$required = '';

		if ( !empty( $myOption->required ) ) {

			$required = '<sup>*</sup>';

			$this->setRequire( $picklistName );	
		}


		$display .= '<div class="cartOptionContent">';

		$display .= '<div class="cartOptionHeading">'. $myOption->name .$required.'</div>';

		$display .= '<div class="cartOptionMain">'. $htmlOutPut .'</div>';

		$display .= '</div>';



	}


	$this->content = $display;

	return true;



}}