<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;






class Product_Displaydiscount_form extends WForms_standard {




function show() {
	

	static $productdiscountM = null;

	if(empty($productdiscountM)) $productdiscountM  = WModel::get( 'product.discount' );

	$productdiscountM->whereE( 'pid', $this->value );

	$productdiscounts = $productdiscountM->load( 'ol', array( 'disid','pid') );



	$display = '';

	if( !empty($productdiscounts) ) {
		

		static $discountM = null;

		if(empty($discountM)) $discountM = WModel::get( 'discount' );

		foreach( $productdiscounts as $productdiscount )

		{

			$discountM->whereE( 'disid', $productdiscount->disid );

			$discounts = $discountM->load( 'ol', array( 'distype','disvalue', 'reqtype', 'reqvalue', 'publishstart', 'publishend' ) );

			

			if( !empty($discounts) )

			{

				foreach( $discounts as $discount )

				{

					if( $discount->distype == 1 ) $display .= TR1240389025PQIE;

					elseif ( $discount->distype == 2 ) $display .= TR1240389025PQIF;

					

				}
			}
			

		}
	}


	$this->content = $display;

	return true;

}















































}