<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Product_Backtocat_form extends WForms_standard {










function show() {

	if (!isset($img))$img = JOOBI_URL_JOOBI_IMAGES . 'toolbar/32/back.png';



	$link = WPage::routeURL( 'previous' );		
	$url = JOOBI_SITE.WView::getURI();		
	


	
	if ($link!=$url) {

		$link = WPage::routeURL( 'previous' );

	}else{

		$catid = WGlobals::get( 'catid' );



		
		if (empty($catid)) {

			if (!isset($modelID))$modelID = WModel::get('product.categoryproduct', 'sid');

			$map = 'catid_'. $modelID;

			$catid = $this->data->$map;



		}


		$link = WPage::routeURL( 'controller=item-category&task=show&catid='. $catid );



	}


	$this->content = '<a href="'. $link .'" style="text-decoration:none;"><img src="'. $img .'"><br>'. TR1206961882TDHA .' </a>';

	return true;

}}