<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'main.form.moneycurrency' , JOOBI_DS_NODE );
class Product_Optcurid_form extends WForm_moneycurrency {
function create() {


	$opid = $this->getValue( 'opid' );
	if ( empty($opid) ) $opid = WGlobals::get( 'opid' );
	
	$productOptionC = WClass::get('product.option');
	$type = $productOptionC->getType($opid);


	if ( 1 != $type ) {
		$formObject = WView::form( $this->formName );
		$formObject->hidden( 'trucs['.$this->modelID.'][curid]', 0 );
	}

	
	
	
	$this->element->notitle = 1;

	$this->content = '';

	return true;





}}