<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Displaypricetype_form extends WForms_standard {












function show() {



	$isDonation = false;

	$pricetype = $this->getValue( 'type', 'product.price' );

	$curid = $this->getValue( 'curid' );

	$price = $this->getValue( 'price' );

	$vendid = $this->getValue( 'vendid' );

	$curidToShow = $curid;



	if ( $pricetype != 110 ) return false;

	

	WPref::load( 'PCURRENCY_NODE_MULTICUR' );

	$useMultiCurrency = PCURRENCY_NODE_MULTICUR;

	

	if ( ( WGlobals::checkCandy(50) ) && $useMultiCurrency ) {

		if ( !defined( 'PPRODUCT_NODE_SHOWNPRICEFORMAT' ) ) WPref::get( 'product.node' );

		if ( !defined( 'PPRODUCT_NODE_SHOWNPRICECOLOR' ) ) WPref::get( 'product.node' );

		$color = PPRODUCT_NODE_SHOWNPRICECOLOR;

		if ( empty($color) ) $color = 'blue';

	

		if ( !defined('CURRENCY_USED') ) {

			$currencyFormatC = WClass::get('currency.format',null,'class',false);

			$currencyFormatC->set();

		}
		

		$curidToShow = CURRENCY_USED;

	}
		

	static $prodPricesC = null;

	if ( empty( $prodPricesC ) ) $prodPricesC = WClass::get( 'product.prices', null, 'class', false );

	if ( !$prodPricesC ) $display = TR1299679326RQFI;

	else $display = $prodPricesC->showBooking( $price, $curid, $curidToShow, $vendid, $this->modelID );



	$this->content = $display;

	return true;



}}