<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Address_form extends WForms_standard {
function create() {

	if ( empty($this->value) ) return false;

	

	$addressHelperC = WClass::get( 'address.helper' );

	$this->content = $addressHelperC->renderAddress( $this->value );


	return true;

}

function show() {
	
	return $this->create();
}
}