<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_Enddate_form extends WForms_standard {


function show()

{

	if ($this->value > 0) $this->content = WApplication::date( JOOBI_DATE3, $this->value );

	else $this->content = '<span style="color:red;">'. TR1237967968ABUO .'</span>';

	return true;

}

}