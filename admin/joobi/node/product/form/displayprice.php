<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Displayprice_form extends WForms_standard {












function show() {



	$hasRole = WGlobals::get( 'rolidViewPrice', true, 'joobi' );

	if ( !$hasRole ) return true;



	$isDonation = false;



	$pricetype = $this->getValue( 'type', 'product.price' );

	$curid = $this->getValue( 'curid' );

	$price = $this->value;

	$vendid = $this->getValue( 'vendid' );

	$usedCurrency = WUser::get('curid');

	
	if ( $pricetype == 17 ) {

		$shoppersPricesC = WClass::get( 'shoppers.prices', null, 'class', false );

		if ( !empty($shoppersPricesC) ) {

			$myGroupPrice = $shoppersPricesC->getPrice( $this->getValue( 'pid' ), $vendid );

			if ( $myGroupPrice !==false ) $price = $myGroupPrice;

			else return false;

		}
	}


	if ( WGlobals::checkCandy(25) ) {


		
		$useTax = WPref::load( 'PCATALOG_NODE_USETAX' );
		if ( $useTax ) {

			$productTaxC = WObject::get( 'product.tax' );

						$pricetaxtype = $productTaxC->definePriceTaxType( $this->getValue( 'pricetaxtype' ) );

						$productTaxC = WObject::get( 'product.tax' );
			$productTaxC->setVendorID( $productTaxC->getVendorID( $vendid ) );
			$productTaxC->setPrice( $price, $pricetaxtype, $this->getValue( 'prodtypid' ) );
			$price = $productTaxC->renderPrice();

		}


	}


	if ( empty( $price ) || $price <= 0 ) {

		if ( ( WGlobals::checkCandy(50) ) && ( $pricetype == 20 ) ) $isDonation = true;

		else return false;

	} else {

		if ( $usedCurrency != $curid ) {

			
			$currencyConvertC = WClass::get('currency.convert',null,'class',false );

			$convertedPrice = $currencyConvertC->currencyConvert( $curid, $usedCurrency, $price );

		} else {

			$convertedPrice = $price;

		}


    	$curencySymbol = WPref::load( 'PCURRENCY_NODE_CODESYMBOL' );
		$withSymbol = ( $curencySymbol == 'symbol' ? 'money' : 'moneyNoSymbol' );
		$price = WTools::format( $convertedPrice, $withSymbol, $usedCurrency );



		if ( $pricetype != 20 ) $isDonation = true;



	}


	$form = WView::form( $this->formName );

	$nameTrans = 'name_' . WModel::get( 'producttrans', 'sid' );

	$form->hidden( 'trucs['.$this->modelID.'][name]', $this->data->$nameTrans );



	switch ( $pricetype ) {

		case 20 :	
				static $prodPricesC = null;

				if ( empty( $prodPricesC ) ) $prodPricesC = WClass::get( 'product.prices', null, 'class', false );

				if ( !$prodPricesC ) $display = TR1299587248PEFO;

				else $display = $prodPricesC->showDonation( $this->value, $curid, $this->modelID );

				break;

		break;

















		default : 	
				
				$form->hidden( 'trucs['.$this->modelID.'][price]', $this->value );		
				$display = '<div class="productPrice">' . $price . '</div>';



				if ( WGlobals::checkCandy(25) ) {

					
					
					if ( PCATALOG_NODE_USETAX && PCATALOG_NODE_SHOWTAX ) {

						$display .= '<div class="productTax">' . $productTaxC->renderTaxMessage( $usedCurrency, true, $curid ) . '</div>';

					}
				}


				break;

	}


	$this->content = $display;



	return true;



}}