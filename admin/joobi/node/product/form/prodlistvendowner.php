<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Prodlistvendowner_form extends WForms_standard {
function create() {

	$storeName = $this->getValue( 'name', 'vendor' );

	if ( !empty( $storeName ) ) $display = $storeName;

	else {

		$vendUID = $this->getValue( 'uid', 'vendor' );

		$display = WUser::get( 'name', $vendUID );

	}


	$this->content = $display;

	return true;

}}