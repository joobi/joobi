<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_Rafflewinnerbe_form extends WForms_standard {




function create()

{

	
	$pid = WGlobals::getEID();

	

	
	static $prodRafC=null;

	if( !isset($prodRafC) ) $prodRafC = WClass::get( 'product.raffle', null, 'class', false );

	if( !empty($prodRafC) )

	{ 

		static $rafid=null;

		if( !isset($rafid) ) $rafid = $prodRafC->getRaffleID( $pid );

	

		
		static $prodRafMemM=null;

		if( !isset($prodRafMemM) ) $prodRafMemM = WModel::get( 'product.rafflemembers' );	

		$prodRafMemM->whereE( 'rafid', $rafid );

		$prodRafMemM->where( 'uid', '>', 0 );

		$rafflesA = $prodRafMemM->load( 'lra', 'rafno' );

		

		if( !empty($rafflesA) )

		{

			
			$random = array_rand( $rafflesA );

			$winNum = $rafflesA[$random];		

		

			
			$prodRafMemM->whereE( 'rafid', $rafid );

			$prodRafMemM->whereE( 'rafno', $winNum );

			$uid = $prodRafMemM->load( 'lr', 'uid' );

			

			

			$display =  TR1254294896LMIR .' #'. $winNum;

			$display .= '<br><br>'. TR1254294896LMIS;

			$display .= '<br>'. TR1206732392OZVB .': '. WUser::get( 'name', $uid );

			$display .= '<br>'. TR1206732411EGRU .': '. WUser::get( 'email', $uid );

		} 

		else $display = TR1254294896LMIT;

	}

	else

	{

		$message = WMessage::get();

		$message->adminW( 'Error in using the draw function for raffles. Class product.raffle does not exist, trace controller product-rafflemembers.draw' );

		return true;

	}
	

	

	$this->content = $display;

	return true;

}}