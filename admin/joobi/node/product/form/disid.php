<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;






class Product_Disid_form extends WForms_standard {


function create()

{

	$disid = WGlobals::get( 'disid' );

	WGlobals::set( 'disid', $disid);

	$this->content = $disid;

	return true;

}

}