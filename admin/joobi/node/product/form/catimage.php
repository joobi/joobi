<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Product_Catimage_form extends WForms_standard {












function show() {

	if (!defined(PPRODUCT_NODE_PRODUCTSEARCH))WPref::get('product.node');

	if ('PPRODUCT_NODE_PRODUCTSEARCH') {

		$displayCSS = '.search-display{display:none}';

		WPage::addCSSScript ($displayCSS );

	}	

	$catid = WGlobals::get('catid');		




	
	$catM = WModel:: get ('product.category');

	$catM->select('filid');

	$catM->whereE('catid' , $catid);

	$filid = $catM->load ('lr');
	
		$imageM= WModel::get('files');
	$imageM->whereE( 'name', 'categoryx' );
	$defaultImage = $imageM->load( 'o', array('path', 'type', 'name', 'width', 'height') );



	
	
	if (!empty($filid)) {

		$img=$imageM->load($filid, array('path', 'type', 'name', 'width', 'height') );





		
		$path = $imageM->convertPath($img->path);



		
		if(file_exists(JOOBI_DS_MEDIA.$path.DS.$img->name.'.'.$img->type)) {



			$url = JOOBI_URL_MEDIA .$path. '/' .$img->name.'.'.$img->type;



		}
	}	
	if (!isset( $url ) ){

		$path = $imageM->convertPath( $defaultImage->path );

		$img->name = ( !empty( $defaultImage ) )? $defaultImage->name : 'categoryx';

		 $url= JOOBI_DS_MEDIA .$path. '/' .$defaultImage->name.'.'.$defaultImage->type;



	}


	
	$this->content = '<div class="catmain-image">';

	$this->content .= '<img src="'.$url.'" alt="'.$img->name.'">';

	$this->content .= '</div>';

	return true;

}}