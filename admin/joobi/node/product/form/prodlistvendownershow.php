<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Prodlistvendownershow_form extends WForms_standard {
function show() {

	$storeName = $this->getValue( 'name', 'vendor' );

	if ( !empty( $storeName ) ) $display = $storeName;

	else {

		$vendUID = $this->getValue( 'uid', 'vendor' );

		$display = WUser::get( 'name', $vendUID );

	}


	$vendid = $this->getValue( 'vendid' );

	$link = 'controller=vendors&task=home&task=show&eid='. $vendid ;

	$this->content = WPage::createPopUpLink( WPage::linkPopUp( $link ), $display, 1000, 600 );

	return true;

}}