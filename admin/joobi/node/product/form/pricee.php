<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_Pricee_form extends WForms_standard {




function show()

{

	if ( $this->value > 0 )

	{	

		static $curid=null;

		if( !isset($curid) ) $curid = 'curid_'. $this->modelID;

		$this->content = '<b>'. WTools::format( $this->value, 'money', $this->data->$curid ) .'</b>';		

	}

	else

	{

		$this->content = '<b>'. TR1206961944PEUR .'</b>';

	}
	return true;

}}