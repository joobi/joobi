<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Quantity_form extends WForms_standard {


function show() {



	$itemShowQuantity = WGlobals::get( 'itemShowQuantity', 0, 'joobi' );

	if ( !$itemShowQuantity ) return false;





	$hasRole = WGlobals::get( 'rolidViewPrice', true, 'joobi' );

	if ( !$hasRole ) return true;



	$price = $this->getValue( 'price' );

	$availability = WGlobals::get( 'productAvailable', null, 'joobi' );

	$vendid = $this->getValue( 'vendid' );



	
	if ( $this->getValue( 'type', 'product.price' ) == 17 ) {

		$shoppersPricesC = WClass::get( 'shoppers.prices', null, 'class', false );

		if ( !empty($shoppersPricesC) ) {

			$myGroupPrice = $shoppersPricesC->getPrice( $this->getValue( 'pid' ), $vendid );

			if ( $myGroupPrice !==false ) $price = $myGroupPrice;

			else return false;

		}
	}


	if ( ( empty( $price ) || $price <= 0 ) || ( $availability == 'unavail' || $availability == 'expired' ) ) return false;

	else {

		$display = '';



		if ( WPage::browser( 'namekey' ) == 'safari' ) {

			$pageTheme = WPage::theme();

			$widCat = WExtension::get( 'catalog.node', 'wid' );

			$folderCat = $pageTheme->getFolder( $widCat, 49 );

			WPage::addCSSFile( JOOBI_URL_THEME . $folderCat . '/css/safari.css' );

		}


		$display .= '<input id="catalogquantity" type="textbox" name="trucs['. $this->modelID .'][quantity]" value="1" size="3" maxlength="4" style="text-align:center;">';	
		$display .= '<div id="quantityarrow"><span id="addquantity" class="jpng-16-uparrow"><img src="'.JOOBI_URL_JOOBI_IMAGES.'listings/up-arrow-green.png" width="16" height="16" border="0" alt="Add" /></span>';

		$display .= '<span id="minusquantity" class="jpng-16-downarrow" style="display:none;"><img src="'.JOOBI_URL_JOOBI_IMAGES.'listings/down-arrow-green.png" width="16" height="16" border="0" alt="Minus" /></span>';

		$display .= '<span id="minusgray" class="jpng-16-downarrow-gray" style="display:blovk;"><img src="'.JOOBI_URL_JOOBI_IMAGES.'listings/down-arrow-gray.png" width="16" height="16" border="0" alt="Minus" /></span></div>';



		$taxInfo='';



		
		$discountRate = $this->getValue( 'discountrate' );

		$discountValue = $this->getValue( 'discountvalue' );

		if ( ( $discountRate > 0 || $discountValue > 0 ) && $price > 0 ) {

			if ( $discountRate > 0 ) {

				$price = $price * ( 100 - $discountRate ) / 100;

			}
			if ( $discountValue > 0 ) {

				$price = $price - $discountValue;

				if ( $price < 0 ) $price = 0;

			}


		}




		
		if ( WGlobals::checkCandy(25) ) {

			
			WPref::load( 'PCATALOG_NODE_USETAX' );

			
			$productTaxC = WObject::get( 'product.tax' );
			$pricetaxtype = $productTaxC->definePriceTaxType( $this->getValue( 'pricetaxtype' ) );


			$productTaxC->setVendorID( $productTaxC->getVendorID( $this->getValue('vendid') ) );

			$productTaxC->setPrice( $price, $pricetaxtype, $this->getValue( 'prodtypid' ) );

			$price = $productTaxC->renderPrice();


			$usedCurrency = WUser::get('curid');

			if ( PCATALOG_NODE_USETAX && PCATALOG_NODE_SHOWTAX ) {

				$taxInfo = $productTaxC->renderTaxMessage( $usedCurrency );

				$totalTaxOneProduct = round( $productTaxC->getTax(), 2 );

				$totalTaxOneProduct = WTools::format( $totalTaxOneProduct, 'price', $usedCurrency );

			}


		}




		$usedCurrency = WUser::get('curid');

		$curid = $this->getValue('curid');



		if ( $usedCurrency != $curid ) {

			
			$currencyConvertC = WClass::get('currency.convert',null,'class',false );

			$price = $currencyConvertC->currencyConvert( $curid, $usedCurrency, $price );

		}


		$currencySymbol =  WTools::format( '1', 'money', $usedCurrency );	
		$currencyDec =  WTools::format( '1', 'price', $usedCurrency );	


		$jsPrice = $price;

		$price = WTools::format( $price, 'price', $usedCurrency );



		$this->content = $display;



		$js = '$(\'catalogquantity\').addEvent(\'blur\', function() {

quantity = $(\'catalogquantity\').value;

changetotal(quantity);

});



$(\'addquantity\').addEvent(\'click\', function() {

	$(\'minusquantity\').setStyle(\'display\', \'block\');

	$(\'minusgray\').setStyle(\'display\', \'none\');

	quantity = $(\'catalogquantity\').value;

	newquantity = parseInt(quantity)+1;

	changequantity(newquantity);

});

$(\'minusquantity\').addEvent(\'click\', function() {

	quantity = $(\'catalogquantity\').value;

	newquantity = parseInt(quantity)-1;

	if (newquantity <= 1) {

		newquantity=1;

		this.setStyle(\'display\', \'none\');

	}

	changequantity(newquantity);

});



function changequantity(quantity) {

$(\'catalogquantity\').value = quantity;

changetotal(quantity);

}



function changetotal(quantity) {



if (isNaN(quantity) || quantity<=1) {

quantity = 1;

$(\'catalogquantity\').value = 1;

$(\'minusquantity\').setStyle(\'display\', \'none\');

$(\'minusgray\').setStyle(\'display\', \'block\');

} else {

$(\'minusquantity\').setStyle(\'display\', \'block\');

$(\'minusgray\').setStyle(\'display\', \'none\');

}//endif

amount = parseFloat("'.$jsPrice.'");

newprice = amount * quantity;

newprice = newprice.toFixed(2);

currency = "'.$currencySymbol.'";

currenynewprice = currency.replace("'.$currencyDec .'", newprice );	';



if ( !empty($taxInfo) ) {

$js .= '

taxamount = parseFloat("'.$totalTaxOneProduct.'");

newtaxprice = taxamount * quantity;

newtaxprice = newtaxprice.toFixed(2);

currency = "'.$totalTaxOneProduct.'";

currenytaxnewprice = currency.replace("'.$totalTaxOneProduct .'", newtaxprice );

';

}


$js .= '$(\'productTotalValue\').innerHTML = currenynewprice ';



if ( !empty($taxInfo) ) {

$taxInfo = str_replace( $totalTaxOneProduct, "'+ currenytaxnewprice +'", $taxInfo );

$js .= '+ \'<div id="productTax">'.$taxInfo.'</div>\';';

}


$js .= '}//endfct

';





		WPage::addJSFile('mootools');

		WPage::addJSScript( $js );



		return true;

	}


}}