<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

WLoadFile( 'form.layout' , JOOBI_LIB_HTML );
class Product_Shoppersprice_form extends WForm_layout {
	function create() {

	


		if ( !defined('PVENDORS_NODE_SHOPPERS_SAME_PRICE') ) WPref::get( 'vendors.node' );
		$samePriceForAllProducts = PVENDORS_NODE_SHOPPERS_SAME_PRICE;
		if ( $samePriceForAllProducts ) return false;
		

		$pid = 	$this->getValue( 'pid');

		if ( empty($pid) ) {

			$popuplink = '#';

			$questionLink = '';

			$text = TR1339084816HSXP;
			$popup = false;

		} else {

			$popup = true;

			$questionLink =  WPage::linkPopUp('controller=shoppers-productprice&task=listing&pid='. $this->getValue( 'pid') . '&titleheader=' . $this->getValue( 'name') );

			$text = TR1338946660CHNQ;

		}
				

		$this->content = '<div class="bttnShoppers">';
		$this->content .= WPage::createPopUpLink( $questionLink, '<span class="smallShoppersRight"><span class="smallShoppersCenter">' . $text . '</span></span>', 550, 500, 'smallShoppersLeft', '', '', !$popup );		

		$this->content .= '</div>';	

		

			

		
				$eid = WGlobals::getEID();

		if ( empty($eid) ) {
						$pageTheme = WPage::theme();
			$folderCat = $pageTheme->getFolder( WExtension::get( 'shoppers.node', 'wid' ), 49 );
			WPage::addCSSFile( JOOBI_URL_THEME . $folderCat . '/css/style.css' );
					
			return true;	
		}		
		$this->content .= '<br>';

		return parent::create();

		

	}
	
}