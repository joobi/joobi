<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Cmtform_form extends WForms_standard {






function show() {

	if (! WUser::isRegistered()) {

		$controller = new stdClass;

		$controller->wid = WExtension::get( 'comment.node', 'wid' );

		$controller->level = 50;

		$controller->controller= 'comment';

		$controller->nestedView = true;

			
		if (!defined('PCOMMENT_NODE_CMTNONORREG')) WPref::get('comment.node');

		if (PCOMMENT_NODE_CMTNONORREG) {

			$viewC = WView::getHTML('post_comment_non-reg_fe', $controller);		
			$this->content = $viewC->make();

		} else {

			$message = WMessage::get();

			$MESSAGE = 'You need to log-in to Post a Comment';		
			$message->userW($MESSAGE);

			return '';

		}
	} else {

		parent::create();

	}


return true;

}}