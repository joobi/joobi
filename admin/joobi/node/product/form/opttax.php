<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

WLoadFile( 'form.select' , JOOBI_LIB_HTML );
class Product_Opttax_form extends WForm_select {
function create() {



	$opid = $this->getValue( 'opid' );

	if ( empty($opid) ) $opid = WGlobals::get( 'opid' );

	
	

	$productOptionC = WClass::get('product.option');

	$type = $productOptionC->getType( $opid );



	
	if ( 1 != $type ) return false;	


	return parent::create();



}}