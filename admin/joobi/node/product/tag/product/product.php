<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




 class Product_Product_tag {	 	


 	public $smartUpdate = false;

 	var $nodeName = 'product';






	function process($givenTagsA) {

		$replacedTagsA = array();
		$productLoadC = WClass::get( 'item.load' );
		$outputThemeC = WClass::get( 'output.theme' );
		$outputThemeC->nodeName = $this->nodeName;
		$outputThemeC->header = $productLoadC->setHeader();

		foreach( $givenTagsA as $tag => $myTagO ) {

						$productA = $productLoadC->get( $myTagO );
			
						if ( !empty($myTagO->countOnly) ) {
				$replacedTagsA[$tag] = $myTagO->totalCount;
				continue;
			}			
			if ( empty( $productA ) ) {
				$replacedTagsA[$tag] = '';
				continue;
			}
 			
			
			
									$productLoadC->extraProcess( $productA, $myTagO );

												
			$replacedTagsA[$tag] = $outputThemeC->createLayout( $productA, $myTagO );


		}
		return $replacedTagsA;

	}

}