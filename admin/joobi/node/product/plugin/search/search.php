<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;













class Product_Search_plugin extends WPlugin {

	


	function onSearchAreas() {
		static $areas = array();

		if (empty($areas)) $areas['products'] = TR1206961902CIFE;

		return $areas;
	}

	function onSearch($text,$phrase='',$ordering='',$areas=null) {

		if (is_array( $areas )) {
			if (!in_array('products' ,$areas )) {
				return array();
			}
		}

				$limit = ( isset($this->search_limit) ) ? $this->search_limit : 10;

				$itemId = ( isset($this->itemid) ) ? (int)$this->itemid : 1;

		$text = trim( $text );
		if ( $text == '' ) return array();

		switch ( $ordering ) {
			case 'alpha':
				$order['field'] = 'name';
				$order['ordering'] = 'ASC';
				$order['as'] = '1';
				break;
			case 'newest':
				$order['field'] = 'modified';
				$order['ordering'] = 'DESC';
				$order['as'] = '0';
				break;
			case 'oldest':
				$order['field'] = 'created';
				$order['ordering'] = 'ASC';
				$order['as'] = '0';
				break;
			case 'category':
			case 'popular':
			default:
				$order['field'] = 'name';
				$order['ordering'] = 'DESC';
				$order['as'] = '1';
				break;
		}
		
		$productnode=WModel::get('product');
		$productnode->select('pid');
		$productnode->select('created');
		$productnode->whereE('publish','1');
		$productnode->makeLJ('producttrans','pid');
		$productnode->select('name',1,'title');
		$productnode->select('description',1);
		$productnode->select('introduction',1);
		$productnode->where('name','LIKE','%'.$text.'%',1);
		$productnode->where('namekey','LIKE','%'.$text.'%',0,null,null,null,1);
		$productnode->where('description','LIKE','%'.$text.'%',1,null,null,null,1);
		$productnode->where('introduction','LIKE','%'.$text.'%',1,null,null,null,1);
		$productnode->orderBy( $order['field'], $order['ordering'], $order['as'] );
		$productnode->groupBy( 'pid' );
		$productnode->setLimit($limit);

				$storeLevel = WExtension::get( 'jstore.application', 'level', null, null, false );
		if ( $storeLevel >= 50) {
			$productnode->checkAccess();
		}
		$rows = $productnode->load('ol');

		$count = count( $rows );
		for ( $i = 0; $i < $count; $i++ ) {
			$rows[$i]->text = $rows[$i]->description.'  -  '.$rows[$i]->introduction;
			$rows[$i]->section = 'jStore Product';
			$rows[$i]->href = WPage::routeURL( 'controller=catalog&task=show&&eid='. $rows[$i]->pid, '', false, false, $itemId );
			$rows[$i]->browsernav=1;
		}

		
		$productcat=WModel::get('product.category');
		$productcat->select('namekey');
		$productcat->makeLJ('product.categorytrans','catid');
		$productcat->select('name',1,'title');
		$productcat->select('description',1,'text');
		$productcat->select('created');
		$productcat->select('catid');
		$productcat->where('name','LIKE','%'.$text.'%',1);
		$productcat->where('description','LIKE','%'.$text.'%',1,null,null,null,1);
		$productcat->whereE('publish','1');
		$productcat->orderBy( $order['field'], $order['ordering'], $order['as'] );
		$productcat->groupBy( 'catid' );
		$productcat->setLimit($limit);

				if ( $storeLevel >= 50) {
			$productcat->checkAccess();
		}			$rows2 = $productcat->load('ol');

		$count = count( $rows2 );
		for ( $i = 0; $i < $count; $i++ ) {
			$rows2[$i]->created = WApplication::date( JOOBI_DATE8, $rows2[$i]->created + WUser::timezone() );
			$rows2[$i]->section = 'jStore List';
			$rows2[$i]->href = WPage::routeURL( 'controller=item-category&task=show&catid='. $rows2[$i]->catid, '', false, false, $itemId );
			$rows2[$i]->browsernav=1;
			if ($rows2[$i]->namekey=='root') unset($rows2[$i]);
		}

		return array_merge($rows, $rows2);
	}
}