<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Node_install extends WInstall {

	public function install(&$object) {


		if ( $this->newInstall ) {

						$this->_insertDefProdType();

						$this->_insertDefaultProductPrice( $this->_listOfProductPrice() );


						$itemMenusC = WClass::get( 'item.menus' );
			$itemMenusC->updateUserMenus();

			$extensionM= WModel::get( 'scheduler' );
			$extensionM->setVal( 'publish', 0 );
			$extensionM->whereE( 'namekey', 'product.merchantfeed.scheduler' );
			$extensionM->update();

		}
				$pref = WPref::get('product.node');
		$option = $pref->getPref('option','');
		if ( empty($option)) {
			$extM = WModel::get('apps');
			$extM->whereIn('namekey', array( 'jstore.application', 'jsubscription.application' ) );
			$extM->whereE('publish',1);
			$ext = $extM->load('lr','namekey');
			if (!empty($ext)) {
				$parts = explode('.',$ext);
				$pref->updatePref('option',$parts[0]);
				$pref->updatePref('option',$parts[0],false,'premium');
			}		}
		return true;
	}







	public function addExtensions() {

		$extension = new stdClass;
		$extension->namekey = 'product.search.plugin';
		$extension->name = "Products search";
		$extension->folder = 'search';
		$extension->type = 50;
		$extension->publish = 1;
		$extension->certify = 1;
		$extension->destination = 'node|product|plugin';
		$extension->core = 1;
		$extension->params = 'publish=0';
		$extension->description = '';

		if ( $this->insertNewExtension( $extension ) ) $this->installExtension( $extension->namekey );

				$extension = new stdClass;
		$extension->namekey = 'product.product.module';
		$extension->name = "Products module";
		$extension->folder = 'product';
		$extension->type = 25;
		$extension->publish = 1;
		$extension->certify = 1;
		$extension->destination = 'node|product|module';
		$extension->core = 1;
		$extension->params = "publish=0\nwidgetView=product_product_module";
		$extension->description = '';
		$libraryCMSMenuC = WAddon::get( 'api.' . JOOBI_FRAMEWORK . '.cmsmenu' );
		$extension->install = $libraryCMSMenuC->modulePreferences();


		if ( $this->insertNewExtension( $extension ) ) $this->installExtension( $extension->namekey );


	}





	private function _insertDefProdType($extra='') {

		$listOfTypeA  = array();
				$typeO = new stdClass;
		$typeO->name = 'Product';
		$typeO->description = 'Product';
		$typeO->namekey = 'product' . $extra;
		$typeO->type = 1;
		$typeO->publish = 1;
		$typeO->params = 'pageprdshowimage=2
pageprdshowdefimg=2
pageprdshowpreview=2
pageprdshowintro=2
pageprdshowdesc=2
pageprdshowrating=2
pageprdshowreview=2
pageprdallowreview=2
pageprdvendor=2
pageprdvendorating=2
pageprdaskquestion=2
pageprdprice=1
pageprdquantity=1
pageprdstock=1
pageprdcartbtn=1
pageprdshowviews=2
pageprdshowlike=2
pageprdshowtweet=2
pageprdshowbuzz=2
pageprdshowsharethis=2
pageprdshowemail=2
pageprdshowemail=2
pageprdshowfavorite=2
pageprdshowwatch=2
pageprdshowwish=2
pageprdshowlikedislike=2
pageprdshowsharewall=2
pageprdshowprint=2
termslicense=general
termsshowlicense=2
termsrefundallowed=2
termsrefund=general
termsshowrefund=2
termsshowrefundperiod=2
requiretermsatcheckout=2
bdlitems=1
bdltitle=1
bdlsorting=newest
bdldisplay=horizontal
bdlnbdisplay=4
bdlshowname=1
bdlshowintro=1
bdlclimit=80
bdlshowprice=1
bdlshowfree=1
bdlshowrating=1
bdlfeedback=1
bdlshowimage=1
prditems=1
prdtitle=1
prdsorting=sold
prddisplay=horizontal
prdnbdisplay=4
prdshowname=1
prdshowintro=1
prdclimit=80
prdshowprice=1
prdshowrating=1
prdfeedback=1
prdshowimage=1
pageprdshowmap=2
pageprdshowmapstreet=2
allowresellers=23
allowsyndication=23';
		$listOfTypeA[] = $typeO;

		

	













		$prodTypeM = WModel::get( 'item.type' );
		$typeImage = 'product';
		foreach( $listOfTypeA as $oneType ) {

			$prodTypeM->whereE( 'namekey', $oneType->namekey );
			$exist = $prodTypeM->exist();
			if ( $exist ) continue;


			$prodTypeM->prodtypid = null;
						$prodTypeM->setChild( 'item.typetrans', 'name', $oneType->name );
			$prodTypeM->setChild( 'item.typetrans', 'description', $oneType->description );
			$prodTypeM->namekey = $oneType->namekey;
			$prodTypeM->type = $oneType->type;
			$prodTypeM->publish = $oneType->publish;
			$prodTypeM->params = $oneType->params;
			$prodTypeM->core = 1;
			$prodTypeM->vendid = 0;
			$prodTypeM->saveItemMoveFile( JOOBI_DS_NODE . $typeImage .DS. 'install' .DS. 'images' .DS. $typeImage . 'typex.png' );


		}
		return true;
	}

	private function _listOfProductPrice() {

				$roleC = WRole::get();
		$storeManagerRole = $roleC->getRole( 'storemanager' );

		$listOfTypeA  = array();

		$typeO = new stdClass;
		$typeO->name = 'Standard';
		$typeO->description = 'Standard Product Price';
		$typeO->namekey = 'standard';
		$typeO->type = 10;
		$typeO->ordering = 1;
		$typeO->publish = 1;
		$listOfTypeA[] = $typeO;

		$typeO = new stdClass;
		$typeO->name = 'Shoppers Group';
		$typeO->description = 'Define Product Price based on Shoppers Group';
		$typeO->namekey = 'shoppers';
		$typeO->rolid = $storeManagerRole;
		$typeO->type = 17;
		$typeO->ordering = 2;
		$typeO->publish = 1;
		$listOfTypeA[] = $typeO;

		$typeO = new stdClass;
		$typeO->name = 'Donation';
		$typeO->description = 'Donation';
		$typeO->namekey = 'donation';
		$typeO->rolid = $storeManagerRole;
		$typeO->type = 20;
		$typeO->ordering = 3;
		$typeO->publish = 1;
		$listOfTypeA[] = $typeO;

		$typeO = new stdClass;
		$typeO->name = 'Link';
		$typeO->description = 'Link to a URL to see the price';
		$typeO->namekey = 'contactus';
		$typeO->rolid = $storeManagerRole;
		$typeO->type = 40;
		$typeO->ordering = 4;
		$typeO->publish = 1;
		$listOfTypeA[] = $typeO;


		$typeO = new stdClass;
		$typeO->name = 'Contact Us';
		$typeO->description = 'Contact Us';
		$typeO->namekey = 'contactus';
		$typeO->rolid = $storeManagerRole;
		$typeO->type = 50;
		$typeO->ordering = 5;
		$typeO->publish = 1;
		$listOfTypeA[] = $typeO;

		$typeO = new stdClass;
		$typeO->name = 'Free';
		$typeO->description = 'Free product';
		$typeO->namekey = 'free';
		$typeO->rolid = $storeManagerRole;
		$typeO->type = 250;
		$typeO->ordering = 6;
		$typeO->publish = 0;
		$listOfTypeA[] = $typeO;

		
		return $listOfTypeA;

	}


	private function _insertDefaultProductPrice($listOfTypeA) {

		$prodTypeM = WModel::get( 'product.price' );
				$roleC = WRole::get();
		$vendorRole = $roleC->getRole( 'vendor' );
		$uid = WUser::get( 'uid' );
		foreach( $listOfTypeA as $oneType ) {
			$prodTypeM->priceid = null;
			$prodTypeM->setChild( 'product.pricetrans', 'name', $oneType->name );
			$prodTypeM->setChild( 'product.pricetrans', 'description', $oneType->description );
			$prodTypeM->namekey = $oneType->namekey;
			$prodTypeM->type = $oneType->type;
			$prodTypeM->ordering = $oneType->ordering;
			$prodTypeM->publish = $oneType->publish;
			$prodTypeM->rolid = ( !empty($oneType->rolid) ? $oneType->rolid : $vendorRole );
			$prodTypeM->core = 1;
			$prodTypeM->vendid = 0;
			$prodTypeM->uid = $uid;
			$prodTypeM->modifiedby = $uid;

			$prodTypeM->save();

		}
		return true;

	}




}