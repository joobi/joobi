<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

 defined('_JEXEC') or die;











class Product_Dropproduct_picklist extends WPicklist {












function create() {

	static $productsA = array();

	
	$pid = WGlobals::getEID();	


	if ( !isset($productsA[$pid]) ) {
		static $productM = null;

		if( !isset($productM) ) $productM = WModel::get('product');

		$productM->makeLJ('producttrans', 'pid', 'pid', 0, 1);
		$productM->select( 'pid' );

		$productM->orderBy( 'pid' );

		$productM->whereE( 'publish', 1 );

		$productM->where( 'type', '<', 240 );

		if( !empty($pid) ) $productM->where( 'pid', '!=', $pid );

		
		$productM->whereLanguage(1);

		$productM->orderBy('name','ASC',1);

		$productM->select('name', 1);
		$productM->setLimit( 10000 );
		$productsA[$pid] = $productM->load( 'ol' );

	}		

	if( !empty($productsA[$pid]) ) {

		foreach( $productsA[$pid] as $product ) $this->addElement($product->pid, $product->name);

	}


	return true;

}}