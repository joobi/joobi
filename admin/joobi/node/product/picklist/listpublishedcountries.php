<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Listpublishedcountries_picklist extends WPicklist {


function create() {

	static $countries=null;





	if ( empty($countries) ) {

		static $countriesM=null;

		if ( empty($countriesM) ) $countriesM = WModel::get( 'countries' );

		$countriesM->whereE( 'publish', 1 );

		$countriesM->orderBy( 'name' );

		$countries = $countriesM->load( 'ol', array( 'ctyid', 'name', 'isocode2' ) );

	}




	if ( !empty($countries) ) {

		$firstLetter = null;

		foreach( $countries as $country ) {

			$countryID = $country->ctyid;

			$countryName = $country->name;

			$countryCode = $country->isocode2;










			$iptrackerInstalled = WExtension::get( 'iptracker.node', 'wid', null, null, false );



			if ( $iptrackerInstalled  && WGlobals::checkCandy(50) ) {

				
				$flagPath = JOOBI_URL_MEDIA .'images/flags/'. strtolower( $countryCode ).'.gif';



				
				$this->addElement( $countryID, $countryName, 'style="background: url('. $flagPath .') no-repeat scroll 0% 0% transparent;text-indent:5px;margin-top:5px;margin-bottom:5px;padding-left:20px;"' );

			} else {

				$this->addElement( $countryID, $countryName );

			}


		}


		return true;

	} else return false;



}}