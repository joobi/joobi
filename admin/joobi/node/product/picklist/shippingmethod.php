<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Shippingmethod_picklist extends WPicklist {
function create() {


	if ( !defined( 'PPRODUCT_NODE_AVAILABLESHIPPINGMETHOD' ) ) WPref::get( 'product.node' );
	$allAvaillableA = explode( ',', PPRODUCT_NODE_AVAILABLESHIPPINGMETHOD );
	
	$availableMethodA = array();
	$availableMethodA['1'] = TR1328998982ELSP;		$availableMethodA['9'] = TR1328998982ELSQ;
	$availableMethodA['3'] = TR1328998982ELSR;

	$availableMethodA['7'] = TR1328998982ELSS;


	if ( count($allAvaillableA) <= 1 ) return false;
	
	foreach( $allAvaillableA as $oneMothod ) {
		$this->addElement( $oneMothod, $availableMethodA[$oneMothod] );
	}		

	return true;



}}