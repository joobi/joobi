<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Pricelist_picklist extends WPicklist {


function create() {



	
	
	static $vendorHelperC = null;

	if ( empty( $vendorHelperC ) ) $vendorHelperC = WClass::get( 'vendor.helper', null, 'class', false );

	$vendid = ( $vendorHelperC ) ? $vendorHelperC->traceVendor() : 0;



	$priceTypeA = array();

	static $prodPriceM = null;

		if ( empty( $prodPriceM ) ) $prodPriceM = WModel::get( 'product.price' );



	if ( !isset( $priceTypeA[$vendid] ) ) {

		
		$prodPriceM->makeLJ( 'product.pricetrans', 'priceid' );

		$prodPriceM->whereLanguage(1);


		$prodPriceM->select( 'name', 1 );

		$prodPriceM->indexResult( 'priceid' );



		if ( $this->onlyOneValue() && !empty($this->defaultValue) ) {

			$prodPriceM->whereE( 'priceid', $this->defaultValue );

			$priceTypeA[$vendid] = $prodPriceM->load( 'lra' );



		} else {

			$prodPriceM->whereE( 'publish', 1 );

			$prodPriceM->orderBy( 'ordering', 'ASC' );

			$prodPriceM->checkAccess();

			$prodPriceM->setLimit( 200 );










			$priceTypeA[$vendid] = $prodPriceM->load( 'lra' );

		}


	}


	
	if ( !empty( $priceTypeA[$vendid] ) ) {

		foreach( $priceTypeA[$vendid] as $key => $nameType ) {

			$this->addElement( $key, $nameType );

		}


		return true;

	} else {

		
		$message = WMessage::get();

		$message->userE('1298350868MINV');

		return false;

	}


}



}