<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Optionstype_picklist extends WPicklist {
	
	function create() {

		$this->addElement( 1, TR1337276592OOSO );

		$this->addElement( 2, TR1337276592OOSP );
		return true;
	}
 
}