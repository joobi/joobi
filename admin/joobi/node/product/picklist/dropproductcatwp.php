<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;














 class Product_dropproductcatwp_picklist extends WPicklist {






	function create() {
		$sql = WModel::get( 'product.category' );
		$eid = 	WGlobals::getEID();
		$sql->select('alias');
		$sql->select('catid');
		$sql->orderBy('alias');
		$sql->whereE( 'publish' , 1 );
		if ( $eid>0 ) $sql->where( $sql->_pkey, '!=', $eid ); 		$sql->setLimit( 5000 );
		
		$categories = $sql->load('ol');
		if ( !empty($categories) ) {
			foreach ($categories as $category)  {
				$this->addElement( $category->catid , $category->alias);
			}		}
   }
 }