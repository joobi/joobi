<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Dropalloptions_picklist extends WPicklist {












function create() {



	$itemType = WGlobals::get( 'itemtype', null, 'joobi' );

	$option = WGlobals::get( 'option' );

	if ( empty( $itemType ) && $option == 'com_jsubscription' ) $itemType = 5;	


	if ( !IS_ADMIN ) {

		$uid = WUser::get('uid');



		static $vendidA = null;

		
		if ( !isset( $vendidA[ $uid ] ) && !empty( $uid ) ) {

			$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

			$vendidA[ $uid ] = $vendorHelperC->getVendorID( $uid );

		}


		$vendid = isset( $vendidA[ $uid ] ) ? $vendidA[ $uid ] : 0;

	} else $vendid = 0;



	static $options = null;

	if ( empty( $options ) ) {

		static $optionM = null;

		if ( empty( $optionM ) ) $poptionM = WModel::get( 'product.option' );



		$poptionM->select( 'opid');

		$poptionM->checkAccess();

		$poptionM->whereE( 'publish', 1 );

		if ( !empty( $itemType ) ) $poptionM->whereE( 'opttype', $itemType );


		if ( !IS_ADMIN && !empty( $vendid ) ) {
			$poptionM->openBracket();
			$poptionM->whereE( 'vendid', $vendid );
			$poptionM->operator();
			$poptionM->whereE( 'share', 1 );
			$poptionM->closeBracket();
		}

		$poptionM->select( 'alias', 0, 'name' );



		$poptionM->orderBy( 'ordering', 'ASC' );

		$poptionM->orderBy( 'alias', 'ASC' );



		$poptionM->groupBy( 'opid' );

		$poptionM->setLimit( 5000 );



		$options = $poptionM->load( 'ol' );



	}


	if ( !empty($options) ) {

		$firstLetter = null;

		foreach($options as $option) {

			$optionID = $option->opid;

			$optionName = $option->name;









			$this->addElement( $optionID, $optionName );

		}


		return true;

	} else {

		return true;

	}


}














function show(&$drop,&$params) {

	return $this->create($drop, $params);



}}