<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Product_Pricetype_picklist extends WPicklist {










	function create() {


		$all = ( !$this->onlyOneValue() ? true : false );


		if ( $this->defaultValue == 10 || $all ) $this->addElement( 10, TR1206961911NYAN );

		if ( $this->defaultValue == 17 || $all ) $this->addElement( 17, TR1338923272FYWU );

		if ( $this->defaultValue == 12 || $all ) $this->addElement( 20, TR1338946669BDIN );

		if ( $this->defaultValue == 40 || $all ) $this->addElement( 40, TR1206961869IGNO );		
		if ( $this->defaultValue == 50 || $all ) $this->addElement( 50, TR1338946669BDIO );





		if ( $this->defaultValue == 250 || $all ) $this->addElement( 250, TR1206961944PEUR );



		return true;



	}

}