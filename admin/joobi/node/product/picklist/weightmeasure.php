<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

 defined('_JEXEC') or die;











class Product_Weightmeasure_picklist extends WPicklist {



function create() {

	$this->addElement( 1, 'Grams (g)' );

	$this->addElement( 2, 'Kilograms (kg)' );

	return true;

}}