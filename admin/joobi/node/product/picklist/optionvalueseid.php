<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;






class Product_Optionvalueseid_picklist extends WPicklist {












	function create() { 

		$opid = WGlobals::get('opid', null, 'joobi');			

		if(!empty($opid))

		{

			static $optionValuesM = null;

			if( empty($optionValuesM) ) $optionValuesM = WModel::get( 'product.optionvalues' );	

			$optionValuesM->whereE( 'opid', $opid );
			$optionValuesM->setLimit( 5000 );
			

			$optionValues = $optionValuesM->load( 'lra', 'opvid' );

				

			if( isset($optionValuesM) && !empty($optionValues) )

			{

				static $optionValuesTransM = null;

				if(empty($optionValuesTransM)) $optionValuesTransM = WModel::get( 'product.optionvaluestrans' );

				

				foreach( $optionValues as $key=>$optionValue )

				{

					$optionValuesTransM->whereE( 'opvid', $optionValue);

					$optionValuesTrans = $optionValuesTransM->load( 'r', 'name');

					

					if( !empty($optionValuesTrans)) $this->addElement($key, $optionValuesTrans );

				}
			}

			else

			{

				 $this->addElement(0, 'None' );

			}
		}
   }











































































}