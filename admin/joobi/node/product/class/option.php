<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Option_class extends WClasses {












	function getType($opid) {

		static $opidA = array();
		if ( empty($opid) ) return 0;

		if ( isset($opidA[$opid]) ) return $opidA[$opid];



		$productOptionM = WModel::get('product.option');
		$productOptionM->whereE( 'opid', $opid );

		$type = $productOptionM->load( 'lr', 'type' );



		$opidA[$opid] = $type;
		

		return $type;

	}
	






	function getOptionType($opid) {
		static $opidA = array();
		if ( empty($opid) ) return 0;
		if ( isset($opidA[$opid]) ) return $opidA[$opid];

		$productOptionM = WModel::get('product.option');
		$productOptionM->whereE( 'opid', $opid );
		$type = $productOptionM->load( 'lr', 'opttype' );

		$opidA[$opid] = $type;
		
		return $type;
	}
	
}