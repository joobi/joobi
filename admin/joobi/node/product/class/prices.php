<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Prices_class extends WClasses {











	public function getReserved($pids) {



		if ( empty($pids) ) return false;



		$productM = WModel::get('product');

		if ( is_array($pids) ) {

			$productM->whereIn( 'pid', $pids );

		} else {

			$productM->whereE( 'pid', $pids );

		}

		$prices = $productM->load('ol', array('price', 'curid', 'pid'));



		if ( empty($prices) ) return null;



		
		if ( !isset($curid) ) {

			if ( !defined('CURRENCY_USED') ) {

				$currencyFormatC = WClass::get('currency.format',null,'class',false);

				$currencyFormatC->set();

			}
			$curid = CURRENCY_USED ;

		}









		$arrayP = array();


		foreach( $prices as $price ) {

			if ( $curid!=$price->price ) {

				
				$myPrice = $price->price;

			} else {

				$myPrice = $price->price;

			}


			$arrayP[$price->pid] = WTools::format( $myPrice, 'price', $curid );

		}


		return $arrayP;



	}












	function priceChange($price) {

		$wholeNumber = floor( $price );

		$roundPrice = round( $price );

		$decimal = $price - $wholeNumber;



		if ( !defined( 'PPRODUCT_NODE_DEFPRICE' ) ) WPref::get( 'product.node' );

		if ( !defined( 'PPRODUCT_NODE_PRICELOWER' ) ) WPref::get( 'product.node' );

		if ( !defined( 'PPRODUCT_NODE_PRICEUPPER' ) ) WPref::get( 'product.node' );



		if ( $price == $wholeNumber  )

		{

			
			$returnPrice = ($wholeNumber - 1) + ( PPRODUCT_NODE_DEFPRICE / 100 );

		}

		else

		{

			
			$decimal *= 10;

			$roundDecimal = round( $decimal );

			$usedDecimal = floor($decimal);



			if ( $decimal >= $roundDecimal )

			{

				$returnPrice = $wholeNumber .'.'. $usedDecimal . PPRODUCT_NODE_PRICELOWER;

			}

			else

			{

				$returnPrice = $wholeNumber .'.'. $usedDecimal . PPRODUCT_NODE_PRICEUPPER;

			}
		}


		return (real)$returnPrice;

	}




function showPriceHTML($priceObj) {

	
	if ( empty( $priceObj ) ) return null;


	$pid = $priceObj->pid;

	$priceType = isset( $priceObj->priceType ) ? $priceObj->priceType : 10;	
	$price = $priceObj->price;

	$curidFrom = $priceObj->curidFrom;

	$curidTo = $priceObj->curidTo;

	$vendid = $priceObj->vendid;

	$link = $priceObj->link;

	$modelID = $priceObj->modelID;

	$HTML = '';


	switch ( $priceType ) {


		case 17 : 				static $shoppersPricesC=null;
			if (!isset($shoppersPricesC) ) $shoppersPricesC = WClass::get( 'shoppers.prices', null, 'class', false );
			if ( !empty($shoppersPricesC) ) {
				$myGroupPrice = $shoppersPricesC->getPrice( $pid, $vendid );
				if ( $myGroupPrice !==false ) $HTML = $myGroupPrice;
			}			break;

		case 20 : 	
			$HTML = $this->showDonation( $price, $curidFrom, $modelID );

			break;

		case 40 :				$text = TR1346104212BDFR;
			$HTML = '<a href="'. $link .'" target="_blank">' . $text . '</a>';
			break;
		case 50 :	
			$HTML = $this->showContactUs( $vendid );

			break;

		case 110 :	
			$HTML = $this->showBooking( $price, $curidFrom, $curidTo, $vendid, $modelID );

			break;

		case 120 :	
			break;

		case 250 :	
			break;
		default:
		case 10 :							if ( $curidFrom != $curidTo ) {
			    static $currencyC = null;
				if( empty($currencyC) ) $currencyC = WClass::get( 'currency.convert',null,'class',false);
			    $convertedPrice = $currencyC->currencyConvert( $curidFrom, $curidTo, $price );
			} else {
				$convertedPrice = $price;
			}
			$HTML = WTools::format( $convertedPrice, 'money', $curidTo );
			break;

	}


	return $HTML;

}
















function showDonation($price,$curid,$modelID) {

	
	if ( empty( $price ) || empty( $curid ) || empty( $modelID ) ) return null;


	static $curFormatC=null;

	if ( !isset($curFormatC) ) $curFormatC = WClass::get( 'currency.format' );

	$curObj = $curFormatC->load( $curid );








	$HTML = '<input name="trucs['.$modelID.'][price]" type="text" value="'. $price .'" size="8" maxlength="9" style="text-align:right;"> '. $curObj->symbol;




	return $HTML;

}














function showContactUs($vendid) {

	
	if ( empty( $vendid ) ) return null;


	$catalogItemPageC = WClass::get( 'catalog.itempage' );
	return $catalogItemPageC->askQuestion( $vendid, 'contactus4price' );






}
















function showBooking($price,$curidFrom,$curidTo,$vendid,$modelID) {

	if ( empty( $price ) || empty( $curidFrom ) || empty( $curidTo ) || empty( $vendid ) ) return null;



	$bookingFee = $this->processBooking( $price, $vendid );



	$HTML = '<input type="hidden" name="trucs['.$modelID.'][bookingfee]" value="'. $bookingFee .'">';

	$HTML .= $this->_convertValue( $bookingFee, $curidFrom, $curidTo );

	return $HTML;

}
















function processBooking($price,$vendid) {

	
	if ( empty( $price ) || empty( $vendid ) ) return 0;



	
	static $resultA = array();

	if ( !isset( $resultA[ $vendid ] ) ) {

		static $affTypeC = null;

		if ( empty( $affTypeC ) ) $affTypeC = WClass::get( 'affiliate.type', null, 'class', false );

		if ( !$affTypeC ) return 0;

		$resultA[ $vendid ] = $affTypeC->getFee( $vendid );

	}


	
	
	if ( !isset( $resultA[ $vendid ] ) || empty( $resultA[ $vendid ] ) ) return 0;

	else $bookingObj = $resultA[ $vendid ];



	
	if ( is_array( $bookingObj ) ) {

		$computedPrice = $price;

		$total = 0;

		foreach( $bookingObj as $bookingO ) {

			$percent = isset( $bookingO->percent ) ? $bookingO->percent : 0;

			$fixed = isset( $bookingO->price ) ? $bookingO->price : 0;



			if ( !empty( $percent ) ) $totalpercent = $computedPrice * ( $percent / 100 );

			else $totalpercent = 0;

			$total += $totalpercent + $fixed;	
			$computedPrice += $total;		
		}
	} else {

		$percent = isset( $bookingObj->percent ) ? $bookingObj->percent : 0;

		$fixed = isset( $bookingObj->price ) ? $bookingObj->price : 0;



		if ( !empty( $percent ) ) $totalpercent = $total * ( $percent / 100 );

		else $totalpercent = 0;

		$total = $totalpercent + $fixed;	
	}


	return $total;

}


















private function _convertValue($amount,$curidFrom,$curidTo) {


	WPref::load( 'PCURRENCY_NODE_MULTICUR' );

	if ( ( WGlobals::checkCandy(50) ) && PCURRENCY_NODE_MULTICUR ) {

		static $currencyC=null;

		if ( !isset($currencyC) ) $currencyC = WClass::get( 'currency.convert' );



		$object = new stdClass;

		$object->currencyFrom = $curidFrom;

		$object->currencyTo = $curidTo;

		$object->amount = $amount;

		$object->formatAmount = true;

		return $currencyC->convertTo( $object );

	} else {
		$curencySymbol = WPref::load( 'PCURRENCY_NODE_CODESYMBOL' );
		$withSymbol = ( $curencySymbol == 'symbol' ? 'money' : 'moneyNoSymbol' );
		$amountR = WTools::format( $amount, $withSymbol, $curidFrom );
		return $amountR;
	}

}}