<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Product_Shippingoption_class extends WClasses {

	private $_pid = null;
	private $_shippingprice = null;
	private $_curid = null;
	private $_optionNamekey = null;
	private $_vendid = null;

	private $_productOptionM = null;






	public function deleteShippingOption($pid) {

		if ( empty($pid) ) return false;
		$this->_pid = $pid;

				$this->_optionNamekey = 'sshipx_pidz_' . $this->_pid;

				$this->_productOptionM = WModel::get( 'product.option' );
		$this->_productOptionM->whereE( 'namekey', $this->_optionNamekey );
		$opid = $this->_productOptionM->load( 'lr', 'opid' );

		if ( empty($opid) ) return true;


		$this->_productOptionM->whereE( 'opid', $opid );
		$this->_productOptionM->deleteAll();

		$productPptionvaluesM = WModel::get( 'product.optionvalues' );
		$productPptionvaluesM->whereE( 'opid', $opid );
		$productPptionvaluesM->deleteAll();

		$productPoptionsM = WModel::get( 'product.poptions' );
		$productPoptionsM->whereE( 'opid', $opid );
		$productPoptionsM->whereE( 'pid', $pid );
		$productPoptionsM->deleteAll();

	}










	public function createShippingOption($pid,$shippingprice,$curid,$vendid,$name='',$pricetaxtype=0) {


		if ( empty($pid) ) return false;
		$this->_pid = $pid;
		if ( empty($shippingprice) ) return false;
		$this->_shippingprice = $shippingprice;
		if ( empty($curid) ) return false;
		$this->_curid = $curid;
		if ( empty($vendid) ) {
						$itemM = WModel::get( 'item' );
			$itemM->whereE( 'pid', $pid );
			$vendid = $itemM->load( 'lr', 'vendid' );
		}		$this->_vendid = $vendid;
		$this->_pricetaxtype = $pricetaxtype;
		$this->_name = $name;

				$this->_optionNamekey = 'sshipx_pidz_' . $this->_pid;

				$this->_productOptionM = WModel::get( 'product.option' );
		$this->_productOptionM->whereE( 'namekey', $this->_optionNamekey );
		$opid = $this->_productOptionM->load( 'lr', 'opid' );

				if ( !empty($opid) ) {
			$this->_updateOption( $opid );
		} else {
			$this->_insertOption();
		}

		return true;

	}




	private function _insertOption() {

				$this->_productOptionM->namekey = $this->_optionNamekey;
		$this->_productOptionM->alias = $this->_optionNamekey;
		$this->_productOptionM->type = 1;			$this->_productOptionM->publish = 1;
		$this->_productOptionM->rolid = 1;
		$this->_productOptionM->ordering = 999999;
		$this->_productOptionM->cumulative = 0;			$this->_productOptionM->opttype = 0;			$this->_productOptionM->vendid = $this->_vendid;
		$this->_productOptionM->setChild( 'product.optiontrans', 'name', TR1330393678HUSJ );
		$this->_productOptionM->returnId();
		$this->_productOptionM->save();

				$productPptionvaluesM = WModel::get( 'product.optionvalues' );
		$productPptionvaluesM->opid = $this->_productOptionM->opid;
		$productPptionvaluesM->groupkey = 'shipPrice';
		$productPptionvaluesM->price = $this->_shippingprice;
		$productPptionvaluesM->curid = $this->_curid;
		$productPptionvaluesM->pricetaxtype = $this->_pricetaxtype;
		$productPptionvaluesM->rolid = 1;
		$productPptionvaluesM->publish = 1;
		$productPptionvaluesM->ordering = 1;
		$productPptionvaluesM->setChild( 'product.optionvaluestrans', 'name', TR1213180320PQFZ );
		$productPptionvaluesM->save();

				$this->_insertCrossTable( $this->_productOptionM->opid );

	}


	private function _updateOption($opid) {

				$productPptionvaluesM = WModel::get( 'product.optionvalues' );
		$productPptionvaluesM->whereE( 'opid', $opid );
		$productPptionvaluesM->setVal( 'price', $this->_shippingprice );
		$productPptionvaluesM->setVal( 'curid', $this->_curid );
		$productPptionvaluesM->setVal( 'pricetaxtype', $this->_pricetaxtype );
		$productPptionvaluesM->update();

				$this->_insertCrossTable( $opid );


	}



	private function _insertCrossTable($opid) {

				$productPoptionsM = WModel::get( 'product.poptions' );
		$productPoptionsM->setVal( 'pid', $this->_pid );
		$productPoptionsM->setVal( 'opid', $opid );
		$productPoptionsM->setVal( 'publish', 1 );
		$productPoptionsM->setVal( 'ordering', 1 );
		$productPoptionsM->replace();

	}

}