<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;













class Product_Virtuemart_class extends WClasses {

	private $VMVersion = null;








	public function importData() {






			$importCatC = WClass::get( 'item.category' );

			$categories = $this->getCategories(); 			$products = $this->getProducts(); 			$this->copyVMcatImages(); 			$this->copyVMprodImages(); 
			if(!empty($categories)){
				foreach($categories as $oneCategory){
					$importCatC->insertCategory( $oneCategory, 'store', 'virtuemart' );
				}			}
			if(!empty($products)){
				static $itemInsertC = null;
				if ( !isset($itemInsertC) ) $itemInsertC = WClass::get('item.insert');
				foreach($products as $product){
					$itemInsertC->insertItem( $product, false, 'virtuemart', 'product' );
				}			}
						$catProds = $this->getCategoryProducts();
			$importCatC->insertCatItem( $catProds ); 
			$message = WMessage::get();
			$message->userS('1308652783LDDK');


	}


	private function _isNewVirtueMart() {
		static $_newVersion = null;

		if ( !isset($_newVersion) ) {
			$message = WMessage::get();

			require_once( JOOBI_DS_ADMIN . 'components' .DS . 'com_virtuemart' . DS . 'version.php'  );
			if ( empty($VMVERSION) || empty($VMVERSION->RELEASE) ) {

				if( class_exists( 'vmVersion' ) ) {
					$VMVERSION = new vmVersion();
					$VMVERSION->RELEASE = vmVersion::$RELEASE;
				}
				if ( empty($VMVERSION)  || empty($VMVERSION->RELEASE) ) {
					$message->userE('1334767411BBEF');
					return true;
				}			}
			$this->VMVersion = $VMVERSION->RELEASE;

			if ( version_compare( $this->VMVersion, '2.0.0', '<' )  ) {
				$this->_newVersion = false;
			} else {
				$this->_newVersion = true;
			}

		}
		return ( !$this->_newVersion );

	}





	function getCategories() {

		if ( !$this->_isNewVirtueMart() ) {
			$virtueMartCategoryTable = WTable::get('#__vm_category');
			$virtueMartCategoryTable->makeLJ('#__vm_category_xref', '', 'category_id', 'category_child_id');
			$virtueMartCategoryTable->select('category_id', 0);
			$virtueMartCategoryTable->select('category_name', 0, 'name');
			$virtueMartCategoryTable->select('category_description', 0, 'description');
			$virtueMartCategoryTable->select('category_publish', 0, 'publish');
			$virtueMartCategoryTable->select('category_full_image', 0, 'imageFile');
			$virtueMartCategoryTable->select('cdate', 0, 'created');
			$virtueMartCategoryTable->select('mdate', 0, 'modified');

		} else {
			$virtueMartCategoryTable = WTable::get( '#__virtuemart_categories' );
			$virtueMartCategoryTable->makeLJ('#__virtuemart_category_categories', '', 'virtuemart_category_id', 'category_child_id' );
			$virtueMartCategoryTable->makeLJ('#__virtuemart_categories_en_gb', '', 'virtuemart_category_id', 'virtuemart_category_id' );
			$virtueMartCategoryTable->select( 'virtuemart_category_id', 0, 'category_id' );
			$virtueMartCategoryTable->select('category_name', 2, 'name');
			$virtueMartCategoryTable->select('category_description', 2, 'description');
			$virtueMartCategoryTable->select('published', 0, 'publish');

			$virtueMartCategoryTable->makeLJ('#__virtuemart_category_medias', '', 'virtuemart_category_id', 'virtuemart_category_id' );
			$virtueMartCategoryTable->makeLJ('#__virtuemart_medias', '', 'virtuemart_media_id', 'virtuemart_media_id', 3, 4 );
			$virtueMartCategoryTable->select('file_url', 4, 'imageFile');

		}
		$virtueMartCategoryTable->select('category_parent_id', 1, 'parent');

		$virtueMartCategoryTable->orderBy('category_parent_id', 'ASC', 1);
		$vmCat = $virtueMartCategoryTable->load('ol');

		foreach( $vmCat as $oneCat ) {
						$oneCat->namekey = 'category_vm_' . $oneCat->category_id;
						$oneCat->parentNamekey = 'category_vm_' . $oneCat->parent;

									if( $oneCat->publish == 'Y') $oneCat->publish = 1;
			else $oneCat->publish = 0;

			$oneCat->vendor = '';
		}
		return $vmCat;

	}
	



	function getProducts() {

		if ( !$this->_isNewVirtueMart() ) {
			$virtueMartProductTable = WTable::get('#__vm_product');
			$virtueMartProductTable->makeLJ('#__vm_product_price', '', 'product_id', 'product_price_id');
			$virtueMartProductTable->makeLJ('#__vm_product_files', '', 'product_id', 'file_product_id');
			$virtueMartProductTable->select('product_id');
			$virtueMartProductTable->select('product_name', 0, 'name');
			$virtueMartProductTable->select('product_s_desc', 0, 'introduction');
			$virtueMartProductTable->select('product_desc', 0, 'description');
			$virtueMartProductTable->select('product_publish', 0, 'publish');
			$virtueMartProductTable->select('cdate', 0, 'created');
			$virtueMartProductTable->select('mdate', 0, 'modified');
			$virtueMartProductTable->select('product_full_image', 0, 'imageFile');
			$virtueMartProductTable->select('product_price', 1, 'price');
			$virtueMartProductTable->select('product_currency', 1, 'currency');
			$virtueMartProductTable->select('product_parent_id', 0, 'parent');
			$virtueMartProductTable->select('file_name', 2, 'itemFile' );

		} else {
			$virtueMartProductTable = WTable::get('#__virtuemart_products');
			$virtueMartProductTable->select('virtuemart_product_id', 0, 'product_id' );
			$virtueMartProductTable->makeLJ('#__virtuemart_product_prices', '', 'virtuemart_product_id', 'virtuemart_product_id');
			$virtueMartProductTable->select('product_price', 1, 'price');


			$virtueMartProductTable->makeLJ('#__virtuemart_product_medias', '', 'virtuemart_product_id', 'virtuemart_product_id' );

			$virtueMartProductTable->makeLJ('#__virtuemart_products_en_gb', '', 'virtuemart_product_id', 'virtuemart_product_id' );

			$virtueMartProductTable->select( 'product_name', 3, 'name' );
			$virtueMartProductTable->select( 'product_s_desc', 3, 'introduction' );
			$virtueMartProductTable->select('product_desc', 3, 'description' );
			$virtueMartProductTable->select('published', 0, 'publish');

			$virtueMartProductTable->makeLJ('#__virtuemart_product_medias', '', 'virtuemart_product_id', 'virtuemart_product_id', 0, 4 );
			$virtueMartProductTable->makeLJ('#__virtuemart_medias', '', 'virtuemart_media_id', 'virtuemart_media_id', 4, 5 );
			$virtueMartProductTable->select('file_url', 5, 'imageFile');

			$virtueMartProductTable->makeLJ('#__virtuemart_currencies', '', 'product_currency', 'virtuemart_currency_id', 1, 6 );
			$virtueMartProductTable->select( 'currency_code_3', 6, 'currency' );


		}
		$virtueMartProductTable->select('product_sku', 0, 'namekey');
		$virtueMartProductTable->select('product_weight', 0, 'weight');
		$virtueMartProductTable->select('product_weight', 0, 'weight');
		$virtueMartProductTable->select('product_in_stock', 0, 'stock');
		$virtueMartProductTable->select('product_available_date', 0, 'availabledate');
		$virtueMartProductTable->select('product_availability', 0, 'availability');
		$virtueMartProductTable->select('product_parent_id', 0, 'parent');

		$vmProducts = $virtueMartProductTable->load('ol');

		foreach( $vmProducts as $oneVmProduct){
			$oneVmProduct->namekey = 'product_vm_'.$oneVmProduct->product_id;

									if( $oneVmProduct->publish == 'Y') $oneVmProduct->publish = 1;
			else $oneVmProduct->publish = 0;

						$oneVmProduct->vendor = 1;
			$oneVmProduct->priceType = 'standard';
		}
		return $vmProducts;

	}




	function getCategoryProducts() {

		if ( !$this->_isNewVirtueMart() ) {
			$virtueMartCategoryProductTable = WTable::get('#__vm_product_category_xref');
			$virtueMartCategoryProductTable->select('category_id');
			$virtueMartCategoryProductTable->select('product_id');
		} else {
			$virtueMartCategoryProductTable = WTable::get('#__virtuemart_product_categories');
			$virtueMartCategoryProductTable->select( 'virtuemart_category_id', 0, 'category_id' );
			$virtueMartCategoryProductTable->select( 'virtuemart_product_id', 0, 'product_id');			}
		$catProd = $virtueMartCategoryProductTable->load('ol');

		return $catProd;

	}






	function copyVMcatImages() {
		$systemFolderC = WGet::folder();		
		$destFolder = JOOBI_DS_TEMP . 'tempvirtuemart' .DS. 'category'; 
		if ( !$this->_isNewVirtueMart() ) {
			$srcFolder = JOOBI_DS_ROOT.'components'.DS.'com_virtuemart'.DS.'shop_image'.DS.'category'; 		} else {
			$srcFolder = JOOBI_DS_ROOT ; 		}				$systemFolderC->copy( $srcFolder, $destFolder,'',true);

		return true;
	}





	public function copyVMprodImages() {

		$systemFolderC = WGet::folder();		
		$destFolder = JOOBI_DS_TEMP . 'tempvirtuemart' .DS. 'product'; 
		if ( !$this->_isNewVirtueMart() ) {
			$srcFolder = JOOBI_DS_ROOT.'components'.DS.'com_virtuemart'.DS.'shop_image'.DS.'product'; 		} else {
			$srcFolder = JOOBI_DS_ROOT ; 		}
				$systemFolderC->copy( $srcFolder, $destFolder,'',true);

		return true;
	}
}