<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;













class Product_Category_class extends WClasses {
	
	






	function insertCategory($category,$type='store',$from='sample'){
				if ( empty( $category->name ) ) return false;
		
		$itemCategoryC = WClass::get('item.category');
		return $itemCategoryC->insertCategory( $category, $type = 'store', $from = 'sample' );
		
	}	
	



	function getCatId($namekey) {
		if ( empty($namekey) ) return false;
		
		$itemCategoryC = WClass::get('item.category');
		$catid = $itemCategoryC->getCatId($namekey);
		
		return $catid;
	}	
	
	



	function insertCatProd($catProds){
		if(empty($catProds)) return false;
		
		$itemCategoryC = WClass::get('item.category');
		return $itemCategoryC->insertCatItem( $catProds );
	}	
	
	



	
	

}