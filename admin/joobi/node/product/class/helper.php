<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Helper_class extends WClasses {












	function price($value,$curid) {


		$usedCurrency = WUser::get('curid');
		if ( $usedCurrency != $curid ) {
						$currencyConvertC = WClass::get('currency.convert',null,'class',false );
			$convertedPrice = $currencyConvertC->currencyConvert( $curid, $usedCurrency, $value );
		} else {
			$convertedPrice = $value;
		}
		$object = new stdClass;

		$object->money = WTools::format( $convertedPrice, 'money', $usedCurrency );

		$object->price = WTools::format( $convertedPrice, 'price', $usedCurrency );

		return $object;



	}















function changeProdCurrency($curid,$pidA=null,$uid=null) {

	
	if ( empty($curid) )

	{

		$message = WMessage::get();

		$message->adminW( 'Cant change the currency of the products. Invalid currency [1st] parameter, trace class product.helper function changeProdCurrency' );

		return true;

	}


	
	if ( !empty($pidA) && !is_array($pidA) )

	{

		$message = WMessage::get();

		$message->adminW( 'Cant change the currency of the products. Invalid pid [2nd] parameter, trace class product.helper function changeProdCurrency' );

		return true;

	}


	static $productM=null;

	if ( !isset($productM) ) $productM = WModel::get( 'product' );

	$productM->setVal( 'curid', $curid );

	if ( !empty($pidA) ) $productM->whereIn( 'pid', $pidA );

	if ( !empty($uid) ) $productM->whereE( 'uid', $uid );

	$result = $productM->update();



	return $result;

}












function productSoldCount($myProducts) {

	$productM=WModel::get('product');



	foreach($myProducts as $product)

	{

	$productM->whereE('pid', $product->pid);

	$productM->updatePlus('nbsold', $product->quantity);

	$productM->update();

	}


	return true;

}












function getProdStock($pid) {

	
	if ( empty( $pid ) ) return 0;



	
	static $productM=null;

	if ( !isset($productM) ) $productM = WModel::get( 'product' );

	$productM->whereE( 'pid', $pid );

	$result = $productM->load( 'lr', 'stock' );



	
	if ( empty( $result ) ) $result = 0;



	
	return $result;
	

}















































































function getProdDetail($pid,$arrayColumnA) {



	
	if ( empty($pid) || empty($arrayColumnA) ) {

		$message = WMessage::get();

		$message->codeE( 'Empty parameter passed to function get in class product_helper' );

	}


	static $resultA = array();

	$key = implode( '-', $arrayColumnA );



	if ( !isset( $resultA[$pid][$key] ) ) {

		static $productM = null;

		if ( empty( $productM ) ) $productM = WModel::get( 'product' );

		$productM->whereE( 'pid', $pid );

		$resultA[$pid][$key] = $productM->load( 'ol', $arrayColumnA );

	}


	return $resultA[$pid][$key];

}




 






function manageStock($pid=0,$quantity=1,$add=true) {
			if(empty($pid)) return;	
			static $productM = null;
			
			$quantity = ($add)?   0-$quantity : $quantity; 
			if ( empty( $productM ) ) $productM = WModel::get( 'product' );
			$productM->whereE( 'pid', $pid );
			$productM->where( 'stock', '>', 0 );
			$productM->updatePlus( 'stock', $quantity);
			
			$productM->update();
			return true;
}
}