<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;













class Product_Export_class extends WClasses {
	
	



	function downloadFile($data){
		if (empty( $data )) return false;
		
		$browser = WPage::browser( 'namekey' );
		
		@set_time_limit(0);
 		if( ! ini_get('safe_mode') ) { 			@set_time_limit( 0 );
        }		

		$mime_type = (strtolower($browser) == strtolower('IE') || strtolower($browser) == strtolower('Opera')) ? 'application/octetstream' : 'application/octet-stream';
		$filename = "products_" . date("Y.d.m_H.i.s");

		ob_end_clean();
		ob_start();
		
		$export = '';
		foreach( $data as $oneLineData){
			$export .= $oneLineData;
			$export .= "\r\n" ;
		}		
		header('Content-Type: ' . $mime_type);
		header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');

		if (strtoupper($browser) == 'IE') {
			header('Content-Disposition: inline; filename="' . $filename . '.csv"');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
		} else {
			header('Content-Disposition: attachment; filename="' . $filename . '.csv"');
			header('Pragma: no-cache');
		}
		print $export;
		exit();
		
		
		return true;
	}
}