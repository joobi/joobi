<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Insert_class extends WClasses {


	var $products = array();





	





	public function getDefaultCur() {

		static $curid = null;



		if (isset($curid)) return $curid;



		if (!defined('PCURRENCY_NODE_PREMIUM')) WPref::get('currency.node');

			$curid = PCURRENCY_NODE_PREMIUM;



		return $curid;

	}








	









	function getPriceId($namekey='standard') {

		static $priceM = null;

		static $priceid = array();



		if ( isset( $priceid[$namekey] ) ) return $priceid[$namekey];

		if ( !isset( $priceM ) ) $priceM = WModel::get('product.price');



		$priceM->select('priceid');

		$priceM->whereE('namekey', $namekey);

		$priceid[$namekey] = $priceM->load('lr');



		return $priceid[$namekey];

	}


















	function getProducts($columns,$lgid=1,$filter='',$producttype='product') {

		if (empty($columns)) return false;
		$itemC = WClass::get('item.insert');

		if ( $producttype == 'catalog' ) $producttype = 'item';


		$prodTypid = $itemC->getProdTypeId( $producttype, false );



		$productM = WModel::get( $producttype );

		$categoryName = false;

		$productM->select('name', 1);

		$specialColumnsA = array( 'name', 'introduction', 'description', 'currency', 'itemType', 'priceType', 'category', 'categoryName', 'imageFile', 'previewFile', 'itemFile' );
		foreach( $columns as $oneColumn ) {
			if ( in_array($oneColumn, $specialColumnsA ) ) {				if ( $oneColumn == 'name' ) $productM->select( 'name', 1 );
				if ( $oneColumn == 'introduction' ) $productM->select( 'introduction', 1 );
				if ( $oneColumn == 'description' ) $productM->select( 'description', 1 );
				if ( $oneColumn == 'currency' ) $productM->select('code', 4, 'currency');

				if ( $oneColumn == 'itemType' ) $productM->select('namekey', 2, 'itemType');
				if ( $oneColumn == 'priceType' ) $productM->select('namekey', 3, 'priceType');
				if ( $oneColumn == 'category' ) $productM->select('namekey', 6, 'category');
				if ( $oneColumn == 'categoryName' ) {
					$productM->select( 'name', 9, 'categoryName' );
					$categoryName = true;
				}
				if ( $oneColumn == 'imageFile' ) {
					$productM->select('filid', 7, 'filid');
					$productM->select('name', 8, 'imagename');
					$productM->select('type', 8, 'imagetype');
				}				if( $oneColumn == 'previewFile' ){
					$productM->select('previewid');
				}				if( $oneColumn == 'itemFile' ){
					$productM->select('filid',0, 'productid');
				}
			} else { 				$productM->select( $oneColumn );
			}

		}
		$productM->makeLJ('producttrans','pid','pid', 0, 1);

		$productM->makeLJ('product.type','prodtypid','prodtypid', 0, 2);

		$productM->makeLJ('product.price','priceid','priceid', 0, 3);

		$productM->makeLJ('currency','curid','curid', 0, 4);



		$productM->makeLJ('product.categoryproduct','pid','pid', 0, 5);

		$productM->orderBy( 'premium', 'DESC', 5 );

		$productM->makeLJ('product.category','catid','catid', 5, 6);
		$productM->makeLJ('product.images','pid','pid', 0, 7);
		$productM->whereOn( 'premium', '=', 1, 7 );

		$productM->makeLJ('files','filid','filid', 7, 8 );

		if ( $categoryName ) {
			$productM->makeLJ('product.categorytrans','catid','catid', 6, 9 );
			$productM->whereLanguage( 9, $lgid );
		}

		$productM->whereLanguage( 1, $lgid );


		$productM->whereE('publish', 1);

		$productM->whereE('block', 0 );

		$productM->whereIn( 'prodtypid', $prodTypid );
		$productM->groupBy('pid');




		if ( $filter == 'google' ) {

			
			$productM->select( 'pid' );

			$productM->select( 'curid' );

			$productM->select( 'catid', 6 );

			$productM->select( 'namekey', 0, 'sku' );



			
			$productM->select( 'stock' );

			$productM->select( 'electronic' );



			
			$productM->makeLJ( 'vendor', 'vendid', 'vendid', 0, 11 );

			$productM->makeLJ( 'vendortrans', 'vendid', 'vendid', 11, 12 );
			$productM->whereLanguage( 12 );
			$productM->select( 'name', 12, 'vendorName' );


			
			$productM->select( array( 'publishend', 'availableend' ) );

			$productM->where( 'availablestart', '<=', time() );

			$productM->where( 'availableend', '>=', time(), 0, null, 1, 0, 1 ); 
			$productM->where( 'availableend', '=', 0, 0, null, 0, 1 );

			$productM->where( 'publishstart', '<=', time() ); 
			$productM->where( 'publishend', '=', 0, 0, null, 1, 0, 0  );

			$productM->where( 'publishend', '>=', time(), 0, null, 0, 1, 1  );



  			
  			$roleC = WRole::get();

  			$allusers = $roleC->getRole( 'allusers' );

  			$productM->whereE( 'rolid', $allusers );



  			
  			$productM->select( 'name', 8, 'imageName' );

  			$productM->select( 'type', 8, 'imageType' );

  			$productM->select( 'path', 8, 'imagePath' );



  			
  			$productM->select( 'weight' );

  			$productM->select( 'symbol', 13, 'weightCode' );

  			$productM->makeLJ( 'unit', 'unitid', 'unitid', 0, 13 );



  			
  			$productM->select( array( 'score', 'votes', 'nbreviews' ) );


		}


		$productM->setLimit( 20000 );

		$products = $productM->load('ol');

				if(!empty($products)){
			if(in_array('previewFile', $columns) || in_array('itemFile', $columns)){
				foreach($products as $product){
					foreach($product as $key=>$value){
						$file->previewid_name = '';
						$file->previewid_type = '';
						$file->productid_name = '';
						$file->productid_type = '';

												if($key == 'previewid' OR $key == 'productid'){
							$fileM = WModel::get('files');
							$fileM->select('name', 0, $key.'_name');
							$fileM->select('type', 0, $key.'_type');
							$fileM->whereE('filid', $value);

							$file = $fileM->load('o');
							if(!empty($file)){
								if($key == 'previewid'){
									$product->previewid_name = $file->previewid_name;
									$product->previewid_type = $file->previewid_type;
								}
								if($key == 'productid'){
									$product->productid_name = $file->productid_name;
									$product->productid_type = $file->productid_type;
								}							}						}					}				}			}		}


		return $products;

	}





}