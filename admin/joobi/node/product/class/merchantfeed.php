<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */


defined('_JEXEC') or die;



















class Product_Merchantfeed_class extends WClasses {

	











	function makeFeedFile() {

		

		WPref::load( 'PCATALOG_NODE_GOOGLE_FEED_COUNTRY' );



		$countriesM = WModel::get( 'currency.country' );

		$countriesM->select( 'curid' );

		$countriesM->select( 'lgid', 1 );

		$countriesM->select( 'publish', 2 );

		$countriesM->makeLJ( 'country.language', 'ctyid', 'ctyid', 0, 1 );

		$countriesM->makeLJ( 'library.languages', 'lgid', 'lgid', 1, 2 );

		$countriesM->whereE( 'ctyid', PCATALOG_NODE_GOOGLE_FEED_COUNTRY );

		$countriesM->where( 'curid', '!=', 0 );

		$countriesM->whereE( 'publish', 1, 2 );

		$countriesM->orderBy( 'ordering', 'ASC', 1 );


		$countries = $countriesM->load( 'o' );



		if ( empty( $countries ) ) {

			$languageM = WModel::get( 'country.language' );

			$languageM->whereE( 'ctyid', PCATALOG_NODE_GOOGLE_FEED_COUNTRY );

			$languageM->orderBy( 'ordering', 'ASC' );

			$lgid = $languageM->load( 'lr', 'lgid' ); 

			

			$LANG = WLanguage::get( $lgid, 'name' );

			$message = WMessage::get();

			$message->adminE( "Cannot proceed with export. The language of the country selected ( $LANG ) is not available for your site.");

			return false;

		}
	

		$productsC = WClass::get( 'product.insert' );

		$columns = array( 'introduction', 'description', 'price', 'currency', 'category', 'publishend', 'featured', 'categoryName' );

		$products = $productsC->getProducts( $columns, $countries->lgid, 'google' );

	

		if ( empty( $products ) ) {

			$message = WMessage::get();

			$message->userW('1340572514EFNF');			

			return false;	

		}
		

		$vendorCurid = $countries->curid;

		

		$curconvertC = WClass::get( 'currency.convert' );

		$curformatC = WClass::get( 'currency.format' );

		$vendorCurCode = $curformatC->load( $vendorCurid, 'code' );

		

		

		$vendorHelperC = WClass::get( 'vendor.helper', null, 'class', false );

		$storeName = $vendorHelperC->getVendor( 0, 'name' );		

		

		
		$content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> \r\n";

		$content .= "<feed xmlns=\"http://www.w3.org/2005/Atom\" xmlns:g=\"http://base.google.com/ns/1.0\">\r\n";

		$content .= "<title>Products Data</title> \r\n";

		$content .= "<link rel=\"self\" href=\"" . JOOBI_SITE . "\"/>\r\n";

		$content .= "<updated>". date( 'Y-m-d\TH:i:s\Z', time() )."</updated>\r\n";

		$content .= "<author><name>".$storeName."</name></author>\r\n";

		

		static $deliveryType = null;

		

		if ( !isset($deliveryType) ) {

			if ( !defined('PPRODUCT_NODE_DELIVERYTYPE') ) WPref::get('product.node');

			$deliveryType = PPRODUCT_NODE_DELIVERYTYPE;		

		}
		

		foreach( $products as $key => $data ) { 



			
			if ( ( $data->publishend > 0 && $data->publishend < time() ) || 

					($data->publishend == 0 && $data->availableend > 0 && $data->availableend < time() ) )

				continue;

			

			

			$content .= "<entry>\r\n";

			$content .= "<title>" . htmlentities( $data->name, ENT_COMPAT, 'UTF-8' ) . "</title>\r\n";

			

			
			$content .= "<g:condition>new</g:condition>\r\n";

			

			$description = trim ( $data->description );

			if ( empty( $description ) )  $description = trim( $data->introduction );

			


			$tagC = WClass::get('output.process');

			$tagC->replaceTags( $description );

						

			

			
			$emailHelperC = WClass::get( 'email.conversion' );

			$description = $emailHelperC->smartHTMLSize( $description, 9500, true, false, false, true ); 	


			

			
			if ( strlen( $description ) > 10000 ) $description = substr( $description, 0, 9995 ) . '...';

			

			if ( empty( $description ) )  $description = TR1309852643BDZK;

			

			$content .= "<description>". htmlentities( $description, ENT_COMPAT, 'UTF-8' )."</description>\r\n";

			

			
			$content .= "<id>" . $data->sku . "</id>\r\n"; 
			$content .= "<g:mpn>" . $data->sku . "</g:mpn>\r\n";

			
			$content .= "<g:brand>" . htmlentities( $data->vendorName, ENT_COMPAT, 'UTF-8' ) . "</g:brand>\r\n";

			

			
			if( !empty( $data->imageName ) && !empty( $data->imageType ) && !empty( $data->imagePath ) ) {

				$path = JOOBI_URL_MEDIA . str_replace( '|', '/', $data->imagePath ) . '/' . $data->imageName . '.' . $data->imageType ;

				$path = htmlentities( $path );

				$content .= "<g:image_link>" . $path . "</g:image_link>\r\n";

			}
			

			$link = WPage::linkHome( 'controller=catalog&task=show&eid='. $data->pid );

			if( $data->catid ) $link .= '&catid='.$data->catid ;

			$content .= "<link>". htmlentities( $link ) . "</link>\r\n";



			
			if ( $data->curid != $vendorCurid ) {

				$conv = new stdClass;

				$conv->currencyFrom = $data->curid;

				$conv->currencyTo = $vendorCurid;

				$conv->amount = $data->price;

				$conv->formatAmount = false; 
				$conv->decimalPlace = 2; 
				$conv->includeFee = true; 
				$value = $curconvertC->convertTo( $conv );

				

				$price = $value . ' ' . $vendorCurCode;

			} else {

				$price = $data->price . ' ' . $data->currency;

			}
			

			$content .= "<g:price> $price </g:price>\r\n";

			



			
			$category = ( !empty( $data->categoryName ) ) ? ' &gt; ' . $data->categoryName : '';

			$content .= "<g:product_type>". $data->vendorName . $category . "</g:product_type>\r\n";

			

			

			
			if ( (!empty( $deliveryType ) && $deliveryType == 1) || (!empty( $data->electronic ) && $data->electronic == 1) ) {

				$content .= "<g:availability> in stock</g:availability>\r\n";

			} else {

				if( $data->stock == 0 ) {

					$content .= "<g:availability> out of stock</g:availability>\r\n";

				} elseif ( $data->stock > 0 && $data->electronic == 1 ) {

					$content .= "<g:availability> limited </g:availability>\r\n";

					$content .= "<g:quantity> ". $data->stock . " </g:quantity>\r\n";

				} else {

					$content .= "<g:availability> in stock</g:availability>\r\n";

				}
			}
			

			

			
			if ( !empty( $data->weight ) ) {

				$weight = $data->weight . ' ' . $data->weightCode;

				$content .= "<g:weight>$weight</g:weight>\r\n";

			}


			
			if ( $data->votes > 0 ) {

				$rating = round( $data->score / $data->votes, 1 );

				if( $rating > 0 ) $content .= "<g:product_review_average>$rating</g:product_review_average>\r\n";

			}
			if( $data->nbreviews > 0 ) $content .= "<g:product_review_count>" . $data->nbreviews . "</g:product_review_count>\r\n";

		

			
			$featured = ( empty( $data->featured ) ) ? 'n' : 'y';

			$content .= "<g:featured_product>$featured</g:featured_product>\r\n";

			

			
			if( !empty( $data->publishend ) || !empty( $data->availableend ) ){

				$enddate = 0;

				if ( $data->availableend > $enddate ) $enddate = $data->availableend;

				if ( $data->publishend < $enddate && $data->publishend != 0 ) $enddate = $data->publishend;

				$enddate = date( "Y-m-d", $enddate );

				

				$content .= "<g:expiration_date>$enddate</g:expiration_date>\r\n";

			}
			



			
			



			

			$content .= $this->_specificationPerCountry();

			

			$content .= "</entry>\r\n";

			

		}
		

		

		

		
		$content .= "</feed>";

		

		$token = WPage::frameworkToken();

		$file = WGet::file();

		$filename = "products_feed_$token.xml"; 
		$file->write( JOOBI_DS_EXPORT . $filename, $content, 'overwrite' );	

	

		$EXPORTFILE = JOOBI_URL_EXPORT . $filename;

		$message = WMessage::get();

		$message->adminS( 'You can find your export file in this location: ' . $EXPORTFILE );

		$message->adminS( 'To edit the integration settings for Google Mechant Feed, kindly go to Catalog>>Preferences>>Integration Tab' );

		return true;

	}
	

	







	

	private function _specificationPerCountry() {

		static $isocode3=null;

		

		$content = '';

		

		if ( !isset($isocode3) ) {

			
			$countriesHelper = WClass::get( 'countries.helper' );

			$isocode3 = strtoupper( $countriesHelper->getData( PCATALOG_NODE_GOOGLE_FEED_COUNTRY, 'isocode3' ) );

		}
		switch( $isocode3 ) {

			case 'USA':

				
				$content .= "<g:tax>\r\n";

				$content .= "	<g:country>US</g:country>\r\n";

				$content .= "	<g:rate>0</g:rate>\r\n";

				$content .= "</g:tax>\r\n";

				
				$content .= "<g:shipping>\r\n";

				$content .= "	<g:country>US</g:country>\r\n";

				$content .= "	<g:price>0 USD</g:price>\r\n";

				$content .= "</g:shipping>\r\n";

				break;

			default:

				break;

		}
		

		

		return $content;

		

	}
	

}