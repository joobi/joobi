<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Product_Raffle_class extends WClasses {














function createRaffle($rafid,$startingnum,$endingnum)

{

	
	if( empty($rafid) || empty($startingnum) || empty($endingnum) )

	{

		$message = WMessage::get();

		$message->adminW( 'Invalid Paramaters for class product.raffle function createRaffle' );

		return true;

	}
	

	
	if( $startingnum < 1 )

	{

		$message = WMessage::get();

		$message->adminW( 'Raffle number should atleast start to 1. Invalid 2nd parameter for class product.raffle function createRaffle' );

		return true;

	}
	

	
	if( $endingnum < 1 )

	{

		$message = WMessage::get();

		$message->adminW( 'Raffle number should atleast end with 1. Invalid 3rd parameter for class product.raffle function createRaffle' );

		return true;

	}
	

	
	if( $startingnum > $endingnum )

	{

		$message = WMessage::get();

		$message->userN('1254123957RYMS');

		$message->adminW( 'Invalid 2nd and 3rd parameter for class product.raffle function createRaffle' );

		return true;

	}
	

	
	static $prodRafMembersM=null;

	if( !isset($prodRafMembersM) ) $prodRafMembersM = WModel::get( 'product.rafflemembers' );

	for( $i=$startingnum; $i <= $endingnum; $i++ )

	{

		$prodRafMembersM->setVal( 'rafid', $rafid );

		$prodRafMembersM->setVal( 'rafno', $i );

		$prodRafMembersM->setVal( 'created', time() );

		$prodRafMembersM->insert();

	}


	return true;

}












function cancelRaffle($rafid,$rafno)

{

	
	
	if( empty($rafid) || empty($rafno) )

	{

		$message = WMessage::get();

		$message->adminW( 'Invalid Paramaters for class product.raffle function cancelRaffle' );

		return true;

	}
	

	
	
	
	static $prodRafMembersM=null;

	if( !isset($prodRafMembersM) ) $prodRafMembersM = WModel::get( 'product.rafflemembers' );

	$prodRafMembersM->setVal( 'uid', 0 );

	$prodRafMembersM->setVal( 'oid', 0 );

	$prodRafMembersM->setVal( 'datepurchased', 0 );

	$prodRafMembersM->whereE( 'rafid', $rafid );

	$prodRafMembersM->whereE( 'rafno', $rafno );

	$prodRafMembersM->update();

	

	
	static $prodRaffleM = null;

	if( !isset($prodRaffleM) ) $prodRaffleM = WModel::get( 'product.raffle' );

	$prodRaffleM->updatePlus( 'numraffle', '-1' );

	$prodRaffleM->whereE( 'rafid', $rafid );

	$prodRaffleM->update();

	

	return true;

}














function assignRaffle($rafid,$rafno,$uid,$oid=0)

{

	
	
	if( empty($rafid) || empty($rafno) || empty($uid) )

	{

		$message = WMessage::get();

		$message->adminW( 'Invalid Paramaters for class product.raffle function assignRaffle' );

		return true;

	}
	

	
	static $prodRafMemM=null;

	if( !isset($prodRafMemM) ) $prodRafMemM = WModel::get( 'product.rafflemembers' );

	$prodRafMemM->setVal( 'uid', $uid );

	$prodRafMemM->setVal( 'datepurchased', time() );

	if( !empty($oid) ) $prodRafMemM->setVal( 'oid', $oid );

	$prodRafMemM->whereE( 'rafid', $rafid );

	$prodRafMemM->whereE( 'rafno', $rafno );

	$result = $prodRafMemM->update();

	

	
	if( $result )

	{

		static $prodRaffleM=null;

		if( !isset($prodRaffleM) ) $prodRaffleM = WModel::get( 'product.raffle' );

		$prodRaffleM->updatePlus( 'numraffle', 1 );

		$prodRaffleM->whereE( 'rafid', $rafid );

		$prodRaffleM->update();

	}


	return $result;

}










function getRaffleID($pid)

{

	
	
	if( empty($pid) )

	{

		$message = WMessage::get();

		$message->adminW( 'Invalid Paramaters for class product.raffle function getRaffleID' );

		return true;

	}


	static $result=array();

	

	
	if( !isset($result[$pid]) )

	{

		static $prodRaffleM=null;

		if( !isset($prodRaffleM) ) $prodRaffleM = WModel::get( 'product.raffle' );

		$prodRaffleM->whereE( 'pid', $pid );

		$result[$pid] = $prodRaffleM->load( 'lr', 'rafid' );

	}


	
	return $result[$pid];

}












function getRaffle($rafid,$rafno=0)

{

	
	
	if( empty($rafid) )

	{

		$message = WMessage::get();

		$message->adminW( 'Invalid Paramaters for class product.raffle function getRaffle' );

		return true;

	}


	static $result=array();

	

	
	if( !isset($result[$rafid]) )

	{

		static $prodRaffleM=null;

		if( !isset($prodRaffleM) ) $prodRaffleM = WModel::get( 'product.raffle' );

		if( !empty($rafno) && ( $rafno > 0 ) ) $prodRaffleM->makeLJ( 'product.rafflemembers', 'rafid', 'rafid', 0, 1 );

		if( !empty($rafno) && ( $rafno > 0 ) ) $prodRaffleM->select( 'datepurchased', 1);

		$prodRaffleM->whereE( 'rafid', $rafid, 0 );

		if( !empty($rafno) && ( $rafno > 0 ) ) $prodRaffleM->whereE( 'rafno', $rafno, 1);

		$result[$rafid] = $prodRaffleM->load( 'o', array( 'pid', 'startingnum', 'endingnum', 'deadline', 'numraffle', 'autoassign' ), 0 );

	}


	
	return $result[$rafid];

}
















function sendRaffleEmail($emailNamekey,$uid,$param=null,$sendAdmin=false,$showSreenMsg=true,$showScreenMsgAdmin=true)

{

	
	
	if( empty($emailNamekey) )

	{

		$message = WMessage::get();

		$message->adminW( 'Invalid 1st parameter [ email namekey ] for class product.raffle function sendRaffleEmail' );

		return true;

	}






	

	$mail = WMail::get();

	if( !empty($param) ) $mail->setParameters( $param );

	$mail->sendNow( $uid, $emailNamekey, $showSreenMsg );


	if( $sendAdmin ) $mail->sendAdmin( $emailNamekey .'_admin', $showScreenMsgAdmin );



	return true;

}












function numAvailableRaffles($rafid)

{

	
	
	if( empty($rafid) )

	{

		$message = WMessage::get();

		$message->adminW( 'Invalid Paramater for class product.raffle function numAvailableRaffles' );

		return true;

	}
	

	static $prodRafMemM=null;

	if( !isset($prodRafMemM) ) $prodRafMemM = WModel::get( 'product.rafflemembers' );

	$prodRafMemM->select( 'rafno', 0, null, 'count' );

	$prodRafMemM->whereE( 'rafid', $rafid );

	$prodRafMemM->where( 'uid', '<=', 0);

	$prodRafMemM->groupBy( 'rafid' );

	$numOfRaffles = $prodRafMemM->load( 'lr');



	return $numOfRaffles;

}














function getAvailableRaffles($rafid,$limit=0)

{

	
	
	if( empty($rafid) )

	{

		$message = WMessage::get();

		$message->adminW( 'Invalid Paramater for class product.raffle function getAvailableRaffles' );

		return true;

	}


	static $prodRafMemM=null;

	if( !isset($prodRafMemM) ) $prodRafMemM = WModel::get( 'product.rafflemembers' );

	$prodRafMemM->whereE( 'rafid', $rafid );

	$prodRafMemM->where( 'uid', '<=', 0);

	if( !empty($limit) ) $prodRafMemM->setLimit( $limit );

	$rafflesA = $prodRafMemM->load( 'lra', 'rafno' );



return $rafflesA;

}}