<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Pricetype_class extends WClasses {

	private static $allPriceTypeA = array();
	private static $allPriceTypeInfoA = array();
	
	private static $correspingTypeA = array( 'free' => 250, 'contactus' => 50,
	 'price' => 10, 'shoppers' => 17 , 'donation' => 20, 'link' => 40  );
	










	public function getPriceTypeID($type='free') {


	
		
		$nbType = self::$correspingTypeA[$type];
		if ( empty( self::$allPriceTypeA ) ) $this->_getAllPrice();
		
		return ( !empty(self::$allPriceTypeA[$nbType]) ? self::$allPriceTypeA[$nbType] : array() );
		
	}





	
	public function removePrice($priceid,$type2check=null) {
		
		if ( !isset( $type2check ) ) $type2check = array( 40, 50, 250 );
		
		if ( empty(self::$allPriceTypeA) ) $this->_getAllPrice();
		
		$namekey = $this->_getPriceTypeType( $priceid );
				if ( !is_array($type2check) ) $type2check = array( $type2check );
		if ( in_array( $namekey, $type2check ) ) return true;			
		return false;
		
	}	



	
	public function getPriceTypeName($priceid,$property='name') {
		
		if ( empty(self::$allPriceTypeA) ) $this->_getAllPrice();
		
		return ( isset(self::$allPriceTypeInfoA[$priceid]->$property) ? self::$allPriceTypeInfoA[$priceid]->$property : '' );
		
		return false;
		
	}	
	
	





	
	private function _getPriceTypeType($priceid) {
		if ( !empty(self::$allPriceTypeA) ) {
			foreach( self::$allPriceTypeA as $key => $valA ) {
				foreach($valA as $recordedPriceID ) {
					if ( $priceid == $recordedPriceID ) return $key;
				}				
			}		}		
	}	
	
	
	



	
	private function _getAllPrice() {

		if ( !empty(self::$allPriceTypeA) ) return true;
		
		$productPriceM = WModel::get( 'product.price' );
		$productPriceM->makeLJ( 'product.pricetrans', 'priceid' );
		$productPriceM->whereLanguage();
		$productPriceM->select( 'name', 1 );
		$productPriceM->whereE( 'publish', 1 );
		$productPriceM->orderBy( 'core', 'DESC' );
		$objListA = $productPriceM->load( 'ol', array( 'priceid', 'type') );

		if ( !empty($objListA) ) {
			self::$allPriceTypeInfoA = array();
			foreach( $objListA as $val ) {
				if ( !isset(self::$allPriceTypeA[$val->type]) ) self::$allPriceTypeA[$val->type] = array();
				self::$allPriceTypeA[$val->type][] = $val->priceid;
				self::$allPriceTypeInfoA[$val->priceid] = $val;
			}		}		
	}	
}