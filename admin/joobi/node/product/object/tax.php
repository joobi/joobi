<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;
















class Product_Tax_object {

	private static $taxRateA = array();	
	private $taxO = null;		private $rate = null;

	private $price = null;

	





	private $taxType = null;

	private $productType = null;

	private $_tax = null;		private $_including = null;		private $_excluding = null;	
	private $_vendid = null;	






	public function definePriceTaxType($priceTaxType=null) {
		$giveChoice = WPref::load( 'PCATALOG_NODE_TAXEDITEDPRICE' );
		if ( !$giveChoice ) {
			$taxTypeReturned = PCATALOG_NODE_TAXEDITEDPRICE;
		} else {
			$taxTypeReturned = $priceTaxType;
			if ( $taxTypeReturned==0 ) {
				$taxTypeReturned = PCATALOG_NODE_TAXEDITEDPRICE;
			}		}
		return $taxTypeReturned;
	}











public function setPrice($price,$taxType,$productType) {


	if ( empty( $this->_vendid ) ) {
		$this->_setVendorID();
	}
	unset($this->_tax);
	unset($this->_including);

	if ( in_array( $taxType, array( 1, -1) ) )	$this->taxType = $taxType;
	else {
				
		$this->taxType = WPref::load('PCATALOG_NODE_TAXEDITEDPRICE');

	}
	$this->price = $price;
	$this->productType = $productType;


		if ( empty(self::$taxRateA[$this->_vendid]) ) {
		$this->_getRaxRates();
	}
		$this->taxO = isset( self::$taxRateA[$this->_vendid][$productType] ) ? self::$taxRateA[$this->_vendid][$productType] : 0;
	$this->rate = isset( $this->taxO->rate ) ? $this->taxO->rate : 0;


}




public function setVendorID($vendid) {
	$this->_vendid = (int)$vendid;	}








	public function getVendorID($vendid) {
		static $vendorID = null;

		$useTax = WPref::load( 'PCATALOG_NODE_USETAX' );
		if ( !empty($vendid) && $useTax && PCATALOG_NODE_VENDORTAX ) {
			return $vendid;
		}
		if ( !empty($vendorID) ) return $vendorID;

		$directPay = WPref::load( 'PBASKET_NODE_DIRECTPAY' );
		if ( empty($directPay) ) {
						$vendorC = WClass::get( 'vendor.helper', null, 'class', false );
			$vendorID = $vendorC->getDefault();

			return $vendorID;

		} else {

			if ( !PCATALOG_NODE_VENDORTAX ) {
				$vendorC = WClass::get( 'vendor.helper', null, 'class', false );
				$vendorID = $vendorC->getDefault();

				return $vendorID;
			}

			
						static $basketC=null;
			static $basketHelperC=null;
			if ( empty($basketHelperC) ) $basketHelperC = WClass::get( 'basket.helper' );
			if ( empty($basketC) ) $basketC = WClass::get( 'basket.previous' );
			$basket = $basketC->getBasket();
			if ( !empty($basket) && !empty($basket->vendorsID) ) {
				$vendorID = $basketHelperC->getVendorID( $basket );
				return $vendorID;
			} else {
				return $vendid;
			}
		}
	}

	



	public function getExcludeTax() {
		if ( !empty( $this->_excluding ) ) return $this->_excluding;
				if ( $this->taxType != 1 || empty($this->rate) ) return $this->price;
				$price = $this->price / ( 1 + $this->rate / 100 );

		return $price;

	}

	public function getIncludeTax() {

		if ( !empty( $this->_including ) ) return $this->_including;

				if ( $this->taxType != -1 || empty($this->rate) ) return $this->price;

				$tax = $this->price * ( 1 + ( $this->rate / 100 ) );
		return $tax;
	}
	public function getTax() {
				if ( empty($this->rate) ) return 0;

		if ( isset( $this->_tax ) ) return $this->_tax;

		
		if ( $this->taxType == -1 ) {				$this->_tax = $this->price * ( $this->rate/100 );
		} elseif ( $this->taxType == 1 ) {				$myBasePrice = $this->price / ( 1 + ( $this->rate/100) );
			$this->_tax = $this->price - $myBasePrice;
		} else {
			$message = WMessage::get();
			$message->userW('1369750942JUCS');
		}


		return $this->_tax;


	}
	public function setTax($tax) {
		$this->_tax = $tax;
	}
	public function setInluding($value) {
		$this->_including = $value;
	}
	public function setExluding($value) {
		$this->_excluding = $value;
	}
	public function getRate() {
		return 	$this->rate;
	}

public function renderPrice() {

	WPref::load( 'PCATALOG_NODE_SHOWTAX' );

	

		if ( PCATALOG_NODE_SHOWPRICETAX == 1 ) {
		return $this->getIncludeTax();
	} else {
		return $this->getIncludeTax() - $this->getTax();
	}
}
public function renderTaxMessage($usedCurrency,$convert=false,$curid=0) {

	WPref::load( 'PCATALOG_NODE_SHOWPRICETAX' );
	if ( PCATALOG_NODE_SHOWPRICETAX == 1 ) {
		if ( $this->rate == 0 ) {
			if ( PCATALOG_NODE_SHOWZEROTAX ) {
				return TR1314933746ORYH;
			}			return '';
		}
		$RATE = rtrim( rtrim( $this->rate, '0'), '.' );
		if ( PCATALOG_NODE_SHOWZEROTAX && $RATE == 0 ) {
				return TR1314933746ORYH;
		}		return str_replace(array('$RATE'),array($RATE),TR1314877116BKUQ);

	} else {
		if ( $this->rate == 0 ) {
			if ( PCATALOG_NODE_SHOWZEROTAX ) {
				return TR1314933746ORYH;
			}			return '';
		}		$RATE = rtrim( rtrim( $this->rate, '0'), '.' );

		if ( isset($this->_tax) ) {
			$price = $this->_tax;
		} else {
			$price = $this->getIncludeTax() - $this->getExcludeTax();
		}
		if ( $convert && $usedCurrency != $curid ) {
						static $currencyConvertC = null;
			if (!isset($currencyConvertC) ) $currencyConvertC = WClass::get('currency.convert',null,'class',false );
			$price = $currencyConvertC->currencyConvert( $curid, $usedCurrency, $price );
		}
		$TAXAMONG = WTools::format( $price, 'money', $usedCurrency );
		return str_replace(array('$TAXAMONG','$RATE'),array($TAXAMONG,$RATE),TR1314877116BKUR);

	}
}











	public function formatPriceWithTax($price,$product,$curid=0,$format=true) {

		if ( empty($product->curid) || empty($product->prodtypid) ) {
			return $price;
		}
		if ( empty($curid) ) $curid = WUser::get('curid');

		if ( WGlobals::checkCandy(25) ) {
									$taxUsed = ( !empty($product->pricetaxtype) ) ? $product->pricetaxtype : 0;
			$this->setPrice( $price, $taxUsed, $product->prodtypid );
			$price = $this->renderPrice();
		}
		if ( $curid != $product->curid ) {
						static $currencyConvertC = null;
			if( !isset( $currencyConvertC ) ) $currencyConvertC = WClass::get('currency.convert',null,'class',false );
			$convertedPrice = $currencyConvertC->currencyConvert( $product->curid, $curid, $price );
		} else {
			$convertedPrice = $price;
		}		return ($format) ? WTools::format( $convertedPrice, 'money', $curid ) : $convertedPrice;

	}

private function _setVendorID() {

		$vendorC = WClass::get( 'vendor.helper', null, 'class', false );
	$this->_vendid = $vendorC->getDefault();

}
private function _getRaxRates() {

	if ( !empty(self::$taxRateA[$this->_vendid]) ) {
		return self::$taxRateA[$this->_vendid];
	}
	$zoneHelperC = WClass::get( 'zones.helper' );
	$zoneHelperC->setVendorID( $this->_vendid );
	$basketC = WClass::get('basket.previous' );
	$basket = $basketC->getBasket();
	$zoneHelperC->setBasket( $basket );
	$zoneidA = $zoneHelperC->get( 2 );	

		$prodtypidA = array();
	if ( !empty($basket->Products) ) {
		foreach( $basket->Products as $code => $product ) {
			$prodtypid = $product->prodtypid;
			$prodtypidA[$prodtypid] = $prodtypid;
		}	}

	if ( !empty($this->productType) ) $prodtypidA[$this->productType] = $this->productType;
	$taxHelperC = WClass::get( 'tax.helper' );

		$myLocalTaxRate = $taxHelperC->getTaxBasedOnProductType( $prodtypidA, $zoneidA );

	if ( !empty($myLocalTaxRate) ) self::$taxRateA[$this->_vendid] = $myLocalTaxRate;

}


}