<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Instance_object {







	

function get($pid,$type=null,$lgid=null) {

	static $productInfoA = array();

	if ( empty($lgid) ) $lgid = WUser::get( 'lgid' );
	$key = $pid .'.'.$lgid;
	if ( !isset( $productInfoA[$key] ) ) {
		
				if ( empty($type) ) {
			$type = $this->_loadType();
		}		
				if ( is_numeric($type) ) {
						$productTypeT = WType::get( 'item.designation' );
			$type = strtolower( $productTypeT->getName($type) );
		}		
				$productM = WModel::get( $type );
		$productM->makeLJ( $type.'trans' , 'pid', 'pid', 0, 1 );
		$productM->makeLJ( $type.'.infos' , 'pid', 'pid', 0, 2 );
		$productM->whereLanguage( 1, $lgid );
		if ( is_numeric($pid) ) $productM->whereE( 'pid', $pid );
		else $productM->whereE( 'namekey', $pid );
		$productInfoA[$key] = $productM->load( 'o' );
		
	}		
	return $productInfoA[$key];
		

}

}