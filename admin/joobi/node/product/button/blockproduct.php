<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Blockproduct_button extends WButtons_external {
function create() {

	

	$modelID = WModel::getID('product');

	$block= 'block_'.$modelID;

	$vendid= 'vendid_'.$modelID;

	$eid = WGlobals::get('eid');

	$controller = WGlobals::get('controller');

	$block = $this->_button->_object->elementsData->$block;

	$vendid = $this->_button->_object->elementsData->$vendid;

	if ($block) {

		$text = TR1307975933EESV;		

		
		$this->setIcon( 'unlock' );

	} else {

		$text = TR1310522876DTCC;

		
		$this->setIcon( 'lock' );		

	}
		
		$this->setAddress( 'controller=product&task=blockproduct&block='.!$block.'&eid='.$eid.'&vendid='.$vendid.'&tocontroller='.$controller );

		

		$this->setTitle( $text );	

		

	return true;

	

}}