<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'button.popup' , JOOBI_LIB_HTML );
class Product_Address_button extends WButtons_Popup {
	public function create() {

	
		$eid = WGlobals::getEID();
				if ( empty($eid) ) return false;


		$this->action = $this->_button->elementsData->action;


		return parent::create();

	

	}
}