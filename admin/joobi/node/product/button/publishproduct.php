<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Publishproduct_button extends WButtons_external {
function create() {
	
	$modelID = WModel::getID('product');

	$publish= 'publish_'.$modelID;

	$vendid= 'vendid_'.$modelID;

	$eid = WGlobals::get('eid');

	$controller = WGlobals::get('controller');

	$publish = $this->_button->_object->elementsData->$publish;

	$vendid = $this->_button->_object->elementsData->$vendid;

	if ($publish) {

		$text = TR1206732372QTKO;		

		
		$this->setIcon( 'unpublish' );		

	} else {

		$text = TR1206732372QTKN;

		
		$this->setIcon( 'publish' );

		

	}
	

		
		$this->setTitle( $text );

		
		$this->setAddress( 'controller=product&task=publishproduct&publish='.!$publish.'&eid='.$eid.'&vendid='.$vendid.'&tocontroller='.$controller);

	

		return true;

	

}}