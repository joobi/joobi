<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Addcommission_button extends WButtons_external {
function create() {

	

	if ( !IS_ADMIN ) return false;

	

	$jaffiliates = WApplication::isEnabled( 'com_jaffiliates', true);

	if (!isset($jaffiliates )) return false;	

	$eid=WGlobals::get('eid');

	
	$this->setIcon( 'up' );

	

	$link = WPage::linkPopUp('controller=product&task=addcommision&eid='.$eid );

	

	$this->setAddress( $link );

	

	$this->setPopup();

		

	$this->setTitle( TR1240888717QTOQ );	

	

	return true;

	

}}