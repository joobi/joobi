<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Onlyvendid_filter {


function create() {



	$uid = WUser::get('uid');



	
	if ( !empty( $uid ) ) {

		$vendorHelperC = WClass::get( 'vendor.helper' );

		$vendid = $vendorHelperC->getVendorID( $uid );
		
		if ( empty($vendid) && IS_ADMIN ) {
			$vendid = $vendorHelperC->getDefault();
		}				

	}	

	if ( empty( $vendid ) || empty( $uid ) ) {

		$message = WMessage::get();

		$message->exitNow( 'Unauthorized access 91' );

	} else return $vendid;

	

}
	

}