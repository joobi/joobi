<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Bundle_filter {






function create() {



	$modelObj = $this->model;

	$whereValue = $modelObj->_whereValues;


		$refPIDs = WGlobals::get( 'refpids', 0, 'joobi' );
	if ( empty($refPIDs) ) $this->model->_cancelQuery = true;


	
	if ( !defined( 'PPRODUCT_NODE_PRODTYPETOSHOW' ) ) WPref::get( 'product.node' );

	$prodTypeToShow = PPRODUCT_NODE_PRODTYPETOSHOW;

	switch ($prodTypeToShow) {

		case 1: 
			
			$objToInsert = null;

			$objToInsert->champ = 'type';

			$objToInsert->cond = '=';

			$objToInsert->value = '1';

			$objToInsert->asi1 = 0;

			$objToInsert->bkbefore = 0;

			$objToInsert->bkafter = 0;

			$objToInsert->operator = 0;

			$objToInsert->type = 'default';



			$whereValue[] = $objToInsert;

			break;

		case 2: 
			
			$objToInsert = null;

			$objToInsert->champ = 'type';

			$objToInsert->cond = '>=';

			$objToInsert->value = '241';

			$objToInsert->asi1 = 0;

			$objToInsert->bkbefore = 0;

			$objToInsert->bkafter = 0;

			$objToInsert->operator = 0;

			$objToInsert->type = 'default';



			$whereValue[] = $objToInsert;



			
			$objToInsert = null;

			$objToInsert->champ = 'type';

			$objToInsert->cond = '<=';

			$objToInsert->value = '245';

			$objToInsert->asi1 = 0;

			$objToInsert->bkbefore = 0;

			$objToInsert->bkafter = 0;

			$objToInsert->operator = 0;

			$objToInsert->type = 'default';



			$whereValue[] = $objToInsert;

			break;

		default: 
			break;

	}


	
	$objToInsert = null;

	$objToInsert->champ = 'bundle';

	$objToInsert->cond = '>';

	$objToInsert->value = '0';

	$objToInsert->asi1 = 0;

	$objToInsert->bkbefore = 0;

	$objToInsert->bkafter = 0;

	$objToInsert->operator = 0;

	$objToInsert->type = 'default';



	$whereValue[] = $objToInsert;



	return $refPIDs;

}}