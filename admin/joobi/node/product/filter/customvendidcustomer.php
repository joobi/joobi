<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Product_Customvendidcustomer_filter {
	function create() {

		if ( IS_ADMIN ) return false;

		else {

			$uid = WUser::get('uid');

			static $vendidA = null;

			

			
			
	 		static $userRoleA = array();

	 		if ( !isset( $userRoleA[ $uid ] ) ) {

	 			$roleC = WRole::get();

				$userRoleA[ $uid ] = $roleC->hasRole( 'storemanager' );

	 		}
			if ( $userRoleA[ $uid ] ) {

				$eid = WGlobals::getEID();

				if ( !empty( $eid ) ) $vendidA[ $uid ] = $eid;

				else return false;

			}
			

			
			if ( !isset( $vendidA[ $uid ] ) && !empty( $uid ) ) {

				$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

				$vendidA[ $uid ] = $vendorHelperC->getVendorID( $uid );

			}
	

			if ( !empty( $vendidA[ $uid ] ) ) {

				static $orderProductM = null;

				if ( empty( $orderProductM ) ) $orderProductM = WModel::get( 'order.products' );

				$orderProductM->whereE( 'vendid', $vendidA[ $uid ] );

				$orderProductM->groupBy( 'oid' );

				$orderProductM->setLimit( 1000 );

				$oidsA = $orderProductM->load( 'lra', 'oid' );

			} else {

				$message = WMessage::get();

				$message->exitNow( 'Unauthorized access 92' );

			}
			 

			if ( !empty( $oidsA ) ) return $oidsA;

			else return array( -1, -2 );

		}
		

	}}