<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Customoidcustomer_filter {
function create() {

	if ( IS_ADMIN ) return false;

	else {

		$profileUID = WGlobals::getEID();

		if ( empty( $profileUID ) ) {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 96' );

		}
		

		$uid = WUser::get('uid');

		

		static $vendidA = null;

		
		if ( !isset( $vendidA[ $uid ] ) && !empty( $uid ) ) {

			$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

			$vendidA[ $uid ] = $vendorHelperC->getVendorID( $uid );

		}


		if ( !empty( $vendidA[ $uid ] ) ) {

			static $orderM = null;

			if ( empty( $orderM ) ) $orderM = WModel::get( 'order' );

			$orderM->makeLJ( 'order.products', 'oid' );

			$orderM->select( 'oid' );

			$orderM->whereE( 'uid', $profileUID );

			$orderM->whereE( 'vendid', $vendidA[ $uid ], 1 );

			$orderM->setLimit( 1000 );

			$oidsA = $orderM->load( 'lra' );	

		} else {

			$message = WMessage::get();

			$message->exitNow( 'Unauthorized access 95' );

		}
		 

		if ( !empty( $oidsA ) ) return $oidsA;

		else return array( -1, -2 );

	}
	

}}