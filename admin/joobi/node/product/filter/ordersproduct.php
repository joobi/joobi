<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Ordersproduct_filter {
	

function create() {

	$oid = WGlobals::get('eid');

	$pidA = array();

	if (!empty($oid)) {

		$orderM=WModel::get('order.products');

		$orderM->whereE('oid', $oid);

		$orderM->select('pid');

		$pidA = $orderM->load('lra');	

	}
	
	if(empty($pidA)) return false;
	

	return $pidA;

}}