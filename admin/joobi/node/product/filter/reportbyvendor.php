<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Product_Reportbyvendor_filter {

	function create(){

		if ( IS_ADMIN ) {

			$vendid = WGlobals::get( 'vendid', 0 );
			if ( !empty( $vendid ) ) WGlobals::setSession( 'reportstatsvendorid', 'vendid', $vendid );
			else $vendid = WGlobals::getSession( 'reportstatsvendorid', 'vendid', 0 );

			
			
			return !empty( $vendid ) ? $vendid : false;

		} else {
			$uid = WUser::get('uid');

						if ( !empty( $uid ) ) {
				$vendorHelperC = WClass::get('vendor.helper',null,'class',false);
				$vendid = $vendorHelperC->getVendorID( $uid );
			}
						$roleC = WRole::get();
			$hasRole = $roleC->hasRole( 'storemanager' );

			if ( $hasRole ) {
				$vendorHelperC = WClass::get('vendor.helper',null,'class',false);
				$vendid = $vendorHelperC->getDefault();
			}
	 		if ( empty( $vendid ) || empty( $uid ) ) {
				$message = WMessage::get();
				$message->exitNow( 'Unauthorized access 90' );
			} else return $vendid;
		}	}
}