<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;





class Zones_Helper_class extends WClasses {



	var $vendid = 0;

	var $zoneType = 0;



	private $_vendid = 0;	


	private $_basket = null; 














public function setBasket(&$O1) {

	$this->_basket =& $O1;

}














public function initialize($O2=2) {



	
	if ( empty($this->_basket) ) {

		$IJ6 = WClass::get( 'basket.previous' );

		$IJ7 = $IJ6->getBasket();

		$this->setBasket( $IJ7 );

	}


	$IJ8 = new stdClass;

	if ( empty($this->_basket->adid) ) {	
		
		$IJ9 = WClass::get( 'iptracker.lookup', null, 'class', false );

		if ( !empty($IJ9) ) {

			$IJA = $IJ9->ipInfo( null, 'ctyid' );

			$IJB = $IJ9->ipInfo( null, 'stateid' );



			if ( !empty($IJA) ) $IJ8->ctyid = $IJA;

			if ( !empty($IJB) ) $IJ8->stateid = $IJB;

		}


	}


	$this->l1z_11_64p( $O2, $IJ8 );

	$this->_basket->zone = $this->_basketZone;



	return true;

}














public function loadFormAddress($O3=1) {



	$IJC = ( 2==$O3 || 1==$O3 ) ? true : false;

	$IJD = ( 3==$O3 || 1==$O3 ) ? true : false;



	if ($IJC) $this->l1z_11_64p( $O3, null );

	if ($IJD) $this->l1z_11_64p( $O3, null );



	$this->_basket->zone = $this->_basketZone;



}








public function setVendorID($O4) {

	$this->_vendid = $O4;

}














public function get($O5=2,$O6=false) {	
	$IJC = ( 2==$O5 || 1==$O5 ) ? true : false;

	$IJD = ( 3==$O5 || 1==$O5 ) ? true : false;



	if ( !$O6 ) {





		
		if ( empty($this->_basket) ) {

			$IJ6 = WClass::get('basket.previous' );

			$IJ7 = $IJ6->getBasket();

			$this->setBasket( $IJ7 );

		}




		if ( $IJC && !empty($this->_basket->zone['tax']) ) return $this->_basket->zone['tax']->id;

		if ( $IJD && !empty($this->_basket->zone['shipping']) ) return $this->_basket->zone['shipping']->id;

	}



	return $this->l1z_11_64p( $O5 );



}
















private function l1z_11_64p($O7=2,$O8=null) {	
	$IJC = ( 2==$O7 || 1==$O7 ) ? true : false;

	$IJD = ( 3==$O7 || 1==$O7 ) ? true : false;



	
	if ( empty($this->_basket) ) {

		$IJ6 = WClass::get('basket.previous' );

		$IJ7 = $IJ6->getBasket();

		$this->setBasket( $IJ7 );

	}




	$IJE = ( !empty($this->_basket->adidbilling) ? $this->_basket->adidbilling : ( !empty($this->_basket->adid) ? $this->_basket->adid : 0 ) );

	$IJF = ( !empty($this->_basket->adid) ? $this->_basket->adid : 0 );



	if ( empty( $this->_vendid ) ) {

		if ( !defined('PBASKET_NODE_DIRECTPAY') ) WPref::get( 'basket.node' );

		if ( PBASKET_NODE_DIRECTPAY ) {		
			$this->_vendid = !empty($this->_basket->vendorsID) ? key($this->_basket->vendorsID) : 0;

		} else {

			$this->_vendid = 0;

		}
	}





	
	if ( empty($this->_vendid) ) {

		$IJG = WClass::get( 'vendor.helper' );

		$this->_vendid = $IJG->getDefault();

	}


	$IJH = 0;





	if ( $O7==1 ) {

		
		$IJH = $this->l110_12_6dl( $IJE, 2, $O8 );

		
		$IJH = $this->l110_12_6dl( $IJF, 3, $O8 );

	} elseif ( $O7==2 ) {

		
		$IJH = $this->l110_12_6dl( $IJE, 2, $O8 );

	} else {

		
		$IJH = $this->l110_12_6dl( $IJF, 3, $O8 );

	}




	return $IJH;



	






















































}




	private function l110_12_6dl($O9,$OA,$OB) {





		
		if ( empty($O9) && empty($OB) ) {

			$IJI = WExtension::exist('iptracker.node');

			if ( $IJI && WGlobals::checkCandy(50) ) {

				
				$IJJ = WClass::get('iptracker.lookup');

				$IJK = $IJJ->ipInfo( null, 'ctyid' );

				if ( !empty($IJK) ) {

					$OB = new stdClass;

					$OB->ctyid = $IJK;

				}
			}
		}



	
		$IJL = 'default';

		$IJM  = '';

		if ( !empty($O9) || !empty($OB) ) {




			$IJN = !empty( $O9 ) ? $O9 : 0;

			
			$IJH = $this->l1a_13_6f1( $IJN, $OB, $OA );



			if ( !empty($OB->stateid) ) {

				$IJL = 'state';

				$IJM = $OB->ctyid . ':' . $OB->stateid;

			} elseif ( !empty($OB->ctyid) ) {

				$IJL = 'country';

				$IJM = $OB->ctyid;

			} else {

				$IJL = 'adid';

				if ( !empty($O9) ) $IJM = $O9;

				else $IJM = '';

			}
		}


		if ( empty($IJH) ) {

			
			$IJH = $this->getDefaultZone( null, $OA );

		}




		$IJC = ( 2==$OA || 1==$OA ) ? true : false;

		$IJD = ( 3==$OA || 1==$OA ) ? true : false;



		
		$IJO = new stdClass;

		$IJO->source = $IJL;

		$IJO->key = $IJM;

		$IJO->id = $IJH;

		if ( $IJC ) $this->_basket->zone['tax'] = $IJO;

		if ( $IJD ) $this->_basket->zone['shipping'] = $IJO;

		$this->_basketZone = $this->_basket->zone;



		return $IJH;



	}
















private function l1a_13_6f1($OC,$OD=null,$OE) {



	if ( empty($OD->ctyid) ) {

		$IJP = WClass::get( 'address.helper' );

		$IJQ = array( 'ctyid', 'stateid' );

		$OD = $IJP->getAddress( $OC, $IJQ );

	}


	
	
	if ( !empty($OD->ctyid) ) {



		$IJR = !empty($OD->stateid) ? $OD->stateid : 0;

		$IJH = $this->l1b_14_73e( $OD->ctyid, $IJR, $OE );

		if ( empty($IJH) ) {

			$this->l1c_15_5ss( $OE, $this->_vendid );

			return null;

		}


		return $IJH;



	} else return false;





}




















public function getDefaultZone($OF=null,$OG=3,$OH=true) {

	static $IJS = array();



	if ( empty( $OF ) ) {

		$OF = $this->_vendid;

	}


	$IJT = $OF . '-';








	$IJT .= $OG;



	if ( isset($IJS[$IJT]) ) return $IJS[$IJT];



	$IJU = WModel::get( 'zones' );



	$IJU->select( 'zoneid' );

	$IJU->whereE( 'publish', 1 );

	$IJU->whereE( 'vendid', $OF );

	switch( $OG ) {

		case 2:	
			$IJU->whereIn( 'type', array( 1, 2 ) ); 
			break;

		case 3:	
			$IJU->whereIn( 'type',  array( 1, 3 ) ); 
			break;

		case 1:

		default;

			$IJU->orderBy( 'type', "ASC" );


			break;

	}


	$IJU->orderBy( 'premium', 'DESC' );

	$IJU->orderBy( 'type', 'DESC' );

	$IJH = $IJU->load( 'lr' );



	
	if ( empty($IJH) ) {

		$IJV = WMessage::get();

		if ( empty($OF) ) {

			$IJV->userE('1314876783PFUZ');

			return null;

		} elseif ( $OH ) {

			
			$IJW = $this->getDefaultZone( $OF, 1, false );



			if ( $IJW ) {

				
				$this->getDefaultZone( $OF, $OG, false );

				$IJH = $this->createDefaultZone( $OF, $OG );

			} else {

				
				$IJH = $this->createDefaultZone( $OF, 1 );

			}


			
			
			if ( empty($IJH) ) {

				$this->l1c_15_5ss( $OG, $OF );

				return null;

			}


		}


	}


	$IJS[$IJT] = $IJH;



	return $IJH;



}




	private function l1c_15_5ss($OI,$OJ) {



		
		$IJG = WClass::get( 'vendor.helper' );

		$IJX = $IJG->getVendor( $OJ );

		$IJY = $IJX->name;

		$IJV = WMessage::get();



		switch( $OI ) {

			case 2:	
				
				$IJV->userE('1323981163OXSO',array('$VENDORNAME'=>$IJY));

				break;

			case 3:	
				$IJV->userE('1323981163OXSP',array('$VENDORNAME'=>$IJY));

				break;

			case 1:

			default;

				$IJV->userE('1323981163OXSQ',array('$VENDORNAME'=>$IJY));

				break;

		}


	}














private function l1b_14_73e($OK,$OL=0,$OM) {

	static $IJZ = array();



	$IJT = 	$OK . '.' . $OL . '.' . $OM . '.' . $this->_vendid;



	if ( !isset( $IJZ[$IJT] ) ) {

		static $IJU = null;

		if ( empty( $IJU ) ) $IJU = WModel::get( 'zones' );




		if ( !empty($this->_vendid) ) $IJU->whereE( 'vendid', $this->_vendid );

		$IJU->makeLJ( 'zones.countries', 'zoneid', 'zoneid', 0, 1 );

		$IJU->select( 'zoneid' );

		$IJU->whereE( 'publish', 1 );



		switch( $OM ) {

			case 2:	
				$IJU->where( 'type', '!=', 3 ); 
				break;

			case 3:	
				$IJU->where( 'type', '!=', 2 ); 
				break;

			case 1:

			default;

				$IJU->orderBy( 'type', "ASC" );


				break;

		}


		$IK0 = WPref::load( 'PBASKET_NODE_ZONEUSESTATES' );



		if ( empty($OL) || !$IK0 ) {

			$IJU->whereE( 'ctyid', $OK, 1, null, 1 );

			$IJU->whereE( 'ctyid', 0, 1, null, 0, 1, 1 ); 
		} else {

			$IJU->whereE( 'ctyid', $OK, 1, null, 2 );

			$IJU->whereE( 'stateid', $OL, 1, null, 0, 1 );



			$IJU->whereE( 'ctyid', $OK, 1, null, 1, 0, 1 );

			$IJU->whereE( 'stateid', 0, 1, null, 0, 1, 0 );



			$IJU->whereE( 'ctyid', 0, 1, null,  0, 1, 1 ); 


			$IJU->orderBy( 'stateid', 'DESC', 1 );

		}


		$IJU->orderBy( 'ctyid', 'DESC', 1 );

		$IJU->orderBy( 'type', 'DESC', 0 );




		$IJZ[$IJT] = $IJU->load( 'lr' );

	}








	return $IJZ[$IJT];

}


















function createDefaultZone($ON,$OO=1) {



	if ( empty( $ON ) ) return false;



	
	$IJW = $this->getDefaultZone( $ON, $OO, false );



	if ( $IJW ) return true;



	


	
	$IJG = WClass::get( 'vendor.helper' );

	$IJX = $IJG->getVendor( $ON );

	if( !is_numeric( $ON ) ) $ON = $IJX->vendid;



	WText::load( 'zones.node' );

	$IJU = WModel::get( 'zones' );

	$IJU->name = $IJU->description =   $IJX->name . ' ' . TR1369750691IKGD;

	$IJU->type = $OO;

	$IJU->publish = 1;

	$IJU->premium = 1;

	$IJU->vendid = $ON;

	$IJU->returnId();

	$IJU->save();



	
	if ( !empty( $IJU->zoneid ) ) {

		$IK1 = WModel::get( 'zones.countries' );

		$IK1->zoneid = $IJU->zoneid;

		$IK1->save();

	}


	return $IJU->zoneid;

}


}
