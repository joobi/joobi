<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Zones_Zones_taxes_zones_multiforms_view extends Output_Forms_class {
	protected function prepareView() {


		$IHZ =  WPref::load( 'PBASKET_NODE_ZONEUSESTATES' );

		if ( empty($IHZ) ) {
			$this->removeElements( array('countries_states_fieldset', 'countries_states_stateid' ) );
		} else {
						$II0 = WGlobals::getEID();

			if ( !empty( $this->_eid ) ) {
				$II1 = WModel::get( 'zones.countries' );
				$II1->whereE( 'zoneid', $this->_eid );
				$II2 = $II1->load( 'lra', 'stateid' );

				WGlobals::set( 'countriesStatesSelected', $II2, 'joobi' );
			}
		}

	return true;



	}}