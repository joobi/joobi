<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Zones_Node_model extends WModel {


	private $_statesID = null;

	private $_countryID = null;

	private $_onlyCountry = false;	




	
	function addValidate() {



		if ( empty($this->author) ) $this->author = WUser::get( 'uid' );



		
		if ( empty( $this->vendid ) ) {

			$IK8 = (!IS_ADMIN) ? WUser::get( 'uid' ) : 0;

			$IK9 = WClass::get( 'vendor.helper' );

			$this->vendid = $IK9->getVendorID( $IK8, true );

		}


		return true;

	}




	function validate() {



		$IKA = WPref::load( 'PBASKET_NODE_ZONEUSESTATES' );



		if ( $IKA ) {



			$this->_statesID = $this->getX( 'stateid' );



			if ( !empty( $this->_statesID ) ) {

				
				$IKB = $this->getChild( 'zones.countries', 'ctyid' );

				if ( count($IKB) > 1 ) {

					$IKC = WMessage::get();

					$IKC->historyW('1330118255PYRJ');

				}
	
				$this->_countryID = $IKB[0];



				
				$IKD = WModel::get( 'states' );

	
				$IKD->whereIn( 'stateid', $this->_statesID );

				$IKE = $IKD->load( 'ol', array('stateid', 'ctyid', 'name') );



				
				if ( !empty($IKE) ) {

					$IKC = WMessage::get();

					$IKF = array();

					foreach( $IKE as $IKG ) {

						if ( $IKG->ctyid != $this->_countryID ) {

							$IKH = $IKG->name . ' ';

							if ( !isset($IKI) ) $IKI = WClass::get( 'countries.helper' );

							$IKJ = ' ' . $IKI->getData( $IKG->ctyid, 'name' );	;

							$IKC->userW('1357432386GYUF',array('$STATENAME'=>$IKH,'$COUNTRYNAME'=>$IKJ));

							$IKF[$IKG->stateid] = true;

						}
					}


					
					$IKK = array();

					foreach( $this->_statesID as $IKL ) {

						if ( !isset($IKF[$IKL]) ) $IKK[] = $IKL;

					}


					$this->_statesID = $IKK;

					if ( empty($this->_statesID) ) $this->_onlyCountry = true;

	
				}
	


				
				$this->unsetChild(  'zones.countries' );	


			}


		}
		return true;



	}




	function extra() {



		$IKA = PBASKET_NODE_ZONEUSESTATES;



		if ( $IKA ) {



			if ( !empty($this->_statesID) && !empty( $this->_countryID ) ) {



				
				$IKM = WModel::get( 'zones.countries' );

	
				$IKM->whereE( 'zoneid', $this->zoneid );

				$IKM->delete();



				$IKN = array();

				
				foreach( $this->_statesID as $IKG ) {

					$IKN[] = array( $this->zoneid, $this->_countryID, $IKG );

				}


				
				$IKM = WModel::get( 'zones.countries' );



				$IKM->insertMany( array( 'zoneid', 'ctyid', 'stateid' ), $IKN );



			} elseif ( $this->_onlyCountry ) {

				$IKM = WModel::get( 'zones.countries' );

				$IKM->whereE( 'zoneid', $this->zoneid );

				$IKM->delete();



				$IKM = WModel::get( 'zones.countries' );

				$IKM->insertMany( array( 'zoneid', 'ctyid', 'stateid' ), array( array( $this->zoneid, $this->_countryID, 0 ) ) );



			}
		}


		return true;



	}



}