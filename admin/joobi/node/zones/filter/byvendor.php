<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Zones_Byvendor_filter {
	

	function create() {

		

		$II3 = WClass::get('vendor.helper',null,'class',false);

		if ( IS_ADMIN ) {

			$II4 = $II3->getVendorID( 0, true );

			return $II4;

		} else {

			$II5 = WUser::get('uid');

				

			static $II6 = null;

			
			if ( !isset( $II6[ $II5 ] ) && !empty( $II5 ) ) {

				$II6[ $II5 ] = $II3->getVendorID( $II5 );

			}
			

	 		if ( empty( $II6[ $II5 ] ) || empty( $II5  ) ) {

				$II7 = WMessage::get();

				$II7->exitNow( 'Unauthorized access 117' );

			} else return $II6[ $II5 ];

		}
	}
	
}