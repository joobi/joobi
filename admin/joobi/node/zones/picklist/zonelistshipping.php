<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Zones_Zonelistshipping_picklist extends WPicklist {

function create() {

	$IIC = WClass::get( 'vendor.helper' );
	

	static $IID=null;
	if ( empty( $IID ) ) {
		static $IIE=null;
		if ( empty( $IIE ) ) $IIE = WModel::get( 'zones' );
		$IIE->whereE( 'publish', 1 );
		$IIE->where( 'type', '!=', 2 );
		
		$IIF = WClass::get('vendor.helper',null,'class',false);
		if ( !IS_ADMIN ) {
						$IIG = WUser::get( 'uid' );	
			$IIH = $IIF->getVendorID( $IIG );
			if ( empty($IIH) ) return true;
		} else {
						$IIH = $IIF->getDefault();
		}		$IIE->whereE( 'vendid', $IIH );
		
		$IIE->orderBy( 'name' );
		$IID = $IIE->load( 'ol', array( 'zoneid', 'name' ) );
	}
	if ( !empty( $IID ) ) {
		$III = null;
		foreach( $IID as $IIJ ) {
			$IIK = $IIJ->zoneid;
			$IIL = $IIJ->name;
			
			$this->addElement( $IIK, $IIL );
		}	} else {
		$IIM = WMessage::get();
		$IIM->historyW('1350062875THIR');
	}
	return true;
}



}