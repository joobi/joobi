<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Zones_Zonetypes_picklist extends WPicklist {
function create() {

	$this->addElement( 3, TR1213180320PQFZ );
	$this->addElement( 2, TR1206961911NYAQ );
	$this->addElement( 1, TR1337273111QYGE );
	
	return true;
}}