<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Zones_Zoneslistbe_picklist extends WPicklist {



function create() {



	static $IIX=null;

	if ( empty( $IIX ) ) {

		static $IIY=null;

		if ( empty( $IIY ) ) $IIY = WModel::get( 'zones' );
		
		$IIZ = WUser::get( 'uid' );	
		$IJ0 = WClass::get('vendor.helper',null,'class',false);
		$IJ1 = $IJ0->getVendorID( $IIZ );
		if ( empty($IJ1) && !IS_ADMIN ) return true;
		$IIY->whereE( 'vendid', $IJ1 );
		

		$IIY->whereE( 'publish', 1 );

		$IIY->orderBy( 'name' );

		$IIX = $IIY->load( 'ol', array( 'zoneid', 'name' ) );

	}


	if ( !empty( $IIX ) ) {

		$IJ2 = null;

		foreach( $IIX as $IJ3 ) {

			$IJ4 = $IJ3->zoneid;

			$IJ5 = $IJ3->name;

			





			

			$this->addElement( $IJ4, $IJ5 );

		}
	}


	return true;

}}