<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Zones_Dropcountriesfilter_picklist extends WPicklist {












	function create() {

		static $II8=null;

		if ( empty( $II8 ) ) {

			static $II9=null;

			if ( empty($II9) ) $II9 = WModel::get('countries');


			$II9->whereE( 'publish', 1 );				$II9->setDistinct();
			$II9->orderBy('name');

			$II8 = $II9->load('ol', array( 'ctyid', 'name', 'namekey' ) );

		}


		if (!empty($II8)) {

			$IIA = null;

			foreach($II8 as $IIB) {



				if ( $IIA != strtoupper( $IIB->namekey[0] ) ) {

					$IIA = strtoupper( $IIB->namekey[0] );

					$this->addElement(-1, '-- '. strtoupper($IIA) .' -----');

				}


				$this->addElement($IIB->ctyid, $IIB->name);

			}
		}
		return true;

	}

}