<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Zones_toggle_controller extends WController {
function toggle() {


		$IK2 = $this->subTask[1];
		if ( $IK2=='premium' ) {
					
			$IK3 = WGlobals::getEID();
	
						$IK4 = WModel::get( 'zones' );
			$IK4->whereE( 'zoneid', $IK3 );
			$IK5 = $IK4->load( 'lr', 'vendid' );
			
						$IK4->whereE( 'vendid', $IK5 );
			$IK4->whereE( 'premium', 1 );
			$IK4->where( 'type', '!=', 1 );
			$IK4->setVal( 'premium', 0 );
			$IK4->update();

			$IK4->resetAll();
			$IK4->whereE( 'zoneid', $IK3 );
			$IK4->setVal( 'premium', 1 );
			$IK4->update();
			
			WPage::redirect( 'controller=zones' );
			return true;
		}


	return parent::toggle();

}}