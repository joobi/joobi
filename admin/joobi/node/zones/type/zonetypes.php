<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Zones_Zonetypes_type extends WTypes {


var $zonetypes = array(

	'3' => 'Shipping',

	'2' => 'Tax',

	'1' => 'Tax and Shipping'

);
}