<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Files_Types_type extends WTypes {






public $types = array(

		'' => 'None',

		'url' => 'URL',

		'file' => 'File',			'x1x1' => '--Video Sites',

		'youtube' => 'Youtube',

		'vimeo' => 'Vimeo',
		'livevideo' => 'Live Video',

		'yahoovideo' => 'Yahoo Video',

		'espn' => 'ESPN Sports',

		'x2x2' => '--Social Network Sites',

		'myvideo' => 'MyVideo',
		'gametrailers' => 'Game Trailers'





	);


}