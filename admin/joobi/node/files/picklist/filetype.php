<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Files_Filetype_picklist extends WPicklist {


function create() {

	static $I62 = null;

	

	$I62 = WModel::get( 'files' );

	$I62->select( 'type' );

	$I62->groupBy( 'type' );

	$I63 = $I62->load('lra');

		

	foreach( $I63 as $I64 ) {

		if ( empty($I64) ) continue;

		$this->addElement(  $I64, $I64 );

	}
	

	return true;

	

}}