<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Files_Types_picklist extends WPicklist {








function create() {



	$I65 = WType::get('files.types');

	$I66 = $I65->getList();



	
	$I67 = ( !empty($this->defaultValue) ? current( $this->defaultValue ) : '');

	$I68 = 'file';

	if ( !empty($I67) ) {

		
		if ( !$I65->inValues($I67) ) {

			
			$I68 = $I67;

		}
	}


	foreach( $I66 as $I69 => $I6A ) {



		if ( $I69 != 'file' ) $this->addElement( $I69 , $I6A );

		else {

			$this->addElement( $I68 , $I68 );

		}


	}


	return true;



}}