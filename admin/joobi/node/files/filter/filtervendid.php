<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Files_Filtervendid_filter {
function create() {



	if ( IS_ADMIN ) return false;

	else {

		$I5Y = WUser::get('uid');	

		

		static $I5Z = null;

		
		if ( !isset( $I5Z[ $I5Y ] ) && !empty( $I5Y ) ) {

			$I60 = WClass::get('vendor.helper',null,'class',false);

			$I5Z[ $I5Y ] = $I60->getVendorID( $I5Y );

		}
		

 		if ( empty( $I5Z[ $I5Y ] ) || empty( $I5Y  ) ) {

			return '0';

		} else return $I5Z[ $I5Y ];

	}
	

}}