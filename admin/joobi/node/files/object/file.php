<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Files_File_object {

	public $name = ''; 	public $type = ''; 	public $fileID = ''; 	public $basePath = '';		public $path = '';

	public $thumbnail = false;	
	public $secure = false; 	
	public $storage = null;	





	public function fileURL() {

		$I5S = WGet::file( $this->storage );
		if ( empty($I5S) ) return false;

		$I5S->setFileInformation( $this );
		return $I5S->fileURL();
	}
}