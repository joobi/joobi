<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Files_Alias_listing extends WListings_standard {
function create() {

if ( empty($this->value) ) $this->value = $this->getValue('name', 'files') . '.' . $this->getValue('type');

return parent::create();

}}