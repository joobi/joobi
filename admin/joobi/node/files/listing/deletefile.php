<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Files_Deletefile_listing extends WListings_standard {

function create() {

	$IB4= WGlobals::get('pid');

	$IB5= WGlobals::get('map');

	if (empty($IB5)) $IB5 = 'filid';

	$IB6 = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/delete.png';

	$IB7 = '<img src="'. $IB6 .'"/>';

	$IB8 =  WPage::linkPopUp( 'controller=files-attach&task=delete&pid='.$IB4.'&eid='. $this->value.'&map='. $IB5 ) ;

	$IB9 = TR1318336000EBWT;

	$this->content = '<a style="cursor: pointer;" onclick=" var yes = confirm(\''.$IB9.'\'); if (yes) { window.location=\''.$IB8.'\';}" >'. $IB7 .'</a>';

	return true;

}
}