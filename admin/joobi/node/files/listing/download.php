<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Files_Download_listing extends WListings_standard {
function create() {



	if ( $this->getValue( 'secure', 'files') ) {	
		

		
		$IAX = $this->getValue( 'uid', 'files' ); 

		$IAY = WUser::get( 'uid' );

		
		$IAZ = WRole::get();

		$IB0 = $IAZ->hasRole( 'storemanager' );

		

		if ( $IAY != $IAX && !$IB0 ) {

			$this->content = TR1326149408DUCB;

			return true;

		}		

		$IB1 = WPage::routeURL( 'controller=files&task=securedownload&eid='. $this->getValue( 'filid' ) );

		

	} else {
		
		$IB2 = $this->getValue( 'type', 'files');
		if ( $IB2 == 'url' ) {
			$IB1 = $this->getValue( 'name', 'files');
		} else {
			$IB3 = JOOBI_URL_MEDIA;
			$IB3 .= str_replace( '|', '/', $this->getValue( 'path', 'files') ) . '/';
			$IB1 =  $IB3 . $this->getValue( 'name', 'files') . '.' . $this->getValue( 'type', 'files');
		}		

	}
	

	$this->content = '<a target="_blank" href="'.$IB1.'">'.TR1206961905BHAV.'</a>';



	return true;

}}