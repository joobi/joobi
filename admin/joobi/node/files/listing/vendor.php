<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Files_Vendor_listing extends WListings_standard {
function create() {



	if (!IS_ADMIN) return false;

	

	if (empty($this->value)) $this->value = 1;

	

	static $IBA=null;

	if ( !isset($IBA) ) $IBA = WClass::get('vendor.helper', null, 'class', false );

	

	if ( !WExtension::exist( 'vendors.node' ) ) {		

		 $IBB = $IBA->getVendor($this->value);		 

		 $IBC = ( isset( $IBB->name ) && !empty( $IBB->name ) ) ? $IBB->name : WUser::get('name', $IBB->uid);

	} else {		

		$IBC = ( !empty($IBA) ) ? $IBA->showVendName($this->value, 0, true) : '';

	}
	

	$this->content = $IBC;

	return true;



}}