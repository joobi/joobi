<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Files_Editbutton_listing extends WListings_standard {
function create() {

	$IBE= WGlobals::get('pid');

	$IBF = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/edit.png';

	$IBG = '<img src="'. $IBF .'"/>';

	$IBH =  WPage::linkPopUp( 'controller=files-attach&task=edit&pid='. $IBE .'&eid='. $this->value ) ;	

	$this->content = WPage::createPopUpLink( $IBH, $IBG, 700, 450 );

return true;

}}