<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Files_Media_class extends WClasses {

	var $videoWidth = 250; 	var $videoHeight =  200; 	var $audioWidth = 200; 	var $audioHeight =  25; 	var $transparency = 'transparent';
	var $autoplay = 'true';
	var $bgcolor = '838B8B';	
	var $fileType = ''; 
	private $_myNewImageO = null;

	private $_myFileInfoO = null;

	private static $IDCount = 0;
	private static $loadJMPlayer = true;

	private static $mediaID = array();







	public function getMediaID($O1) {
		if( !empty( self::$mediaID[$O1] ) ) return self::$mediaID[$O1];
		return null;
	}
	public function getPlayerType() {
		$I6X = WPref::load( 'PFILES_NODE_JWPLAYER' );
		if ( $I6X ) return 'jwplayer';
		else return 'standard';
	}






    public function renderHTML($O2,$O3=null) {
    	if ( !isset($O3) ) $O3 = new stdClass;
    	if ( !isset($O3->fileicon) ) $O3->fileicon = null;
    	if ( !isset($O3->advmedia) ) $O3->advmedia = true;

    	WText::load( 'files.node' );
    	$I6Y = '';

		$I6Z = WClass::get( 'files.helper' );
		$this->_myFileInfoO = $I6Z->getInfo( $O2 );

				if( empty($this->_myFileInfoO) ) return '';

		$this->_myFileInfoO->filename = $this->_myFileInfoO->name . '.' . $this->_myFileInfoO->type;

		if ( $this->_myFileInfoO->secure == 1 ) {
			$I6Y .= $this->_myFileInfoO->filename.' <strong><em>('.TR1298350386KTQO. ') </em></strong>';
			return $I6Y;
		} else {
			$I70 = $this->l1u_M_2eg();
		}
				if ( empty($O3->advmedia) ) {

			if ( $this->_myFileInfoO->type!='url' ) $I6Y .= '<a href="'.$I70.'">' . $this->displayIcon( $this->_myFileInfoO ) .$this->_myFileInfoO->filename.'</a>'; 			else $I6Y .= '<a href="'.$this->_myFileInfoO->name.'" onclick="window.open(\''.$this->_myFileInfoO->name.'\', \'win2\', \'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes\')">'.$this->_myFileInfoO->name.' </a>';
			return $I6Y;
		}

						if (in_array( $this->_myFileInfoO->type, array( 'jpg', 'jpeg', 'gif', 'png', 'bmp' ) ) ) {
			$this->fileType = 'image';
			$I6Y .= $this->image( null, $O3 );

		} elseif ( in_array( $this->_myFileInfoO->type, array( 'mp3', 'wav', 'aiff', 'wma' ) ) ) { 
			$this->fileType = 'audio';
			$I6Y .= $this->l1v_N_79l();

		} elseif ( in_array( $this->_myFileInfoO->type, array( '3gp','flv', 'divx', 'mov', 'mp4', 'm4v', 'mov', 'swf', 'wmv') ) ) {
						$this->fileType = 'video';
			$I6Y .= $this->video();

		} else {	
			$I6Y = $this->l1w_O_6am( $this->_myFileInfoO );

		}
		return $I6Y;

	}









	public function setMediaStyle($O4,$O5,$O6=null,$O7=null) {

		if ( !empty($O4) ) $this->videoWidth = (int)$O4;
		if ( !empty($O5) ) $this->videoHeight = (int)$O5;
		if ( isset($O6) ) $this->autoplay = (string)$O6;
		if ( isset($O7) ) $this->bgcolor = $O7;

	}









	public function renderExternalFile($O8,$O9='',$OA='') {

		if ( empty($OA) && empty($O9) ) {
						$I71 = WClass::get( 'files.analyze' );
			$O8 = $I71->getTypeAndName( $O8, $OA );
		}
		$I72 = new stdClass();
		$I72->filid = rand( 100, 999 );			$I72->name = $O8;
		if ( empty($OA) ) {
			$I72->type = 'mp4';
		} else {
			$I72->type = $OA;
		}
		$I72->path = $O9;
		$I72->folder = '';
		$I72->storage = 0;
		$I72->secure = 1;

		$I6Y = $this->l1w_O_6am( $I72 );

		return $I6Y;

	}





	private function l1w_O_6am($OB) {

		$I6Y = '';
						$I73 = WType::get('files.types');
			if ( $OB->type != 'url' && $I73->inValues( $OB->type ) ) {
				$I6Y .= $this->video( $OB );
			} else {
												$I74 = explode( '.', $OB->name );
				$I75 = array_pop( $I74 );
				if (in_array( $I75, array( 'jpg', 'jpeg', 'gif', 'png', 'bmp' ) ) ){
					$this->fileType = 'image';
					$I6Y .= $this->image();
				}

								elseif ( in_array( $I75, array( 'mp3', 'wav', 'aiff', 'wma' ) ) ){
					$this->fileType = 'audio';
					$I6Y .= $this->l1v_N_79l();
				}

								elseif ( in_array( $I75, array( '3gp','flv', 'divx', 'mov', 'mp4', 'mov', 'swf', 'wmv') ) ) {

					$this->fileType = 'video';
					$I6Y .= $this->video( $OB, $I75 );

				} else {	
					$I76 = TR1305603045PMTM;
					if( $OB->type != 'url' ) {
												$I76 = TR1305603045PMTM;
						$I77 = JOOBI_URL_MEDIA;
						$I77 .= $this->convertPath( $OB->path );
						$I77 .= '/' . $OB->name . '.' . $OB->type;

						WPage::addJSFile( 'joobibox');
						$I6Y .= '<a target="_blank" href="'. $I77 .'">'. $this->displayIcon( $OB ) . $I76 .'</a>';
						if ( IS_ADMIN ) $I6Y .= ' ' . $OB->name . '.' . $OB->type;


					} else {
												$I6Y .= WPage::createPopUpLink( $OB->name, $I76, 800, 450 );

					}				}

			}
			return $I6Y;

	}






	public function video($OC=null,$OD=null) {

		if ( !empty($OC) ) $this->_myFileInfoO = $OC;

		if ( empty( $this->_myFileInfoO ) ) return '';

		$I6X = WPref::load( 'PFILES_NODE_JWPLAYER' );

				if ( $I6X ) {
			$I78 = WType::get( 'files.scriptsjwplayer' );

			if ( self::$loadJMPlayer ) {
								WPage::addJSFile( JOOBI_URL_INC . 'mediaplayer/jwplayer/jwplayer.js' );
				$I79 = PFILES_NODE_JWPLAYERKEY;
				if ( !empty($I79) ) WPage::addJS( 'jwplayer.key="'.$I79.'";' );
				self::$loadJMPlayer = false;
			}		} else {
			$I78 = WType::get( 'files.scripts' );
		}
		self::$IDCount++;

		if( $this->_myFileInfoO->type != 'url' ) {
			$I70 = $this->l1u_M_2eg();
		} else {
			$I70 = $this->_myFileInfoO->name;
			if ( !empty($OD) ) $this->_myFileInfoO->type = $OD;
		}
		$I7A = $I78->getName( $this->_myFileInfoO->type );

		self::$mediaID[$this->_myFileInfoO->filid] = $this->_myFileInfoO->name . '-' . self::$IDCount;


		$I7B = array();
		$I7B['{NAME}'] = $this->_myFileInfoO->name;
		$I7B['{ID}'] = $this->_myFileInfoO->name . '-' . self::$IDCount;
		$I7B['{SITEURL}'] = JOOBI_SITE;
		$I7B['{URLINC}'] = JOOBI_URL_INC;
		$I7B['{TEXTLOADING}'] = TR1352764977FLOB;

		$I7B['{TRANSPARENCY}'] = $this->transparency;
		$I7B['{WIDTH}'] = $this->videoWidth;
		$I7B['{HEIGHT}'] = $this->videoHeight;
		$I7B['{BACKGROUND}'] = $this->bgcolor;
		$I7B['{AUTOPLAY}'] = $this->autoplay;
		$I7B['{FILESOURCE}'] = $I70;
		$I7B['{FILEID}'] = $this->_myFileInfoO->filid;
		$I7B['{MEDIAID}'] = $this->_myFileInfoO->name;
		$I7B['{BACKGROUNDQT}'] = $this->bgcolor;

				$I6Y = str_replace( array_keys($I7B), array_values($I7B), $I7A );

				if ( $this->_myFileInfoO->type == 'wmv'){
			WPage::addJSFile( JOOBI_URL_INC.'mediaplayer/wmvplayer/wmvplayer.js' );
			WPage::addJSFile( JOOBI_URL_INC.'mediaplayer/wmvplayer/silverlight.js' );

		}elseif ( in_array( $this->_myFileInfoO->type, array('mov','3gp','mp4') ) ) {
			WPage::addJSFile( JOOBI_URL_INC.'mediaplayer/quicktimeplayer/AC_Quicktime.js' );
		}
		return $I6Y;

	}






	public function image($OE=null,$OF=null) {

		if ( !empty($OE) ) $this->_myFileInfoO = $OE;

		if( empty( $this->_myFileInfoO ) ) return '';

		if ( $this->_myFileInfoO->type == 'url' ) {
			$I7C = 150;
			$I7D = 150;

			$I7E = $this->_myFileInfoO->name;
			$I7F = '';
			$I7G = '';
			$I70 = $I7E;

			if ( empty($OF) ) {
				$OF = new stdClass;
				$OF->imgfull = $this->_myFileInfoO->name;
				$this->idLabel = 'imagepreview';
			}
		} else {

			$I7C = $this->_myFileInfoO->width;
			$I7D = $this->_myFileInfoO->height;
			$I70 = $this->l1u_M_2eg();
			$I7G = $this->_myFileInfoO->name;
		}


				if( !empty( $OF->thumb) ) {
			$I7C = $this->_myFileInfoO->twidth;
			$I7D = $this->_myFileInfoO->theight;
			$I7H = $this->l1u_M_2eg( true );
		} else {
			$I7H = $I70;
		}

						if ( !empty($OF->imageWidth) && !empty($OF->imageHeight) ) {
			
						if ( $I7D > $OF->imageHeight ) {
				$I7I =  $OF->imageHeight / $I7D;
				$I7C = defined('PHP_ROUND_HALF_DOWN') ? round( $I7C * $I7I, 0, PHP_ROUND_HALF_DOWN ) : round( $I7C * $I7I, 0 );
				$I7D = $OF->imageHeight;
			}
						if ( $I7C > $OF->imageWidth ) {
				$I7I =  $OF->imageWidth / $I7C;
				$I7D = defined('PHP_ROUND_HALF_DOWN') ? round( $I7D * $I7I, 0, PHP_ROUND_HALF_DOWN ) : round( $I7D * $I7I, 0 );
				$I7C = $OF->imageWidth;
			}
		}

		$I7J = '<img src="'.$I7H.'" style="width:'.$I7C.'px; height:'.$I7D.'px; float: left;" border="0" alt="'.$I7G.'"/>';

		$I7K = ( !empty( $OF->classes ) ) ? $OF->classes : 'a';

		if ( !isset($OF->link) && !isset($OF->imgfull) ) {
			$I6Y =	$I7J;
		} elseif( !isset($OF->link) && isset($OF->imgfull) ) {

			$I6Y = WPage::createPopUpLink( $I70, $I7J, ($this->_myFileInfoO->width*1.15), ($this->_myFileInfoO->height*1.15), $I7K, $this->idLabel );
		} else {
			if ( empty($this->idLabel) ) $this->idLabel = 'mediaID-' . time();
			$I6Y = '<a id="'.$this->idLabel.'"  class="'.$I7K.'" href="'.WPage::routeURL( $OF->link ).'">';
			$I6Y .= $I7J.'</a>';
		}
		return $I6Y;	
	}







	function convertPath($OG='',$OH='hdd') {
		if($OH=='hdd'){
			$OH = DS;
		}elseif($OH=='url'){
			$OH = '/';
		}		return str_replace( array('|','\\','/'), $OH, $OG );
	}






	function displayIcon($OI=null) {
		$I7L = '';
		if ( !empty($OI) ) {
			$I7M = $OI->type;

			$I7N = $this->iconImages( $I7M );
			$I7L .= '<img src="'.$I7N.'" width=32px height=32px alt="'.$OI->filename.'" align="middle" />';
		}
		return $I7L;
	}






	private function l1u_M_2eg($OJ=false) {

		if ( !isset($this->_myNewImageO) ) $this->_myNewImageO = WObject::get( 'files.file' );
		$this->_myNewImageO->name = $this->_myFileInfoO->name;
		$this->_myNewImageO->type = $this->_myFileInfoO->type;
		$this->_myNewImageO->folder = ( empty($this->_myFileInfoO->folder) ? 'media' : $this->_myFileInfoO->folder );
		$this->_myNewImageO->basePath = JOOBI_URL_MEDIA;
		$this->_myNewImageO->path = $this->_myFileInfoO->path;
		$this->_myNewImageO->fileID = $this->_myFileInfoO->filid;
		$this->_myNewImageO->thumbnail = $OJ;
		$this->_myNewImageO->storage = $this->_myFileInfoO->storage;
		$this->_myNewImageO->secure = $this->_myFileInfoO->secure;
		return $this->_myNewImageO->fileURL();

	}
	




	private function l1v_N_79l() {

		if( empty( $this->_myFileInfoO ) ) return '';

		if ( $this->_myFileInfoO->type == 'url' ) {
			$I7O = $this->_myFileInfoO->name;
		} else {
			$I7O = $this->l1u_M_2eg();
		}
				$I6X = false;

				if ( $I6X ) {
			$I78 = WType::get( 'files.scriptsjwplayer' );
			if ( self::$loadJMPlayer ) {
								WPage::addJSFile( JOOBI_URL_INC . 'mediaplayer/jwplayer/jwplayer.js' );
				$I79 = PFILES_NODE_JWPLAYERKEY;
				if ( !empty($I79) ) WPage::addJS( 'jwplayer.key="'.$I79.'";' );
				self::$loadJMPlayer = false;
			}
			$I7A = $I78->getName( $this->_myFileInfoO->type );

			self::$IDCount++;
			$I7B = array();
			$I7B['{NAME}'] = $this->_myFileInfoO->name;
			$I7B['{ID}'] = $this->_myFileInfoO->name . '-' . self::$IDCount;
			$I7B['{SITEURL}'] = JOOBI_SITE;
			$I7B['{URLINC}'] = JOOBI_URL_INC;
			$I7B['{TEXTLOADING}'] = TR1352764977FLOB;

			$I7B['{TRANSPARENCY}'] = $this->transparency;
			$I7B['{WIDTH}'] = $this->videoWidth;
			$I7B['{HEIGHT}'] = $this->videoHeight;
			$I7B['{BACKGROUND}'] = $this->bgcolor;
			$I7B['{AUTOPLAY}'] = $this->autoplay;
			$I7B['{FILESOURCE}'] = $I70;
			$I7B['{FILEID}'] = $this->_myFileInfoO->filid;
			$I7B['{MEDIAID}'] = $this->_myFileInfoO->name;
			$I7B['{BACKGROUNDQT}'] = $this->bgcolor;

						$I6Y = str_replace( array_keys($I7B), array_values($I7B), $I7A );


		} else {

			$I7P = WPage::browser( 'namekey' );
			if ( $I7P != 'msie' ) {
				$I7Q = JOOBI_URL_INC . 'mediaplayer/audioPlayer.swf';
				$I6Y = '<object classid="audioPlayer" width="'. $this->audioWidth .'" height="' . $this->audioHeight .'" id="flashContent">
					 <param name="movie" value="'.$I7Q.'" />
					 <param name="FlashVars" value="playerID=1&noinfo=yes&bg=ffffff&soundFile='.$I7O.'" />
					 <param name="wmode" value="transparent" />
					 <!--[if !IE]>-->
						<object type="application/x-shockwave-flash" data="'.$I7Q.'" width="177" height="20">
						<param name="FlashVars" value="playerID=1&noinfo=yes&bg=ffffff&soundFile='.$I7O.'" />
						<param name="wmode" value="transparent" />
					<!--<![endif]-->
					<a href="'.$I7O.'" class="text_bodytiny">Play Audio</a>
					<!--[if !IE]>-->
					</object>
					<!--<![endif]-->
					</object>';

			} else {
				$I6Y = '<embed height="' . $this->videoHeight . 'px" width="' . $this->videoWidth . 'px" src="'.$I7O.'" />';
				




			}
		}

		return $I6Y;

	}






	function iconImages($OK) {			if ( empty($OK) ) $OK= '';

		$I7R = WType::get('files.icontypes');
		$I7S = $I7R->getName($OK);
		if(empty($I7S)) $I7S = 'default';

		$I7N = JOOBI_URL_JOOBI_IMAGES . 'mime/'.$I7S.'.png';
		return $I7N;
	}












	function uploadDirectory($OL) {
		$I7T = WMessage::get();
		 if(empty($OL->directory)){
		 	 $I7T->historyN('1315887069HOQD');
		 	 return true;
		 }
		 $OL->directory = JOOBI_DS_ROOT . $OL->directory . DS;
		 		 $OL->directory =  preg_replace('#[/\\\\]+#', DS, $OL->directory);

		 $I7U = strpos($OL->extension, '.');
		 if($I7U){
		 	$OL->filename=$OL->extension;
		 	$OL->extension = '';
		 }
		 $OL->extension	= $OL->extension ? explode(',', $OL->extension) : array();


		 $I7V = WGet::folder();
		 $I7W = array();


		 if(!empty($OL->filename))	 $I7W[0] = $OL->filename;
		 else $I7W = $I7V->files($OL->directory);

		 $I7O =  ($OL->secure)?  '': 'download';
		 $I7X = ($OL->secure)? 'safe' : 'media';

		 $I7Y = JOOBI_DS_USER . $I7X . DS . $I7O;

				if (!$I7V->exist($I7Y)) {
			if (!$I7V->create($I7Y)) {
				$I7T->adminE('Unable to create folders');
				return true;
			}
		}
		$I7Z = WGet::file();
		$I80 = WModel::get('files');
		$I81 = false;
		$I82 = array();
		$I83 = 0;

		$I84 = WTools::increasePerformance();
		$I85 = $I84 - ($I84 / 10);
		$I86 = time();
		$I87 = $I86 + $I85;

		if ( $I7W ) {

			for( $I88=0; $I88<count($I7W); $I88++ ) {

				$I89 = $OL->directory . $I7W[$I88];
				$I8A = strtolower( substr( $I89, (strrpos($I89, '.'))+1 ) );

				if ( in_array($I8A , $OL->extension) || empty($OL->extension)) {
					$I8B = $OL->directory . $I7W[$I88];
					if ( !file_exists($I8B) ) {
						$I7T->adminN('The file '.$I8B.' doesn\'t exist');
						$I81 = false;
						continue;
					}
					$I8C = $I7W[$I88];
					$I8D = $I7Y .DS. $I8C;


					$I80 = WModel::get('files');
					if(!empty($OL->filid))	$I80->filid = $OL->filid;
					$I80->alias = (!empty($OL->name))? $OL->name: $I8C;
					$I80->type = $I8A;
					$I80->thumbnail = 0;
					$I80->secure = $OL->secure;
					$I80->_name = $I8C;
					$I80->_tmp_name = $I8B;
					$I80->_error = 0;
					$I80->_size = $I7Z->size($I8B);
					$I80->_path =  $I7O;
					$I80->_folder = $I7X;

					if ( !defined( 'PITEM_NODE_DWLDFORMAT' ) ) WPref::get('item.node');
					$I80->_format = PITEM_NODE_DWLDFORMAT;						$I80->_maxSize = PITEM_NODE_DWLDMAXSIZE * 1028;	
					$I8E = array('jpg', 'jpeg', 'png', 'gif');
					$I80->_fileType = (in_array($I8A, $I8E))? 'images': 'files';

					if ($OL->keepfile) $I80->_copy = true; 					else $I80->_uploadFile = false; 
					$I80->returnId();
					$I81 = $I80->save();

					if($I81){
						if($I83 < 10){
							if( !empty($OL->showUploadedFiles)){
																								$I82[$I83]= 'Successfully Uploaded '.$I80->name. '.' .$I8A;
							}						}						$I83++;
					}
					$I86 = time();
					if($I86 >= $I87) {
						$I7T->adminN('Upload Stopped because the session is about to expire.');
						$I7T->adminN('Stopped at '.$I80->name. '.' .$I8A);
						break;
					}
				}			}
			if ($I81){
				$I7T->userS('1315887070LGQT',array('$count'=>$I83));
				if($I83 < 10){
					if(!empty($I82)){
						foreach($I82 as $I8F){
							$I7T->userS($I8F);
						}					}				}			}			else {
				if($I83 == 0){
					$I8G = implode(' , ', $OL->extension);
					if(empty($I8G))$I7T->adminN('No Files on directory');
					else $I7T->adminN('No Files found that have '.$I8G.' extension');
				}else $I7T->adminN('There\'s a problem on uploading the files.');			}		}else{
			$I7T->adminN('No Files on directory');
		} 
		return $I81;

	}
}