<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;










class Files_Helper_class extends WClasses{








	public function getInfo($O1,$O2='filid') {
		static $I6J=null;
		static $I6K = array();

		if ( !isset($I6K[$O1]) ) {
			if( empty($I6J) ) $I6J = WModel::get('files');

			$I6J->whereE( $O2, $O1 );
			$I6L = $I6J->load( 'o' );

			if ( !empty($I6L) ) $I6L->filename = $I6L->name.'.'.$I6L->type;
			else $I6L = true;
			$I6K[$O1] = $I6L;

		}
		return ( $I6K[$O1] === true ? null : $I6K[$O1] );

	}






	public function getURL($O3) {

		$I6L = $this->getInfo( $O3 );

		$I6M = WGet::file( $I6L->storage );
		$I6M->setFileInformation( $I6L );
		$I6N = $I6M->fileURL();

		return $I6N;

	}






	public function copyFile($O4,$O5=null) {
				$I6O = WModel::get( 'images' );

		$I6P = WModel::get( 'images' );
		$I6P->whereE( 'filid', $O4 );
		$I6Q = $I6P->load( 'o' );

		$I6R = $I6P->getFilePath();

				foreach( $I6Q as $I6S => $I6T ) {
			$I6O->$I6S = $I6T;
		}

		foreach( $O5 as $I6S => $I6T ) {
			$I6U = '_' . $I6S;
			$I6O->$I6U = $I6T;
		}
		$I6O->thumbnail = 0;			$I6O->secure = $O5->secure;


		$I6O->_folder = $O5->folder;
		$I6O->folder = $O5->folder;
		$I6O->_path = $O5->path;

		$I6O->width = $I6Q->width;
		$I6O->height = $I6Q->height;


		$I6O->_copy = true;



		unset( $I6O->filid );


		$I6V = JOOBI_DS_TEMP . 'previewid' . rand( 1000, 9999 );
		$I6M = WGet::file();
		$I6M->copy( $I6R, $I6V );

		$I6O->_tmp_name = $I6V;
		$I6O->_name = 'preview_' . $O4 . '.' . $I6Q->type;
		$I6O->_completePath = JOOBI_DS_TEMP;



		$I6O->returnId();

				$I6W = WClass::get('images.process');
		$I6W->process( $I6O );

		$I6O->save();

		return $I6O->filid;

	}
}