<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;










class Files_Analyze_class extends WClasses {









	public function getTypeAndName($O1,&$O2) {

				if ( strpos( $O1, 'youtube.com' ) ) {
			$O2 = 'youtube';
		} elseif ( strpos( $O1, 'vimeo.com' ) ) {
			$O2 = 'vimeo';
		} elseif ( strpos( $O1, 'livevideo.com' ) ) {
			$O2 = 'livevideo';
		} else {
			$I6F = WMessage::get();
			$I6F->adminW( 'Type of media not yet supported: ' . $O1 );
		}
		return $this->l1t_L_1n1( $O2, $O1 );

	}







    public function cleanURL(&$O3) {

    	$O3->_name = $this->l1t_L_1n1( $O3->_type, $O3->_name );

    }





    private function l1t_L_1n1($O4,$O5) {

		switch ( $O4 ) {
    		case 'youtube':
    			    			$I6G = strpos( $O5, 'v=' );
    			if ( $I6G !== false ) {
	    			$I6H = substr( $O5, $I6G+2 );
	    			$I6I = explode( '&', $I6H );
	    			$O5 = $I6I[0];
    			}     			break;
    		case 'vimeo':
    			$I6G = strpos( $O5, 'vimeo.com' );
    			if ( $I6G !== false ) {
	    			$I6H = substr( $O5, $I6G+10 );
	    			$O5 = $I6H;
    			}     			break;
    		case 'livevideo':
    			$I6G = strpos( $O5, 'livevideo.com/video' );
    			if ( $I6G !== false ) {
	    			$I6H = substr( $O5, $I6G+20 );
	    			$I6I = explode( '/', $I6H );
	    			$O5 = $I6I[0];
    			}     			break;
    		case 'yahoovideo':
    			break;
    		case 'espn':
    			break;
    		case 'myvideo':
    			break;
    		case 'gametrailers':
    			break;
    		case 'myspace':
    			break;
    		case 'url':
    			break;
    		default:	    			break;
    	}
    	return $O5;

	}

}