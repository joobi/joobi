<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Files_Download_class extends WClasses {

	private $_fileInfoO = null;







	public function getFile($O1,$O2=true) {

		if ( empty($O1) ) return false;

		$I6B = WClass::get( 'files.helper' );
		$this->_fileInfoO = $I6B->getInfo( $O1 );

		if ( empty($this->_fileInfoO) ) {
			$I6C = WMessage::get();
			$I6C->userE('1326470276GRUO');
		}
		if ( $O2 ) {
			$I6D = WUser::get( 'uid' );
			if ( $I6D != $this->_fileInfoO->uid ) {
				$I6C = WMessage::get();
				$I6C->userE('1326470276GRUP');
			}		}


				if ( $this->_fileInfoO->type == 'url' ) {
			WPage::redirect( $this->_fileInfoO->name );
			return true;
		}
		$I6E = WGet::file( $this->_fileInfoO->storage );
		$this->_fileInfoO->fileID = $this->_fileInfoO->filid;
		$I6E->setFileInformation( $this->_fileInfoO );
		$I6E->download();

		exit();


	}


}