<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;










class Files_Fancyupload_class extends WClasses{





    function check() {


    	$I8H = WPref::load( 'PLIBRARY_NODE_FANCYUPLOAD' );

    	if ( !empty($I8H) ) {
    		$I8I = WPage::browser( 'namekey' );
    		if ( 'msie' == $I8I ) {
    			$I8J = WPage::browser( 'version' );
    			if ( version_compare( $I8J, '10.0', '<' ) ) $I8H = false;
    		}    	}
    	return $I8H;

	}
}