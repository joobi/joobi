<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Files_Node_model extends WModel {

	
	var $_name = '';		var $_map = '';			var $_multiple = false;	
	public $_fileType = 'files';

	var $type = '';		public $_mimeType = '';		public $_tmp_name = '';		var $_error = 0;		var $_size = 0; 	
		public $_folder = 'media'; 	public $_path = ''; 	var $secure = false; 
	var $_format = array(); 	var $_maxSize = 0;	
	var $_completePath = '';		var $_local = false;

	public $thumbnail = 0;		public $_maxTHeight = 0;
	public $_maxTWidth = 0;
	public $_maxHeight = 0;
	public $_maxWidth = 0;
	public $_watermark = 0;
	public $_storage = 'local';		public $storage = null;


	var $_returnId = true; 	public $_uploadFile = true; 
	var $_resized = false;	
		private $_noFileJustContent = false;

	var $_copy = false; 






	public function getInfo($O1) {

		$IBI = WClass::get( 'files.helper' );
		return $IBI->getInfo( $this->value );

	}







	public function setFileInfoForSave($O2,$O3='filid') {

		$this->_fileInfo[$O3] = $O2;

	}











	public function saveOneFile($O4,$O5,$O6='',$O7='',$O8=false) {

				if ( !empty($O6) ) {
			$this->_path = str_replace( array( '|', '/', '\\'), DS, $O6 );
		}
		if ( !empty($O7) ) {
			$this->_folder = trim( str_replace( JOOBI_DS_USER, '', $O7 ), DS);
		}
				$this->_name = $O5;

				$this->thumbnail = 0;
				$this->secure = 0;
		$this->_size = strlen( $O4 );
		$this->_noFileJustContent = true;

		$IBJ = 'write';
		if ($O8){
			$IBK = pathinfo($O5, PATHINFO_FILENAME);
			$this->whereE( 'name', $IBK );
			$this->whereE( 'path', str_replace( DS, '|', $this->_path ) );
			$IBL = $this->load('o');

			if ( !empty($IBL) ) $IBJ = 'force';
		} 
				if ( empty($IBL) ) {
						$this->save();
		} else {
			$this->whereE( 'filid', $this->filid );
			$this->update();
		}
				$IBM = WGet::file( $this->_storage );
		if( empty( $this->_completePath ) ) $this->_completePath = $IBM->makePath( $this->_folder, $this->_path );


		$this->size = $IBM->write( $this->_completePath .DS. $this->name . '.' . $this->type, $O4, $IBJ );

		return $this->filid;

	}






	public function saveOnlyData($O9='',$OA='') {

		$this->_name = $O9;
		$this->_noFileJustContent = true;
		$this->_path = $OA;

	}





	public function save($OB=null,$OC=null) {

				if ( !empty( $this->_storage ) && is_numeric($this->_storage) ) {
			$this->storage = $this->_storage;
		}

				$this->secure = (int)( $this->secure ) ? 1 : 0;
		$this->thumbnail = (int)( $this->thumbnail ) ? 1 : 0;
		if ( !isset($this->uid) ) $this->uid = WUser::get('uid');

				if ( !empty($this->_externalFile) ) {
			$this->name = $this->_name;
			$this->type = $this->_type;
			$this->path = '';

			$this->returnId();

						if ( !$this->l1y_Q_4m8( false, true ) ) {
								$this->filid = $this->_currentFileID;
				 return true;
			}
			$this->returnId();

						$IBN = parent::save();

			return $IBN;

		}
		
		if ( isset($this->folder) ) $this->_folder = $this->folder;

		$IBO = WGet::folder( $this->_storage );
		if( empty($this->_tmp_name) && !$this->_noFileJustContent ) return true;
		$this->_completePath = $IBO->makePath( $this->_folder, $this->_path );

		$IBP = WMessage::get();

								if ( empty($this->size) ) $this->size = $this->_size;

		$IBQ = explode( '.', $this->_name );
		$this->type = strtolower( array_pop($IBQ) );

		if ( !empty($this->_type) ) {
			$IBR = explode( '/', $this->_type );
			$this->_mimeType = $IBR[0];
			if ( empty($this->type) ) {
				$this->type = array_pop($IBR);
			}		}
				if ( is_string($this->_format) ) {
			$this->_format = explode( ',', $this->_format );
			if ( !empty($this->_format) ) {
				$IBS = array();
				foreach( $this->_format as $IBT ) {
					$IBT = trim( $IBT );
					if ( !empty($IBT) ) $IBS[] = $IBT;
				}				$this->_format = $IBS;
			}		}
				if ( is_array($this->_format) && !empty($this->_format) && !in_array($this->type, $this->_format) ) {
			$IBU = $this->type;
			$IBV = implode(' , ', $this->_format );

			$IBP->userW('1298294171AWEH',array('$TYPE'=>$IBU,'$FORMAT'=>$IBV));
			return false;
		}
				if ( !empty($this->_maxSize) ) {
			if ($this->_size > $this->_maxSize) {
				$IBW = $this->_name;
				$IBP = WMessage::get();

				$IBX = WTools::returnBytes( $this->_maxSize, true );
				$IBP->userW('1359654979DESV',array('$FILENAME'=>$IBW,'$MAX_SIZE'=>$IBX));

				return false;
			}		}		
				if ( $IBO->checkExist() && !$IBO->exist( $this->_completePath ) ) $IBO->create( $this->_completePath, '', true );

				$this->path = str_replace( DS, '|', $this->_path );
				$IBK = preg_replace( '#[^a-z0-9\.\_]#i','_' , $this->_name );
		$IBK = strtolower($IBK);
				$IBY = strrpos($IBK, '.');
		$this->name = substr($IBK, 0, $IBY );

				if ($this->secure) {
			static $IBZ = 0;
			static $IC0 = null;
			$IBZ++;
			if ( $IC0!=time() ) {
				$IC0=time();
				$IBZ=1;
			}
						$this->md5 = $IC0 .'-'. $IBZ . substr( md5($this->name . '.' . $this->type ), 0, 20 );
		}

				if ( !$this->l1y_Q_4m8( false ) ) return false;			if ( !$this->l1y_Q_4m8( true ) ) return false;	
		$this->_fullName = $this->name . '.' . $this->type;

		if ( !isset($this->mime) && !empty($this->_type) ) $this->mime = $this->_type;

				if ( $this->_fileType !='files' ) {

			if ( $this->_fileType =='images' || $this->_mimeType=='image' ) {

								$IC1 = WClass::get( 'images.process' );
				$IC1->process( $this );
								$IBO->create( $this->_completePath , '', true );

			}
			
		}

				if ( WExtension::exist('vendor.node') ) {
			$IC2 = WUser::get('uid');
			$IC3 = WClass::get('vendor.helper',null,'class',false);
			$IC4 = $IC3->getVendorID( $IC2 );
		} else $IC4 = 1;

				$this->vendid = $IC4;


		$this->returnId();
				$IBN = parent::save();

		if (!$IBN) return false;

				if ( $this->_noFileJustContent ) return true;
		if ( !$this->move() ) {

			$IBP->userW('1206732415ABES');
			return false;
		}
		return true;

	}
	function saveparent() {
		return parent::save();
	}








	function deleteValidate($OD=null) {
		$this->_x = $this->load( $OD );
		return true;
	}






	function deleteExtra($OE=0) {

		if ( !empty($this->_x->path) || !empty($this->_x->secure) ) {

						if ( empty($this->_basePath) ) $this->_basePath = JOOBI_DS_USER;
			else $this->_basePath = rtrim( $this->_basePath, DS ) . DS;


			$this->_x->path = str_replace( '|', DS, $this->_x->path );


			$IC5 = WGet::file( $this->_x->storage );
			$IC5->setFileInformation( $this->_x );


			$this->_completePath = $IC5->makePath( $this->_x->folder, $this->_x->path ) . DS;

			if ( !empty($this->_x->thumbnail) ) {
				$IC6 = $this->_completePath . 'thumbnails' . DS . $this->_x->name . '.' . $this->_x->type;
				$IC5->delete( $IC6 );
			}
			$this->_x->thumbnail = false;
			$IC5->setFileInformation( $this->_x );

			$IC7 = $this->_completePath . $this->_x->name . '.' . $this->_x->type;


			$IC5->delete( $IC7 );

		}		return true;
	}












	public function move() {

		$IBM = WGet::file( $this->_storage );
		$IC8 = WObject::get( 'files.file' );
		$IC8->fileID = $this->filid;
		$IC8->name = $this->name;
		$IC8->type = $this->type;
		$IC8->folder = ( empty($this->folder) ? 'media' : $this->folder );		$IC8->path = $this->path;
		$IC8->basePath = JOOBI_DS_USER;
		$IC8->secure = $this->secure;
		$IC8->thumbnail = $this->thumbnail;
		if ( !empty($this->filid) ) $IBM->setFileInformation( $IC8 );

		if ( $this->_noFileJustContent ) {
			return $IBM->write( $this->_tmp_name, $this->_completePath . DS . $this->name . '.' . $this->type );
		} elseif ( $this->_copy ) {
			return $IBM->copy( $this->_tmp_name, $this->_completePath . DS . $this->name . '.' . $this->type );
		} elseif ( $this->_uploadFile ) {
			return $IBM->upload( $this->_tmp_name, $this->_completePath . DS . $this->name . '.' . $this->type );
		}  else {
			return $IBM->move( $this->_tmp_name, $this->_completePath . DS . $this->name . '.' . $this->type );
		}
	}






	public function write($OF,$OG=null) {

		$IBM = WGet::file( $this->_storage );
		$IC9 = $this->name . '.' . $this->type;

		$ICA = rtrim( $OG, DS ) . DS . rtrim($this->path, DS) . DS . $IC9;
		$this->size = $IBM->write($ICA, $OF, 'force');

				if ($this->size > 0) {

			$ICB = explode(DS, $this->path);
			$this->path = implode('|', $ICB);
			if ($this->_md5)
				$this->md5 = md5($OF . microtime());

			$this->whereE('type', $this->type);
			$this->whereE('name', $this->name);
			$this->whereE('path', $this->path);
			$ICC = $this->existId();

			if (!empty ($ICC)) {
				$this->filid = $ICC;
				$this->_filid = $ICC;
				$this->_new = false;
			}
						if (!parent::save()) {
				$IBP = WMessage::get();
				$IBP->codeW('The file could not be saved in the file table: ' . $ICA . ' . You might have a dupliacate.');
				return false;
			}
			if ( !isset($this->filid) ) $this->_filid = $this->lastId();

			return true;

		} else {

			$IBP = WMessage::get();
			$IBP->userW('1212843257NVLI',array('$LOCATION'=>$ICA));
			return false;
		}
	}








	public function convertPath($OH='',$OI='hdd') {
		if ( $OI=='hdd' ) {
			$OI = DS;
		} elseif( $OI=='url' ) {
			$OI = '/';
		}		return str_replace( array('|','\\','/'), $OI, $OH );
	}





	public function getFilePath() {

		if ( $this->secure ) {
			$ICD = JOOBI_DS_USER . 'safe' . DS . $this->convertPath( $this->path ) . DS . $this->name . '.' . $this->type;
		} else {
			$ICD = JOOBI_DS_USER . $this->convertPath( $this->path ) . DS . $this->name . '.' . $this->type;
		}
		return $ICD;

	}









	private function l1y_Q_4m8($OJ=true,$OK=false) {

		$IBM = WGet::file( $this->_storage );
		$ICE = ( $this->secure ) ? $this->md5 : $this->name;
		$ICF = true;
		$this->_currentFileID = 0;

		$ICG=1;			while ( $ICF ) {
			if ( $OJ ) {					if ( !$IBM->checkExist() || !$IBM->exist( $this->_completePath . DS . $ICE .'.'. $this->type ) ) return true;
			} else {					$this->whereE('name', $this->name);
				$this->whereE('path', $this->path);
				$this->whereE('type', $this->type);
				if ( $OK ) {

					if ( ($this->_currentFileID = $this->load('lr', 'filid') ) ) return false;
					else return true;
				} else {
					if ( !$this->existId() ) return true;
				}			}						$ICH = rand( 1000000000, 9999999999 );
			$this->name = $this->name . '_' . $ICH;
			$ICE = $this->name;
			if ( strlen($this->name) >= 254 ) $this->name = substr( $this->name , 0, 245 - $ICG );				$ICG++;
			if ( $ICG> 40 ) return false;			}
		return true;

	}
}