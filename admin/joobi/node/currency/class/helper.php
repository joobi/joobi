<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Currency_Helper_class extends WClasses {















	 function getValue($O1,$O2=array()) {



	 	if ( empty($O1) ) {	
	 		$ICR = WMessage::get();

	 		return $ICR->adminW( 'Please check currency parameters' );

	 	}


	 	
	 	static $ICS=array();

	 	if ( !isset($ICS[$O1]) ) {

	 		
	 		
	 		
	 		
		 	static $ICT=null;

		 	if ( !isset($ICT) ) $ICT = WModel::get( 'currency' );


		 	$ICT->whereE( 'curid', $O1 );

		 	$ICS[$O1] = $ICT->load( 'o' );

		}


	 	return ( is_array($O2) ) ? $ICS[$O1] : $ICS[$O1]->$O2;

	 }









	 function getCurrencyID($O3) {
	 	static $ICU = array();

	 	if ( empty( $O3 ) ) return false;

	 	if ( empty( $ICU[ $O3 ] ) ) {
	 		static $ICT = null;
	 		if ( !isset($ICT) ) $ICT = WModel::get( 'currency' );
	 		$ICT->whereE( 'code', $O3 );
	 		$ICU[ $O3 ] = $ICT->load( 'lr', 'curid' );
	 	}
	 	return $ICU[ $O3 ];
	 }












	 function isAccepted($O4,$O5=false) {

	 	static $ICT = null;
	 	$ICV = null;


	 		 	if ( !is_array($O4) ) $O4 = array($O4);

	 	$ICW = serialize($O4);

	 	if ( !isset($ICV[$ICW][$O5]) ) {

		 	if ( !isset($ICT) ) $ICT = WModel::get( 'currency' );
			$ICT->whereIn( 'curid', $O4 );
			if (!$O5) $ICT->whereE( 'accepted', 1 );
			$ICT->whereE( 'publish', 1 );
			$ICX = $ICT->load( 'r', 'curid' );

			$ICV[$ICW][$O5] = ( !empty($ICX) && ( $ICX > 0 ) ) ? true : false;

	 	}
		return $ICV[$ICW][$O5];

	 }
	 






	 function setPublishAccepted($O6,$O7=1,$O8=1) {

	 	if ( empty( $O6 ) ) return false;

	 	$ICT = WModel::get( 'currency' );

	 		 	if ( !$ICT->exist( $O6 ) ) return false;

	 	$ICT->setVal( 'publish', $O7 );
	 	$ICT->setVal( 'accepted', $O8 );
	 	$ICT->whereE( 'curid', $O6 );
	 	return $ICT->update();

	 }
}