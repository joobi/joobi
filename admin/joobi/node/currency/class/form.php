<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Currency_Form_class extends WClasses {


















function createForm($O1,$O2=null,$O3=null,$O4=null,$O5=0,$O6=null) {

	
	$ICM = WView::form( $O1 );

	if (!empty($O2)) $ICM->hidden( 'controller', $O2 );

	if (!empty($O3)) $ICM->hidden( 'task', $O3 );



	$ICN = 'com_jcenter'; 
	$ICM->hidden( 'option', $ICN );

	$ICM->hidden( 'returnid', base64_encode( WGlobals::currentURL( false ) ) );


	if ( !empty($O4) ) {

		
		$ICO = 'return '.WView::addJSAction( $O3, $O1 );

		$ICP = new stdClass;



		$ICP->default = $O6;	
		$ICP->outputType = $O5;	
		$ICP->nbColumn = 1;	


		$ICQ = WView::picklist( $O4, $ICO, $ICP );

		$ICM->addContent( $ICQ->display() );

	}


	return $ICM->make();

}
}