<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Currency_Rate_class extends WClasses {

	
	private $_convertionSiteURL = 'http://themoneyconverter.com/rss-feed/EUR/rss.xml';









public function updateExchangeRate($O1,$O2=null,$O3=null,$O4=null,$O5=0) {
	$IB4 = null;

	$O1 = $this->_convertionSiteURL;

		if ( !empty($O1) ) $IB5 = $this->l1k_M_1nh($O1);
	else $IB5 = null;

		if ( !empty($IB5) ) {
				if ( empty($O3) && empty($O4) ) {
						static $IB6=null;
			if (empty($IB6)) $IB6 = WModel::get( 'currency' );
			$IB6->whereE( 'publish', 1 );
			$IB7 = $IB6->load( 'ol', array( 'curid', 'code', 'title' ) );
		} else {
			$IB8=array();
			$IB8[]=$O3;
			$IB8[]=$O4;

						static $IB6=null;
			if (empty($IB6)) $IB6 = WModel::get( 'currency' );
			$IB6->whereIN( 'curid', $IB8 );
			$IB6->whereE( 'publish', 1 );
			$IB7 = $IB6->load( 'ol', array( 'curid', 'code', 'title' ) );
		}

		if ( !empty($IB7) ) {
						if ( !defined( 'PCURRENCY_NODE_CONVERSIONHISTORY' ) ) WPref::get( 'currency.node' );
			if ( PCURRENCY_NODE_CONVERSIONHISTORY ) $IB9 = true;
			else $IB9 = false;

			$IBA = $this->l1l_N_4dl( $IB7, $IB5 );

			if ( !empty($IBA) ) {
				$IBB = $O5;
				static $IBC=null;
				if ( empty($IBD) ) $IBD = WModel::get( 'currency.conversion' );

				$IBE = 0;
				foreach( $IB7 as $IBF ) {
					foreach( $IB7 as $IBG ) {
						$O5 = $IBB;

												$IBD->whereE( 'curid', $IBF->curid );
						$IBD->whereE( 'curid_ref', $IBG->curid );
						$IBH = $IBD->load( 'o', array( 'curid','fee') );

												if ( $IBF->curid == $IBG->curid ) $O5 = 0;
								
								
								
												$IBE = $this->l1m_O_73e( $IBA, $IB7, $IBF, $IBG );

						if ( !empty($O5)) $IBI = $IBE + ( $IBE * ( $O5 / 100 ) );
						else $IBI = $IBE;

						if ( !empty($IBE) ) {
														$IBD->setVal( 'exchange', $IBE );
							$IBD->setVal( 'rate', $IBI );
							$IBD->setVal( 'fee', $O5 );
							$IBD->setVal( 'publish', 1 );

							if ( empty($O2) || ( $O2 == 0 ) ) $IBD->setVal( 'modified', time() );
							else $IBD->setVal( 'modified', $O2 );

							if ( !empty($IBH->curid) ) {
								$IBD->whereE( 'curid', $IBF->curid );
								$IBD->whereE( 'curid_ref', $IBG->curid );
								$IBD->update();
							} else {
								$IBD->setVal( 'name', $IBF->title .' => '. $IBG->title );
								$IBD->setVal( 'alias', $IBF->title .' => '. $IBG->title );
								$IBD->setVal( 'curid', $IBF->curid );
								$IBD->setVal( 'curid_ref', $IBG->curid );
								$IBD->insert();
							}
							if ( $IB9 ) $this->l1n_P_130( $IBE, $IBF, $IBG, $O1 );
						}
					}				}			}		}	}
	return true;
}


function viewRateHTML($O6) {

	$O6 = $this->_convertionSiteURL;

		if ( !empty($O6) ) $IB5 = $this->l1k_M_1nh($O6);
	else $IB5 = null;

		if ( !empty($IB5) ) $IBJ = $this->l1o_Q_1f6( $IB5 );

		static $IB6=null;
	if ( empty($IB6) ) $IB6 = WModel::get( 'currency' );

	$IBK = '<table border="0">';
	if ( !empty($IBJ) )
	{
		foreach( $IBJ as $IB4 )
		{
			$IBK .= '<tr><td><br></td>';
						if ( !empty($IB4) ) foreach( $IB4 as $IBL=>$IBI ) $IBK .= '<tr><td>'. $IBL .'</td><td>'. $IBI .'</td></tr>';
			$IBK .= '</tr>';
		}
	} else {

		$IBK .= '<tr><td>'. TR1246516643YYN .'</td></tr>';
	}	$IBK .= '</table>';

	return $IBK;

}






private function l1k_M_1nh($O7) {

	$IBM = $this->l1p_R_3fs( $O7 );

	if ( empty($IBM) ) {
	  $IBN = WMessage::get();
	  if (empty($IBO)) $IBO = 'No data received for '.$O7;
	  else $IBN->userE($IBO);
	  return false;
	}

	$IBP = WClass::get('library.parser');
	$IBQ = $IBP->parse($IBM);

	if (!$IBQ) return false;

	return $IBQ;

}






private function l1l_N_4dl($O8,$O9) {

		static $IBR=array();
	foreach( $O8 as $IBS ) $IBR[] = $IBS->code;

		static $IBA=array();
	static $IBJ=array();
	if ( empty($IBJ) ) $IBJ = $this->l1o_Q_1f6( $O9 );
	$IBT = array();

		foreach( $IBJ as $IBU )
	{
		if ( isset($IBU['code']) ) $IBV = $IBU['code'];
		elseif ( isset($IBU['currency']) ) $IBV = $IBU['currency'];
		else $IBV = null;

		if ( !empty($IBV) )
		{
			if ( in_array( $IBV, $IBR ) )
			{
				$IBA[$IBV] = $IBU['rate'];
				$IBT[] = $IBV;
			}		}	}
			foreach( $IBR as $IBW  )
	{
		if ( !in_array( $IBW, $IBT ) ) $IBA[$IBW] = 0;
	}
	return $IBA;

}





private function l1o_Q_1f6($OA) {

	return $this->l1q_S_wd( $OA );

}

private function l1r_T_76a($OB) {

	$IBX = explode( '/', $OB);
	return $IBX[0];
}

private function l1s_U_7mz($OC) {

	$IBY = str_replace( '1 Euro = ', '', $OC );
	$IBZ = explode( ' ',$IBY );
	return $IBZ[0];

}





private function l1q_S_wd($OD) {
	$IC0=array();

	if ( empty($OD[0]['children'][0]['children']) ) return false;
	$IC1 = $OD[0]['children'][0]['children'];
	foreach( $IC1 as $IC2 ) {
		if ( empty( $IC2['nodename'] ) || 'item' != $IC2['nodename'] ) continue;
		if ( empty($IC2['children']) ) continue;

		$IBM = $IC2['children'];
		$IC3 = array();
		foreach( $IBM as $IC4 ) {

			switch( $IC4['nodename'] ) {
				case 'title':
					$IC5 = $this->l1r_T_76a( $IC4['nodevalue'] );
										if ( strlen($IC5) == 3 ) {
						$IC3['code'] = $IC5;
					}					break;
				case 'pubDate':
					$IC6 = $IC4['nodevalue'];
					$IC7 = strtotime( $IC6 );
					$IC8 = 359200;
					if ( ($IC7 + $IC8) < time() ) {
						$IBN = Wmessage::get();
						$IBN->codeE( 'The feed is out of date' );
					}					break;
				case 'description':
					$IC3['description'] = $IC4['nodevalue'];
					$IC3['rate'] = $this->l1s_U_7mz( $IC4['nodevalue'] );
					break;
				default:
			}
		}
		if ( empty($IC3['code']) || empty($IC3['rate']) ) continue;

		$IC0[] = $IC3;

	}
	return $IC0;
}















private function l1n_P_130($OE,$OF,$OG,$OH) {
		static $IC9=null;
	if ( empty($IC9) ) {
		$ICA = WModel::get( 'currency.exchangesite' );
		$ICA->whereE( 'url' , $OH );
		$ICA->whereE( 'publish', 1 );
		$IC9 = $ICA->load( 'r', 'exsiteid' );
	}
		static $ICB=null;
	if ( empty($ICB) ) $ICB = WModel::get( 'currency.conversionhistory' );

	$ICB->setVal( 'alias', $OF->title .' => '. $OG->title );
	$ICB->setVal( 'exchange', $OE );
	$ICB->setVal( 'modified', time() );
	$ICB->setVal( 'curid', $OF->curid );
	$ICB->setVal( 'curid_ref', $OG->curid );
	$ICB->setVal( 'exsiteid', $IC9 );
	$ICB->insert();

	return true;
}








private function l1m_O_73e($OI,$OJ,$OK,$OL) {
		static $ICC=null;
	if ( empty($ICC) )
	{
		$ICD = WClass::get('vendor.helper',null,'class',false);
		$ICC = $ICD->getCurrency();
	}
		$ICE = $this->l1t_V_4f6( $OI, $OJ );

		if ( !empty($ICE) ) {

				$ICF = $OI[$OK->code];
		$ICG = $OI[$OL->code];

				if ( $OK->curid == $OL->curid )  $IBE = 1;
		elseif ( $OK->curid == $ICC ) $IBE = $ICE * $ICG;
		else {

									if ( $ICF == 1  )  $IBE = $ICG;
			else {

				if ( $OL != $ICC ) {
					if (!empty($ICF)) $IBE = ( 1 / $ICF ) * $ICG;
					else $IBE = 0;
				} else {
					$ICH = $ICE * $ICF;
					if ( !empty($ICH) && ( $ICH != 0 ) ) $IBE = 1 / $ICH;
					else $IBE = 0;
				}
			}		}	}
	return $IBE;

}






private function l1t_V_4f6($OM,$ON) {
	static $ICI=null;
	if (empty($ICI)) $ICI = $this->l1u_W_46r($ON);

			if ( isset( $OM[$ICI] ) ) $ICE = $OM[$ICI];
	else $ICE = 1;

		if ( empty($ICE)) $ICE = 1;
	else
	{
				if ( $ICE != 1 ) $ICE = 1 / $ICE;
	}
	return $ICE;
}






private function l1u_W_46r($OO,$OP=1) {
		static $ICC=null;
	$ICJ = null;
	if ( empty($ICC) )
	{
		$ICD = WClass::get('vendor.helper',null,'class',false);
		$ICC = $ICD->getCurrency();
	}
		if ( !empty($OO) )
	{
		foreach( $OO as $ICK )
		{
			if ( $ICK->curid == $ICC ) $ICJ = $ICK->code;
		}	}
	return $ICJ;
}








 	private function l1p_R_3fs($OQ) {

		if  ( in_array('curl', get_loaded_extensions() ) ) {
			$ICL = curl_init( $OQ );
			curl_setopt($ICL, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ICL, CURLOPT_HEADER, 0);
			$IBM = curl_exec($ICL);
			curl_close($ICL);
			return $IBM;
		} elseif ( ini_get('allow_url_fopen') ) {							ob_start();
			$IBM = file_get_contents($OQ);
			$IBO = ob_get_clean();
			return $IBM;
		} else {
			$IBN = WMessage::get();
			$IBN->adminE( 'Could not connect to the URL, check your PHP configuration: '.$OQ  );
		}
		return false;

 	}
}