<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Currency_Convert_class extends WClasses {

	var $error = '';	

































public function currencyConvert($O1,$O2,$O3,$O4=false,$O5=true) {
		if ( empty($O3) ) return 0;

	static $IDH=null;

	if ( empty( $O1 ) || empty( $O2 ) ) {
		$IDI = WMessage::get();
		$IDI->codeE( 'One of the currency is not defined! Check your code! function currencyConvert().' );
		return TR1341596429OMYB;

	}

	if ( !is_numeric( $O3 ) ) {
		if ( !isset($IDH) ) $IDH = WClass::get( 'currency.format' );
		$O3 = $IDH->SQLizePrice( $O3 );
	}
		









	
	if ( !is_numeric( $O1 ) ) {
		$O1 = $this->l1v_X_49m( $O1 );
		if( empty($O1) ) TR1341596429OMYB;

	}


	
	if ( !is_numeric( $O2 ) ) {

		$O2 = $this->l1v_X_49m( $O2 );
		if( empty($O2) ) TR1341596429OMYB;
	}


	
	if ( (int)$O1 == (int)$O2 ) {

		return $O3;

	}


		

	if ( $O3 < 0 ) {
		$IDJ = true;
		$O3 = -1 * $O3;
	} else {
		$IDJ = false;
	}

	
	if ( $O4 ) {

		
		static $IDK=null;

		if ( empty($IDK) ) $IDK = WClass::get('vendor.helper',null,'class',false);

		$IDL = $IDK->getCurrency();



		
		if ( $IDL == $O1 ) $O4 = false;



		
		if ( $O4 ) {

			
			$IDM = $this->l1w_Y_6bw( $IDL, $O1, 'rate' );

			if ( empty($IDM) ) $IDM = 1;

		}
	}



	if ( $O4 ) $IDN = $IDL ;

	else $IDN =  $O1 ;

	$IDO = $this->l1w_Y_6bw( $IDN, $O2 );

	if ( !empty($IDO) && !empty($IDO->rate) ) {

		
		if ( $O5 ) {

			$IDP = $IDO->rate;

		} else {

			if ( !empty($IDO->fee) ) {

				$IDQ = ( $IDO->exchange * $IDO->fee ) / 100;

				$IDR = $IDO->exchange - $IDQ;

				$IDP = $IDR;

			}
		}
	}

	
	if ( !empty( $IDP ) && $IDP > 0 ) {



		
		if ( $O4 ) {

			if ( $O1 == $IDL ) $O3 *= ( 1 / $IDP);

			else {

				$IDS = $O3 / $IDM;

				$O3 = $IDS * $IDP;

			}
		} else {
			$O3 *= $IDP;
		} 

				if ( $IDJ ) {
			return ( -1 * $O3 );
		} else {
			return $O3;
		}

	} else {
		$IDI = WMessage::get();
		$IDT = WClass::get( 'currency.helper', null, 'class', false );
		$IDU = $IDT->getValue( $O1, 'title' );
		$IDV = $IDT->getValue( $O2, 'title' );
		$IDI->adminE( "The conversion rate between $IDU and $IDV is not defined." );
		return TR1341596429OMYB;

	}




	return false;


}


















function acceptedCurrency($O6) {

    static $IDW  = array();

    static $IDX = null;



    if ( empty( $O6 ) ) return false;

    if ( !empty( $IDW[ $O6 ] ) ) return (bool)$IDW[ $O6 ];



    if ( !isset( $IDX ) ) $IDX = WModel::get( 'currency' );

    $IDX->whereE( 'accepted', 1 );

    $IDX->whereE( 'curid', $O6 );

    $IDW[ $O6 ] = $IDX->load( 'lr', 'curid' );



    if ( empty( $IDW[ $O6 ] ) ) return false;



    return true;

}
























function convertProductToOrder($O7,$O8) {



    if ( empty( $O8 ) ) return $O7;



    
    foreach( $O8 as $IDY ) {

        if ( !is_numeric( $IDY ) ) return 'Check Currency IDs';



    }


    $IDZ = $O7;



    
    if ( $O8[0] != $O8[1] )

       $IDZ = $this->currencyConvert( $O8[0], $O8[1], $IDZ, true, true );



    
    if ( $O8[1] != $O8[2] )

       $IDZ = $this->currencyConvert( $O8[1], $O8[2], $IDZ, false, true );



    return $IDZ;

}









































function convertTo($O9) {



	
	$IE0 = ( isset($O9->currencyFrom) ) ? $O9->currencyFrom : 0;

	$IE1 = ( isset($O9->currencyTo) ) ? $O9->currencyTo : 0;

	$IE2 = ( isset($O9->amount) ) ? $O9->amount : 0;

	$IE3 = ( isset($O9->formatAmount) ) ? $O9->formatAmount : false; 
	$IE4 = ( isset($O9->formatType ) ) ? $O9->formatType : 'money';

	$IE5 = ( isset($O9->decimalPlace) ) ? $O9->decimalPlace : 2; 
	$IE6 = ( isset($O9->includeFee) ) ? $O9->includeFee : true; 


	
	if ( empty($IE0) || empty($IE1) ) {

		$IDI = WMessage::get();

		$IDI->userE('1257240514BWPE');

		$IDI->codeE( 'Check currency values passed through object parameter for function convertTo in class Currency_Convert_class' );

	}


	
	if ( empty($IE2) || ( $IE2 == 0 ) ) return 0;

	if ( $IE0 == $IE1 ) {

		if ( $IE3 ) return WTools::format( $IE2, $IE4, $IE1 );

		else return $IE2;

	}


	
	$IE2 = $this->l1x_Z_vs( $IE2, $IE0 );



	
	$IE0 = $this->returnCurrencyID( $IE0 );

	$IE1 = $this->returnCurrencyID( $IE1 );



	
	$IE7 = $this->l1w_Y_6bw( $IE0, $IE1 );



	
	if ( empty($IE7) ) return 'No Conversion Found';



	


	if ( isset( $IE7 ) && !empty( $IE7 ) )

	{

		
		$IDP = ( isset( $IE7->rate ) ) ? $IE7->rate : 0;

		$IE8 = ( isset( $IE7->exchange ) ) ? $IE7->exchange : 0;

		$IE9 = ( isset( $IE7->fee ) ) ? $IE7->fee : 0;



		
		if ( $IE6 ) $IEA = $IE2 * $IDP;

		else $IEA = $IE2 * $IE8;

	}

	else return 'No Conversion Rate Available';



	
	$IEA = round( $IEA, $IE5 );

	if ( $IE3 ) return WTools::format( $IEA, $IE4, $IE1 );

	else return $IEA;

}




















function returnCurrencyID($OA)

{

	
	if ( empty($OA) ) return 0;



	
	if ( is_numeric($OA) ) return $OA;



	
	static $IDH=null;

	if ( !isset($IDH) ) $IDH = WClass::get( 'currency.format' );

	$IEB = $IDH->load( $OA );



	
	$IEC = ( isset($IEB->curid) ) ? $IEB->curid : 0;



	if ( !empty($IEC) ) return $IEC;

	else {

		$IED = $OA;

		$IDI = WMessage::get();

		$IDI->userN('1257240515GXYD');

		$IDI->adminN( 'Currency passed in returnCurrencyID function failed to get its ID for '. $IED );

		return $OA;

	}


	return true;

}

private function l1v_X_49m($OB) {

	$IEE = false;
	if ( !isset($IDH) ) $IDH = WClass::get( 'currency.format' );
	$IEB = $IDH->load( $OB );

	if ( !empty($IEB) ) {
		$OB = $IEB->curid;
	} else {
		$IEE = true;
	}
	if ( empty($OB) || $IEE ) {
		$IDI = WMessage::get();
		$IDI->codeE( 'The currency does not exist so we could not convert.' );
		return false;
	}
	return $OB;

}
















private function l1w_Y_6bw($OC,$OD,$OE=null) {

	static $IEF = array();


	
	$IEG = $OC .'-'. $OD;

	if ( !isset($IEF[$IEG]) ) {

		static $IEH=null;

		if ( !isset($IEH) ) $IEH = WModel::get( 'currency.conversion' );

		$IEH->whereE( 'curid', $OC );

		$IEH->whereE( 'curid_ref', $OD );

		$IEH->whereE( 'publish', 1 );

		$IEF[$IEG] = $IEH->load( 'o', array( 'rate', 'exchange', 'fee' ) );

	}


	return ( !isset($OE) ) ? $IEF[$IEG] :

			( isset( $IEF[$IEG]->$OE) ? $IEF[$IEG]->$OE : null );

}
















private function l1x_Z_vs($OF,$OG) {

	
	if ( empty($OF) || ( $OF == 0 ) ) return $OF;

	if ( empty($OG) ) return $OF;



	
	$IEI = WTools::format( $OF, 'price', $OG );



	
	static $IDH=null;

	if ( !isset($IDH) ) $IDH = WClass::get( 'currency.format' );

	$IEI = $IDH->SQLizePrice( $IEI );



	return $IEI;

}}