<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Currency_Format_class extends WClasses {










	function set() {

		if ( !defined( 'CURRENCY_USED' ) ) {

						$ICY = WPref::load('PCURRENCY_NODE_MULTICUR');

						if ( empty($ICY) ) {
				$ICZ = WPref::load('PCURRENCY_NODE_PREMIUM');
				define( 'CURRENCY_USED', $ICZ );
				WGlobals::setSession( 'currency', 'currencyId', $ICZ );
				return true;
			}
			if ( empty($ID0) ) $ID0 = WGlobals::getSession( 'iptracker', 'currencyId', null );

			if ( !empty($ID0) ) {

				$ID1 = WClass::get( 'currency.helper', null, 'class', false );
				$ID2 = false;
				if ( !empty($ID3) ) {
					$ID2 = $ID1->isAccepted( $ID0, true );
				}

				if( !empty( $ID2 ) ){
					define( 'CURRENCY_USED', $ID0 );
					WGlobals::setSession( 'currency', 'currencyId', $ID0 );
					return true;
				}								else $ID0 = null;
			}
						if ( empty( $ID0 ) ) {

								$ID4 = WGlobals::getSession( 'iptracker', 'localization', null );
				if ( !empty($ID4->currency) ) {
					$ID5 = array();
					foreach( $ID4->currency as $ID6 ) {
						$ID5[] = $ID6->curid;
					}
					$ID1 = WClass::get( 'currency.helper', null, 'class', false );
					$ID2 = false;
					if ( !empty($ID3) ) {
						$ID2 = $ID1->isAccepted( $ID5, true );
					}

					if ( !empty($ID2) ) {
						WGlobals::setSession( 'currency', 'currencyId', $ID2 );
						define( 'CURRENCY_USED', $ID2 );
						return true;


					}				}			}
			if ( empty( $ID0 ) ) {

				WPref::load('PCURRENCY_NODE_PREMIUM');
				if( defined('PCURRENCY_NODE_PREMIUM') ) {
					define( 'CURRENCY_USED', PCURRENCY_NODE_PREMIUM );
					WGlobals::setSession( 'currency', 'currencyId', PCURRENCY_NODE_PREMIUM );
					return true;
				}
			}
						if ( !defined( 'CURRENCY_USED' ) ) {
				define( 'CURRENCY_USED', 0 );
			}

		}
		return true;

	}






	function load($O1,$O2='object') {
		static $ID7 = null;
		static $ID8 = array();

		if ( empty($O1) ) return null;

		if ( !isset($ID8[$O1]) ) {

			if ( !isset($ID7) ) $ID7 = WModel::get( 'currency');

			if ( is_numeric($O1) ) {
				$ID7->whereE('curid', $O1);
			} else {
				$ID7->whereE('code', $O1);
			}
			$ID9 = $ID7->load('o', array ( 'curid', 'code', 'symbol', 'basic', 'title' ));

			if ( !empty($ID9) ) {
				$ID8[$O1] = $ID9;
			}
		}
		if ( isset($ID8[$O1]) ) {
			if ( $O2!='object' ) {
				return ( isset($ID8[$O1]->$O2) ) ? $ID8[$O1]->$O2 : null;
			} else {
				return $ID8[$O1];
			}		} else {
			return null;
		}
	}










	public function displayAmount($O3,$O4=null,$O5=1,$O6=null) {


		if ( WGlobals::checkCandy(25) ) {

			if (!defined('PPRODUCT_NODE_CATALOGUE') || !defined('PPRODUCT_NODE_LOGPRICE') ) WPref::get('product.node');
			if (PPRODUCT_NODE_CATALOGUE ) return '';

			$IDA = WUser::get('uid');

			if (PPRODUCT_NODE_LOGPRICE && $IDA == 0) return '';

		}
				if($O5 == 1) {

			if (is_numeric($O3)) {
				if ($O3 == 0) {
					return TR1206961944PEUR;

				} else {
					if($O6 != null){
						$IDB = $O3+(($O6/100)*$O3);
					}					$IDB = WTools::format( $O3, 'moneyCode', $O4 );
				}				return $IDB;

			}
				}elseif($O5 == 2){
			if($O3 == 0){
				return '';
			}else{
				return $O3.'%';
			}		}
	}








	public function SQLizePrice($O7) {

				if ( is_numeric($O7) || $O7 === (float)$O7 ) {
			return 	$O7;
		}

				$O7 = str_replace( ' ', '', $O7 );

				$IDC = substr( $O7, -3 );
		$IDD = substr( $O7, 0, -3 );

				$IDD = str_replace( array(',','.'), '', $IDD );

				$IDE = substr_count( $IDC, ',' );
		if ( $IDE == 1 ) {
			$IDC = str_replace( ',', '.', $IDC );
			$O7 = $IDD . $IDC;
		}
		$O7 = $IDD . $IDC;

		return $O7;

	}


















	public function example($O8='',$O9=null,$OA=null,$OB=true,$OC=null) {
		$IDF = $this->getCurrency($O9);
		$IDG = $this->regionalize('12345.56', $IDF);
		if (isset($OA))
			$IDF->position = $OA;

		if($OC !=null) $IDG = $this->regionalize((($IDG*($OC/100))+$IDG), $IDF);
		$IDG = $this->symbol($IDG, $IDF);
		if ($OB == true)
			if($OC == null){
				return $O8 . ' ' . str_replace(array('$amount'),array($IDG),TR1206961945HIVT);
			}else return $O8 . ' ' . str_replace(array('$amount','$vat'),array($IDG,$OC),TR1213180337NRCV);
		else
			if($OC == null){
				return $O8 . ' ' . $IDG;
			}else return $O8 . ' ' . str_replace(array('$amount','$vat'),array($IDG,$OC),TR1213180337NRCW);

	}
}