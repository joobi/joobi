<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;














class Currency_positionsymbol_form extends WForms_standard {

	





	function create() {
		$I9T = 0;
		$I9U = 1;
		$I9V = '';
		$I9W = WClass::get('currency.format',null,'class',false);
		$I9X = $this->model->_pkey . '_' . $this->modelID;
		if ($this->value == $I9T) {
			$I9V .= $this->radio($this->map, $this->element->sid . $this->element->map . $I9T, $I9W->example(TR1206961945HIVW, $this->model-> $I9X, $I9T), $I9T, true);
			$I9V .= $this->radio($this->map, $this->element->sid . $this->element->map . $I9U, $I9W->example(TR1206961945HIVX, $this->model-> $I9X, $I9U), $I9U);
		} else {
			$I9V .= $this->radio($this->map, $this->element->sid . $this->element->map . $I9T, $I9W->example(TR1206961945HIVW, $this->model-> $I9X, $I9T), $I9T);
			$I9V .= $this->radio($this->map, $this->element->sid . $this->element->map . $I9U, $I9W->example(TR1206961945HIVX, $this->model-> $I9X, $I9U), $I9U, true);
		}		$this->content = $I9V;
		return true;
	}

	








	function radio($O1='default',$O2=0,$O3='empty',$O4='1',$O5=false) {
		if ($O5 == false) {
			$I9V = '<input type="radio" name="' . $O1 . '" id="' . $O2 . '" value="' . $O4 . '"  /><label for="' . $O2 . '">' . $O3 . '</label>';
		} else {
			$I9V = '<input type="radio" name="' . $O1 . '" id="' . $O2 . '" value="' . $O4 . '" CHECKED /><label for="' . $O2 . '">' . $O3 . '</label>';
		}		return $I9V;
	}}