<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Currency_rate_form extends WForms_standard {
	




	function create() {

		if ( !defined('CURRENCY_USED') ) {
			$I9R = WClass::get('currency.format',null,'class',false);
			$I9R->set();
		}		$I9S = WClass::get('currency.format',null,'class',false);
		$this->content = '<input id="' . $this->idLabel . '" name="' . $this->map . '" size="7" value="' . $I9S->regionalize($this->value) . '" type="text"';
		if($this->eid == CURRENCY_USED ){
			$this->content .= 'class="disabled" DISABLED>';
			$this->content .= TR1206961945HIVU;
		}else{
			$this->content .= 'class="inputbox" >';
			$this->content .=  $I9S->display(1).TR1206961945HIVV;
		}

		return true;
	}

}