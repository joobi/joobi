<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Currency_toggle_controller extends WController {
function toggle() {



	WPref::load( 'PCURRENCY_NODE_MULTICUR' );

	if ( PCURRENCY_NODE_MULTICUR ) {


		if ( WGlobals::checkCandy(50) ) {


			static $IF1=null;

			static $IF2=null;



			$IF3 = WGlobals::getEID();
			$IF4 = $this->subTask[1];



			
			$IF5 = WExtension::get( 'jstore.application', 'wid' );

			$IF6 = WUser::getRoleUsers('manager', array('uid') );



			
			$IF7 = WMessage::get();

			$IF8 = '';



			$IF9 = false;

			$IFA = 0;

			$IFB = false;

			if ( !isset( $IF1 ) ) $IF1 = WModel::get( 'currency' );



			
			if ( $IF4 == 'accepted' )

			{



				
				$IF1->whereE( 'accepted', 1);

				$IF1->whereE( 'publish', 1 );

				$IFC = $IF1->load( 'ol' );



				if ( !empty( $IFC ) )

				{

					$IFA = sizeof( $IFC ) + 1;

				}

				else

				{

					$IFA = 1;

				}


				
				$IF1->whereE( 'curid', $IF3 );

				$IF1->whereE( 'publish', 1 );

				$IFD = $IF1->load( 'o' );



				if ( !empty( $IFD ) )

				{

					if ( $IFD->accepted == 0 )

					{

						$IF1->setVal( 'accepted', 1 );

						$IF1->setVal( 'ordering', $IFA );

						$IFB = true;

					}

					else

					{

						
						if( (sizeof( $IFC ) == 1) || ( $IFA == 1 ) )

						{

							$IF7->userN('1246699627QZRH');



							return true;

						}


						$IF1->setVal( 'accepted', 0 );

						$IF1->setVal( 'ordering', 0 );

						$IF9 = true;

					}


					$IF1->whereE( 'curid', $IF3 );

					$IF1->whereE( 'publish', 1 );

					$IF1->update();

				}

				else

				{

					$IF7->userN('1235463799PSWK');

				}


			}

			else 
			{

				$IF1->whereE( 'curid', $IF3 );

				$IF1->whereE( 'publish', 1 );

				$IFC = $IF1->load( 'ol' );



				if ( !empty( $IFC ) )

				{

					
					$IF1->setVal( 'ordering', 0 );

					$IF1->setVal( 'accepted', 0 );

					$IF1->setVal( 'publish', 0 );

					$IF1->whereE( 'curid', $IF3 );

					$IF1->update();

					$IF9 = true;

				}

				else

				{

					parent::toggle();

					$IFB = true;

				}
			}


			if ( $IFB )

			{

				static $IFE=null;

				static $IFF=null;



				if ( !isset( $IFE ) ) $IFE = WModel::get( 'currency' );

				$IFE->select( 'curid' );


				$IFE->whereE( 'publish', 1 );

				$IFG = $IFE->load( 'lra' );



				$IFH = array();

				$IFI = array();



				
				if ( !isset( $IFF ) ) $IFF = WModel::get( 'currency.conversion' );



				if ( !empty( $IFF ) )

				{

					$IFF->select( 'curid_ref');

					$IFF->whereE( 'curid', $IF3);

					$IFH[$IF3] = $IFF->load( 'lra' );

				}


				
				if ( !empty( $IFH ) && !empty( $IFH ) )

				{

					foreach( $IFH as $IFJ=>$IFK )

					{

						$IFL = $IFJ;

						if ( isset( $IFG ) && !empty( $IFG ) )

						{

							foreach( $IFG as $IFD )

							{

								if ( $IFJ == $IFD )

								{

									$IFM = 1;

								}

								else

								{

									$IFM = array_search($IFD, $IFH[$IFL]);

								}


								if ( $IFM === false )

								{

									$IFI[$IFL][] = $IFD;

								}
							}
						}
					}
				}


				
				$IFN = TR1235463799PSWL;

				$IF8 = $IFN .' :';

				$IFO = PCURRENCY_NODE_MULTICUR;

				if ( isset( $IFI ) && !empty( $IFI ) )

				{

					foreach( $IFI as $IFP=> $IFQ )

					{



						$IFE->select( 'title' );

						$IFE->whereE( 'curid', $IF3 );

						$IFR = $IFE->load('lr');



						if ( !empty( $IFQ ) )

						{



							foreach( $IFQ as $IFS )

							{

								$IFE->select( 'title' );

								$IFE->whereE( 'curid', $IFS );

								$IFT = $IFE->load('lr');



								$IFU = $IFR .' => '. $IFT;

								$IF8 .=  $IFU .'<br>';



								$IFF->setVal( 'curid', $IF3 );

								$IFF->setVal( 'curid_ref', $IFS );

								$IFF->setVal( 'name', $IFU );

								$IFF->setVal( 'alias', $IFU );

								$IFF->setVal( 'fee', $IFO );

								$IFF->setVal( 'modified', time() );

								$IFF->setVal( 'publish', 0 );

								$IFF->insert();

							}
						}


					}


					$IF7->userW( $IF8 );

					$IFV = new stdClass;

					$IFV->content = $IF8;

					$IF7->persistantM( 'currency_conversion_check', $IF6, $IFV, $IF5 );



				}
			}


			
			if ( $IF9 )

			{

				$IFA = 0;

				$IF1->whereE( 'accepted', 1 );

				$IF1->whereE( 'publish', 1 );

				$IF1->orderBy( 'ordering' );

				$IFC = $IF1->load( 'ol' );



				if ( isset( $IFC ) && !empty( $IFC ) )

				{

					foreach( $IFC as $IFL=> $IFD )

					{

						$IFA += 1;



						$IF1->setVal( 'ordering', $IFA );

						$IF1->whereE( 'accepted', 1 );

						$IF1->whereE( 'publish', 1 );

						$IF1->whereE( 'curid', $IFD->curid );

						$IF1->update();

					}
				}
			}
		}

	}

	return parent::toggle();



}























}