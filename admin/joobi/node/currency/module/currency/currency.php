<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;





class Currency_Currency_module extends WModule {


	function create() {

	
		if ( !isset($this->displayby) ) $this->displayby = 0;
		if ( !isset($this->listtype) ) $this->listtype = 0;
		if ( !isset($this->format) ) $this->format = 3;
		

		
		$IB0 = WGlobals::get( 'currency-show', true, 'joobi' );

	

		if ( !defined('PCURRENCY_NODE_MULTICUR') ) WPref::get( 'currency.node' );


		if ( PCURRENCY_NODE_MULTICUR && $IB0 ) {

			
			WGlobals::set( 'currency-displayby', $this->displayby, 'joobi'  );

			WGlobals::set( 'currency-format', $this->format, 'joobi'  );

			WGlobals::set( 'listtype', $this->listtype, 'joobi'  );

			
			$IB1 = WUser::get( 'curid' );
			
									if ( empty( $IB1 ) &&  !defined('CURRENCY_USED') ) {
					$IB2 = WClass::get('currency.format',null,'class',false);
					$IB2->set();
					$IB1 = CURRENCY_USED;
			}			
			static $IB3=null;

			if ( !isset($IB3)) $IB3 = WClass::get( 'currency.form', null, 'class', false );

			$this->content = $IB3->createForm( 'curr6ency_123_abc', 'basket' , 'changecurrency', 'currency', $this->listtype, $IB1 );


		
		} else {
			$this->content = TR1319800425BXDU;
		}		

		return true;

	}
}