<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Currency_Node_install extends WInstall {








	public function install(&$O1) {

		if( $this->newInstall ) {

						$IEJ = WModel::get( 'currency' );
			$IEJ->setVal( 'publish', 0 );
			$IEJ->setVal( 'accepted', 0 );
			$IEJ->update();

						$IEK = array( 'CAD', 'USD', 'EUR', 'GBP', 'JPY', 'AUD', 'NZD' );
			$IEJ->setVal( 'publish', 1 );
			$IEJ->setVal( 'accepted', 1 );
			$IEJ->whereIn( 'code', $IEK );
			$IEJ->update();



			$IEL = 'http://themoneyconverter.com/rss-feed/EUR/rss.xml';
						$IEM = WModel::get( 'currency.exchangesite' );
			$IEM->url = $IEL;
			$IEM->publish = 1;
			$IEM->website = 'http://themoneyconverter.com';
			$IEM->name = 'The Money Converter';
			$IEM->save();



								if( empty($IEN) ) $IEN = WClass::get( 'currency.rate' );
				$IEN->updateExchangeRate( $IEL, time() );

			
		}

	}







	public function addExtensions() {

				$IEO = new stdClass;
		$IEO->namekey = 'currency.currency.module';
		$IEO->name = "Currency module";
		$IEO->folder = 'currency';
		$IEO->type = 25;
		$IEO->publish = 1;
		$IEO->certify = 1;
		$IEO->destination = 'node|currency|module';
		$IEO->core = 1;
		$IEO->params = "publish=0\nwidgetView=currency_currency_module";
		$IEO->description = '';
		$IEP = WAddon::get( 'api.' . JOOBI_FRAMEWORK . '.cmsmenu' );
		$IEO->install = $IEP->modulePreferences();

		if ( $this->insertNewExtension( $IEO ) ) $this->installExtension( $IEO->namekey );


	}



}