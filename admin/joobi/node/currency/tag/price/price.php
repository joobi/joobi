<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */


defined('_JEXEC') or die;

















 class Currency_Price_tag {









 	public $smartUpdate = false;

























	function process($O1) {

		static $I9K=null;



		$I9L = WUser::get('curid');

		$I9M = array();

		foreach( $O1 as $I9N => $I9O ) {



			if ( !isset($I9O->value) ) {

				$I9M[$I9N] = '';

				continue;

			}









			if ( !isset($I9O->currency) ) {

				
				
				$I9M[$I9N] = WTools::format( $I9O->value, 'price', $I9L );

				continue;

			}


			if ( !isset($I9K) ) $I9K = WClass::get( 'currency.convert',null,'class',false);

			if ( !empty($I9K) ) {

				$I9P = $I9K->currencyConvert( $I9O->currency, $I9L, $I9O->value );

				if ( empty( $I9O->symbol ) ) {

					$I9Q = 'money';

				} elseif ( 'money-code' == $I9O->symbol ) {

					$I9Q = 'moneyCode';

				} else {

					$I9Q = $I9O->symbol;

				}
				$I9M[$I9N] = WTools::format( $I9P, $I9Q, $I9L );

			} else {

				$I9M[$I9N] = WTools::format( $I9O->value, 'price', $I9L );

			}


		}

		return $I9M;



	}


}
