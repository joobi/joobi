<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;






class Currency_Conversion_model extends WModel {


	function validate() {



		if(empty($this->name))

			$this->name=$this->l1y_10_6ur($this->curid).' => '.$this->l1y_10_6ur($this->curid_ref);

		if(empty($this->alias))

			$this->alias=$this->l1y_10_6ur($this->curid).' => '.$this->l1y_10_6ur($this->curid_ref);



		$this->rate = $this->exchange * ( ( $this->fee + 100 )/100 );




		return true;

	}








	function l1y_10_6ur($O1){

		$IHY=WModel::get( 'currency');

		$IHY->whereE('curid',$O1);

		return $IHY->load('lr','title');

	}











}