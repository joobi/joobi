<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Currency_Allcurrencies_picklist extends WPicklist {


	function create() {



		static $IAI=null;

		if(empty($IAI)) $IAI=WModel::get( 'currency');

		$IAI->select(array('curid','title','publish'));

		$IAI->orderBy('publish','DESC','1');

		$IAI->orderBy('title','ASC','2');

		$IAJ=$IAI->load('ol');



		if (empty($IAJ)){ return; }



		foreach ($IAJ as $IAK)  {

			$IAL=$IAK->title;



			if($IAK->publish=='0') $IAL.=' ('.TR1227580958QHGM.')';



			$this->addElement($IAK->curid,$IAL);

		}
		

		return true;

   }















}