<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Currency_Formcurrency_picklist extends WPicklist {






function create() {

	static 	$IAC = array();

	

	
	if ( !IS_ADMIN ) {

	

		$IAD = WPref::load( 'PCURRENCY_NODE_MULTICUREDIT' );

		if ( !$IAD ) {

		

		   if ( !defined('CURRENCY_USED') ) {

			    $IAE = WClass::get('currency.format',null,'class',false);

			    $IAE->set();

		   }
		

			$IAF = WClass::get( 'currency.helper', null, 'class', false );

			

			$this->addElement( CURRENCY_USED, $IAF->getValue( CURRENCY_USED, 'title' ) . ' ('. $IAF->getValue( CURRENCY_USED, 'symbol' ) .')' );

		

			return true;

		}
	

	}
	

	

	if ( empty($IAC) ) {

		if ( !isset($IAG) ) $IAG = WModel::get( 'currency' );

	
		$IAG->whereE( 'publish', 1 );

		$IAG->orderBy( 'ordering' );

		$IAG->setLimit( 500 );

		$IAC = $IAG->load( 'ol', array( 'curid', 'title', 'code', 'symbol') );

		

	}


	if ( !empty($IAC) ) {

		foreach( $IAC as $IAH ) {

			$this->addElement( $IAH->curid, $IAH->title . ' ('. $IAH->symbol .')' );

		}
	}
	

	return true;

}}