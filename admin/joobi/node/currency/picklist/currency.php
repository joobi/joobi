<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Currency_Currency_picklist extends WPicklist {


function create() {



	$IAM = WModel::get( 'currency');



	
	$IAN = WGlobals::get( 'currency-displayby', true, 'joobi'  );

	$IAO = WGlobals::get( 'currency-format', true, 'joobi'  );



	switch( $IAN )

	{

		case 1: $IAM->whereE('accepted', 1);

			break;

		default : $IAM->whereE('publish', 1);

			break;

	}


	$IAP = $IAM->load('ol', array( 'curid', 'title', 'symbol', 'code' ) );



	if ( !empty($IAP) )  {

		if ( !empty( $IAP ) )  {

			switch( $IAO ) {

				case 1 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, $IAQ->title .' ('. $IAQ->symbol .') ' );

					break;

				case 2 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, $IAQ->title .' ('. $IAQ->symbol .') ('. $IAQ->code .') ' );

					break;

				case 3 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, $IAQ->title .' ('. $IAQ->code .') ' );

					break;

				case 4 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, $IAQ->title .' ('. $IAQ->code .') ('. $IAQ->symbol .') ' );

					break;

				case 5 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, $IAQ->code );

					break;

				case 6 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, $IAQ->code .' ('. $IAQ->symbol .') ' );

					break;

				case 7 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, ' ('. $IAQ->code .') ('. $IAQ->symbol .') '. $IAQ->title );

					break;

				case 8 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, ' ('. $IAQ->code .') '. $IAQ->title );

					break;

				case 9 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, ' ('. $IAQ->code .') '. $IAQ->title .' ('. $IAQ->symbol .') ' );

					break;

				case 10 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, $IAQ->symbol );

					break;

				case 11 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, ' ('. $IAQ->symbol .') '. $IAQ->title );

					break;

				case 12 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, ' ('. $IAQ->symbol .') '. $IAQ->title  .' ('. $IAQ->code .') ' );

					break;

				case 13 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, ' ('. $IAQ->symbol .') '. $IAQ->code );

					break;

				case 14 : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, '('. $IAQ->symbol .') ('. $IAQ->code  .') '. $IAQ->title );

					break;

				default : foreach( $IAP as $IAQ ) $this->addElement( $IAQ->curid, $IAQ->title );

					break;

			}
		}
	}


}

}