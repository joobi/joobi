<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Currency_Listofsitewdef_picklist extends WPicklist {






function create()

{

	static $IA6=null;

	if( empty($IA6) ) $IA6 = WModel::get( 'currency.exchangesite' );

	$IA6->whereE( 'publish', 1 );

	$IA7 = $IA6->load( 'ol', array('exsiteid', 'name') );

	

	$this->addElement( 0, 'All Exchange Sites' );

	if( !empty($IA7) ) foreach( $IA7 as $IA8 ) $this->addElement( $IA8->exsiteid, $IA8->name );

	return true;

}







}