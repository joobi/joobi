<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;






class Currency_Cur_picklist extends WPicklist {


function create(){

	$IA3=WModel::get('countries');

	$IA3->orderBy('name','ASC');

	

	$IA4=$IA3->load('ol', array('ctyid','name'));

	$this->addElement(0,'Please Select a County');

	

	if(!empty($IA4)){

		foreach($IA4 as $IA5){

			$this->addElement($IA5->ctyid,$IA5->name);

		}

	

	}


	return true;

}}