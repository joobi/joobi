<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Currency_Currencies_picklist extends WPicklist {












	function create() {



		$IAR = WModel::get( 'currency');

		$IAR->select('curid');

		$IAR->select('title');

		$IAR->select('symbol');

		$IAR->whereE( 'publish', 1 );

		$IAR->orderBy('curid');

		$IAS = $IAR->load('ol');



		if ( !empty($IAS) ) {

			$IAT = 0;

			WPref::get( 'currency.node' );

			$IAU = PCURRENCY_NODE_PREMIUM;



			if ( !empty( $IAU ) ) $IAT = PCURRENCY_NODE_PREMIUM;

			

			if ( empty($IAT) && !defined('CURRENCY_USED') ) {

				$IAV = WClass::get('currency.format',null,'class',false);

				$IAV->set();

				$IAU = CURRENCY_USED;

				$IAT = !empty($IAU) ? CURRENCY_USED : 1; 

			}


			if ( empty($IAT) ) $IAT = 1;

			

			$this->setDefault( $IAT );

			

			foreach( $IAS as $IAW ) {



				if ( !empty($IAW->symbol) ) {

					$this->addElement($IAW->curid, $IAW->title .' ('. $IAW->symbol .')');

				} else {

					$this->addElement($IAW->curid, $IAW->title );

				}


			}
		}
		

	}}