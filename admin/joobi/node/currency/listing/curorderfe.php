<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Currency_Curorderfe_listing extends WListings_standard {


function create()

{

	if( $this->value == 0 ) $this->content = TR1206961944PEUR; 

	else

	{

		$IHM = $this->getValue( 'curid', 'order.products' );

		$IHN = $this->getValue( 'curid', 'order' );

	

		if( $IHM != $IHN )

		{

			$IHO = $this->getValue( 'vendid' );

			

			
			static $IHP=null;

			if(empty($IHP)) $IHP = WClass::get('vendor.helper',null,'class',false);

			$IHQ = $IHP->getCurrency($IHO);

		

			static $IHR = null;

			if(empty($IHR)) $IHR = WClass::get( 'currency.convert',null,'class',false);

			if( $IHM == $IHQ ) $IHS = false;

			else $IHS = true;

			$IHT = $IHR->currencyConvert( $IHM, $IHN, (real)$this->value, $IHS );

			

			if( is_numeric($IHT) ) $this->content = WTools::format( $IHT, 'money', $IHN );

			else $this->content = '';

		}

		else $this->content = WTools::format($this->value, 'money', $IHM);

	}
	return true;

}









}