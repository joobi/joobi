<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Currency_Curunitpriceconvert_listing extends WListings_standard {


function create() {
	

	if ( $this->value > 0 ) {

	    $IH7 = $this->getValue( 'curid', 'order.products' );

	    $IH8 = $this->getValue( 'curid', 'order' );

	    $IH9 = $this->getValue( 'vendid' );

	    $this->content = WTools::format($this->value, 'money', $IH7);

	

	    if( $IH7 != $IH8 ) {
	    	

	        
	        static $IHA=null;

	        if(empty($IHA)) $IHA = WClass::get('vendor.helper',null,'class',false);

	        $IHB = $IHA->getCurrency($IH9);

	   

	        static $IHC = null;

	        if(empty($IHC)) $IHC = WClass::get( 'currency.convert',null,'class',false);

	        if( $IH7 == $IHB ) $IHD = false;

	        else $IHD = true;

	        $IHE = $IHC->currencyConvert( $IH7, $IH8, (real)$this->value, $IHD );

	       

	        if( is_numeric($IHE) ) $this->content .= '<br>(<span style="color:blue;">'. WTools::format( $IHE, 'money', $IH8 ) .'</span>)';

	    }
	}

	else $this->content = TR1206961944PEUR;

  

    return true;

}













}