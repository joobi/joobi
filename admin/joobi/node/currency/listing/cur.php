<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Currency_Cur_listing extends WListings_standard {






function create() {

	$IGU = $this->name;

	$IGV = WClass::get('currency.format',null,'class',false);

	$IGW = 'currency_code_'.WModel::get('order','sid');



	static $IGX = null;



	if ( !isset($IGX))

	{

		if ( !defined('CURRENCY_USED') ) {

	            $IGY = WClass::get('currency.format',null,'class',false);

	            $IGY->set();

	        }


	        $IGX = CURRENCY_USED;

	}


	if(isset($this->data->$IGW))

	{

		$this->content = $IGV->displayAmount($this->data->$IGU,$this->data->$IGW);

	}

	else

	{

		$this->content = $IGV->displayAmount($this->data->$IGU, $IGX);

	}


	return true;

}











































}