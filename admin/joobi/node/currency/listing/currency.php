<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Currency_Currency_listing extends WListings_standard {


function create() {



	
	$IH2 = $this->getValue( 'curid' );



	
	if( empty($IH2) )

	{

		$IH3 = $this->getValue( 'vendid' );



		
		if( !empty($IH3) )

		{

			static $IH4=null;

			if(empty($IH4)) $IH4 = WClass::get('vendor.helper',null,'class',false);

			$IH2 = $IH4->getCurrency( $IH3 );

		}


		
		if( empty($IH2) )

		{

			if ( !defined('CURRENCY_USED') )

			{

		            $IH5 = WClass::get('currency.format',null,'class',false);

		            $IH5->set();

		        }


		        $IH2 = CURRENCY_USED;

		}
	}


	
	$IH6 = WClass::get('currency.format',null,'class',false);

	$this->content = $IH6->displayAmount($this->value, $IH2, 1);

	return true;

}



















}