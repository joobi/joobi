<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Vendor_Node_model extends WModel {










function __construct() {



	$filid = new stdClass;

	$filid->fileType = 'images';

	$filid->folder = 'media';

	$filid->path = 'images' . DS . 'vendors';

	$filid->secure = false;

	$filid->format = array('jpg','png','gif','jpeg');



	
	$filid->thumbnail = 1;		
	$filid->maxHeight = 0;		
	$filid->maxWidth = 0;

	$filid->maxTHeight = 50;	
	$filid->maxTWidth = 50;



	$this->_fileInfo = array();

	$this->_fileInfo['filid'] = $filid;



    parent::__construct( 'vendor', 'node', 'vendid' );

}








function addExtra() {


		if ( empty( $this->premium ) ) {


		if ( !defined('PVENDOR_NODE_NEWVENDORUSER') ) WPref::get( 'vendor.node' );

		$uid = WUser::get('uid');

		$user = WUser::get('name');



		$mailingM = WMail::get();


		$name = !empty( $this->name ) ? $this->name : $user;

		$email = !empty( $this->email ) ? $this->email : WUser::get('email');

		$myParams = new stdClass;

		$myParams->vendName = $name;

		$myParams->vendEmail = $email;

		$myParams->vendSite =(!empty($this->website))? $this->website : '';

		if ( isset($this->created) ) $myParams->dateCreated = WApplication::date( JOOBI_DATE1, $this->created );

		else $myParams->dateCreated = WApplication::date( JOOBI_DATE1, time() );

		$myParams->user = $user;



		$mailingM->setParameters( $myParams );

		$emailNameKey = 'vendor_created';



		if ( PVENDOR_NODE_NEWVENDORUSER ) {

			$mailingM->sendNow( $uid, $emailNameKey );

		}


		if ( PVENDOR_NODE_NEWVENDORADMIN ) {

			$mailingM->sendAdmin( $emailNameKey );

		}
	}



	return true;

}





function validate() {





	
	if ( empty($this->curid) ) {

		if ( !defined('PCURRENCY_NODE_PREMIUM') ) WPref::get('currency.node');

		if ( defined('PCURRENCY_NODE_PREMIUM') && PCURRENCY_NODE_PREMIUM > 0 ) {

			$this->curid = PCURRENCY_NODE_PREMIUM;

		}
	}




	if ( empty($this->alias) && !empty($this->name) ) $this->alias = $this->name;


	return true;

}








	public function secureTranslation($sid,$eid) {

		$translationC = WClass::get( 'vendor.translation', null, 'class', false );
		if ( empty($translationC) ) return false;

				if ( !$translationC->secureTranslation( $this, $sid, $eid ) ) return false;
		return true;

	}
}