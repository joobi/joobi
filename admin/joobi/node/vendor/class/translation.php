<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Vendor_Translation_class extends WClasses {






	
	public function secureTranslation($itemO,$sid,$eid) {

		$uid = WUser::get( 'uid' );
		if ( empty($uid) ) return false;
		
						$roleHelper = WRole::get();
		if ( $roleHelper->hasRole( 'storemanager' ) ) return true;

		if ( !$roleHelper->hasRole( 'vendor' ) ) return false;
		
				$vendorHelperC = WClass::get('vendor.helper',null,'class',false);				
		$vendid = $vendorHelperC->getVendorID( $uid );
		if ( empty($vendid) ) return false;
		
		if ( $eid == $vendid ) return true;		
		
		return false;
		
	}	
	
}