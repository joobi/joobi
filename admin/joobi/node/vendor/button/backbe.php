<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Vendor_Backbe_button extends WButtons_external {




function create() {

	

	$roleC = WRole::get();

	$roleVend = $roleC->hasRole('vendor');

	if (!$roleVend) return false;

		

	if ( IS_ADMIN ) $link = WPage::routeURL( 'controller=vendors' );

	else {
		$lastPage = WGlobals::getSession( 'pageLocation', 'lastPage', 'controller=catalog' );
		$lastPageItem = WGlobals::getSession( 'pageLocation', 'lastPageItem', true );
		$link = WPage::routeURL( $lastPage, '', false, false, $lastPageItem );
	}
	$this->setAddress( $link );

	$this->setIcon('back');

	$this->setTitle( TR1206961882TDHA );

	

	return true;

	

}}