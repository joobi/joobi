<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Vendor_inbox_redirectsender_controller extends WController {


function redirectsender() {

	$uid = WGlobals::get( 'eid' );



	$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

	$vendid = $vendorHelperC->getVendorID( $uid );

	

	if ( empty($vendid ) ) WPage::redirect( 'controller=users&task=show&eid='. $uid , true );

	else WPage::redirect( 'controller=vendors&task=home&eid='. $vendid, true );

	return true;

}}