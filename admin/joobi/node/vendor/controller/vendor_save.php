<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Vendor_save_controller extends WController {








function save() {

	
	parent::save();

	

	
	$modelID = WModel::getID( 'vendor' );

	

	
	$trucs = WGlobals::get( 'trucs' );

	$curid = $trucs[$modelID]['curid'];

	

	
	if ( empty($curid) )

	{

		
		$message = WMessage::get();

		$message->adminW( 'Currency id from vendor form was not found or not defined properly. Unable to save currency to preference. Trace controller vendor.save' );

		return true;

	}
	

	
	$extensionM = WModel::get('apps' );

	$extensionM->whereE( 'namekey', 'currency.node' );

	$extensionID = $extensionM->load( 'lr', 'wid' );

	

	if ( !empty($extensionID) )

	{

		
		$preferenceM = WModel::get( 'library.preferences' );

		$preferenceM->setVal( 'text', $curid );

		$preferenceM->whereE( 'wid', $extensionID );

		$preferenceM->whereE( 'namekey', 'premium' );

		$preferenceM->update();

		

		
		$vendid = 1;

		

		
		$vendorC = WClass::get( 'vendor.helper', null, 'class', false );

		if ( !empty($vendorC) )

		{

			$vendorO = $vendorC->getVendor( $vendid );

			

			if ( !empty($vendorO) ) $vendUID = $vendorO->uid;

			else $vendUID = 0;

		}

		else $vendUID = 0;

		

		
		if ( !empty($vendUID) && ( $vendUID > 0 ) )

		{

			$productC = WClass::get( 'product.helper', null, 'class', false );

			if ( !empty($productC) ) $productC->changeProdCurrency( $curid, null, $vendUID );

			else

			{

				$message = WMessage::get();

				$message->adminW( 'Problem of changing the currencies of products. Class not found, trace controller vendor.save' );

			}
		} 

		else

		{

			$message = WMessage::get();

			$message->adminW( 'Problem of changing the currencies of products. Cant get vendor user id, trace controller vendor.save' );

		}
	}

	else

	{

		
		$message = WMessage::get();

		$message->adminW( 'Unable to save preference, currency of the vendor is not save as premium to store. Extension Id of currency.node was not found. Trace controller vendor.save' );

	}
	

	WPage::redirect( 'controller=jstore' );

	

	return true;

	

}}