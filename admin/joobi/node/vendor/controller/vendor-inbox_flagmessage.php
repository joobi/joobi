<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Vendor_inbox_flagmessage_controller extends WController {




function flagmessage()

{

	$mcid = WGlobals::getEID();

	$flag = WGlobals::get( 'flag' );

	

	$setFlag = ( !empty($flag) ) ? 0 : 1;

	

	$conversationM = WModel::get( 'conversation' );

	$conversationM->setVal( 'flag', $setFlag );

	$conversationM->whereE( 'mcid', $mcid );

	$conversationM->update();

	

	WPage::redirect( 'controller=vendor-inbox&task=listing', true );

	return true;

}}