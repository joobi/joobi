<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Vendor_edit_controller extends WController {
function edit() {



	
	$vendorC = WClass::get( 'vendor.helper', null, 'class', false );

	$vendid = $vendorC->getDefault();



	WGlobals::setEID( $vendid );



	return parent::edit();



}}