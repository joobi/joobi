<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Vendor_inbox_cancelmessage_controller extends WController {




function cancelmessage() {

	$trucs = WGlobals::get( 'trucs' );

	$vendid = ( isset($trucs['x']['id']) ) ? $trucs['x']['id'] : 0;

	

	parent::cancel();

	

	$link = ( !empty($vendid)) ? 'controller=vendor-inbox&task=listing&id='. $vendid : 'controller=vendor-inbox&task=listing';

	WPage::redirect( $link, true );

	return true;

}}