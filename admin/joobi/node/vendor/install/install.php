<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;





class Vendor_Node_install extends WInstall {

	public function install(&$object) {


		if ( $this->newInstall ) {

						$this->_defaultImage();

						$usersSyncroleC = WClass::get( 'users.syncrole' );
			$usersSyncroleC->updateThisRole( 'vendor', 'manager' );
			$usersSyncroleC->updateThisRole( 'storemanager', 'admin' );
			$usersSyncroleC->process();

		}
		return true;


	}


	private function _defaultImage() {

		
		$imageM = WModel::get( 'files' );

		$imageM->whereE( 'name', 'vendorx' );

		$imageID = $imageM->load( 'lr', 'filid' );



		if( empty($imageID) ) {

			
			$vendorM = WModel::get( 'vendor' );
			$vendorM->saveItemMoveFile( JOOBI_DS_NODE .'vendor'.DS.'install'.DS.'image'.DS. 'vendorx.png', '', true, 'filid' );
			$imageM->whereE( 'name', 'vendorx' );
			$imageM->setVal( 'core', 1 );
			$imageM->update();
		}


		return true;

	}

}