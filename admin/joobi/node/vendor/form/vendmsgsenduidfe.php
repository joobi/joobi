<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

WLoadFile( 'form.select' , JOOBI_LIB_HTML );
class Vendor_Vendmsgsenduidfe_form extends WForm_select {




function create()

{

	$sentoid = WGlobals::get( 'uid' );

	if ( !empty($sentoid) )	{

		$this->value = $sentoid;

		$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

		$vendid = $vendorHelperC->getVendorID( $sentoid);

		$vendObj = $vendorHelperC->getVendor( $vendid );

		$this->content = '<b style="color:red;">'.$vendObj->name.'</b>';	

	}
	else return parent::create();

	return true;

}}