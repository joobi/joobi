<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Email_Email_preferences_view extends Output_Forms_class {
protected function prepareView(){





WPref::load( 'PEMAIL_NODE_MAILER' );



if(PEMAIL_NODE_USECMS){



$this->removeElements( array( 'apps_general_preferences_mail_config', 'apps_general_preferences_smtp_config'));

}else{

if(PEMAIL_NODE_MAILER !='smtp' ) $this->removeElements( array('apps_general_preferences_smtp_config'));

}


return true;



}}