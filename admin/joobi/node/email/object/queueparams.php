<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Email_Queueparams_object extends WClasses {




public $parameters=null;





public $frequency=null;


private $_queueObject=null;

private $_queueEncoded='';




public function setMailParams($O1){
$this->parameters=$O1;
}
public function getMailParams(){
return $this->_queueObject->parameters;
}
public function setMailFrequency($O2){
$this->frequency=$O2;
}
public function getMailFrequency(){
return $this->_queueObject->frequency;
}

public function setSQLParams($O3){
$this->_queueEncoded=$O3;
}







public function encodeQueue(){

 if(!empty($this->parameters)){
 if(!isset($this->_queueObject)) $this->_queueObject=new stdClass;
 $this->_queueObject->parameters=$this->parameters;
 }
 if(!empty( $this->frequency )){
 if(!isset($this->_queueObject)) $this->_queueObject=new stdClass;
 $this->_queueObject->frequency=$this->frequency;
 }

 if(!empty($this->_queueObject)){
 if(version_compare( PHP_VERSION, '5.2') < 0){
 $this->_queueEncoded=serialize( $this->_queueObject );
 }else{
 $this->_queueEncoded=json_encode( $this->_queueObject );
 } }else{
 $this->_queueEncoded='';
 }

 return $this->_queueEncoded;

}





public function decodeQueue(){

 if(!empty($this->_queueEncoded)){
 if(version_compare( PHP_VERSION, '5.2') < 0){
 $this->_queueObject=unserialize( $this->_queueEncoded );
 }else{
 $this->_queueObject=json_decode( $this->_queueEncoded );
 } }
}

}