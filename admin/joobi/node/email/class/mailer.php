<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;





WLoadFile('api.addon.'.JOOBI_FRAMEWORK.'.mailer');
class Email_Mailer_class extends Joobi_Mailer {






private $_parameters=null;





var $embedimages='';



var $processTags=true;



private $_user=null;






var $sendMultiplePart='';

var $_emailAddAddress=array();
var $_emailAddReplyTo=array();
var $_emailAddCC=array();
var $_emailAddBCC=array();

var $ErrorInfo2='';

var $_fromname='';
var $_fromemail='';
var $_mailer='';












function __construct(){

if(!$this->_validMailer){
return false;
}
if(!defined('PEMAIL_NODE_USECMS')) WPref::get( 'email.node' );

if(PEMAIL_NODE_USECMS){
$I40M=WPage::getMailInfo();
$this->_fromname=$I40M->fromname;
$this->_fromemail=$I40M->mailfrom;
$this->_mailer=$I40M->mailer;
$this->_sendmail=$I40M->sendmail;
$this->_smtpauth=$I40M->smtpauth;
$this->_smtpsecure=$I40M->smtpsecure;
$this->_smtpport=$I40M->smtpport;
$this->_smtpuser=$I40M->smtpuser;
$this->_smtppass=$I40M->smtppass;
$this->_smtphost=$I40M->smtphost;
}else{

$this->_fromname=PEMAIL_NODE_FROMNAME;
$this->_fromemail=PEMAIL_NODE_FROMEMAIL;
$this->_mailer=PEMAIL_NODE_MAILER;
$this->_sendmail=PEMAIL_NODE_SENDMAILPATH;
$this->_smtpauth=PEMAIL_NODE_SMTP_AUTH_REQUIRED;
$this->_smtpsecure=PEMAIL_NODE_SMTP_SECURE;
$this->_smtpport=PEMAIL_NODE_SMTP_PORT;
$this->_smtpuser=PEMAIL_NODE_SMTP_USERNAME;
$this->_smtppass=PEMAIL_NODE_SMTP_PASSWORD;
$this->_smtphost=PEMAIL_NODE_SMTP_HOST;
}
if( !PEMAIL_NODE_DISPLAYNAME ) $this->FromName='';

$this->addSender( $this->_fromemail, $this->_fromname );

$this->Sender=trim(PEMAIL_NODE_SENDEREMAIL);


if(empty($this->Sender)) $this->Sender='';

$this->embedimages=PEMAIL_NODE_EMBEDIMAGES;

switch( strtoupper( substr( PHP_OS, 0, 3 ))){
case "WIN":
$this->LE="\r\n";
break;
case "MAC":
case "DAR":
$this->LE="\r";
break;
default:
$this->LE="\n";
break;
}
switch ( $this->_mailer){
case 'smtp' :
$this->IsSMTP();
$this->Host=$this->_smtphost;
$this->Port=$this->_smtpport;
$this->SMTPAuth=$this->_smtpauth ;
$this->Username=$this->_smtpuser;
$this->Password=$this->_smtppass;
$this->SMTPSecure=$this->_smtpsecure;
if(empty($this->SMTPSecure)) $this->SMTPSecure='';
if(empty($this->Sender)) $this->Sender=$this->_smtpuser;

break;
case 'sendmail' :
$this->IsSendmail();
$this->SendMail=$this->_sendmail;
if(empty($this->SendMail)) $this->SendMail='/usr/sbin/sendmail';
break;
case 'qmail' :
$this->IsQmail();
break;
default :
$this->IsMail();
break;
}
$this->CharSet=PEMAIL_NODE_CHARSET;
if(empty($this->CharSet)) $this->CharSet='utf-8';

$this->SetLanguage('en', $this->PluginDir . 'language' . DS );

$this->sendMultiplePart=PEMAIL_NODE_MULTIPLEPART;


$this->Encoding=PEMAIL_NODE_ENCODING;
if(empty($this->Encoding)) $this->Encoding='binary';

$this->Hostname=PEMAIL_NODE_HOSTNAME;

$this->WordWrap=intval( PEMAIL_NODE_WORDWRAP );

return true;

}





public function validMailer(){
return $this->_validMailer;
}















































function sendUser($O1,$O2=true){

$I40N=false;
if(is_numeric($O1) && $O1>0){
$I40O=WUser::get('data', $O1 );
$this->_user=$I40O;
if(empty($I40O->email)) $I40N=true;
else{
$I40P=$I40O->email;
$I40Q=!empty($I40O->name)? $I40O->name : $I40O->username;
}
}elseif(is_object($O1) && !empty($O1->email)){
if(!empty($O1->username) AND $O1->username !=$O1->email){
$I40Q=$O1->username;
}elseif(!empty($O1->name)){
$I40Q=$O1->name;
}else{
$I40Q=$O1->email;
}

$this->_user=$O1;
$I40P=$O1->email;
}elseif(is_string($O1) && !empty($O1)){
$I40P=$O1;
$I40Q=$I40P;
} else $I40N=true;

if($I40N){
$I40R=WMessage::get();
$I40R->codeE( 'Wrong user in function sendUser in class node/mail/mailer.php' );
$I40R->codeE( 'Two possibilities, either you specified a wrong uid or you are not login and try to send an email with uid=0 ' );

return false;
}
$this->address( $I40P , $I40Q );
$I40S=$this->sendMail( $O2 );

$this->clearAllData();

return $I40S;

}











public function sendMail($O3=true){

if(!$this->_validMailer ) return false;

$I40T=WMessage::get();

if(empty( $this->Subject) || ( empty($this->Body) && empty($this->AltBody))){
$this->ErrorInfo='There is no Subject or Body in this email';
if($O3) $I40T->userW('1230529128EPES');


return false;
}
if($this->processTags){

$I40U=WClass::get('output.process');
$I40U->setParameters( $this->_parameters );
$I40V=array(&$this->Subject,&$this->Body,&$this->AltBody);
$I40U->replaceTags( $I40V, $this->_user );

}
if(!empty($this->AltBody)){
$this->AltBody=preg_replace( '#(href|src|action)[ ]*=[ ]*\"(?!(https?://|\#|mailto:))(?:\.\./|\./|/)?#','$1="'.JOOBI_SITE,$this->AltBody );
}
$this->Body=preg_replace( '#(href|src|action)[ ]*=[ ]*\"(?!(https?://|\#|mailto:))(?:\.\./|\./|/)?#','$1="'.JOOBI_SITE,$this->Body );

if($this->ContentType=='text/html'){
if($this->sendMultiplePart){
if(empty($this->AltBody)){
$this->AltBody=$this->getTextBody($this->Body);
}else{
$this->AltBody=$this->getTextBody($this->AltBody);
}}else{
$this->AltBody='';
}}else{
$this->Body=$this->getTextBody($this->Body);
}
if(!empty($this->_donotsend)) return $this;

if(empty($this->_emailReplyTo) && empty($this->ReplyTo)){
WPref::load( 'PEMAIL_NODE_REPLYEMAIL' );
$this->replyTo( PEMAIL_NODE_REPLYEMAIL, PEMAIL_NODE_REPLYNAME );
}
if(!empty($this->embedimages) && $this->ContentType=='text/html'){
$I40W=$this->l1w_6Y_4zw();
if(!$I40W && $O3 && $this->l1x_6Z_1si()){
$I40X=$this->ErrorInfo;
$I40T->userW( $I40X );
}}
ob_start();
$I40Y=$this->Send();
$I40Z=ob_get_clean();

if(!empty($I40Z) && $O3 && $this->l1x_6Z_1si()){
$I40T->userW( $I40Z );
}
if( !$I40Y ) $this->l1y_70_4es();

$I410=implode( ' ,', $this->l1z_71_3oy());
$I411=$this->Subject;

if( FALSE AND !empty($this->_parameters->mgid)){
if($I40Y){
$I40T->log('SUCCESS [MGID '.$this->_parameters->mgid.'][LGID '.$this->_parameters->lgid.'] '.$I411.' to '.$I410,'email');
}else{
$I40T->log('ERROR [MGID '.$this->_parameters->mgid.'][LGID '.$this->_parameters->lgid.'] '.$I411.' to '.$I410.' | '.$this->ErrorInfo,'email');
}}
if( $O3){

if($this->l1x_6Z_1si()){
if( !$I40Y){
$this->l1y_70_4es();
if(!empty($this->ErrorInfo2)){
$I412=$this->ErrorInfo2;
$I40T->userW( $I412 );
}
$I413=$this->ErrorInfo;
$I40T->userE('1343261978OICB');
$I414=' "'.$I411.'" to '.$I410.' | '.$I413;
$I40T->userE( $I414 );
}else{
$I40T->userS('1236656823OLAQ',array('$SUBJECT'=>$I411,'$RECIPIENT'=>$I410));
}
}else{
if( !$I40Y){
$I40T->userE('1343261978OICB');
}else{
$I40T->userS('1343261978OICC');
}}}

$this->clearAllData();
return $I40Y;

}











public function addSender($O4,$O5=''){

if( !empty($O4)){
$this->From=trim($O4);
}if( !empty($O5) && PEMAIL_NODE_DISPLAYNAME ) $this->FromName=trim( $O5 );

}










  public function address($O6,$O7=''){
  $this->l110_72_5ul( $O6, $O7, 'AddAddress' );
  }










  public function replyTo($O8,$O9=''){
  $this->l110_72_5ul( $O8, $O9, 'AddReplyTo' );
  }






  public function CC($OA,$OB=''){
$this->l110_72_5ul( $OA, $OB, 'AddCC');
  }

  





  public function BCC($OC,$OD=''){
$this->l110_72_5ul( $OC, $OD, 'addBCC');
  }
  




  public function getBody(){
  return $this->Body;
  }

   




  public function getSubject(){
  return $this->Subject;
  }

   




  public function getText(){
  return $this->AltBody;
  }










public function createFromTrans(&$OE,$OF=true){

$this->IsHTML($OF);
$this->Subject=$OE->name;

$OE->ctext=trim($OE->ctext);
if( $OF || empty($OE->ctext)){
$this->Body=$OE->chtml;
$this->AltBody=$OE->ctext;
}else{
$this->Body=$OE->ctext;
}
if(!empty($OE->rmail)){
$this->replyTo($OE->rmail, $OE->rname);
}

if(!empty($OE->smail)){
$this->addSender($OE->smail,$OE->sname);
}

$this->addParameter( 'mgid', $OE->mgid );
$this->addParameter( 'lgid', $OE->lgid );


}






public function clearAllData(){
$this->ClearReplyTos();
$this->ClearAllRecipients();
$this->ClearAttachments();
$this->Subject='';
$this->Body='';
$this->AltBody='';
$this->ErrorInfo='';
$this->ErrorInfo2='';
}







public function addParameter($OG,$OH=null){

if(empty($OG)){
return;
}
if(!isset($this->_parameters)) $this->_parameters=new stdClass;
if(is_array($OG) || is_object($OG)){
foreach($OG as $I415=> $I416){
$this->_parameters->$I415=$I416;
}
}else{
$this->_parameters->$OG=$OH;
}
}




public function setParameters($OI){
$this->_parameters=$OI;
}









public function getTextBody($OJ){
$I417=array('#< */? *br */? *>#i','#< */ *p *>#i','#< */ *tr *>#i');
$I418='/<[ ]*a[ ]*href[ ]*=[ ]*"([^"]*)"[^>]*>([^<]*)<[ ]*\/[ ]*a[ ]*>/i';

return trim(@html_entity_decode(strip_tags(preg_replace($I417, "\n", preg_replace($I418,'${2} ( ${1} )', $OJ))),ENT_QUOTES,'UTF-8'));
}








public function keepAlive($OK=true){
if($OK){
$this->SMTPKeepAlive=true;
}else{
$this->SmtpClose();
}
}





public function returnObject($OL=true){
$this->_donotsend=$OL;
}

private function l1x_6Z_1si(){
static $I419=array();

$I41A=WUser::get( 'uid' );

if(isset($I419[$I41A])) return $I419[$I41A];

if(!empty( $this->_user->uid ) && $I41A==$this->_user->uid ) $I419[$I41A]=true;
else {
$I41B=WRole::get();
if($I41B->hasRole( 'sadmin' )) $I419[$I41A]=true;
else $I419[$I41A]=false;
}
return $I419[$I41A];

}




private function l1w_6Y_4zw(){
    preg_match_all("/(src|background)=\"(.*)\"/Ui", $this->Body, $I41C);
   $I40Y=true;

    if(!empty($I41C[2])){

   $I41D=array('bmp'  =>  'image/bmp',
      'gif'  =>  'image/gif',
      'jpeg' =>  'image/jpeg',
      'jpg'  =>  'image/jpeg',
      'jpe'  =>  'image/jpeg',
      'png'  =>  'image/png',
      'tiff' =>  'image/tiff',
      'tif'  =>  'image/tiff');

   $I41E=array();

      foreach($I41C[2] as $I41F=> $I41G){
                              if(isset($I41E[$I41G])) continue;
      $I41E[$I41G]=1;

            $I41H=str_replace(array(JOOBI_SITE,'/'),array(JOOBI_DS_ROOT,DS),$I41G);
        $I41I=basename($I41G);
        $I41J=md5($I41I);
        $I41K     ='cid:' . $I41J;

        $I41L=strrpos( $I41I, '.' );
        $I41M=substr( $I41I, $I41L+1 );

                if(!isset($I41D[$I41M])) continue;
        $I41N=$I41D[$I41M];
                        if($this->AddEmbeddedImage($I41H, $I41J, $I41I, 'base64', $I41N)){
       $this->Body=preg_replace("/".$I41C[1][$I41F]."=\"".preg_quote($I41G, '/')."\"/Ui", $I41C[1][$I41F]."=\"".$I41K."\"", $this->Body);
        }else{
        $I40Y=false;
        }
      }
    }
    return $I40Y;
}









private function l1z_71_3oy(){
$I41O=array();
foreach( $this->_emailAddAddress as $I41P){
if(empty($I41P->name)){$I41O[$I41P->email]=$I41P->email;
}else{
$I41O[$I41P->email]=$I41P->name . ' ('.$I41P->email.')';
}}return $I41O;
}









  private function l110_72_5ul($OM,$ON='',$OO){

  if( !PEMAIL_NODE_DISPLAYNAME ) $ON='';

  $I41Q=new stdClass;
  $I41Q->email=trim( $OM );
  $I41Q->name=$ON;
  $I41R='_email' . $OO;
  $this->$I41R=array_merge( $this->$I41R, array($I41Q));
  $this->$OO( $OM, $ON );

  }





private function l1y_70_4es(){
$I41S=WClass::get('users.email');

if(!$I41S->validateEmail($this->From)){
$this->ErrorInfo2 .=$this->From.' | '.TR1230529129CVIJ;
}

if(!$I41S->validateEmail($this->Sender)){
$this->ErrorInfo2 .=$this->Sender.' | '.TR1230529129CVII;
}

$I41T=array( '_emailReplyTo', '_emailTo', '_emailAddCC', '_emailAddBCC' );
foreach($I41T as $I41U){

if(!empty($this->$I41U)){
foreach($this->$I41U as $I41V){
if(!$I41S->validateEmail( $I41V->email )){
$this->ErrorInfo2 .=$I41V->email.' | '.TR1230529129CVIK;
}
}}
}
return false;
}
}