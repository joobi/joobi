<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */


defined('_JEXEC') or die;





class Email_Conversion_class extends WClasses {

























public function smartHTMLSize($O1,$O2=null,$O3=false,$O4=true,$O5=true,$O6=false){



if(empty($O2)){

return $O3 ? $this->HTMLtoText( $O1, false, $O5, $O6 ) : $O1;
}else{

if(strlen($O1) > $O2){



if($O3){

$I3ZK=$this->HTMLtoText( $O1, $O4, $O5, $O6 );

$O1=$this->cleanShortenHTML( $I3ZK, $O2 );

}else{

$O1=$this->cleanShortenHTML( $O1, $O2 );

}













}else{

$O1=($O3 ? $this->HTMLtoText( $O1, false, $O5, $O6 ) : $O1);


}


}


return $O1;

}




















public function HTMLtoText($O7,$O8=true,$O9=false,$OA=false){



$O7=str_replace( array( "\n", "\r", "\t" ), '', $O7 );

if($O9){

$O7=str_replace( array( '<br>', '<br />', '<br/>',  '<BR>', '<BR />', '<BR/>' ), "\n\r<br />", $O7 );

$O7=str_replace( array( '<p', '<P' ), "\n\r<p", $O7 );

$O7=str_replace( array( '<li', '<LI' ), "\n\r<li", $O7 );

}


$I3ZL="#< *a[^>]*> *< *img[^>]*> *< *\/ *a *>#isU";

$I3ZM="#< *script(?:(?!< */ *script *>).)*< */ *script *>#isU";

$I3ZN="#< *style(?:(?!< */ *style *>).)*< */ *style *>#isU";

$I3ZO='#< *strike(?:(?!< */ *strike *>).)*< */ *strike *>#iU';

$I3ZP='#< *(h1|h2)[^>]*>#Ui';

$I3ZQ='#< *li[^>]*>#Ui';

$I3ZR='#< */ *(li|td|tr|div|p)[^>]*> *< *(li|td|tr|div|p)[^>]*>#Ui';

$I3ZS='#< */? *(br|p|h1|h2|legend|h3|li|ul|h4|h5|h6|tr|td|div)[^>]*>#Ui';

$I3ZT='/< *a[^>]*href *=*"([^#][^"]*)"[^>]*>(.*)< *\/ *a *>/Uis';



$I3ZU=( $O8 ) ? '${2} ( ${1} )' : '${2}';

$I3ZV=preg_replace(

array($I3ZL, $I3ZM, $I3ZN, $I3ZO, $I3ZP, $I3ZQ, $I3ZR, $I3ZS, $I3ZT),

array('','','','',"\n\n","\n* ","\n","\n", $I3ZU ), $O7 );

$I3ZV=strip_tags( $I3ZV );

$I3ZV=str_replace( array(" ","&nbsp;"), ' ', $I3ZV );

$I3ZV=@html_entity_decode( $I3ZV, ENT_QUOTES, 'UTF-8' );

$I3ZV=trim($I3ZV);



if($O9){


$I3ZV=nl2br( $I3ZV );

}elseif($OA){

$I3ZV=str_replace( array( "\n\r", "\n", "\r", "\r\n", '<br />', '<br/>', '<br>'), ' ', $I3ZV );

}


return $I3ZV;



}




























function cleanShortenHTML($OB,$OC=100,$OD='...',$OE=false,$OF=true){



if(empty($OB) || empty($OC)) return $OB;




if( strlen($OB) < $OC ) return $OB;



$OB=str_replace( array( '<br>', '<br />', '<br/>',  '<BR>', '<BR />', '<BR/>' ), "HHH", $OB );

if(strpos( $OB, 'HHH' ) !==false ) $OE=false;



if($OF){


if(strlen(preg_replace('/<.*?>/', '', $OB)) <=$OC){

return $OB;

}


preg_match_all('/(<.+?>)?([^<>]*)/s', $OB, $I3ZW, PREG_SET_ORDER);

$I3ZX=strlen($OD);

$I3ZY=array();

$I3ZZ='';

foreach ($I3ZW as $I400){


if(!empty($I400[1])){


if(preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $I400[1])){



} else if(preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $I400[1], $I401)){


$I402=array_search($I401[1], $I3ZY);

if($I402 !==false){

unset($I3ZY[$I402]);

}


} else if(preg_match('/^<\s*([^\s>!]+).*?>$/s', $I400[1], $I401)){


array_unshift($I3ZY, strtolower($I401[1]));

}


$I3ZZ .=$I400[1];

}


$I403=strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $I400[2]));

if($I3ZX+$I403> $OC){


$I404=$OC - $I3ZX;

$I405=0;


if(preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $I400[2], $I406, PREG_OFFSET_CAPTURE)){


foreach ($I406[0] as $I407){

if($I407[1]+1-$I405 <=$I404){

$I404--;

$I405 +=strlen($I407[0]);

}else{


break;

}

}

}

$I3ZZ .=substr($I400[2], 0, $I404+$I405);


break;

}else{

$I3ZZ .=$I400[2];

$I3ZX +=$I403;

}


if($I3ZX>=$OC){

break;

}

}

}else{

if(strlen($OB) <=$OC){

return $OB;

}else{

$I3ZZ=substr($OB, 0, $OC - strlen($OD));

}

}




if(!$OE){


$I408=strrpos($I3ZZ, ' ');

if(isset($I408)){


$I3ZZ=substr($I3ZZ, 0, $I408);

}

}


$I3ZZ .=$OD;

if($OF){


foreach ($I3ZY as $I409){

$I3ZZ .='</' . $I409 . '>';

}

}



$I3ZZ=str_replace( "HHH", '<br />', $I3ZZ );



return $I3ZZ;

}




}
