<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




 class Email_Mailing_class extends WClasses {

 private static $_mailer=null;




var $processTags=true;




private $_parameters=null;

private $_frequency=null;





var $forceHtml=null;




var $force=false;






var $_to=array();






 var $_cc=array();






 var $_bcc=array();

 



 private $_sender=array();






 var $_replyTo=array();

   





   var $_donotsend=false;


   private $_validMailer=false;





function __construct(){
if(!isset(self::$_mailer)) self::$_mailer=WClass::get('email.mailer');
$this->_validMailer=self::$_mailer->validMailer();
}









public function sendNow($O1,$O2,$O3=true){
$I3YP=$this->l1v_6X_38z( $O1, $O2, '', '', null, $O3 );
$this->clear();
return $I3YP;
}








private function l1v_6X_38z($O4=null,$O5=0,$O6='',$O7='',$O8=false,$O9=true){

if(!$this->_validMailer ) return false;

static $I3YQ=null;
static $I3YR=null;
static $I3YS=array();
static $I3YT=array();


if(empty($O4)){
return false;
}

if(!empty($O4) && is_array($O4)){

$this->keepAlive(true);
$I3YP=true;
foreach( $O4 as $I3YU){
$I3YP=$this->l1v_6X_38z( $I3YU, $O5, '', '', null, $O9 ) && $I3YP;
}$this->keepAlive(false);
return $I3YP;
}
$I3YV=WMessage::get();


if(( is_numeric($O4) || is_string($O4))){
$I3YW=$O4;
$I3YX=$O4;
$O4=WUser::get( 'data', $I3YX );

if( empty($O4->email) && is_string($I3YX)){
$O4=new stdClass;
$O4->email=$I3YX;
}}
if(empty($O4) || empty($O4->email)){
return false;
}

if(empty($O6)){


$I3YY=( defined('PLIBRARY_NODE_CACHING')) ? PLIBRARY_NODE_CACHING : 10;
$I3YY=( $I3YY > 0 ) ? 'cache' : 'static';

if(!empty($O4->uid)){
$I3YZ=empty($O4->lgid) ? WUser::get('lgid', $O4->uid ) : $O4->lgid;
}else{
$I3YZ=WUser::get( 'lgid' );
}
$I3Z0=$O5 . '|' . $I3YZ;

$I3Z1=WCache::getObject( $I3Z0, 'Mailing', $I3YY, true );
if(empty($I3Z1)) $I3Z1=WCache::getObject( $I3Z0, 'Mailing', 'static', true );

if(empty($I3Z1)){
if(1 !=$I3YZ){
$I3YZ=1;
$I3Z0=$O5 . '|' . $I3YZ;
$I3Z1=WCache::getObject( $I3Z0, 'Mailing', $I3YY, true );
if(empty($I3Z1)) $I3Z1=WCache::getObject( $I3Z0, 'Mailing', 'static', true );
}}

if(empty($I3Z1)){
$I3YV->codeE( 'The mail does not exist: ' . $O5 );
return false;
}
if($I3Z1->publish < 1 && !$this->force){
if($O9){
$I3Z2=$I3Z1->namekey;
$I3YV->userW('1213107686CYMG',array('$MAILING'=>$I3Z2));
}return false;
}
if(!isset($this->forceHtml)){
if( empty($I3Z1->html) || $I3Z1->html=='2' || !isset($O4->html)){
$O8=$I3Z1->html;
}else{
$O8=$O4->html;
}}else{
$O8=$this->forceHtml;
}

self::$_mailer->setParameters($this->_parameters);
self::$_mailer->createFromTrans( $I3Z1, $O8 );
self::$_mailer->processTags=$this->processTags;

}else{

self::$_mailer->IsHTML( $O8 );
self::$_mailer->Subject=$O6;
self::$_mailer->Body=$O7;

}

self::$_mailer->returnObject( $this->_donotsend );

if(!empty($this->_to)){
foreach($this->_to as $I3Z3) self::$_mailer->address( $I3Z3[0], $I3Z3[1] );
}
if(!empty($this->_replyTo)){
foreach($this->_replyTo as $I3Z3) self::$_mailer->replyTo($I3Z3[0],$I3Z3[1]);
}
if(!empty($this->_cc)){
foreach($this->_cc as $I3Z3) self::$_mailer->CC($I3Z3[0],$I3Z3[1]);
}
if(!empty($this->_bcc)){
foreach($this->_bcc as $I3Z3) self::$_mailer->BCC($I3Z3[0],$I3Z3[1]);
}
if(!empty($this->_sender)){
self::$_mailer->addSender( $this->_sender[0], $this->_sender[1] );
}
if(empty($O4)) return self::$_mailer->sendMail( $O9 );
return self::$_mailer->sendUser( $O4, $O9 );

}
















function sendSchedule($OA,$OB,$OC=0,$OD=null,$OE=300,$OF=true){

if($OE > 0){
$I3Z4=WPref::load( 'PEMAIL_NODE_PROCESS_LAST' );
$I3Z5=PEMAIL_NODE_PROCESS_NEXT;
$I3Z6=$I3Z5 - $I3Z4;
if($I3Z4<1 || $I3Z5<1 || $I3Z6 > $OE
|| ( $OC>0 && $OC < time())){return $this->sendNow( $OA, $OB, $OF );
}}
$I3Z7=WClass::get( 'email.scheduled' );
$I3YP=$I3Z7->processScheduledMail( $OA, $OB, $OC, $OD, $this->_parameters, $this->_frequency, $OF );

if($OF){

if($I3YP){
$I3YV=WMessage::get();
if($OE<1){
$I3Z8=WTExt::translate( 'few' );
}else{
$I3Z8=ceil( $OE / 60 );
}$I3YV->userS('1374537510PXRN',array('$MINUTES'=>$I3Z8));
}}
return true;

}






function sendAdmin($OG,$OH=false){

$this->clear(); 
$I3Z9=WUser::getRoleUsers( 'sadmin', array( 'email', 'username' ));

if(empty($I3Z9)){
$I3YV=WMessage::get();
$I3YV->codeE('There is no super admin on the website');
return false;
}
foreach($I3Z9 as $I3ZA=> $I3ZB){
if(empty($I3ZA)){
$I3ZC=$I3ZB;
}else{
$this->address( $I3ZB->email, $I3ZB->username );
}
}
return $this->sendNow( $I3ZC, $OG, $OH );

}

























function sendTextNow($OI,$OJ,$OK='',$OL=false,$OM=true){

$I3YP=$this->l1v_6X_38z( $OI, null, $OJ, $OK, $OL, $OM );

$this->clear();

return $I3YP;

}











function sendTextQueue($ON,$OO,$OP='',$OQ=false,$OR=false){
return $this->sendTextNow( $ON, $OO, $OP, $OQ, $OR );
}










function sendTextAdmin($OS,$OT='',$OU=false,$OV=null,$OW=null){

if(!isset($OV)){if(!defined( 'PEMAIL_NODE_ADMINNOTIF' )) WPref::get( 'email.node' );
$OV=PEMAIL_NODE_ADMINNOTIF;
if(!empty( $OV )){
if(empty($OW)) $OW=JOOBI_SITE_NAME;
$this->address( $OV, $OW );
}else{
$I3Z9=WUser::getRoleUsers('sadmin', array('email','username'));

if(!empty($I3Z9)){
foreach($I3Z9 as $I3ZB) $this->address( $I3ZB->email, $I3ZB->username );
}else{
$I3YV=WMessage::get();
$I3YV->codeE('There is no super admin on the website');
return false;
}
}}else{if(is_array($OV)){
$I3ZA=0;
foreach($OV as $I3ZB){
$I3ZD=( is_array($OW)) ? $OW[$I3ZA] : $OW;
if(empty($OW)) $OW=JOOBI_SITE_NAME;
$this->address( $I3ZB, $I3ZD );
}}else{
if(empty($OW)) $OW=JOOBI_SITE_NAME;
$this->address( $OV, $OW );
}}
$I3YP=$this->l1v_6X_38z( null, null, $OS, $OT, false, false );
$this->clear();

if($OU){
$I3YV=WMessage::get();
if($I3ZE){
$I3YV->userS('1211280107OPVR');
}else{
$I3YV->userE('1299236985FLIB');
$I3ZF=$this->ErrorInfo;
$I3YV->userE($I3ZF);
}}
return $I3YP;

}








function replaceTags($OX,$OY){
$this->returnObject();
return $this->sendNow($OX,$OY,false);
}








function addParameter($OZ,$O10=null){

if(empty($OZ)){
return;
}
if(is_array($OZ) OR is_object($OZ)){


foreach($OZ as $I3ZG=> $I3ZH){
$this->_parameters->$I3ZG=$I3ZH;
}
}else{
$this->_parameters->$OZ=$O10;
}
}





public function setParameters($O11){
$this->_parameters=$O11;
}
public function setFrequency($O12){
$this->_frequency=$O12;
}






public function keepAlive($O13=true){
self::$_mailer->keepAlive($O13);
}







  function address($O14,$O15=''){
$this->_to[]=array( $O14, $O15 );
  }








  function replyTo($O16,$O17=''){
$this->_replyTo[]=array( $O16, $O17 );
  }
    





  function CC($O18,$O19=''){
$this->_cc[]=array( $O18, $O19 );
  }
  





  function BCC($O1A,$O1B=''){
$this->_bcc[]=array( $O1A, $O1B );
  }




  function addSender($O1C,$O1D=''){
$this->_sender=array( $O1C, $O1D );
  }











function addFile($O1E,$O1F='',$O1G='base64',$O1H='application/octet-stream'){
return self::$_mailer->AddAttachment( $O1E, $O1F, $O1G, $O1H );
}

 



function clear(){
$this->_bcc=array();
  $this->_cc=array();
  $this->_to=array();
  $this->_replyTo=array();
  $this->_donotsend=false;
}

 




function returnObject($O1I=true){
$this->_donotsend=$O1I;
}
}






class WMailing {








public function getSQL($O1J,$O1K=true){
static $I3YQ=null;

$I3ZI=explode( '|', $O1J );

$I3ZJ=$I3ZI[0];
$I3YZ=$I3ZI[1];

if(!isset($I3YQ)) $I3YQ=WModel::get('email');
$I3YQ->makeLJ( 'emailtrans', 'mgid' );

$I3YQ->select( array( 'publish', 'html', 'mgid', 'alias', 'namekey' ));

if(is_numeric($I3ZJ)){
$I3YQ->whereE('mgid', $I3ZJ );
}else{
$I3YQ->whereE('namekey', $I3ZJ );
}
$I3YQ->whereE( 'lgid', $I3YZ, 1 );
$I3YQ->select( array('mgid','rname','rmail','sname','smail','chtml','lgid','name','ctext','chtml'), 1 );

$I3YS=$I3YQ->load( 'o' );


if(empty($I3YS)){
if($O1K){
$I3YV=WMessage::get();
$I3YV->codeE( 'Could not find the mail for : '. $I3ZJ );
}return null;
}
$I3YS->id=$I3YS->mgid;

return $I3YS;

}
}