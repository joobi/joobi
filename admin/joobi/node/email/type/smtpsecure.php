<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Email_Smtpsecure_type extends WTypes {


var $smtpsecure=array(

0=> '- - -',

'ssl'=> 'SSL',

'tls'=> 'TLS',

);
}