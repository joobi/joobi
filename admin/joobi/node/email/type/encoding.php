<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Email_Encoding_type extends WTypes {

public $encoding=array(

'binary'=> 'Binary',

'quoted-printable'=> 'Quoted-printable',

'7bit'=> '7 Bit',

'8bit'=> '8 Bit',

'base64'=> 'Base 64'

);
}