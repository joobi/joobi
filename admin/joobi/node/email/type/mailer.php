<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Email_Mailer_type extends WTypes {

var $mailer=array(

'phpmail'=> 'PHP Mail Function',

'sendmail'=> 'SendMail',

'qmail'=> 'QMail',

'smtp'=> 'SMTP Server'

);
}