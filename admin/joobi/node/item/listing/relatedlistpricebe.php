<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Relatedlistpricebe_listing extends WListings_standard {




function create() {

	$price = $this->value;



	if ( $price <= 0 ) $display = TR1206961944PEUR;

	else {

		
		$curid = $this->getValue( 'curid' );

		

		$display = WTools::format( $price, 'money', $curid );

	}
	

	$this->content = $display;

	return true;

}}