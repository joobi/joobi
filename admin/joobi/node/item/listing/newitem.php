<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Newitem_listing extends WListings_standard {
function create()  {

	static $itemTypeC = null;

	

	
	$prodtypid = $this->getValue( 'prodtypid' );



	if ( empty( $prodtypid ) ) {

		$this->content = TR1327698302NOMA;

		return true;

	}




	if (empty($itemTypeC) ) $itemTypeC = WClass::get( 'item.type' );

	$ITEM_TYPE_NAME =  $itemTypeC->loadTypeName( $prodtypid );

	$this->content = str_replace(array('$ITEM_TYPE_NAME'),array($ITEM_TYPE_NAME),TR1359160979NZWA);


	

	$designation = $itemTypeC->loadData( $prodtypid, 'designation' );

	$this->element->lien = 'controller='.$designation.'&task=new(categoryid=catid)(prodtypid=prodtypid)';



	return true;






	
	$option = WGlobals::get( 'option' );

	if ( !empty($option) ) $option = substr( $option, 4 );



	
	
	switch ( $designation ) {

		case 'jsubscription': 

			$text = TR1327698302NOLY;

			break;

		case 'jmarket': 

		case 'jstore': 

			$text = TR1327698302NOLZ;

			break;

		case 'jauction': 

		default: 

			$text = TR1327698302NOMA;

			break;	

	}
	

	$display = $text; 
	

	

	$this->content = $display;

	return true;

	

}}