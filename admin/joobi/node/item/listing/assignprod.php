<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Assignprod_listing extends WListings_standard {
function create()  {

	
	$option = WGlobals::get( 'option' );

	if ( !empty($option) ) $option = substr( $option, 4 );



	
	
	switch ( $option ) {

		case 'jsubscription': 

			$text = TR1218029673NSUZ;

			break;

		case 'jmarket': 

		case 'jstore': 

			$text = TR1206961902CIFE;

			break;

		case 'jauction': 

			$text = TR1298357874SMVP;

			break;

		default: 

			$text = TR1233642085PNTA;

			break;

			

	}
	

	$display = $text .' (<span style="color:red;">'. $this->value .'</span>)';

	

	
	$this->content = '<b>'. ( ($this->value > 0 ) ? $display : $display  ) .'</b>';

	return true;

	

}}