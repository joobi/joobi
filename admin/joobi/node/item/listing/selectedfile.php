<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

WLoadFile( 'listing.yesno' , JOOBI_LIB_HTML );
class Item_Selectedfile_listing extends WListings_yesno {


function create() {



	$pid= WGlobals::get('pid');

	$myid = $this->getValue('filid', 'files');

	$map = WGlobals::get('map');

	$model = WGlobals::get('model');



	$listOfSelectedFilesA = WGlobals::get( 'listOfSelectedFilesA', array(), 'joobi' );


	$value = 0;

	if ( !empty($listOfSelectedFilesA) ) {

		foreach( $listOfSelectedFilesA as $oneFile ) {


			if ( $oneFile->filid == $myid ) $value = 1;

		}
		

	}




	if ( $value ) {

		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/yes.png';

		$display = '<img src="'. $src .'"/>';

	} else {

		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/cancel.png';

		$display = '<img src="'. $src .'"/>';

	}


	$link = 'controller=files-attach&task=selectfile&pid='. $pid .'&filid='. $myid .'&attach='. !$value .'&map='. $map .'&model='. $model;

	$this->content = '<a href="'. WPage::routeURL( $link ) .'">'. $display .'</a>';



	return true;



}}