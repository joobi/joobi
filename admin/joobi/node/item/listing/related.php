<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Related_listing extends WListings_standard {

function create() {

	

	$relatedID = $this->getValue( 'relpid', 'item.related' );

	

	

	
	$eid = WGlobals::get( 'eid' );

	$prodtypid = WGlobals::get( 'prodtypid' );

	$titleheader = WGlobals::get( 'titleheader' );

	$pid = $this->value;



	if ( !empty($relatedID) ) { 

		$related = 1; 
		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/yes.png';

		$display = '<img src="'. $src .'"/>';

	} else {

		$related = 0; 
		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/cancel.png';

		$display = '<img src="'. $src .'"/>';

	}
	

	$link = WPage::routeURL( 'controller=item-related&task=add&relpid='. $pid .'&rel='. $related .'&eid='. $eid .'&prodtypid='. $prodtypid .'&titleheader='. $titleheader );



 	$this->content = '<a href="'. $link .'" >'.$display.'</a>';

	return true;

}}