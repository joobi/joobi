<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Deletefile_listing extends WListings_standard {
function create() {

	$pid= WGlobals::get('pid');

	$map= WGlobals::get('map');

	if (empty($map)) $map = 'filid';

	$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/delete.png';

	$display = '<img src="'. $src .'"/>';

	$link =  WPage::linkPopUp( 'controller=files-attach&task=delete&pid='.$pid.'&eid='. $this->value.'&map='. $map ) ;

	$text = TR1318336000EBWT;

	$this->content = '<a style="cursor: pointer;" onclick=" var yes = confirm(\''.$text.'\'); if (yes) { window.location=\''.$link.'\';}" >'. $display .'</a>';

return true;

}}