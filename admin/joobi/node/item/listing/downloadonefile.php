<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Downloadonefile_listing extends WListings_standard {


function create() {



	$filid = $this->getValue( 'filid' );

	$myKey = WTools::randomString( 100, false );

	WGlobals::setSession( 'order', 'secretKey-' . $filid, md5( $myKey . JOOBI_SITE_TOKEN . $filid ) );

	

	
	$link = WPage::routeURL( 'controller=item&task=downloadall&eid='. $filid . '&secretkey=' . $myKey, '', false, false, true, null, true );



	$this->content = '<a target="_blank" href="'.$link.'">'.TR1206961905BHAV.'</a>';

	

	return true;



}}