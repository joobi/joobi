<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Download_listing extends WListings_standard {

	function create() {



		$path = ( $this->getValue( 'secure', 'files') ) ? JOOBI_URL_SAFE : JOOBI_URL_MEDIA;

		$path .= str_replace( '|', '/', $this->getValue( 'path', 'files') ) . '/';

		$link =  $path . $this->getValue( 'name', 'files') . '.' . $this->getValue( 'type', 'files');

		$this->content = '<a target="_blank" href="'.$link.'">'.TR1206961905BHAV.'</a>';



		return true;

	}
}