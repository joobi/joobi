<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Categoryassign_listing extends WListings_standard {
function create() {

	
	

	$catid = $this->value;

	$catidOrigin = $this->getValue( 'catid', 'item.category' );


	$pid = WGlobals::get('pid');

	$prodtypid = WGlobals::get( 'prodtypid' );

	$titleheader = WGlobals::get( 'titleheader' );



	if ( !empty($catid) ) { 

		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/yes.png';

		$display = '<img src="'. $src .'"/>';

	} else {

		$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/cancel.png';

		$display = '<img src="'. $src .'"/>';

	}
	


	$link = 'controller=item-category-assign&task=assignproducts&pid='. $pid .'&prodtypid='. $prodtypid;

	$link .= '&catid='. $catidOrigin .'&titleheader='. $titleheader;

	 $this->content = '<a href="'. WPage::routeURL( $link ) .'">'. $display .'</a>';

	return true;

}}