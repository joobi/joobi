<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Termtype_listing extends WListings_standard {
function create() {

	if ($this->value == 1) $this->content = TR1206732400OWZK;

	elseif ($this->value == 2) $this->content = TR1341596519RVAQ;

	elseif ($this->value == 3) $this->content = TR1311845275JGPO;

	else $this->content = TR1341596519RVAR;

	

	return true;

}}