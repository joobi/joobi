<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Editbutton_listing extends WListings_standard {
function create() {

	

	if ( WGlobals::get( 'is_popup', false, 'joobi' ) ) return false;

	

	$pid= WGlobals::get('pid');

	$src = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/edit.png';

	$display = '<img src="'. $src .'"/>';

	$link =  WPage::linkPopUp( 'controller=files-attach&task=edit&pid='. $pid .'&eid='. $this->value ) ;

	

	$this->content = '<a href="'. $link .'"'.$extraLink.'>'. $display .'</a>';

	$this->content = WPage::createPopUpLink( $link, $display, '80%', '60%', '', '', '', WGlobals::get( 'is_popup', false, 'joobi' ) );



return true;



}}