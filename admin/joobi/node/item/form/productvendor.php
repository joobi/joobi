<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Productvendor_form extends WForms_standard {


function show() {





	if ( WExtension::exist( 'vendors.node' ) ) {

		$vendid = $this->getValue( 'vendid' );



		$uid = $this->getValue( 'uid' );

		
		$link = 'controller=vendors&task=home&eid='. $this->value;

		$itemid = WGlobals::get( 'Itemid' );

		if ( !empty($itemid) ) $link .= '&Itemid='. $itemid;

		$vendlink = WPage::routeURL( $link, null, true, true, false );





		
		static $vendorHelperC = null;

		if ( empty($vendorHelperC) ) $vendorHelperC = WClass::get('vendor.helper',null,'class',false);

		if ( $vendorHelperC ) $vendor = $vendorHelperC->showVendName( $vendid, $uid, $vendlink );



		$this->content = $vendor;





		return true;



	} else return false;



}}