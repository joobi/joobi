<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Productgallery_form extends WForms_standard {







function show() {



	$definedImgWidth = (int)WGlobals::get( 'maxImageWidth', 180 );

	$definedImgHeight = (int)WGlobals::get( 'maxImageHeight', 180 );
	$imageUseZoom = WPref::load( 'PITEM_NODE_USEZOOM' );




	
	if ( empty($this->value) ) { 


		
		
		$previewID = $this->getValue( 'previewid' );

		if ( !empty( $previewID ) ) return false;



		$path= JOOBI_URL_MEDIA . 'images/products/';



		if ( !empty($definedImgHeight) || !empty($definedImgWidth) ) {

			$newSize = new stdClass;	
			$newSize->width = 180;

			$newSize->height = 180;

			$this->_resizePicture( $newSize, $definedImgHeight, $definedImgWidth );



			$imageHTMLSize = 'width="' . $newSize->width . '" height="' . $newSize->height . '" ';

		} else {

			$imageHTMLSize = '';

		}


		if ( !isset($url) ) $url = $path . 'productx.png';



		$image = '<img  '.$imageHTMLSize.'src="' . $url .'" />';

		$x = $definedImgWidth;

		$y = $definedImgHeight;



		$this->content .= WPage::createPopUpLink( $url, $image, ($x*1.05), ($y*1.05) );





		WGlobals::set( 'imageURL', $url );

		return true;



	} else {



		static $resultA = array();



		if ( !isset( $resultA[ $this->eid ] ) ) {

			
			static $productimgM = null;

			if ( empty( $productimgM ) ) $productimgM = WModel::get('item.images');

			$productimgM->makeLJ( 'files', 'filid' );


			$productimgM->select('filid');

			$productimgM->select( array('path', 'type', 'name', 'twidth', 'theight', 'width', 'height','thumbnail', 'storage' ), 1 );

			$productimgM->whereE('pid' , $this->eid );

			$productimgM->orderBy( 'premium', 'DESC' );

			$productimgM->orderBy( 'ordering', 'ASC' );

			$resultA[ $this->eid ] = $productimgM->load('ol');

		}


		$imagesA = $resultA[ $this->eid ];



		$myNewImageO = WObject::get( 'files.file' );


		if ( $imageUseZoom ) {
						WPage::addJSFile( JOOBI_URL_INC.'jquery/jquery.elevatezoom.js' );
									$JScodeZoom = 'jQuery(".zoomPhoto").elevateZoom({scrollZoom:true,borderSize:2,responsive:true,tint:true,tintColour:\'#74D7F2\'});';
			WPage::addJSScript( $JScodeZoom, JOOBI_URL_INC.'jquery/slides.min.jquery.js', true );
		}


		$numberOfPictures = count($imagesA);

		
		
		if ( $numberOfPictures == 1 ) {



			$oneImage = ( is_array($imagesA) ) ? $imagesA[0] : $imagesA;

			WGlobals::set( 'image-show-filid', $oneImage->filid );



			
			$oneImage->path = implode( '/' , explode('|' , $oneImage->path) );



 			
 			$imgN = $oneImage->name;				
			$imgT = $oneImage->type;				
			$urlID = $oneImage->path .'/'. $imgN .'.'. $imgT;

 			$imgURL = JOOBI_URL_MEDIA . $urlID;

			if ( !empty($definedImgHeight) || !empty($definedImgWidth) ) {

				$newSize = new stdClass;

				$newSize->width = $oneImage->width;

				$newSize->height = $oneImage->height;

				$this->_resizePicture( $newSize, $definedImgHeight, $definedImgWidth );



				$imageHTMLSize = 'width="' . $newSize->width . '" height="' . $newSize->height . '" ';

			} else {

				$imageHTMLSize = '';

			}


			$myNewImageO->name = $oneImage->name;

			$myNewImageO->type = $oneImage->type;

			$myNewImageO->basePath = JOOBI_URL_MEDIA;

			$myNewImageO->folder = ( empty($oneImage->folder) ? 'media' : $oneImage->folder );

			$myNewImageO->path = $oneImage->path;

			$myNewImageO->fileID = $oneImage->filid;

			$myNewImageO->thumbnail = false;

			$myNewImageO->storage = $oneImage->storage;

			$url = $myNewImageO->fileURL();



			$image = '<img';
			if ( $imageUseZoom ) $image .= ' class="zoomPhoto"';

			$image .= ' ' . $imageHTMLSize.'src="' . $url .'" />';
			$x = $oneImage->width;

			$y = $oneImage->height;



			$this->content .= WPage::createPopUpLink( $url, $image, ($x*1.05), ($y*1.05) );



			WGlobals::set( 'imageURL', $url );





		} else {



			
			if ( !is_array($imagesA) ) {

				$imaA = array();

				$imaA[] = $imagesA;

				$imagesA = $imaA;

			}


			WGlobals::set( 'image-show-filid', $imagesA[0]->filid );



			
			$pageTheme = WPage::theme();

			$widCat = WExtension::get( 'catalog.node', 'wid' );

			$folderCat = $pageTheme->getFolder( $widCat, 49 );

			WPage::addCSSFile( JOOBI_URL_THEME . $folderCat . '/css/slider.css' );



			
			$JScode = 'jQuery(function() {jQuery(\'#products\').slides({preload:true,preloadImage:\''.JOOBI_URL_THEME_JOOBI ;

			$JScode .= 'images/loading.gif\',effect:\'fade\',container:\'slideContainer\',crossfade:true,slideSpeed:5000,fadeSpeed:200,generateNextPrev:true,generatePagination:false,play:3500,pause:2500,hoverPause:true});});';





			
			WPage::addJSFile( 'jquery' );

			WPage::addJSScript( $JScode, JOOBI_URL_INC.'jquery/slides.min.jquery.js', false );





			if ( $numberOfPictures > 4 ) {

				
				WPage::addCSSFile( JOOBI_URL_THEME . $folderCat . '/css/littlecarroussel.css' );

				$JSSlider = 'jQuery("#mycarousel").jcarousel();';

				WPage::addJSFile( JOOBI_URL_INC.'jquery/littlecarousel.js' );

				WPage::addJSScript( $JSSlider, JOOBI_URL_INC.'jquery/slides.min.jquery.js', true );

			}




			$imageHTML = '';

			$imagePagiHTML = '';

			$productName = $this->getValue( 'name' );

			$mainImage = null;

			$imgM= WModel::get('files');



			
			$maxHeight = 0;

			$maxWidth = 0;

			foreach( $imagesA as $oneImage ) {

				if ( $oneImage->height > $maxHeight ) $maxHeight = $oneImage->height;

				if ( $oneImage->width > $maxWidth ) $maxWidth = $oneImage->width;

			}
			if ( $maxHeight > $definedImgHeight ) $maxHeight = $definedImgHeight;

			if ( $maxWidth > $definedImgWidth ) $maxWidth = $definedImgWidth;



			
			foreach( $imagesA as $oneImage ) {	


				
				$imgN		= $oneImage->name;				
				$imgT		= $oneImage->type;				
				$imgW		= $oneImage->width;				
				$imgH		= $oneImage->height;			
				$imgtH		= $oneImage->theight;			
				$imgtW		= $oneImage->twidth;			



				
				$myNewImageO->name = $oneImage->name;

				$myNewImageO->type = $oneImage->type;

				$myNewImageO->basePath = JOOBI_URL_MEDIA;

				$myNewImageO->folder = ( empty($oneImage->folder) ? 'media' : $oneImage->folder );

				$myNewImageO->path = $oneImage->path;

				$myNewImageO->fileID = $oneImage->filid;

				$myNewImageO->thumbnail = true;

				$myNewImageO->storage = $oneImage->storage;

				$thumbURL = $myNewImageO->fileURL();



				$myNewImageO->thumbnail = false;

				$mainURL = $myNewImageO->fileURL();



				$newSize = new stdClass;

				$newSize->height = $imgH;

				$newSize->width = $imgW;

				$this->_resizePicture( $newSize, $definedImgHeight, $definedImgWidth );



				
				if ( !isset($mainImage) ) $mainImage = $mainURL;



				$alt = $productName . '-' . $imgN;

				$onClickLink = " onclick=\"window.open('".$mainURL."', 'win2','status=no,toolbar=no,scrollbars=no,titlebar=no,menubar=no,resizable=no,width=400,height=400,directories=no,location=no');return false;\"";

	 			$margin = ( ($maxHeight - $newSize->height - 48 ) * 50 ) / $maxHeight;

	 			if ( $margin < 0 ) $margin = 0;





				
				$image = '<img class="zoomPhoto" style="margin-top:'.$margin.'%;" src="'.$mainURL.'" width="'.$newSize->width.'" height="'.$newSize->height.'"  alt="'.$alt.'">';



				$x = $oneImage->width;

				$y = $oneImage->height;



				$imgXMax = round( $x*1.05 );

				$imgYMax = round( $y*1.05 );





				
				if ( $imgYMax > 800 || $imgXMax > 1000 ) {

					$newSize = new stdClass;

					$newSize->height = $imgYMax;

					$newSize->width = $imgXMax;

					$this->_resizePicture( $newSize, 800, 1000 );

					$imgYMax = $newSize->height;

					$imgXMax = $newSize->width;

				}




				$imageHTML .= WPage::createPopUpLink( $mainURL, $image, $imgXMax, $imgYMax );



	 			$newSize = new stdClass;

				$newSize->height = $imgtH;

				$newSize->width = $imgtW;

				$this->_resizePicture( $newSize, 48, 48 );

	 			$margin = ( 48 - $newSize->height ) / 2;



				$imagePagiHTML .= '<li><a href="#"><img style="margin-top:'.$margin.'%;" src="'.$thumbURL.'" width="'.$newSize->width.'" alt="'. $alt . '-thumb"></a></li>';



			}


			$html = '<div id="slideItemPhoto"><div id="products"><div class="slideContainer">';

			$html .= $imageHTML;






			$html .= '</div><ul id="mycarousel" class="imgthumb jcarousel-skin-tango">';

			$html .= $imagePagiHTML;

			$html .= '<div class="clr"></div></ul></div></div>';






			
			$CSS = '#products .slideContainer{width:'.$definedImgWidth.'px;}';

			$CSS .= '.slideContainer a {width:'.$definedImgWidth.'px;}';


			$CSS .= '#slideItemPhoto .slides_control{height:'. ( $maxHeight  ) . 'px!important;}';	
			$CSS .= '#products .next,#products .prev {top:'. ( $maxHeight / 2 ) . 'px}';	
			WPage::addCSS( $CSS );



			WGlobals::set( 'imageURL', $mainImage );



			$this->content = $html;



		}


	}


	WPage::addJSFile( 'joobibox' );



	return true;



}


















private function _resizePicture(&$newSize,$maxHeight,$maxWidth) {



	$php5 = ( defined('PHP_ROUND_HALF_DOWN') ) ? true : false;



	
	if ( $newSize->height > $maxHeight && $maxHeight > 0 ) {

		$ratio =  $maxHeight / $newSize->height;

		$newSize->width = $php5 ? round( $newSize->width * $ratio, 0, PHP_ROUND_HALF_DOWN ) : round( $newSize->width * $ratio, 0 );

		$newSize->height = $maxHeight;

	}


	
	if ( $newSize->width > $maxWidth && $maxWidth > 0 ) {

		$ratio =  $maxWidth / $newSize->width;

		$newSize->height = $php5 ? round( $newSize->height * $ratio, 0, PHP_ROUND_HALF_DOWN ) : round( $newSize->height * $ratio, 0 );

		$newSize->width = $maxWidth;

	}


}}