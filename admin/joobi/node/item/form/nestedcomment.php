<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'form.layout' , JOOBI_LIB_HTML );
class Item_Nestedcomment_form extends WForm_layout {

function show() {

	$hideComment = WGlobals::get( 'hidecomment' );
	$feedback = $this->getValue( 'feedback' );
	if ( empty( $feedback ) || ( !empty( $hideComment ) && $hideComment == 1 ) ) return false;

		$comment = WGlobals::get( 'itemAllowReview', false, 'joobi' );

		if ( $comment ) {

		if ( !empty($this->value) ) parent::create();

		$list = $this->content;

		if ( empty($list) ) {
			$type = 'first';
		} else $type = 'addcomment';

		$commentC = WClass::get('comment.button', null, 'class', false);
		if ( empty($commentC) ) return true;
		$list .= $commentC->addComment( $type, 'review' );

		$this->content = $list;
		return true;

	}
	return false;
}

}