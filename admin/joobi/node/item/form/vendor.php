<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Vendor_form extends WForms_standard {
function create() {



	if ( !$this->getValue('vendid') ) return false;

	$myLinks = WPage::linkPopUp( 'controller=item-vendor&pid=' . $this->getValue('pid') );

	$this->content .= '<div style="display:block;vertical-align:middle;"><span style="float:left;margin-right:5px;margin-top:7px;">'.$this->value.'</span><span style="float:left;" class="SBLeft"><span class="SBRight"><span class="SBCenter"><div style="display: block;padding-top:7px; vertical-align: middle;"><a rel="{handler:\'iframe\',size:{x:600, y:550}}" rev="joobibox" href="'.$myLinks.'" style="vertical-align:middle;">' . TR1343163677IBQT . '</a></div></span></span></span></div>';



	return true;

}}