<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'form.textonly' , JOOBI_LIB_HTML );
class Item_Googlefeedfile_form extends WForm_textonly {


	function create() {



		$token = WPage::frameworkToken();



		$filename = 'products_feed_' . $token . '.xml';

		$path = JOOBI_DS_EXPORT . $filename;

		$urlpath =  JOOBI_URL_EXPORT . $filename;



		$fileC = WGet::file();

		$exists = $fileC->exist( $path );

		if ( $exists ) {

			$modtime = filemtime( $path );

			$this->content =  '<a href="'.$urlpath.'" > '. $filename . '</a> <em>( ' .TR1310010293JWDN . ': ' .  WApplication::date( JOOBI_DATE5, $modtime ) . ' )</em>';

		}

		else $this->content = $filename . '  <em>( '. TR1310010293JWDO .' )</em>';



		return true;

	}}