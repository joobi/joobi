<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Commenttotal_form extends WForms_standard {


function show() {



	$hideComment = WGlobals::get( 'hidecomment' );

	$feedback = $this->getValue( 'feedback' );

	if ( empty( $feedback ) || ( !empty( $hideComment ) && $hideComment == 1 ) ) return false;



	

	$comment = WGlobals::get( 'itemAllowReview', false, 'joobi' );



	
	if ( $comment == 1 ) {



		$nbREviews = $this->getValue( 'nbreviews', $this->modelID );



		if ( !empty($nbREviews) ) {



			$pid = WGlobals::getEID();

			$commentC= WClass::get( 'comment.restrictions' );

			$itemType = $this->getValue( 'type' );

			WGlobals::set( 'sharedItemType', 10, 'joobi' );

			$total = $commentC->count( $pid, false, $itemType );		


		} else {

			$total = 0;

		}


		$commentButtonC = WClass::get( 'comment.button', null, 'class', false );



		if ( empty($commentButtonC) ) return true;



		$this->content = $commentButtonC->getTitle( $total, 'review' );



		return true;



	}else return false;



}}