<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'form.publish' , JOOBI_LIB_HTML );
class Item_Publishapproval_form extends WForm_publish {
	

function create() {

	

	if ( IS_ADMIN ) return parent::create();

	else {

		if ( !defined( 'PVENDORS_NODE_PRODNOBLOCK' ) ) WPref::get( 'vendors.node' );

		if ( !PVENDORS_NODE_PRODNOBLOCK ) return false;

		

		$block = $this->getValue( 'block' );

		

		if ( $block ) return false;

		else {			



			$unpublishApprove = PVENDORS_NODE_UNPUBLISHAPPROVE;			

			$publishApprove = PVENDORS_NODE_PUBLISHAPPROVE;

			if ( !empty($unpublishApprove) || !empty($publishApprove) ) $this->element->infonly = 1;	

			

			return parent::create();		

		}
	}
	

	return true;

}}