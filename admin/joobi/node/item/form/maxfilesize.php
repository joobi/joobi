<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'form.text' , JOOBI_LIB_HTML );
class Item_Maxfilesize_form extends WForm_text {
function create() {



$status = parent::create();



$maxFileSize1 = @ini_get('post_max_size');

$maxFileSize2 = @ini_get('upload_max_filesize');

$maxFileSize = ( $maxFileSize2 > $maxFileSize1 ) ? $maxFileSize2 : $maxFileSize1;


$maxFileSizeShow = WTools::returnBytes( WTools::returnBytes( $maxFileSize ), true );


$this->content .= '&nbsp;&nbsp;' . TR1331164428MGHN . ' : ' . $maxFileSizeShow;



return $status;

}
}