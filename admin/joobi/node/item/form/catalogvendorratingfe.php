<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Catalogvendorratingfe_form extends WForms_standard {





function show() {

	    $vendid = $this->getValue( 'vendid' );

		if ( empty( $vendid ) ) return false;

		$vote = $this->getValue( 'votes', 'vendor' );
	$rating = $this->getValue( 'score', 'vendor' );
	$nbreviews = $this->getValue( 'nbreviews', 'vendor' );

        if ( $rating > 0 ) $rating = ( $rating / $vote );

    if (!defined('PAPPS_NODE_STARCOLOR')) WPref::get('apps.node');
    $starColor = defined( 'PAPPS_NODE_STARCOLOR' ) ? PAPPS_NODE_STARCOLOR : 'yellow';

	    static $rateClass=null;
    if ( empty($rateClass) ) $rateClass= WClass::get('output.rating');

        $rateC = $rateClass;
    $rateC->primaryId = $vendid;
    $rateC->restriction = 1;
    $rateC->colorPref = $starColor;
    $rateC->rating = $rating;
   	$rateC->option = 0;
    $rateC->type = 0;

				$nbreviews = !empty( $nbreviews ) ? $nbreviews : 0;
	$commentHTML = '( '. $nbreviews .' )';

		$returnId = WView::getURI();				$realVal = base64_encode($returnId);		$option	= WGlobals::get('option');			$vendorURL = 'controller=vendors&task=home&eid='. $vendid;
	$route = WPage::link( $vendorURL ) . '#comment';

        $display = null;
    $display .= $rateC->createHTMLRating( $this );	    $display .= '<a href="'.$route.'">';
    $display .= $commentHTML;
    $display .= '</a>';

    $this->content = $display;
	return true;
}
}
