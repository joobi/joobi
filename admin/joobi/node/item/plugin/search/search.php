<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;












class Item_Search_plugin extends WPlugin {

	


	function onSearchAreas() {
		static $areas = array();

		if (empty($areas)) $areas['items'] = TR1233642085PNTA;

		return $areas;
	}

	function onSearch($text,$phrase='',$ordering='',$areas=null) {

		if (is_array( $areas )) {
			if (!in_array('items' ,$areas )) {
				return array();
			}
		}

				$limit = ( isset($this->search_limit) ) ? $this->search_limit : 10;

				$itemId = ( isset($this->itemid) ) ? (int)$this->itemid : 1;

		$text = trim( $text );
		if ( $text == '' ) return array();

		switch ( $ordering ) {
			case 'alpha':
				$order['field'] = 'name';
				$order['ordering'] = 'ASC';
				$order['as'] = '1';
				break;
			case 'newest':
				$order['field'] = 'modified';
				$order['ordering'] = 'DESC';
				$order['as'] = '0';
				break;
			case 'oldest':
				$order['field'] = 'created';
				$order['ordering'] = 'ASC';
				$order['as'] = '0';
				break;
			case 'category':
			case 'popular':
			default:
				$order['field'] = 'name';
				$order['ordering'] = 'DESC';
				$order['as'] = '1';
				break;
		}
		
		$itemM=WModel::get('item');
		$itemM->select('pid');
		$itemM->select('created');
		$itemM->whereE('publish','1');
		$itemM->makeLJ('itemtrans','pid');
		$itemM->select('name',1,'title');
		$itemM->select('description',1);
		$itemM->select('introduction',1);
		$itemM->where('name','LIKE','%'.$text.'%',1);
		$itemM->where('namekey','LIKE','%'.$text.'%',0,null,null,null,1);
		$itemM->where('description','LIKE','%'.$text.'%',1,null,null,null,1);
		$itemM->where('introduction','LIKE','%'.$text.'%',1,null,null,null,1);
		$itemM->orderBy( $order['field'], $order['ordering'], $order['as'] );
		$itemM->groupBy( 'pid' );
		$itemM->setLimit($limit);

					$itemM->checkAccess();

		$rows = $itemM->load('ol');

		$count = count( $rows );
		for ( $i = 0; $i < $count; $i++ ) {
			$rows[$i]->text = $rows[$i]->description.'  -  '.$rows[$i]->introduction;
			$rows[$i]->section = 'Items';
			$rows[$i]->href = WPage::routeURL( 'controller=catalog&task=show&&eid='. $rows[$i]->pid, '', false, false, $itemId );
			$rows[$i]->browsernav=1;
		}

		
		$itemCategoryM=WModel::get('item.category');
		$itemCategoryM->select('namekey');
		$itemCategoryM->makeLJ('item.categorytrans','catid');
		$itemCategoryM->select('name',1,'title');
		$itemCategoryM->select('description',1,'text');
		$itemCategoryM->select('created');
		$itemCategoryM->select('catid');
		$itemCategoryM->where('name','LIKE','%'.$text.'%',1);
		$itemCategoryM->where('description','LIKE','%'.$text.'%',1,null,null,null,1);
		$itemCategoryM->whereE('publish','1');
		$itemCategoryM->orderBy( $order['field'], $order['ordering'], $order['as'] );
		$itemCategoryM->groupBy( 'catid' );
		$itemCategoryM->setLimit($limit);

					$itemCategoryM->checkAccess();
			$rows2 = $itemCategoryM->load('ol');

		$count = count( $rows2 );
		for ( $i = 0; $i < $count; $i++ ) {
			$rows2[$i]->created = WApplication::date( JOOBI_DATE8, $rows2[$i]->created + WUser::timezone() );
			$rows2[$i]->section = 'Items List';
			$rows2[$i]->href = WPage::routeURL( 'controller=item-category&task=show&catid='. $rows2[$i]->catid, '', false, false, $itemId );
			$rows2[$i]->browsernav=1;
			if ($rows2[$i]->namekey=='root') unset($rows2[$i]);
		}

		return array_merge($rows, $rows2);

	}
}