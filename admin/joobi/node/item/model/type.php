<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Type_model extends WModel {

	public $_fileInfo = array();





	function __construct() {

		$previewid= new stdClass;
		$previewid->fileType = 'medias';
		$previewid->folder = 'media';
		$previewid->path = 'itemtype';
		$previewid->secure = false;
		$previewid->format = 'jpeg,png,jpg,gif';
		$previewid->maxSize = 500 * 1028;	
				$previewid->thumbnail = 1;			$previewid->maxHeight = 50;
		$previewid->maxWidth = 50;
		$previewid->maxTHeight = array( 25 );
		$previewid->maxTWidth = array( 25 );
		$previewid->watermark = false;

		$this->_fileInfo['filid'] = $previewid;

				parent::__construct();

	}


function validate() {

		$typeM = WModel::get( 'item.type' );
	$typeM->whereE( 'namekey', $this->namekey );
	$exit = $typeM->exist();

	if ( $exit ) {
		$this->namekey = $this->genNamekey( '', 64, $this->namekey );
	}
	return true;

}


function extra() {



	$itemMenusC = WClass::get( 'item.menus' );

	$itemMenusC->updateUserMenus();



	return true;



}








	public function secureTranslation($sid,$eid) {

		$translationC = WClass::get( 'item.translation', null, 'class', false );
		if ( empty($translationC) ) return false;

				if ( !$translationC->secureTranslation( $this, $sid, $eid ) ) return false;
		return true;

	}

}