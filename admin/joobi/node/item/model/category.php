<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'category.model.node', JOOBI_DS_NODE );
class Item_Category_model extends Category_Node_model {








function __construct() {



	if ( !defined('PITEM_NODE_CATFORMAT') ) WPref::get('item.node');



	$filid = new stdClass;

	$filid->fileType = 'images';

	$filid->folder = 'media';

	$filid->path = 'images' . DS . 'products' . DS . 'categories';

	$filid->secure = false;

	$filid->format = array('jpg','png','gif','jpeg');





	$valuePref = PITEM_NODE_CATFORMAT;

	if ( !empty( $valuePref ) ) $imgFormat = explode( ',', $valuePref );



	$filid->format = ( !empty($imgFormat) ) ? $imgFormat : array( 'jpg', 'png', 'gif', 'jpeg');



	$filid->maxSize = PITEM_NODE_CATMAXSIZE * 1028;

	
	$filid->thumbnail = 1;	
	$filid->maxHeight = PITEM_NODE_CATMAXH;				
	$filid->maxWidth = PITEM_NODE_CATMAXW;

	$filid->maxTHeight = array( PITEM_NODE_TCATH );				
	$filid->maxTWidth = array( PITEM_NODE_TCATW );

	$filid->watermark = PITEM_NODE_WATERMARKCATEGORY;
	$filid->storage = PITEM_NODE_FILES_METHOD_PHOTOS;


	$this->_fileInfo = array();

	$this->_fileInfo['filid'] = $filid;


	

	parent::__construct();	

}










function addValidate() {



	if ( !empty( $this->namekey ) ) {

		$thisbis = WModel::get('item.category');

		$thisbis->whereE('namekey', $this->namekey);

		$results = $thisbis->load('o');

		if ( !empty ($results) ) {

			if (!isset($this->catid) || $results->catid != $this->catid) {

				$old = $this->namekey;

				$this->namekey = $this->namekey . rand( 0, 100000 );

				$message = WMessage::get();

				$new = $this->namekey;

				$message->userW('1317983130NHRL',array('$old'=>$old,'$new'=>$new));

			}
		}
	}
	
	if ( IS_ADMIN ) {

		$uid = 0;

	} else {

		$uid = WUser::get('uid');

	}

		$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

	if ( empty($this->vendid ) ) $this->vendid = $vendorHelperC->getVendorID( $uid, true );


	parent::addValidate();

	if ( !IS_ADMIN ) {
				if ( !defined( 'PVENDORS_NODE_CATNOBLOCK' ) ) WPref::get( 'vendors.node' );
		if ( !PVENDORS_NODE_CATNOBLOCK ) {
			$this->block = 1;
			$this->publish = 0;
		} else {
			$this->block = 0;
			$this->publish = 1;
		}
	}

	return true;


}



function editValidate() {

	if ( empty($this->filid) ) {

		unset($this->filid);

		return true; 
	}
	return parent::editValidate();

}






function deleteValidate($catid=0) {
	static $doItOnluOnce = false;



	if ( empty($catid) ) return false;

	if ( !$doItOnluOnce ) {
		$doItOnluOnce = true;
		




				$categoryM = WModel::get( 'item' );
		$categoryM->makeLJ( 'item.categoryitem', 'pid' );
		$categoryM->whereE( 'catid', $catid, 1 );
		$categoryM->updatePlus( 'numcat', -1 );
		$categoryM->update();

	}

	




















	return parent::deleteValidate( $catid );



}





function validate() {




	if (empty($this->alias)) $this->alias = $this->getChild('item.categorytrans', 'name'); 


	
	
	$namekey = isset( $this->namekey ) ? $this->namekey : null;

	if ( empty($this->uid) && ( ( $namekey != 'all_products' ) && ( $namekey != 'other_vendors' ) ) ) {

		$uid = WUser::get( 'uid' );

		$this->uid = $uid;

	}


	
	
	
	if ( empty( $this->type ) ) {

		$option = WGlobals::get( 'option' );

		if ( !empty($option) ) $option = substr( $option, 4 );



		$this->type = ( $option == 'jsubscription' ) ? 1 : 0;

	}


	return parent::validate();


}






	public function addExtra() {

		if ( !IS_ADMIN && !PVENDORS_NODE_CATNOBLOCK ) {

			$uid = WUser::get( 'uid' );
			$link = WPage::routeURL( 'controller=item-category-vendor&task=verification&eid='. $this->catid .'&id='. $uid, 'admin' );
			$htmlLink = '<a href="'. $link .'">'. TR1298350849IBYC .'</a>';
			$link = WPage::routeURL( 'controller=item-category&task=listing&search='. $this->catid, 'admin' );
			$showLink = '<a href="'. $link .'">'. TR1340850149PUGR .'</a>';

			$vendorHelperC = WClass::get( 'vendor.helper' );
			$vendorName = $vendorHelperC->getVendor( $this->vendid, 'name' );

						$param = new stdClass;
			$param->vendorName = $vendorName;
			$param->email = WUser::get( 'email' );
			$param->categoryName = $this->getChild( 'item.categorytrans', 'name' );
			if ( empty($param->categoryName) ) $param->categoryName = $vendorName;
			$param->description = $this->getChild( 'item.categorytrans', 'description' );
			if ( empty($param->description) ) $param->description = '...';
			$param->datecreated = WApplication::date( JOOBI_DATE2, time() );
			$param->showLink = $showLink;
			$param->verifyLink = $htmlLink;

						$vendorsEmailC = WClass::get( 'vendors.email' );
			$uidStoreManager = $vendorsEmailC->getStoreMangerContact();

			$mail = WMail::get();
			$mail->setParameters( $param );
			$mail->sendNow( $uidStoreManager, 'vendor_item_category_verification', false );

						$message = WMessage::get();
			$message->userS('1329254268GSEW');


		}
		return parent::addExtra();

	}

	public function extra() {

		$status = parent::extra();
		if ( !$status ) return false;

						$itemWallC = WClass::get( 'item.wall' );
		if ( $itemWallC->available() ) {

			$extraLink = '';
			if ( WExtension::exist( 'affiliate.node') ) {
				$affiliateHelperC = WClass::get( 'affiliate.helper', null, 'class', false );
				$extraLink = $affiliateHelperC->addAffilateToLink();
			}
			$post = new stdClass;
			$pageID = APIPage::cmsGetItemId();
			$link = WPage::routeURL('controller=catalog&task=category&eid='.$this->catid . $extraLink, 'home', false, false, $pageID );
			$CATEGORY_NAME = '<a href="'. $link .'">'.$this->getChild( 'item.categorytrans', 'name' ).'</a>';

			if ( $this->_new ) {
				$post->title = str_replace(array('$CATEGORY_NAME'),array($CATEGORY_NAME),TR1338591625AZEE);
				$post->callingFunction = 'wallcategorynew';
			} else {
				$post->title = str_replace(array('$CATEGORY_NAME'),array($CATEGORY_NAME),TR1338591625AZEF);
				$post->callingFunction = 'wallcategoryedit';
			}
			$content = '';

									$itemCategoryM = WModel::get( 'item.category' );
			$itemCategoryM->whereE( 'catid', $this->catid );
			$imageFile = $itemCategoryM->load( 'lr', 'filid' );
			if ( !empty($imageFile) ) {
				$this->element = new stdClass;
				$this->element->imageWidth = 90;
				$this->element->imageHeight = 90;
								$filesMediaC = WClass::get( 'files.media' );
				$content .= $filesMediaC->renderHTML( $imageFile, $this->element );
			}
			$description = $this->getChild( 'item.categorytrans' ,'description' );
			if ( !empty($description) ) {
				$emailHelperC = WClass::get( 'email.conversion' );
				$content .= $emailHelperC->smartHTMLSize( $description, 150, false, false, false, true );
			}
			$post->content = $content;
			$itemWallC->postWall( $post );

		}
		return parent::extra();

	}
















  	public function getItemDefaultCategory($eid) {

  		if ( empty($eid) ) return 0;

  		$categoryProduct = WModel::get( 'item.categoryitem' );

  		$categoryProduct->whereE( 'pid', $eid );

  		$categoryProduct->orderBy( 'ordering' );

  		return $categoryProduct->load('lr', 'catid');

  	}









	public function secureTranslation($sid,$eid) {

		$translationC = WClass::get( 'item.translation', null, 'class', false );
		if ( empty($translationC) ) return false;

				if ( !$translationC->secureTranslation( $this, $sid, $eid ) ) return false;
		return true;

	}

}