<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Item_Images_model extends WModel {








	function __construct() {



		WPref::load( 'PITEM_NODE_IMGFORMAT' );



		$myImageO = new stdClass;

		$myImageO->fileType = 'images';

		$myImageO->folder = 'media';

		$myImageO->path = 'images' . DS . 'products';

		$myImageO->secure = false;

		$prodPref = PITEM_NODE_IMGFORMAT;

		if ( !empty($prodPref) ) {

			$imgFormat = explode(',', $prodPref );

		}
		$myImageO->format = ( !empty($imgFormat) ) ? $imgFormat : array( 'jpg', 'png', 'gif', 'jpeg');



		
		$myImageO->thumbnail = 1;	
		$myImageO->maxSize = PITEM_NODE_IMGMAXSIZE * 1028;

		$myImageO->maxHeight = ( PITEM_NODE_MAXPH > 50 ? PITEM_NODE_MAXPH : 50 );

		$myImageO->maxWidth = ( PITEM_NODE_MAXPW > 50 ? PITEM_NODE_MAXPW : 50 );
		$myImageO->maxTHeight = ( PITEM_NODE_SMALLIH > 20 ? array( PITEM_NODE_SMALLIH ) : 20 );

		$myImageO->maxTWidth = ( PITEM_NODE_SMALLIW > 20 ? array( PITEM_NODE_SMALLIW ) : 20 );
		$myImageO->watermark = PITEM_NODE_WATERMARKITEM;
		$myImageO->storage = PITEM_NODE_FILES_METHOD_PHOTOS;


		$this->_fileInfo = array();

		$this->_fileInfo['filid'] = $myImageO;


		
		parent::__construct();



	}
}