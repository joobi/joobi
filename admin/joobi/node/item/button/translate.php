<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Translate_button extends WButtons_external {
	

	function create() {



		$useMultipleLang = defined( 'PLIBRARY_NODE_MULTILANG' ) ? PLIBRARY_NODE_MULTILANG : 0;

		if ( empty($useMultipleLang) ) return false;

	

		WPage::addJSFile( 'joobibox' );



		$outputLinkC = WClass::get( 'output.link' );

		$this->_href = $outputLinkC->convertLink( 'controller=item-translate&task=edit(eid=eid)(index=popup)', '', '' );

		$this->setAddress( $this->_href, true );

		

		$this->setPopup();



		return true;

	

	}
	
}