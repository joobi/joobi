<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Publishapproval_button extends WButtons_external {
	
	function create() {
		

		if (IS_ADMIN) return false;
		

		if ( !defined( 'PVENDORS_NODE_PUBLISHAPPROVE' ) ) WPref::get( 'vendors.node' );

		$publishApprove = PVENDORS_NODE_PUBLISHAPPROVE;

		if (empty($publishApprove)) return false;

		

		$text = TR1306763087RUZH;

	

		
		$this->setTitle( $text );

	

		
		$this->setIcon( 'publish' );

	

		
		$this->setAction( 'publishapproval' );

		return true;

		

	}
}