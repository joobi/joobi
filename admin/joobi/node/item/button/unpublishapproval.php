<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Unpublishapproval_button extends WButtons_external {

function create() {

	if (IS_ADMIN) return false;

	if ( !defined( 'PVENDORS_NODE_UNPUBLISHAPPROVE' ) ) WPref::get( 'vendors.node' );

	$unpublishApprove = PVENDORS_NODE_UNPUBLISHAPPROVE;

	if (empty($unpublishApprove)) return false;



	$text = TR1306763087RUZI;



	
	$this->setTitle( $text );



	
	$this->setIcon( 'unpublish' );



	
	$this->setAction( 'unpublishapproval' );

	return true;

}
}