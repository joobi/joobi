<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Catitemassignbe_button extends WButtons_external {




function create() {

	
	$option = WGlobals::get( 'option' );

	if ( !empty($option) ) $option = substr( $option, 4 );



	
	
	
	$text = ( $option == 'jsubscription' ) ? TR1218029673NSUZ : TR1206732372QTKR;



	
	$this->setTitle( $text );



	
	$this->setIcon( 'up' );



	
	$this->setAction( 'assignprod' );

	return true;

}}