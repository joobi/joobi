<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Vendorcategory_button extends WButtons_external {
function create() {



	if ( !defined('PITEM_NODE_ALLOWVENDORCAT') ) WPref::get( 'item.node' );

	if ( !IS_ADMIN && !PITEM_NODE_ALLOWVENDORCAT ) return false;
	

	return parent::create();
	

}}