<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_type_toggle_controller extends WController {
	function toggle() {


		
		$status = parent::toggle();


		$itemMenusC = WClass::get( 'item.menus' );
		$itemMenusC->updateUserMenus();

		return $status;


	}}