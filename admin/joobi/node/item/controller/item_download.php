<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_download_controller extends WController {
function download() {



	$pid = WGlobals::getEID();

	$filid = WGlobals::get( 'filid' );



	if ( empty($pid) || empty($filid) ) return false;

	

	


	$orderDownloadC = WClass::get( 'files.download' );

	$orderDownloadC->getFile( $filid, false );



	return true;



}}