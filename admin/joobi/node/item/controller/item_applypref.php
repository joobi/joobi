<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_applypref_controller extends WController {
function applypref() {


	parent::savepref();
	

	$itemPreferencesC = WClass::get('item.preferences');
	$itemPreferencesC->onSavePref(  $this->generalPreferences  );
	
	WPage::redirect( 'controller=item&task=preferences' );
	

	return true;



}}