<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Item_categoryitem_assigncategories_controller extends WController {




function assigncategories() {
	

	$titleheader = WGlobals::get( 'titleheader' );

	$catid = WGlobals::get( 'catid' );

	$pids = WGlobals::getEID( true );



	if(!empty($pids) && !empty($catid))

	{

		static $categoryproductM = null;

		static $categoryM = null;

		static $productM = null;



		foreach( $pids as $pid)

		{



			if( !isset($categoryproductM)) $categoryproductM = WModel::get( 'item.categoryitem' );

			$categoryproductM->whereE( 'catid', $catid );

			$categoryproductM->whereE( 'pid', $pid );

			$categoryproduct = $categoryproductM->load('lr', 'pid');



			if(empty($categoryproduct))

			{

				
				if( !isset($categoryproductM)) $categoryproductM = WModel::get( 'item.categoryitem' );

				$categoryproductM->setVal( 'catid', $catid );

				$categoryproductM->setVal( 'pid', $pid );

				$categoryproductM->insert();

			}

			else

			{

				
				if( !isset($categoryproductM)) $categoryproductM = WModel::get( 'item.category.item' );

				$categoryproductM->whereE( 'catid', $catid );

				$categoryproductM->whereE( 'pid', $pid );

				$categoryproductM->delete();

			}
									$prodHelperC = WClass::get('item.helper',null,'class',false);

						$obj = new stdClass;
			$obj->tablename = 'item.categoryitem';
			$obj->column = 'pid';
			$obj->groupBy = 'catid';
			$obj->filterCol1 = 'catid';
			$obj->filterVal1 = $catid;
			$numOfProd = $prodHelperC->noOfItem( $obj );

						if( !empty($numOfProd) && is_numeric($numOfProd) )
			{
				if( !isset($categoryM) ) $categoryM = WModel::get( 'item.category' );
				$categoryM->setVal( 'numpid', $numOfProd );
				$categoryM->whereE('catid', $catid);
				$categoryM->update();
			}
						$obj = new stdClass;
			$obj->tablename = 'item.categoryitem';
			$obj->column = 'catid';
			$obj->groupBy = 'pid';
			$obj->filterCol1 = 'pid';
			$obj->filterVal1 = $pid;
			$numOfCat = $prodHelperC->noOfItem( $obj );

						if( !empty($numOfCat) && is_numeric($numOfCat) )
			{
				if( !isset($productM) ) $productM = WModel::get( 'item' );
				$productM->setVal( 'numcat', $numOfCat );
				$productM->whereE('pid', $pid);
				$productM->update();
			}

		}
	}


	WPage::redirect( 'controller=item-categoryproduct&catid='. $catid .'&titleheader='. $titleheader);

	return true;

}

}