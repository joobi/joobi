<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_presave_controller extends WController {


	function presave() {



		$this->save();

		$eid = $this->getElement();

		WGlobals::setEID( $eid );



		if ( empty($this->_model->prodtypid) ) {

			$message = WMessage::get();

			$message->historyE('1298350856KAZQ');

		}


		
		$productM = WModel::get('product.type');



		
		$model = $this->getModel();

		$productM->whereE( 'prodtypid', $model->prodtypid );

		$productDegination = $productM->load( 'lr', 'type');


		$updateDefaultCurrency = false;


		
		switch ( $productDegination ) {

			case '5':	
				$updateDefaultCurrency = true;

				$otherProductM = WModel::get( 'subscription.infos' );

				$otherProductM->setVal( 'pid', $eid );

				$otherProductM->insert();
				break;

			case '11':	
				$updateDefaultCurrency = true;

				$otherProductM = WModel::get( 'auction.infos' );

				$otherProductM->setVal( 'pid', $eid );

				$otherProductM->insert();


				
				$ProductM = WModel::get( 'product' );

				$ProductM->whereE( 'pid', $eid );

				$ProductM->setVal( 'stock', 1);

				$ProductM->update();

				break;

			case '17':	
				$updateDefaultCurrency = true;




				break;

			case '1':					$updateDefaultCurrency = true;
			default:

				
				break;

		}

		if ( $updateDefaultCurrency ) {
								$ProductM = WModel::get( 'product' );
				$ProductM->whereE( 'pid', $eid );
				WPref::load('PCURRENCY_NODE_PREMIUM');
				$defaultCurrency = ( defined('PCURRENCY_NODE_PREMIUM') && PCURRENCY_NODE_PREMIUM ) ? PCURRENCY_NODE_PREMIUM : 1;
				$ProductM->setVal( 'curid', $defaultCurrency );
				$ProductM->update();
		}
		
		if ( !IS_ADMIN ) {

			
			WPage::redirect( 'controller=product-vendor&task=save&eid='. $eid );

		} else {

			
			$controller = WGlobals::get( 'controller' );

			WPage::redirect( 'controller='.$controller.'&task=edit&eid='. $eid );

		} 


	return true;

}}