<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_savepref_controller extends WController {
function savepref() {


	parent::savepref();
	

	$itemPreferencesC = WClass::get('item.preferences');
	$itemPreferencesC->onSavePref(  $this->generalPreferences );
	

	return true; 



}}