<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_toggle_controller extends WController {
function toggle() {


		$pid = WGlobals::getEID();
	$subTask = $this->subTask;
	
	$allowOnlyPublish = false;
	if ( !IS_ADMIN && WExtension::exist( 'vendors.node' ) ) {
				$roleHelper = WRole::get();
		$storeManagerRole = $roleHelper->hasRole( 'storemanager' );
				
		if ( !$storeManagerRole ) {
								$roleHelper = WRole::get();
				if ( $roleHelper->hasRole( 'vendor' ) ) {
					$vendorHelperC = WClass::get('vendor.helper',null,'class',false);
					$uid = WUser::get('uid');				
					$this->_vendid = $vendorHelperC->getVendorID( $uid );
					if ( !empty($this->_vendid) ) {
						$allowOnlyPublish = true;
												$itemM = WModel::get( 'item' );
						$itemM->whereE( 'pid', $pid );
						$itemM->whereE( 'vendid', $this->_vendid );
						if ( !$itemM->exist() ) {
							return false;
						}					}				} else {
					return false;
				}			
		}		
	}	

	

	if ( $allowOnlyPublish  ) {

	

		if ( $subTask[1] != 'publish' ) return false;
		

		$taskvar = $this->taskvar;

		

		

		$value = $taskvar[1];



		

			
		

			if ( !defined( 'PVENDORS_NODE_PRODNOBLOCK' ) ) WPref::get( 'vendors.node' );

			if ( !PVENDORS_NODE_PRODNOBLOCK ) {

			
			if ( !defined( 'PVENDORS_NODE_PUBLISHAPPROVE' ) ) WPref::get( 'vendors.node' );
			$unpublishApprove = PVENDORS_NODE_UNPUBLISHAPPROVE;			
			$publishApprove = PVENDORS_NODE_PUBLISHAPPROVE;


			if ( $value == 0 && $unpublishApprove ) {
								$message = WMessage::get();
				$message->userN('1342574026QQWP');
				$itemApprovalC = WClass::get( 'item.approval' );
				$itemApprovalC->emailRequestApproval( $pid, 'admin_unpublish_approval' );				
				$controller = WGlobals::get( 'controller' );
				WPage::redirect( 'controller=' . $controller );
				return true;
			}

			if ( $value == 1 && $publishApprove ) {
								$message = WMessage::get();
				$message->userN('1342574026QQWP');
				$itemApprovalC = WClass::get( 'item.approval' );
				$itemApprovalC->emailRequestApproval( $pid, 'admin_publish_approval' );				
				$controller = WGlobals::get( 'controller' );
				WPage::redirect( 'controller=' . $controller );
				return true;
			}			

			

			}

	

	}


	return parent::toggle();



}}