<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_category_assign_back_controller extends WController {


function back() {

	WPage::redirect( 'controller=catalog&task=listing' );

	return true;

}}