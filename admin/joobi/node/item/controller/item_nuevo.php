<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_nuevo_controller extends WController {

	function nuevo() {



		
		$itemQueryC = WClass::get( 'item.query' );
		$total = $itemQueryC->count( true );
		$categoryID = WGlobals::get( 'categoryid' );
		$prodtypid = WGlobals::get( 'prodtypid' );

		$productNodeExits = WExtension::exist( 'product.node' );
		if ( $productNodeExits ) $this->setView( 'product_new' );


		if ( empty($total) ) {
			$message = WMessage::get();
			$message->userE('1327273916WPA');
			WPage::redirect( 'previous' );
		} elseif ( $total == 1 ) {

			$this->_checkRestriction();

						$typePublished = $itemQueryC->getPublishedType();

			$link = 'controller=' . $typePublished . '&task=add';
			if ( !empty($prodtypid) ) $link .= '&prodtypid=' . $prodtypid;
			if ( !empty($categoryID) ) $link .= '&categoryid=' . $categoryID;

			WPage::redirect( $link );

		} else {

			$this->_checkRestriction();

			
						if ( !empty($prodtypid) ) {
				$itemTypeC = WClass::get('item.type');
				$itemDesignation = $itemTypeC->loadData( $prodtypid, 'type'  );

								$itemDesignationT = WType::get( 'item.designation' );
				$controller2Use = strtolower( $itemDesignationT->getName( $itemDesignation ) );
								if ( empty( $controller2Use ) ) {
					$controller2Use = 'item';
				}
				$link = 'controller=' . $controller2Use . '&task=add&=prodtypid' . $prodtypid;
				if ( !empty($categoryID) ) $link .= '&categoryid=' . $categoryID;

				WPage::redirect( $link );

			}
		}

		$downloadNodeExits = WExtension::exist( 'download.node' );
				if ( !$productNodeExits && $downloadNodeExits ) {
			$link = 'controller=download&task=add';
			if ( !empty($prodtypid) ) $link .= '&prodtypid=' . $prodtypid;
			if ( !empty($categoryID) ) $link .= '&categoryid=' . $categoryID;
			WPage::redirect( $link );
		}


		return true;



	}





	private function _checkRestriction() {

			if ( !defined('PCATALOG_NODE_SUBSCRIPTION_INTEGRATION') ) WPref::get( 'catalog.node' );
			$integrate = PCATALOG_NODE_SUBSCRIPTION_INTEGRATION;

						if ( $integrate && WExtension::exist( 'subscription.node' ) ) {
				$subscriptionCatalogrestrictionC = WClass::get( 'subscription.catalogrestriction' );
				$subscriptionCatalogrestrictionC->itemCreate();
			}
	}
}