<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_deleteimage_controller extends WController {




function deleteimage() {



	$pid=WGlobals::get('pid');

	$premium=WGlobals::get('premium');

	$filid=WGlobals::get('filid');

	$controller = WGlobals::get('tocontroller');

		$productImageM=WModel::get('item.images');
	$productImageM->whereE('pid', $pid);
	$productImageM->whereE('filid', $filid);
	$productImageM->delete();

	if ( $premium==1 ) {

				$productImageM->whereE( 'pid', $pid );
		$productImageM->setVal( 'premium', 1 );
		$productImageM->setLimit( 1 );
		$productImageM->update();


	}

	WPage::redirect('controller='.$controller.'&task=edit&eid='.$pid);


	return true;


}}