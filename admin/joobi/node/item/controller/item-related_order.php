<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_related_order_controller extends WController {
function order() {

	$eid=WGlobals::getEID();

	parent::order();



	WPage::redirect('controller=item-related&eid='.$eid);

	return true;

}}