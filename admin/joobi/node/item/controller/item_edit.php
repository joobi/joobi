<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Edit_controller extends WController {


function edit() {



	$eid = WGlobals::getEID();

	
	$productM = WModel::get('item');

	$productM->makeLJ( 'item.type', 'prodtypid' );

	$productM->select( 'type', 1 );

	$productM->whereE( 'pid', $eid );

	$productM->checkAccess();


	$productDegination = $productM->load( 'lr');

	
	switch ( $productDegination ) {

		case '5':	
			$this->setView( 'subscription_edit_item' );

			break;

		case '11':	
			$this->setView( 'auction_edit_item' );

			break;

		case '7':				$this->setView( 'voucher_edit_item' );
			break;
		case '16':				$this->setView( 'classified_edit_item' );
			break;
		case '17':	
			
			
		case '1':	
			$this->setView( 'product_edit_item' );

			break;

		case '100':	
		case '101':	
		case '121':	
			$this->setView( 'item_edit_item' );

			break;

		case '141':	
			$this->setView( 'download_edit_item' );

			break;

		default:

			if ( IS_ADMIN ) {

				$this->setView( 'item_edit_item' );

			} else {

				$message = WMessage::get();

				$message->userW('1298350856KAZP');

				WPage::redirect( 'controller=catalog' );

			}
			break;



	}


	return parent::edit();




}}