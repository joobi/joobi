<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_category_assignprod_controller extends WController {


	

	function assignprod() {

		$catid = WGlobals::getEID();

		static $categorytransM = null;

	

		if (empty($categorytransM)) $categorytransM = WModel::get( 'item.categorytrans' );

		$categorytransM->select( 'name' );

		$categorytransM->whereE( 'catid', $catid );

		$categorytrans = $categorytransM->load( 'r' );

	

		WPage::redirect( 'controller=item-assign&task=listing&catid='. $catid .'&titleheader='. $categorytrans );

		return true;

	}}