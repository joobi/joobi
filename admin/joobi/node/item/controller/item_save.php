<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Save_controller extends WController {


function save() {



	$status = parent::save();

	if ( !$status ) return $status;



	if ( empty($this->_model->numcat) ) {

		$name = $this->_model->getChild( $this->_model->getModelNamekey() . 'trans', 'name' );

		$message = WMessage::get();

		$message->userN('1338404830HCNT');

		WPage::redirect( 'controller=item-category-assign&task=listing&pid='.$this->_model->pid.'&prodtypid='.$this->_model->prodtypid.'&titleheader='. $name );

	}

	return $status;



}

}