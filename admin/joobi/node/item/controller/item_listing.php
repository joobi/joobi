<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_listing_controller extends WController {
	function listing() {

	
	if (  !IS_ADMIN ) {

	
		if ( !defined('PCATALOG_NODE_EDITITEMLOCATION') ) WPref::get('catalog.node');
		
		if ( PCATALOG_NODE_EDITITEMLOCATION == 'site' || PCATALOG_NODE_EDITITEMLOCATION == 'popup' ) {
						WGlobals::setSession( 'page', 'space', PCATALOG_NODE_EDITITEMLOCATION );
			$simplifiedView = PCATALOG_NODE_ITEMLISTTYPE;
						if ( !empty( $simplifiedView ) ) {
				$this->setView( 'item_my_show_list' );
			}			
		} elseif ( PCATALOG_NODE_EDITITEMLOCATION == 'vendors' ) {
			WGlobals::setSession( 'page', 'space', PCATALOG_NODE_EDITITEMLOCATION );
		} else {
						$simplifiedView = PCATALOG_NODE_ITEMLISTTYPE;
			if ( !empty( $simplifiedView ) ) {
				$this->setView( 'item_my_show_list' );
			}		}		
	}
	

	return parent::listing();

	}
}