<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_deletedownload_controller extends WController {
function deletedownload() {



	$pid=WGlobals::get('pid');

	$filid=WGlobals::get('filid');

	$controller = WGlobals::get('tocontroller');



	
	$productImageM=WModel::get('item.downloads');

	$productImageM->whereE('pid', $pid);

	$productImageM->whereE('filid', $filid);

	$productImageM->delete();



	WPage::redirect('controller='.$controller.'&task=edit&eid='.$pid);



	return true;



}}