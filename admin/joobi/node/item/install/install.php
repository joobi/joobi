<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Node_install extends WInstall {

	public function install(&$object) {



				$this->_checkInstallCategory();

				if ( $this->newInstall ) {


									$this->_defaultimage();


			$this->_insertDefProdType();

			$productExist = WExtension::exist('product.node');

			if ( $productExist ) {
								if ( !defined( 'PPRODUCT_NODE_DWLDFORMAT' ) ) WPref::get('product.node');
				if ( defined('PPRODUCT_NODE_DWLDFORMAT') ) {
					$tobeUpdated = array( 'dwldformat','dwldmaxsize','prwformat','prwmaxsize',
					'imgformat','maxph','maxpw','smallih','smalliw','imgmaxsize',
					'catformat','catmaxsize','catmaxh','catmaxw','tcath','tcatw','allowprodallcat','maxproductassign');

					foreach( $tobeUpdated as $oneUpdated ) {
						$constantName = 'PPRODUCT_NODE_'. strtoupper($oneUpdated);
						$tempVar = constant($constantName);
						$itemPref = WPref::get('item.node');
						if ( !empty($tempVar) ) $itemPref->updatePref( $oneUpdated, $tempVar );
					}
				}
			}
		}


		return true;


	}








	public function addExtensions() {

		$extension = new stdClass;
		$extension->namekey = 'item.search.plugin';
		$extension->name = "Items search";
		$extension->folder = 'search';
		$extension->type = 50;
		$extension->publish = 1;
		$extension->certify = 1;
		$extension->destination = 'node|item|plugin';
		$extension->core = 1;
		$extension->params = 'publish=1';
		$extension->description = '';

		if ( $this->insertNewExtension( $extension ) ) $this->installExtension( $extension->namekey );



				$extension = new stdClass;
		$extension->namekey = 'item.item.module';
		$extension->name = "Items module";
		$extension->folder = 'item';
		$extension->type = 25;
		$extension->publish = 1;
		$extension->certify = 1;
		$extension->destination = 'node|item|module';
		$extension->core = 1;
		$extension->params = "publish=0\nwidgetView=item_item_module";
		$extension->description = '';
		$libraryCMSMenuC = WAddon::get( 'api.' . JOOBI_FRAMEWORK . '.cmsmenu' );
		$extension->install = $libraryCMSMenuC->modulePreferences();

		if ( $this->insertNewExtension( $extension ) ) $this->installExtension( $extension->namekey );




				$extension = new stdClass;
		$extension->namekey = 'item.itemcat.module';
		$extension->name = "Items' categories module";
		$extension->folder = 'itemcat';
		$extension->type = 25;
		$extension->publish = 1;
		$extension->certify = 1;
		$extension->destination = 'node|item|module';
		$extension->core = 1;
		$extension->params = "publish=0\nwidgetView=item_itemcat_module";
		$extension->description = '';
		$libraryCMSMenuC = WAddon::get( 'api.' . JOOBI_FRAMEWORK . '.cmsmenu' );
		$extension->install = $libraryCMSMenuC->modulePreferences();

		if ( $this->insertNewExtension( $extension ) ) $this->installExtension( $extension->namekey );


	}








	public function version_955() {


		$itemM = WModel::get( 'item', 'object' );
		$itemM->select( array('pid','filid') );
		$itemM->where( 'filid', '!=', '0' );
		$selectQ = $itemM->printQ('load');

		$itemDownloadsM = WModel::get( 'item.downloads', 'object' );
		$itemDownloadsM->setIgnore();
		$itemDownloadsM->insertSelect( array('pid','filid'), $selectQ );

		return true;

	}





	private function _defaultimage() {

				$imageM = WModel::get( 'files' );
		$imageM->whereE( 'name', 'productx' );
		$imageID = $imageM->load( 'lr', 'filid' );

		if ( empty($imageID) ) {
						$productM = WModel::get('item.images');
			$fileLocation = JOOBI_DS_NODE .'item'.DS.'install'.DS.'images'.DS. 'productx.png';
			$status = $productM->saveItemMoveFile( $fileLocation, '', true, 'filid' );

			$productCatM = WModel::get('item.category');
			$productCatM->saveItemMoveFile( JOOBI_DS_NODE .'item'.DS.'install'.DS.'images'.DS. 'categoryx.png', '', true, 'filid' );

			$imageM->whereIn( 'name', array( 'productx', 'categoryx' ) );
			$imageM->setVal( 'core', 1 );
			$imageM->update();

		}
		return true;

	}





	private function _checkInstallCategory() {

		$prodcatM = WModel::get( 'item.category');
		$prodcatM->whereIn( 'namekey', array( 'root', 'default', 'other_vendors' ) );
		$categoryA = $prodcatM->load( 'lra', 'namekey' );

				if ( empty( $categoryA ) || !in_array( 'root', $categoryA ) ) $this->_rootCategory();
		if ( empty( $categoryA ) || !in_array( 'default', $categoryA ) ) $this->_defaultCategory();
		if ( empty( $categoryA ) || !in_array( 'other_vendors', $categoryA ) ) $this->_otherVendor();

		return true;

	}






	private function _rootCategory() {

				$prodcatM = WModel::get( 'item.category' );
		$prodcatM->noValidate();
		$prodcatM->namekey = 'root';
		$prodcatM->alias = 'root';
		$prodcatM->setChild( 'item.categorytrans', 'name', 'Home' ); 		$prodcatM->setChild( 'item.categorytrans', 'description', 'Top: root category, do not remove it!' ); 		$prodcatM->setChild( 'item.categorytrans', 'lgid', 1); 		$prodcatM->publish=1;
		$prodcatM->rgt=2;
		$prodcatM->lft=1;
		$prodcatM->parent=0;
		$prodcatM->uid=0;
				$prodcatM->params='ctygeneral=1
catcrslsorting=featured
catcrsldisplay=standard
ctyitems=1
ctysorting=ordering
ctydisplay=standard
ctylayoutcol=4
ctyshowname=1
ctyshownbitm=1
ctyshowimage=1
itmsorting=ordering
itmdisplay=standard';
		$prodcatM->save();

		return true;

	}






	private function _defaultCategory() {

				$prodcatM = WModel::get( 'item.category');
		$prodcatM->namekey='default';
		$prodcatM->alias='Default';
		$prodcatM->setChild( 'item.categorytrans', 'name', 'Default' ); 		$prodcatM->setChild( 'item.categorytrans', 'description', 'Default category for items.' ); 		$prodcatM->setChild( 'item.categorytrans', 'lgid', 1); 		$prodcatM->publish=1;
		$prodcatM->parent=1;
		$prodcatM->rolid=1;
		$prodcatM->vendid=1;
		$prodcatM->depth=1;
		$prodcatM->uid=0;
		$prodcatM->created=time();
		$prodcatM->modified=time();
		$prodcatM->save();

		return true;
	}

	




	private function _otherVendor() {

				$prodcatM = WModel::get( 'item.category');
		$prodcatM->namekey='other_vendors';
		$prodcatM->alias='Vendors Category';
		$prodcatM->setChild( 'item.categorytrans', 'name', 'Vendors' ); 		$prodcatM->setChild( 'item.categorytrans', 'description', 'Category for vendors who applied to this store.' );
		$prodcatM->setChild( 'item.categorytrans', 'lgid', 1); 		$prodcatM->publish=0;		$prodcatM->parent=1;
		$prodcatM->rolid=1;
		$prodcatM->vendid=0;
		$prodcatM->depth=1;
		$prodcatM->uid=0;
		$prodcatM->created=time();
		$prodcatM->modified=time();
		$prodcatM->save();

		return true;
	}



	private function _insertDefProdType($extra='') {

		$listOfTypeA  = array();
				$typeO = new stdClass;
		$typeO->name = 'Item';
		$typeO->description = 'Item';
		$typeO->namekey = 'item' . $extra;
		$roleC = WRole::get();
		$typeO->rolid_edit = $roleC->getRole( 'vendor' );
		$typeO->type = 100;
		$typeO->publish = 0;
		$typeO->params = 'pageprdshowimage=2
pageprdshowdefimg=2
pageprdshowpreview=2
pageprdshowintro=2
pageprdshowdesc=2
pageprdshowrating=2
pageprdshowreview=2
pageprdallowreview=2
pageprdvendor=2
pageprdvendorating=2
pageprdaskquestion=2
pageprdshowviews=2
pageprdshowlike=2
pageprdshowtweet=2
pageprdshowbuzz=2
pageprdshowsharethis=2
pageprdshowemail=2
pageprdshowfavorite=2
pageprdshowwatch=2
pageprdshowwish=2
pageprdshowlikedislike=2
pageprdshowsharewall=2
pageprdshowprint=2
termslicense=general
termsshowlicense=2
termsrefundallowed=2
termsrefund=general
termsshowrefund=2
termsshowrefundperiod=2
requiretermsatcheckout=2
bdlsorting=featured
bdldisplay=standard
bdlnbdisplay=6
prditems=1
prdtitle=1
prdsorting=rated
prddisplay=horizontal
prdnbdisplay=4
prdlayoutcol=4
prdshowname=1
prdshowintro=1
prdclimit=100
prdshowpreview=1
prdshowfree=1
prdshowrating=1
prdfeedback=1
prdreadmore=1
prdshowimage=1
pageprdshowmap=2
pageprdshowmapstreet=2
allowsyndication=0
';

		$listOfTypeA[] = $typeO;

		$prodTypeM = WModel::get( 'item.type' );
		$typeImage = 'item';
		foreach( $listOfTypeA as $oneType ) {

			$prodTypeM->whereE( 'namekey', $oneType->namekey );
			$exist = $prodTypeM->exist();
			if ( $exist ) continue;

			$prodTypeM->prodtypid = null;
						$prodTypeM->setChild( 'item.typetrans', 'name', $oneType->name );
			$prodTypeM->setChild( 'item.typetrans', 'description', $oneType->description );
			$prodTypeM->namekey = $oneType->namekey;
			$prodTypeM->type = $oneType->type;
			$prodTypeM->publish = $oneType->publish;
			$prodTypeM->params = $oneType->params;
			$prodTypeM->core = 1;
			$prodTypeM->vendid = 0;
			$prodTypeM->saveItemMoveFile( JOOBI_DS_NODE . $typeImage .DS. 'install' .DS. 'images' .DS. $typeImage . 'typex.png' );

		}		return true;

	}


}