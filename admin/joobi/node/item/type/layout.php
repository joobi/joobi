<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Item_Layout_type extends WTypes {
	public $layout = array(
		'' => 'Default',
		'badge' => 'Badge',
		'badgebig' => 'Big Badge',
		'badgemini' => 'Mini Badge',
		'table' => 'Table'
	 );

}