<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Item_Sorting_type extends WTypes {
var $sorting = array(
	'featured' => 'Featured',
	'rated' => 'Highest Rated',
	'sold' => 'Bestselling',
	'hits' => 'Most Popular',
	'reviews' => 'Most Reviewed',
	'rated' => 'Highest Rated',
	'highprice' => 'Highest Price',
	'lowprice' => 'Lowest Price',
	'alphabetic' => 'Alphabetic',
	'reversealphabetic' => 'Reverse Alphabetic',
	'newest' => 'Latest',
	'oldest' => 'Oldest',
	'justsold' => 'Just Sold',
	'recentlyupdated' => 'Recently Updated',
	'random' => 'Random',	
	'ordering' => 'IDs Ordering'
);

}