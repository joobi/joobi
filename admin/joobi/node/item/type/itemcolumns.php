<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Item_Itemcolumns_type extends WTypes {
var $itemcolumns = array(

	'name'=>'Name',

	'alias'=>'Alias',

	'namekey'=>'SKU',

	'introduction'=>'Introduction',

	'description'=>'Description',

	'price'=>'Price',

	'currency'=>'Currency',

	'itemType'=>'Item Type',

	'priceType'=>'Price Type',

	'category'=>'Category',

	'publish'=>'Published',

	'featured'=>'Featured',


	'created'=>'Created Date',

	'modified'=>'Modified Date',


	'publishstart'=>'Start Publishing',
	'publishend'=>'End Publishing',
	'availablestart'=>'Start Purchase',
	'availableend'=>'End Purchase',

	'imageFile'=>'Image File',

	'previewFile'=>'Preview File',

	'itemFile'=>'Item File',

	
	'weight'=>'Weight',
	'stock'=>'Stock'



);
}