<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Item_Terms_type extends WTypes {
var $terms = array(

	1 => 'License',

	2 => 'Return Policy',

	3 => 'Privacy Policy',

	4 => 'Terms and Conditions',

);
}