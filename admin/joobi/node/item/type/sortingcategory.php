<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Item_Sortingcategory_type extends WTypes {
	var $sortingcategory = array(
		'ordering' => 'Ordering',
		'featured' => 'Featured',
		'alphabetic' => 'Alphabetic',
		'reversealphabetic' => 'Reverse Alphabetic',
		'newest' => 'Latest',
		'oldest' => 'Oldest',
		'random' => 'Random',		
		'recentlyupdated' => 'Recently Updated'
		);

}