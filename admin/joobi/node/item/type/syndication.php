<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Item_Syndication_type extends WTypes {
	var $syndication = array(
		'0' => 'No',
		'1' => 'Yes',
		'23' => 'Vendor',
		'12' => 'Item'
	 );

}