<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Item_Sortingcat_type extends WTypes {
	var $sortingcat = array(
		'ordering' => 'Ordering',
		'featured' => 'Featured',
		'sold' => 'Bestselling',
		'rated' => 'Highest Rated',
		'hits' => 'Most Popular',
		'reviews' => 'Most Reviewed',
		'random' => 'Random',	
		'newest' => 'Latest'
	);

}