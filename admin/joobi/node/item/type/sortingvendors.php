<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Item_Sortingvendors_type extends WTypes {
	var $sortingvendors = array(
		'rated' => 'Highest Rated',
		'alphabetical' => 'Alphabetical',
		'hits' => 'Most Popular',
		'reviews' => 'Most Reviewed',
		'newest' => 'Latest',
		'random' => 'Random',	
		'ordering' => 'IDs Ordering'
	);

}