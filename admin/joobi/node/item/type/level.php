<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Item_Level_type extends WTypes {
	var $level = array(
		'1' => '1 level',
		'2' => '2 level',
		'3' => '3 level',
		'4' => '4 level'
	);

}