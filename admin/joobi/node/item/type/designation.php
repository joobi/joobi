<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Designation_type extends WTypes {











	public $designation = array (

		'1' => 'product',

		'5' => 'subscription',

		'7' => 'voucher',
		'11' => 'auction',
		'16' => 'classified',
		'17' => 'lottery',




		'100' => 'item',			'101' => 'article',
		'121' => 'media',
		'141' => 'download'


	);





	protected function translatedType() {

		$typeA = array(
		'1' => TR1206961841DNYS,
		'5' => TR1206732411EGSA,
		'7' => TR1358508569GXCY,
		'11' => TR1298350962OSCE,
		'16' => TR1375216513HVXX,
		'17' => TR1302519286LPPQ,

		'100' => TR1206732372QTKR,			'101' => TR1212761231MDKH,
		'121' => TR1242282450QJCF,
		'141' => TR1206961905BHAV

		);

		return $typeA;

	}
}