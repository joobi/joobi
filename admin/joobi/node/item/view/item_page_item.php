<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Item_page_item_view extends Output_Forms_class {
protected $_catalogItemPageC = null;



	protected $pid = null;



	protected $itemTypeInfoO = null;



	protected $vendid = 0;	


	protected $pageParams = null;









	function prepareTheme() {

		$mediaType = WGlobals::get( 'media-type-show-view' );

		$this->setValue( 'previewType', ucfirst($mediaType) );



		
		$pageTheme = WPage::theme();

		$widCat = WExtension::get( 'catalog.node', 'wid' );

		$folderCat = $pageTheme->getFolder( $widCat, 49 );

		WPage::addCSSFile( JOOBI_URL_THEME . $folderCat . '/css/style.css' );



		return true;

	}




	protected function prepareView() {



		
		$this->pid = WGlobals::getEID();

		if ( empty( $this->pid ) ) $this->pid = $this->getValue( 'pid' );



		$itemLoadC = WClass::get( 'item.type' );

		$this->itemTypeInfoO = $itemLoadC->loadTypeBasedOnPID( $this->pid );

		$this->customItemType();



		
		$adid = $this->getValue( 'adid' );

		$this->vendid = $this->getValue('vendid');



		$this->_catalogItemPageC = WClass::get( 'catalog.itempage' );



		
		
		$pageID = $this->getValue( 'pageprdpgid' );

		if ( empty($pageID) && !empty($this->itemTypeInfoO->pageprdpgid) ) $pageID = $this->itemTypeInfoO->pageprdpgid;	
		if ( !empty($pageID) ) $this->_catalogItemPageC->handlePageID( $pageID, $this->pid );



		
		$this->bctrail = WPref::load( 'PCATALOG_NODE_BREADCRUMBS' );



		$printPage = WGlobals::get( 'printpage' );



		
		$this->pageParams = $this->_catalogItemPageC->setupItemInformation( $this, $this->itemTypeInfoO, true );




		$checkFeedback = ( WGlobals::checkCandy(50,true) ? false : true );

		
		WGlobals::set( 'itemAllowReview', ( $this->pageParams->allowReview && $checkFeedback ), 'joobi' );



		if ( $this->pageParams->showAskQuestion ) $this->setValue( 'askQuestionLink', $this->_catalogItemPageC->askQuestion( $this->vendid ) );



		
		$this->_catalogItemPageC->getTerms( $this->itemTypeInfoO, $this );



		
		$this->_communProcessing();



		
		
		$itemName = $this->getValue( 'name' );

	
		$catalogShowproductC = WClass::get('catalog.showsocial');

		if ( !$catalogShowproductC->shareDisplay( $this, $this->pid, $itemName, $this->pageParams, 'item', $this->getValue( 'hits' ) ) ) return false;





		
		
		$availableStartDate = $this->getValue( 'availablestart' );

		$availableEndDate = $this->getValue( 'availableend' );



		
		
		
		
		if ( !empty( $availableEndDate ) && ( time() > $availableEndDate ) ) $isAvailable = 'expired';

		else {

			if ( time() > $availableStartDate ) $isAvailable = 'avail';

			else $isAvailable = 'unavail';

		}

		WGlobals::set( 'productAvailable', $isAvailable, 'joobi' );


		$this->_modelName = $this->_model->getModelNamekey();


		
		$this->setValue( 'feedback', $checkFeedback );

		if ( !$checkFeedback ) {

			
			$this->removeElements( $this->_modelName . '_page_item_' . $this->_modelName . '_comment', $this->_modelName . '_page_item_vendor_rating', $this->_modelName . '_page_item_' . $this->_modelName . '_nbreviews', $this->_modelName . '_page_item_comment_total' );

		}




		if ( !WExtension::exist( 'vendors.node' ) ) {
			$this->removeElements( array( $this->_modelName . '_page_item_vendor_rating', $this->_modelName . '_page_item_' . $this->_modelName . '_vendid' ) );

		}




		if ( $this->getValue( 'showprint' ) ) {

			
			$link = 'controller=catalog&task=show&eid='. $this->pid .'&printpage=1&hidecomment=1';	
			$printLink = WPage::routeURL( $link, 'home', 'popup', false, false, null, true );

			$this->setValue( 'showPrintButton', $printLink );



			
			
			if ( $printPage ) WPage::addJSScript( 'window.print();' );



		}


				$pageParams = WGlobals::get( 'itemPageMain', '', 'joobi' );
		if ( empty( $pageParams->showDesc ) ) {
			$this->removeElements( $this->_modelName . '_page_item_' . $this->_modelName . 'trans_description' );
		}


		
		$this->_showRelatedItems();

				$this->_createItemMap();



		return true;


	}




	private function _createItemMap() {

		if ( empty($this->pageParams->showMap) ) {
			$this->removeElements( $this->_modelName . '_page_item_map' );
			$this->removeElements( $this->_modelName . '_page_item_' . $this->_modelName . '_location' );
			return false;
		} else {
						WGlobals::set( 'mapWidth', ( empty($this->pageParams->showMapWidth) ? 600 : $this->pageParams->showMapWidth ) );
			WGlobals::set( 'mapHeight', ( empty($this->pageParams->showMapHeight) ? 400 : $this->pageParams->showMapHeight ) );
			WGlobals::set( 'mapShowStreetView', ( empty($this->pageParams->showMapStreet) ? false : $this->pageParams->showMapStreet ) );
			WGlobals::set( 'mapStreetHeight', 300 );
		}
	}










	protected function _showRelatedItems() {



		$nbRelated = $this->getValue( 'relnum' );

		if ( empty( $nbRelated ) ) return false;



		$relatedPID = WGlobals::getEID();

		if ( empty($relatedPID) ) return false;



		$itemPrmsO = WGlobals::get( 'itemPageRelated', null, 'joobi' );



		
		if ( !$itemPrmsO->prditems ) return false;



		WGlobals::set( 'itmRelatedtitle', $itemPrmsO->prdtitle, 'joobi' );



		if (!empty($itemPrmsO->prdtitle)) $this->setValue( 'itemRelatedTitle', TR1298350960NBLZ );

		$this->setValue( 'itemRelated', $this->_catalogItemPageC->showRelatedItems( $itemPrmsO, $relatedPID, $this->firstFormName ) );

		$this->setValue( 'itemsRelatedPagination', $this->_catalogItemPageC->paginationRelatedItems() );



	}




	











	protected function _communProcessing() {



		$createdDateAdjusted = $this->getValue('created') + PCATALOG_NODE_NEWPERIOD * 86400;

		$this->setValue( 'newItem', ( ( $createdDateAdjusted >= time() ) ? true : false ) );



		$this->setValue( 'newItemText', TR1206732361LXFD );

		$this->setValue( 'featuredText', TR1256629159GBCH );



		
		$this->_processList();



		
		if ( PCATALOG_NODE_EDITITEMCATALOG ) {



			$showEditDeleteButton = false;

			
			$roleHelper = WRole::get();

			$showEditDeleteButton = $roleHelper->hasRole( 'storemanager' );





			if ( !$showEditDeleteButton ) {

				
				
				$roleHelper = WRole::get();

				if ( $roleHelper->hasRole( 'vendor' ) ) {

					$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

					$uid = WUser::get('uid');

					$vendid = $vendorHelperC->getVendorID( $uid );

				}


				if ( !empty($vendid) ) {

					$ItemVendID = $this->getValue( 'vendid' );

					
					if ( $ItemVendID == $vendid ) {

						$showEditDeleteButton = true;

					}
				}
			}


			if ( $showEditDeleteButton ) {

				$editArea = PCATALOG_NODE_EDITITEMLOCATION;

				$this->setValue( 'editButton', TR1337879810ASDU );

				$this->setValue( 'deleteButton', TR1206732372QTKL );

				$extraSpace = ( PCATALOG_NODE_EDITITEMLOCATION != 'both' ) ? '&space=' . PCATALOG_NODE_EDITITEMLOCATION : '';



				if ( $editArea == 'popup' ) {

					$myLink = WPage::routeURL( 'controller=item&task=edit&eid=' . $this->pid, 'home', 'popup', false, false, null, true );

				} else {

					$myLink = WPage::routeURL( 'controller=item&task=edit&eid=' . $this->pid . $extraSpace, 'home' );	
				}
				$this->setValue( 'editButtonLink', $myLink );



				$myLink = WPage::routeURL( 'controller=item&task=deleteall&eid=' . $this->pid . '&returnback=true' . $extraSpace, 'home' );	
				$this->setValue( 'deleteButtonLink', $myLink );



			}


		}


	}
















	protected function _processList() {



		
		if ( empty( $this->pid ) ) return false;



		$wishlistGetcountC = WClass::get( 'wishlist.getcount' );

		$typeA = $wishlistGetcountC->countItemTypes( $this->pid, 'item' );



		foreach( $typeA as $name => $value ) {

			$this->setValue( $name, $value );

		}


		return true;



	}












	protected function customItemType() {

	}
}