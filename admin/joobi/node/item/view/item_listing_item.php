<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Item_listing_item_view extends Output_Listings_class {
protected function prepareView() {



		if (  !IS_ADMIN ) {



			if ( !defined('PCATALOG_NODE_EDITITEMLOCATION') ) WPref::get('catalog.node');



			if ( PCATALOG_NODE_EDITITEMLOCATION == 'site' || PCATALOG_NODE_EDITITEMLOCATION == 'popup' ) {	
				

				WGlobals::setSession( 'page', 'space', PCATALOG_NODE_EDITITEMLOCATION );



				$simplifiedView = PCATALOG_NODE_ITEMLISTTYPE;

				
				if ( !empty( $simplifiedView ) ) {

					if ( $this->namekey != 'item_my_show_list' ) WPage::redirect( 'controller=item' );

					WPage::redirect( 'controller=item' );

				}


			} elseif ( PCATALOG_NODE_EDITITEMLOCATION == 'vendors' ) {


				WGlobals::setSession( 'page', 'space', PCATALOG_NODE_EDITITEMLOCATION );

			} else {

				
				$simplifiedView = PCATALOG_NODE_ITEMLISTTYPE;

				if ( !empty( $simplifiedView ) ) {

					WPage::redirect( 'controller=item' );

				}
			}

						$importExport = WPref::load( 'PVENDORS_NODE_ALLOWIMPORTEXPORT' );
			if ( empty($importExport) ) {
				$className = get_class( $this );
				$classNameA = explode( '_', $className );
				$ItemType = strtolower( $classNameA[0] );

				$this->removeMenus( array( 'item_listing_'.$ItemType.'_import', 'item_listing_'.$ItemType.'_export', 'item_listing_'.$ItemType.'_divider_export' ) );
			}

		}

		return true;



	}

}