<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Item_new_view extends Output_Forms_class {
protected function prepareView() {

	
	$productNodeExits = WExtension::exist( 'product.node' );
	$subscriptiontExtensionExist = WExtension::exist( 'subscription.node' );
	$auctionExtensionExist = WExtension::exist( 'auction.node' );

	if ( !$productNodeExits && !$subscriptiontExtensionExist && !$auctionExtensionExist ) $this->removeElements( array( 'item_new_delivery', 'item_new_pricing' ) );
	else {			if ( !defined('PPRODUCT_NODE_DELIVERYTYPE') ) WPref::get('product.node');
		$deliveryType = PPRODUCT_NODE_DELIVERYTYPE;		
		if ( !empty( $deliveryType ) ) $this->removeElements( array( 'item_new_delivery', 'product_new_delivery' ) );
	}	

	return true;



}}