<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Item_category_edit_view extends Output_Forms_class {
protected function prepareView() {

	

		
		$auctionExtensionExist = WExtension::exist( 'auction.node' );

		if ( !$auctionExtensionExist ) {

			$this->removeElements( array('category_edit_catcrslshowbid', 'category_edit_itmshowbid' ) );

		} else {

			
			$prodtypid = $this->getValue( 'prodtypid' );

			$itemTypeC = WClass::get('item.type');

			$type = $itemTypeC->loadData( $prodtypid, 'type' );

			if ( !(empty($type) || $type == 11 ) ) {

				$this->removeElements( array('category_edit_catcrslshowbid', 'category_edit_itmshowbid' ) );

			}
			

		}
	

		
		WGlobals::set( 'maxFileUpload-item_category_image_upload', PITEM_NODE_CATMAXSIZE, 'joobi' );	
		

		$multiLanguage = WPref::load( 'PLIBRARY_NODE_MULTILANG' );

		if ( !$multiLanguage ) $this->removeMenus( array('item_category_translate' ) );

			

		if ( IS_ADMIN ) return true;

		

		if ( !defined('PCATALOG_NODE_CATCTYVENDOR') ) WPref::get( 'catalog.node' );

	

		if ( ! PCATALOG_NODE_CATCTYVENDOR ) {

			
			$this->removeElements( array( 'catalog_category_edit_layout' ) );

		}
		

		return true;

	}}