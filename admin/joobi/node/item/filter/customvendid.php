<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Customvendid_filter {
	

	function create() {



		if ( IS_ADMIN ) return false;

		else {

			$uid = WUser::get('uid');

			

			
			
			static $userRoleA = array();

	 		if ( !isset( $userRoleA[ $uid ] ) ) {

	 			$roleC = WRole::get();

				$userRoleA[ $uid ] = $roleC->hasRole( 'storemanager' );

	 		}
	 		

			if ( !empty($userRoleA[ $uid ]) ) {

				$eid = WGlobals::getEID();

				return !empty( $eid ) ? $eid : false;

			}
			

			static $vendidA = null;

			
			if ( !isset( $vendidA[ $uid ] ) && !empty( $uid ) ) {

				$vendorHelperC = WClass::get('vendor.helper',null,'class',false);

				$vendidA[ $uid ] = $vendorHelperC->getVendorID( $uid );

			}
			

	 		if ( empty( $vendidA[ $uid ] ) || empty( $uid  ) ) {

				$message = WMessage::get();

				$message->exitNow( 'Unauthorized access 130' );

			} else return $vendidA[ $uid ];

		}
		

	}
	
}