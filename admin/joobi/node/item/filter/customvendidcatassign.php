<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Item_Customvendidcatassign_filter {


	function create() {


		$roleC = WRole::get();
		$isStoreManger = $roleC->hasRole( 'storemanager' );

		if ( IS_ADMIN || $isStoreManger ) return false; 
		$uid = WUser::get('uid');
		if ( empty( $uid ) ) {
			$message = WMessage::get();
			$message->exitNow( 'Unauthorized access 132' );
		}

		
		if ( !defined( 'PITEM_NODE_ALLOWPRODALLCAT' ) ) WPref::get( 'item.node' );
				

		
		if ( PITEM_NODE_ALLOWPRODALLCAT ) {
			return false;


		} else {

			$uid = WUser::get('uid');
						if ( PITEM_NODE_ALLOWVENDORCAT ) {

				$vendorHelperC = WClass::get( 'vendor.helper' );
				$vendid = $vendorHelperC->getVendorID( $uid );

		 		if ( empty( $vendid ) ) {
					$message = WMessage::get();
					$message->exitNow( 'Unauthorized access 134' );
				} else return $vendid;

			} else {


				
								$vendorHelperC = WClass::get('vendor.helper',null,'class',false);
				$vendid = $vendorHelperC->getVendorID( 0, true );
				return $vendid;

			}

		}


	}

}