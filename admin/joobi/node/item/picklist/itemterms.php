<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Item_Itemterms_picklist extends WPicklist {
function create() { 

	

	$controller = WGlobals::get('controller');	

	$task = WGlobals::get('task');	

	$namekey = $this->getParamsValue('namekey');

	$type = 1;

	$itemTermsM = WModel::get('item.terms'); 

	

	if ($namekey == 'item_term_licenses_general' 
	|| $namekey == 'item_term_licenses_type' 
	|| $namekey == 'item_term_licenses_item') $type = array( 1,3,4 );
	else $type = array( 2 ); 
	

	$itemTermsM->select('termid'); 

	$itemTermsM->select('alias'); 

	$itemTermsM->whereIn('type', $type ); 

	$itemTermsM->whereE('publish', 1); 


	$resultsA = $itemTermsM->load('ol'); 

	

	if ( empty($resultsA) ) return false;

	

	if (!empty($controller)) {

		if ( ( $controller == 'item-type' || $controller=='item' ) && 'preferences' != $task ) {

			$this->addElement( 'general', TR1310465174GEKS ); 

		}
		

		if ($controller == 'product' && $task != 'preferences' ) {

			$this->addElement( 'general', TR1310465174GEKS );

			$this->addElement( 'type', TR1310465174GEKT );

		}

	}
	

	if (!empty($resultsA)) { 

		foreach( $resultsA as $key => $result) { 

			$this->addElement( $result->termid , $result->alias); 

		}
	} else {

		return false;

	}
	return true; 

}}