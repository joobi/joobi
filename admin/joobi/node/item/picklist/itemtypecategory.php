<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Item_Itemtypecategory_picklist extends WPicklist {

function create() {



	
	
	static $vendorHelperC = null;

	if ( empty( $vendorHelperC ) ) $vendorHelperC = WClass::get( 'vendor.helper', null, 'class', false );

	$vendid = ( $vendorHelperC ) ? $vendorHelperC->traceVendor() : 0;



	static $productTypeM = null;

	if ( empty( $productTypeM ) ) $productTypeM = WModel::get( 'item.type' );




	static $productTypesA = array();

		
		if ( WGlobals::get('task', 'listing' ) == 'listing' ) {

			$defaultprodtypid = WGlobals::get( 'prodtypid' );

		}


		if ( !isset( $productTypesA[$vendid] ) ) {



			$productTypeM->makeLJ( 'item.typetrans', 'prodtypid' );

			$productTypeM->select( 'name', 1 );



			
			$controller = WGlobals::get( 'controller' );

			if ( $controller != 'product' && $controller != 'item-category' ) {

				if ( $controller == 'subscription-created' ) $controller = 'subscription';

				$controller = ucFirst($controller);

				$productDesignationT = WType::get( 'item.designation' );

				$onlyOneType = $productDesignationT->inNames( $controller );

				if ( $onlyOneType ) $productTypeM->whereE( 'type', $productDesignationT->getValue($controller) );

			}


			$productTypeM->makeLJ( 'item.typetrans', 'prodtypid' );

			$productTypeM->whereLanguage(1);

			$productTypeM->select( 'name', 1 );





			if ( !empty( $vendid ) && !IS_ADMIN ) {

				$productTypeM->whereE( 'vendid', 0, 0, null, 1 );

				$productTypeM->whereE( 'vendid', $vendid, 0, null, 0, 1, 1 ); 	
			} elseif ( !IS_ADMIN ) $productTypeM->whereE( 'vendid', 0 );



			$productTypeM->checkAccess();

			$productTypeM->checkAccess( 0, 0, 0, 0, 'rolid_edit' );

			$productTypeM->orderBy( 'type' );

			$productTypeM->setLimit( 500 );

			$productTypesA[$vendid] = $productTypeM->load( 'ol', array( 'prodtypid', 'namekey', 'type' ) );

		}




	if ( empty( $productTypesA[$vendid] ) ) {

		$message = WMessage::get();

		$message->userE('1310010297BALL');

		return false;

	}


	$total = count($productTypesA[$vendid]);



	$this->addElement( 0, '- ' . TR1327708944HAPH . ' -' );	




	if ( !empty( $productTypesA[$vendid] ) ) {



		
		
		$prodDesignationT = WType::get( 'item.designation' );



		$firstWord = null;

		$previousWord = null;

		$defaultText = '';

		foreach( $productTypesA[$vendid] as $productType ) {

			$productTypeID = $productType->prodtypid;

			$productTypeName = $productType->name;

			$designationName = $prodDesignationT->getTranslatedName( $productType->type );


			if ( $total > 1 ) {

				$firstWord = $designationName;

				if ( $firstWord != $previousWord  ) {

					$this->addElement( -1, '-- '. $firstWord );

					$previousWord = $firstWord;

				}


			}


			if ( !empty($defaultprodtypid) && $defaultprodtypid == $productTypeID ) $defaultText = $productTypeName;

			
			$this->addElement( $productTypeID, $productTypeName );

		}


		if ( !empty($defaultText) ) WGlobals::set( 'titleheader', $defaultText );

	}
	return true;



}}