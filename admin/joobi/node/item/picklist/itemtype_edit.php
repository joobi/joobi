<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Item_Itemtype_edit_picklist extends WPicklist {


function create() {

	
	
	if ( $this->onlyOneValue() ) {

		$productTypeM = WModel::get( 'item.type' );
		$productTypeM->makeLJ( 'item.typetrans', 'prodtypid' );
		$productTypeM->select( 'name', 1 );
				$task = WGlobals::get('task');
		if ( empty($this->defaultValue) && $task=='add' ) $this->defaultValue = WGlobals::get('prodtypid');
				if ( empty($this->defaultValue) ) {
									$itemTypeC = WClass::get('item.type');
			$this->defaultValue = $itemTypeC->getDefaultItemType( WGlobals::get('controller') );
		}
		$productTypeM->whereE( 'prodtypid', $this->defaultValue );
		$productTypeName = $productTypeM->load('lr');
		$this->addElement( $this->defaultValue, $productTypeName );
		return true;

	}


	$productTypeM = WModel::get( 'item.type' );


	$productTypeM->makeLJ( 'item.typetrans', 'prodtypid' );

	$productTypeM->select( 'name', 1 );



	$itemType = WGlobals::get( 'itemtype', '', 'joobi' );	
	if ( empty($itemType) )	$itemType = WGlobals::get( 'controller' );
	if ( !is_numeric($itemType) ) {
		$productDesignationT = WType::get( 'item.designation' );
		$myType = $productDesignationT->getValue( $itemType );
	} else {
		$myType = $itemType;
	}


	
	if ( !empty($myType) ) {

		$productTypeM->whereE( 'type', $myType );

	}




	$productTypeM->makeLJ( 'item.typetrans', 'prodtypid' );

	$productTypeM->whereLanguage(1);

	$productTypeM->select( 'name', 1 );

	$productTypeM->whereE( 'publish', 1 );



	












	$productTypeM->checkAccess( 0, 0, 0, 0, 'rolid_edit' );

	$productTypeM->orderBy( 'core', 'DESC' );

	$productTypeM->orderBy( 'type' );

	$productTypeM->setLimit( 500 );

	$productTypesA = $productTypeM->load( 'ol', array( 'prodtypid', 'namekey', 'type' ) );



	if ( empty( $productTypesA ) ) {

		$message = WMessage::get();

		$message->userE('1341269697SNUR');

		return false;

	}



	if ( count($productTypesA) == 1 ) {



		$formInstance = WView::form( $this->formName );

		$formInstance->hidden( $this->name, $productTypesA[0]->prodtypid, false, true );



		return false;





	}


	if ( !empty( $productTypesA ) ) {



		
		




		$defaultText = '';

		foreach( $productTypesA as $productType ) {

			$productTypeID = $productType->prodtypid;

			$productTypeName = $productType->name;









			if ( !empty($defaultprodtypid) && $defaultprodtypid == $productTypeID ) $defaultText = $productTypeName;

			
			$this->addElement( $productTypeID, $productTypeName );

		}


		if ( !empty($defaultText) ) WGlobals::set( 'titleheader', $defaultText );

	}
	return true;

}}