<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Item_Depth_picklist extends WPicklist {








	function create() {



		$sid = $this->sid;

		$depth = WGlobals::get( 'catdepth'.$sid, 0, 'session' );

		if ( !$depth ) {

			$myM = WModel::get( $sid );

			$myM->select( 'depth' ,0, null, 'max');

			$depth = $myM->load( 'lr') +1;

			WGlobals::set( 'catdepth'.$sid, $depth, 'session' );

		}


		$this->addElement( 0,  TR1215098992JNKQ  );

		for ($index = 1; $index < $depth; $index++) {

			$this->addElement( $index,  TR1206732389NXTA . ' ' . $index  );

		}


	}}