<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Item_Categoriesedit_picklist extends WPicklist {
	function create() {

		$pid = WGlobals::getEID();
		$prodtypid = WGlobals::get( 'prodtypid', 0 );


		if ( !empty($pid) ) {
						if ( empty($prodtypid) ) {
				$prodtypid = $this->getValue( 'prodtypid' );
			}
						$productCategoryM = WModel::get('item.categoryitem');
			$productCategoryM->whereE( 'pid', $pid );
			$productCategoryM->whereE( 'premium', 1 );
			$catidDefault = $productCategoryM->load('lr', 'catid' );

		} else {
			$catidDefault = 0;
		}				$this->setDefault( $catidDefault, false );

		$productCategoryM = WModel::get( 'item.category' );
		$productCategoryM->makeLJ( 'item.categorytrans', 'catid' );
		$productCategoryM->whereLanguage(1);
		$productCategoryM->select( 'name', 1 );
		$productCategoryM->select( array( 'catid', 'parent', 'namekey', 'publish', 'alias' ) );



				$controller = WGlobals::get( 'controller' );
		$prodTypeT = WType::get( 'item.designation' );
		$allTernativeA = $prodTypeT->allNames();
		$thereIsAlternative = in_array( $controller, $allTernativeA ) ? true : false;


		if ( !empty($prodtypid) || $thereIsAlternative ) {
			$productCategoryM->openBracket();
			if ( $thereIsAlternative ) {
				$productTypeM = WModel::get( 'item.type' );

				$typeValue = $prodTypeT->getValue( $controller, false );
				$productTypeM->whereE( 'type', $typeValue );
				$productTypeM->whereE( 'publish', 1 );
				$auctionTypesA = $productTypeM->load('lra', 'prodtypid' );
				if ( !empty($auctionTypesA) ) $productCategoryM->whereIn( 'prodtypid', $auctionTypesA );
			} else {
				$productCategoryM->whereE( 'prodtypid', $prodtypid );
			}			$productCategoryM->operator( 'OR' );
			$productCategoryM->where( 'prodtypid', '=', '0' );
			$productCategoryM->closeBracket();
		}
		$productCategoryM->where( 'namekey', '!=', 'root' );


		
								WPref::load( 'PITEM_NODE_ALLOWVENDORCAT' );



		if ( PITEM_NODE_ALLOWVENDORCAT && !PITEM_NODE_ALLOWPRODALLCAT ) {

			$uid = WUser::get('uid');
						if ( PITEM_NODE_ALLOWVENDORCAT ) {

				$vendorHelperC = WClass::get('vendor.helper',null,'class',false);
				$vendid = $vendorHelperC->getVendorID( $uid );

			}
	 		if ( empty( $vendid ) ) {
				$vendorHelperC = WClass::get('vendor.helper',null,'class',false);
				$vendid = $vendorHelperC->getVendorID( 0, true );
			}
			$productCategoryM->whereE( 'vendid', $vendid );

		}
		if ( !IS_ADMIN ) $productCategoryM->checkAccess();

		$maxCount = 10000;
		$productCategoryM->orderBy( 'ordering', 'ASC' );
		$productCategoryM->orderBy( 'name', 'ASC', 1 );
		$productCategoryM->setLimit( $maxCount );
		$allCategoriesA = $productCategoryM->load( 'ol' );
				if ( empty($allCategoriesA) ) return false;

		if ( count($allCategoriesA) > ( $maxCount * 0.95 ) ) {
			$message = WMessage::get();
			$message->adminE( 'You are getting close to the maximum number of categories, please inform the development team to increase the number of categories' );
		}
		$parent = array();
		$parent['pkey'] = 'catid';
		$parent['parent'] = 'parent';
		$parent['name'] = 'name';
		$childOrderParent = array();
		$list = WOrderingTools::getOrderedList( $parent, $allCategoriesA, 1, false, $childOrderParent );

		if ( empty($list) ) return false;

		$this->addElement( 0, TR1359160980BTYH );
		foreach( $list as $itemList ) {
			if ( $itemList->publish != 1 ) $extra = ' ( '.TR1206732372QTKO.' )';
			else $extra = '';
			$this->addElement( $itemList->catid, $itemList->name . $extra );
		}
		return true;

	}

}