<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;





class Role_User_class extends WClasses {







public function insertRole($O1,$O2){

if(empty($O1) || empty($O2)) return false;

if(!is_array($O2)) $O2=array( $O2 );


$I21N=WAddon::get( 'api.'. JOOBI_FRAMEWORK . '.role' );
$I21O=$I21N->getColumnName();
$I21P=WRole::get();
$I21Q=WModel::get( 'role' );
$I21R=WModel::get('users.role');
foreach( $O2 as $I21S){

if(!is_numeric($I21S)){
$I21S=$I21P->getRole( $I21S, 'rolid' );
}
$I21T=$I21P->hasRole( $I21S, $O1 );
if($I21T ) continue;


$I21R->setVal( 'uid', $O1 );
$I21R->setVal( 'rolid', $I21S );
$I21R->insertIgnore();

$I21Q->whereE( 'rolid', $I21S );
$I21U=$I21Q->load( 'lr', $I21O );

if(!empty($I21U)){
$I21N->insertRole( $O1, $I21U );
}
}
$I21V=WRole::get();
$I21V->reloadSession( $O1 );

return true;

}








public function deleteRole($O3,$O4){

if(empty($O3) || empty($O4)) return false;

if(!is_array($O4)) $O4=array( $O4 );

$I21W=array();
$I21P=WRole::get();
foreach( $O4 as $I21S){
if(!is_numeric($I21S)){
$I21W[]=$I21P->getRole( $I21S, 'rolid' );
}else{
$I21W[]=$I21S;
}}

$I21R=WModel::get('users.role');
if(is_array($O3)){
$I21R->whereIn( 'uid', $O3 );
}else{
$I21R->whereE( 'uid', $O3 );
}
$I21R->whereIn( 'rolid', $I21W );
$I21R->delete();


$I21N=WAddon::get( 'api.'. JOOBI_FRAMEWORK . '.role' );
$I21O=$I21N->getColumnName();
$I21Q=WModel::get( 'role' );
foreach( $I21W as $I21S){

$I21Q->whereE( 'rolid', $I21S );
$I21U=$I21Q->load( 'lr', $I21O );

if(!empty($I21U)){
$I21N->deleteRole( $O3, $I21U );
}
}
$I21V=WRole::get();
$I21V->reloadSession( $O3 );

return true;

}







}