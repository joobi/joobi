<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;





class Role_Helper_class extends WClasses {















public function getUserRoles($O1=null,$O2=false){
static $I20O=array();

if( empty($O1)) $O1=WUser::get('uid');
if($O2 ) $I20O=array();

if( empty($O1)){
if(empty($I20O[0])){
$I20P=WModel::get('role');
$I20P->whereE( 'namekey', 'allusers' );
$I20Q=$I20P->load('lr');
$I20O[0]=array( $I20Q );
}return $I20O[0];

}else{
if(isset($I20O[$O1])) return $I20O[$O1];


$I20R=WModel::get('users.role');
$I20R->whereE('uid',$O1);
$I20R->select('rolid');
$I20R->setLimit( 10000 );
$I20S=$I20R->load('lra');

$I20T=WUser::get( 'rolid', $O1 );
if(!empty($I20T)){
if(!empty($I20S)){
if(!in_array( $I20T, $I20S )) $I20S[]=$I20T;
}else{
$I20S[]=$I20T;
}}else{
$I20U=WMessage::get();
$I20U->codeE( 'The User does not have any role define in the members_node table. It should not be this way.');
$I20S[]=1;
}


$I20O[$O1]=$this->getChildRoles( $I20S, true, true );

}
return $I20O[$O1];

}









public function getChildRoles($O3=1,$O4=true,$O5=false){
static $I20V=array();

if(empty($O3)) $O3=1;

if($O5){
$I20W='>';
}else{
$I20W='<';
}
if($O4){
$I20W .='=';
}
$I20X=$I20W . '.' . serialize( $O3 );
if(!isset($I20V[$I20X])){

$I20Y=WModel::get('role');
$I20Y->makeLJ( 'role', 'lft', 'lft', 0, 1, $I20W );
$I20Y->where('rgt',$I20W,'rgt',1,0);
if(is_numeric($O3)){
$I20Y->whereE('rolid',$O3);
}elseif(is_string( $O3)){
$I20Y->whereE('namekey',$O3);
}elseif(is_array($O3)){
$I20Y->whereIn('rolid',$O3);
}$I20Y->setDistinct();
$I20Y->orderBy( 'lft', 'ASC' ,1 );
$I20Y->select('rolid',1);
$I20Y->setLimit( 10000 );
$I20V[$I20X]=$I20Y->load('lra');
}
return $I20V[$I20X];

}









public function hasRole($O6,$O7=0,$O8=false){
static $I20Z=array();

if( empty($O7)) $O7=WUser::get( 'uid' );

if(!is_numeric($O6)){
$I20T=$this->getRole($O6);
}else{
$I20T=$O6;
}
if(empty($O7)){
if($I20T <=1 ) return true;
else return false;
}

$I20X=$O7 .'-'. $I20T . '-' . $O8; if( !isset( $I20Z[ $I20X ] )){

if($O8 ) $I210=array( WUser::get('rolid', $O7 ));
$I210=$this->getUserRoles( $O7 );

if(empty($I210)) return false;

$I20Z[ $I20X ]=( in_array( $I20T, $I210 )) ? true : false;
}
return $I20Z[ $I20X ];
}









public function getRoleUsers($O9,$OA=array()){

if(empty($O9)) return false;

$I211=array();
$I212=WRole::get();
$I20S=$I212->getChildRoles($O9);

if(empty($I20S)){
$I20U=WMessage::get();
$I20U->codeE('The rolid "'.$O9.'" does not exist');
return $I211;
}
$I213=WModel::get('users.role');
$I213->whereIn('rolid',$I20S);
$I213->setDistinct();
if( empty($OA)){
$I213->select('uid');
$I214=$I213->load('lra');
}else{
$I213->makeLJ( 'users','uid','uid');
$I213->select($OA,1);
$I214=$I213->load('ol');
}
$I215=WModel::get('users');
$I215->whereIn('rolid',$I20S);
if( empty($OA)){
$I215->select('uid');
$I216=$I215->load('lra');
}else{
$I215->select($OA);
$I216=$I215->load('ol');
}
if(!empty($I214)){
if(!empty($I216)){
$I217=array();
foreach( $I214 as $I218){
if(!empty($I218->uid)) $I217[$I218->uid]=$I218;
}$I219=$I216;
foreach( $I219 as $I218){
if(!empty($I218->uid) && !isset($I217[$I218->uid])){
$I216[]=$I218;
}}return $I216;
}else{
return $I214;
}
}else{
return $I216;
}
}







public function getRole($OB,$OC='rolid'){
static $I21A=array();

if(empty($OB)) return false;

$I20X=$OB .'-'. $OC;
if(!isset($I21A[$I20X])){
$I20Y=WModel::get('role');
$I20Y->makeLJ('roletrans');
$I20Y->whereLanguage( 1 );
if(!is_numeric($OB)) $I20Y->whereE( 'namekey', $OB );
else $I20Y->whereE( 'rolid', $OB );
$I21A[$I20X]=$I20Y->load('o');}
if($OC=='object') $I21B=$I21A[$I20X];
else $I21B=( isset($I21A[$I20X]->$OC) ? $I21A[$I20X]->$OC : null );


return $I21B;
}





public function reloadSession($OD=null){

$I21C=WUser::get( 'uid' );
if(empty($OD) || $I21C==$OD){

$I20S=$this->getUserRoles( null, true );

$I21D=WGlobals::get('JoobiUser', null, 'session' );

$I21D->rolids=$I20S;

WGlobals::set( 'JoobiUser', $I21D, 'session' );

}else{

}
}













function compareRole($OE,$OF){
static $I21E;

if(is_numeric($OF) && $OF < 1 ) $OF=1;
if(is_numeric($OE) && $OE < 1 ) $OE=1;

$OE=strtolower($OE);
$OF=strtolower($OF);

if(isset($I21E[$OE][$OF])){
if($I21E[$OE][$OF]==='null') return null;
else return $I21E[$OE][$OF];
}
$I21F=WModel::get('role');

if(is_numeric($OE)){
$I21F->whereE('rolid',$OE);
}else{
$I21F->whereE('namekey',$OE);
}
if(is_numeric($OF)){
$I21F->whereE('rolid',$OF,0,null,0,0,1);
}else{
$I21F->whereE('namekey',$OF,0,null,0,0,1);
}
if($OE===$OF ) return true;

$I21F->setDistinct();
$I21F->setLimit( 1000 );
$I21G=$I21F->load('ol',array('rolid','namekey','lft','rgt'));

if(count($I21G) !==2){
if( count($I21G)===1){
if($I21G[0]->rolid===$OE AND $I21G[0]->namekey===$OF){
$I21E[$OE][$OF]=true;
$I21E[$OF][$OE]=true;
return true;
}
if($I21G[0]->rolid===$OF AND $I21G[0]->namekey===$OE){
$I21E[$OE][$OF]=true;
$I21E[$OF][$OE]=true;
return true;
}
}$I20U=WMessage::get();
$I20U->codeE('One of the two roles is not found so we can not compare them in the function compareRole()');
$I20U->codeE( 'rolid1: ' . $OE . ' and rolid2: ' . $OF );
$I21E[$OE][$OF]='null';
$I21E[$OF][$OE]='null';
return null;
}

foreach($I21G as $I21H){
if($I21H->rolid===$OE OR $I21H->namekey===$OE){
$I21I=$I21H;
}else{
$I21J=$I21H;
}
}

if($I21I->lft >=$I21J->lft AND $I21I->rgt <=$I21J->rgt){
$I21E[$OE][$OF]=true;
$I21E[$OF][$OE]=false;
return true;
}
if($I21I->lft < $I21J->lft AND $I21I->rgt > $I21J->rgt){
$I21E[$OE][$OF]=false;
$I21E[$OF][$OE]=true;
return false;
}
$I21E[$OE][$OF]='null';
$I21E[$OF][$OE]='null';
return null;

}







function getMainRole($OG=array()){
if(empty($OG)) return 0;
if(count($OG)==1) return reset($OG);

$I20Y=WModel::get('role');
$I20Y->whereIn('rolid',$OG);
$I20Y->makeLJ('role','lft','lft',0,1,'>=');
$I20Y->whereOn('rgt','>=','rgt',1,0);
$I20Y->whereE('core',1,1);
$I20Y->orderBy('lft','DESC',1);
$I20Y->select('rgt',1);
$I20Y->select('lft',1);
$I21K=$I20Y->load('o');

$I20Y->whereIn('rolid',$OG);
$I20Y->where('lft','>=',$I21K->lft);
$I20Y->where('rgt','<=',$I21K->rgt);
$I20Y->whereE('core',1 );
$I20Y->orderBy('lft','DESC');
$I21L=$I20Y->load('lr',array('rolid'));

return $I21L;
}













function insertRole($OH=null){
if(!isset($OH)) return false;
static $I20Y=null;
$I20T='0';

if(!isset($I20Y))$I20Y=WModel::get('role');
if(!empty($OH->namekey)){
$I20Y->namekey=$OH->namekey;
$I21M=$this->getRole( 'registered', 'rolid' );
$I20Y->parent=(!empty($OH->parent))? $OH->parent: $I21M;
$I20Y->type=(!empty($OH->type))? $OH->type: 1;
$I20Y->x['position']=(!empty($OH->position))? $OH->position : 1; 
$OH->name=(!empty($OH->name))? $OH->name: $OH->namekey;
$I20Y->setChild( 'roletrans', 'name', $OH->name);

$OH->description=(!empty($OH->description))? $OH->description : $OH->namekey;
$I20Y->setChild( 'roletrans', 'description', $OH->description);

$I20Y->returnId();
$I20Y->save();
$I20T=$I20Y->rolid;
}




return $I20T;

}

}