<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Role_Useraccess_picklist extends WPicklist {








function create(){

$I20G=WModel::get( 'role' ); 







$I20H=array();

$I20H['pkey']='rolid';

$I20H['parent']='parent';

$I20H['name']='name';



$I20G->makeLJ( 'roletrans', 'rolid');


$I20G->whereLanguage(1);

$I20G->select('name', 1);

$I20G->orderBy( 'lft','ASC' );

$I20G->select( 'rolid' );  
$I20G->select('parent');

if(!IS_ADMIN ) $I20G->checkAccess();



if($this->onlyOneValue()){



if(!empty($this->defaultValue)){

$I20G->whereE( 'rolid', $this->defaultValue );

$I20I=$I20G->load('o');

if(!isset($I20I->rolid)){

$I20J=WMessage::get();

$I20J->codeE('Could not find the role id "'.$this->defaultValue .'"');

$this->addElement($this->defaultValue, $this->defaultValue );

return true;

}

$this->addElement( $I20I->rolid, $I20I->name);



}
return true;

}


$I20G->where('type','!=','2');

$I20G->setLimit( 500 );

$I20K=$I20G->load('ol');



$I20L=array();

$I20M=WOrderingTools::getOrderedList( $I20H, $I20K, 1, false, $I20L );



foreach( $I20M as $I20N){

$this->addElement($I20N->rolid, $I20N->name);

}
}}