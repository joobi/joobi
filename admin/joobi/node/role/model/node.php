<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;


WLoadFile( 'category.model.node', JOOBI_DS_NODE );
class Role_Node_model extends Category_Node_model {


var $_notAssign=false;

var $_childRolid=array();

var $_firstChildRolid=null;
var $_newParent=false;

var $_frameworkClass=null;





function addValidate(){

if( $this->getX('position')==2){

$I22T=WRole::get();
$this->_childRolid=$I22T->getChildRoles( $this->parent, false);

$I22U=WModel::get('role');

$I22U->select('rolid');

$I22U->whereE('parent',$this->parent);
$I22U->setLimit( 100000 );

$this->_firstChildRolid=$I22U->load('lr');
}


if( empty($this->namekey)){

$I22V=WType::get('role.type');

$this->namekey=substr( preg_replace('#[^a-z0-9\.]#i','', $I22V->getName($this->type)).time(), 0, 30 );

}

return $this->triggerAPI('addValidate') && parent::addValidate();

}












function addExtra(){



$I22W=parent::addExtra() && $this->triggerAPI('addExtra');



if(!$this->_notAssign){


$I22X=WModel::get('users.role');

$I22X->uid=WUser::get('uid');

$I22X->rolid=$this->rolid;

$I22X->setIgnore();

$I22X->insert();

}



$I22Y=WRole::get();

$I22Y->reloadSession();


return $I22W;
}





function extra(){

$I22W=parent::extra() && $this->triggerAPI('extra');

if( $I22W && $this->getX('position')==2 && !empty($this->_childRolid)){

$I22Z=WModel::get('role' );
$I22Z->rolid=$this->_firstChildRolid;
$I22Z->parent=$this->rolid;
$I22W=$I22W && $I22Z->save();
 

unset( $I22Z->parent );
$I22Z->updatePlus('lft', 1);
$I22Z->updatePlus('rgt', 1);
$I22Z->whereIn( 'rolid',  $this->_childRolid );
$I22Z->update();

$I22Z->whereE( 'rolid', $this->rolid );
$I230=-2 * count( $this->_childRolid );
$I22Z->updatePlus('lft', $I230 );
$I22Z->update();

}

$I231=WUser::session();
$I231->setUserSession( null, true );

return $I22W && parent::extra();
}












function deleteValidate($O1=0){




if(!empty($O1)){

$I232=WModel::get('role');

$I232->whereE('rolid',$O1);


$I233=$I232->load('o',array('parent','joomla'));
$this->_newParent=$I233->parent;

}


return $this->triggerAPI('deleteValidate') && parent::deleteValidate($O1);



}


 










function deleteExtra($O2=0){



$I22W=parent::deleteExtra($O2) && $this->triggerAPI('deleteExtra');

if(!$I22W) return false;



if(!empty($this->_newParent) && !empty($O2)){


$I234=WModel::get('sql.foreign');

$I234->makeLJ('library.model','dbtid','dbtid'); 
$I234->makeLJ('sql.columns','feid','dbcid'); 
$I234->whereE('ref_dbtid',$this->getTableId());

$I234->where('ondelete','!=',3);

$I234->where('dbtid','!=',$this->getTableId());

$I234->whereE('map','rolid');

$I234->whereE('publish',1);

$I234->whereE('publish',1,1);

$I234->groupBy('dbtid');

$I234->groupBy('name',2);

$I234->select('sid',1);

$I234->select('name',2);
$I234->setLimit( 500 );

$I235=$I234->load('ol');



if( !empty( $I235 )){

foreach( $I235 as $I236){

$I237=WModel::get( $I236->sid ,'object');
if(empty( $I237->_infos->tablename )) continue;

$I237->setVal( $I236->name, $this->_newParent );

$I237->whereE( $I236->name, $O2 );

$I237->setIgnore();

$I22W=$I237->update();
}
}
}


return  parent::deleteExtra($O2);

}









function triggerAPI($O3){




if(!empty($this->type) && $this->type==2 ) return true;



if(!isset($this->_frameworkClass)) $this->_frameworkClass=WAddon::get( 'api.'.JOOBI_FRAMEWORK.'.acl' );



if( method_exists($this->_frameworkClass,$O3)){

return $this->_frameworkClass->$O3( $this );

}

return true;

}


}