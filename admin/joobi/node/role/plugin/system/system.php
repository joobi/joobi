<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Role_System_plugin extends WPlugin {

function onAfterRoute(){
$I22H='onAfterRoute'.JOOBI_FRAMEWORK;
if(method_exists( $this, $I22H )){
return $this->$I22H();
}else{
return $this->onAfterRoutejoomla16();
}
}
function onAfterRoutejoomla16(){

 if(!WExtension::exist( 'subscription.node' )) return;


$I22I=WGlobals::get( 'option' );

$I22J=WModel::get( 'joomla.extensions' );
$I22J->makeLJ( 'role.components', 'extension_id', 'id' );
$I22J->whereE( 'element', $I22I, 0 );

$I22J->select( array( 'rolid', 'site', 'admin' ), 1 );
$I22K=$I22J->load( 'o' );

if(!empty($I22K)){
if(IS_ADMIN){
if(empty($I22K->admin)) return;
}else{
if(empty($I22K->site)) return;
}
$I22L=WUser::get( 'rolids' );

if(!in_array( $I22K->rolid, $I22L )){

$I22M=WMessage::get();
$I22M->userW('1206732348RCNT');

if(!defined('PSUBSCRIPTION_NODE_COMPREDIRECTLINK')) WUser::pref ('subscription.node');
$I22N=PSUBSCRIPTION_NODE_COMPREDIRECTLINK;

if(true){
$I22O=(!empty($I22N))? $I22N : WPage::routeURL( 'controller=subscription&task=possible&rolid='. $I22K->rolid, '', false, false, true, 'jsubscription' );
}else{
$I22O=WPage::routeURL( 'controller=subscription&task=invalid', '', false, false, true, 'jaccess' );
}WPage::redirect( ltrim( $I22O, '/' ));

}
}

}
function onAfterRoutejoomla15(){

if(!WExtension::exist( 'subscription.node' )) return;


$I22I=WGlobals::get( 'option' );

$I22J=WModel::get( 'joomla.components' );
$I22J->makeLJ( 'role.components', 'id', 'id' );
$I22J->whereE( 'option', $I22I, 0 );
$I22J->whereE( 'parent', 0, 0 );
$I22J->select( array( 'rolid', 'site', 'admin' ), 1 );
$I22K=$I22J->load( 'o' );
if(!empty($I22K)){
if(IS_ADMIN){
if(empty($I22K->admin)) return;
}else{
if(empty($I22K->site)) return;
}
$I22L=WUser::get( 'rolids' );

if(!in_array( $I22K->rolid, $I22L )){

$I22M=WMessage::get();
$I22M->userW('1206732348RCNT');

if(!defined('PSUBSCRIPTION_NODE_COMPREDIRECTLINK')) WUser::pref ('subscription.node');
$I22N=PSUBSCRIPTION_NODE_COMPREDIRECTLINK;

if(true){
$I22O=(!empty($I22N))? $I22N : WPage::routeURL( 'controller=subscription&task=possible&rolid='. $I22K->rolid, '', false, false, true, 'jsubscription' );
}else{
$I22O=WPage::routeURL( 'controller=subscription&task=invalid', '', false, false, true, 'jaccess' );
}WPage::redirect( ltrim( $I22O, '/' ));

}
}
}}