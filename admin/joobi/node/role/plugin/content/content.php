<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Role_Content_plugin extends WPlugin {
























public function onBeforeDisplayContent(&$O1,&$O2,$O3){


if(IS_ADMIN ) return '';

if(!WExtension::exist( 'subscription.node' )) return '';

static $I21X=null;

static $I21Y=null;



if(!isset($I21X)){

$I21X=( WGlobals::get('view')=='article' ) ? true : false;

}










$I21Z=!empty($O1->id) ? $O1->id : 0;

$I220=!empty($O1->sectionid) ? $O1->sectionid : 0;

$I221=!empty($O1->catid) ? $O1->catid : 0;

$I222=WUser::get( 'rolids' );

 if(!($I222))   $I222=array(1,2);






if($I21X){



if($this->l110_3S_3v6( 'content', $O1, $I21Y, $I21X, $I222, $I21Z )) return '';

if($this->l110_3S_3v6( 'categories', $O1, $I21Y, $I21X, $I222, $I221 )) return '';

if($this->l110_3S_3v6( 'sections', $O1, $I21Y, $I21X, $I222, $I220 )) return '';

}else{


if($this->l110_3S_3v6( 'sections', $O1, $I21Y, $I21X, $I222, $I220 )) return '';

if($this->l110_3S_3v6( 'categories', $O1, $I21Y, $I21X, $I222, $I221 )) return '';

if($this->l110_3S_3v6( 'content', $O1, $I21Y, $I21X, $I222, $I21Z )) return '';

}




return '';





}
















public function onContentBeforeDisplay($O4,&$O5,&$O6,$O7){

if(IS_ADMIN ) return '';

if(!WExtension::exist( 'subscription.node' )) return;

static $I21X=null;
static $I21Y=null;

if(!isset($I21X)){
$I21X=( WGlobals::get('view')=='article' ) ? true : false;
}

$I21Z=!empty($O5->id) ? $O5->id : 0;

$I221=!empty($O5->catid) ? $O5->catid : 0;
$I222=WUser::get( 'rolids' );
 if(!($I222))   $I222=array(1,2);


if($I21X){
if($this->l110_3S_3v6( 'content', $O5, $I21Y, $I21X, $I222, $I21Z )) return '';
if($this->l110_3S_3v6( 'categories', $O5, $I21Y, $I21X, $I222, $I221 )) return '';
}else{
if($this->l110_3S_3v6( 'categories', $O5, $I21Y, $I21X, $I222, $I221 )) return '';
if($this->l110_3S_3v6( 'content', $O5, $I21Y, $I21X, $I222, $I21Z )) return '';
}

return '';


}






















private function l110_3S_3v6($O8,&$O9,$OA,$OB,$OC,$OD){

static $I223=array();



$I224=$O8 . $OD;





if(!isset($I223[$I224])){


$I225=WModel::get( 'role.'.$O8 );

$I225->whereE( 'id', $OD );

$I223[$I224]=$I225->load( 'o', array( 'introrolid', 'rolid'));




if(empty($I223[$I224])) $I223[$I224]='';

}


if(!empty($I223[$I224])){

if(!in_array( $I223[$I224]->rolid, $OC )){




$this->l1a_3T_1r7( $O9, $OA, $OB, $I223[$I224]->introrolid, $I223[$I224]->rolid );

return true;

}
}
return false;



}




















private function l1a_3T_1r7(&$OE,$OF,$OG,$OH,$OI){

$I226=(JOOBI_FRAMEWORK !='joomla15') ? true : false;
$I227='text';

if( $I226){
if(!$OG ) $I227='introtext';
}

$I228=WUser::get( 'rolids' );


if(!($I228))   $I228=array(1,2);



$I229='';


if($OG){

if(in_array( $OH, $I228 )){

$OE->$I227=$OE->introtext;

}else{

$OE->$I227=$I229;

}
}else{

if(!in_array( $OH, $I228 )){
$OE->$I227=$I229;

}
}



if(!defined('PSUBSCRIPTION_NODE_RESTMESSAGE')) WUser::pref ('subscription.node');


if(!defined('JOOBI_URL_THEME_JOOBI')) WView::definePath();

$I22A='<img src="'.JOOBI_URL_JOOBI_IMAGES. 'toolbar/16/lock.png'.'" align="middle">';


if(true){

WText::load( 'role.node' );



$I22B=WUser::get( 'uid' );



if($I22B>0) 
{




$I22C=PSUBSCRIPTION_NODE_REDIRECTLINK;

if(!empty($I22C) AND isset($I22C)) $I22D=WPage::routeURL( $I22C );

else $I22D=WPage::routeURL( 'controller=subscription&task=possible&rolid='. $OI, '', false, false, true, 'jsubscription' );






$I22E=PSUBSCRIPTION_NODE_RESTMESSAGE;

if(!empty($I22E) AND isset($I22E)) $I22F='<span class="restricted"><a href="'.$I22D.'">'.$I22E.'</a></span>';

 else $I22F='<span class="restricted"><a href="'.$I22D.'">'.TR1236926015ATTV.'</a></span>';






$I22G=PSUBSCRIPTION_NODE_RESTMESSAGEPOS;

if(!empty($I22G) AND isset($I22G) AND $I22G==true) $OE->$I227=$OE->$I227 . $I22A . $I22F;

else  $OE->$I227=$I22A . $I22F . $OE->$I227;



}else{ 





$I22C=PSUBSCRIPTION_NODE_VREDIRECTLINK;

if(!empty($I22C) AND isset($I22C)) $I22D=WPage::link( $I22C );

else $I22D=WPage::routeURL( 'controller=subscription&task=possible&rolid='. $OI, '', false, false, true, 'jsubscription' );






$I22E=PSUBSCRIPTION_NODE_VRESTMESSAGE;

if(!empty($I22E) AND isset($I22E)) $I22F='<span class="restricted"><a href="'.$I22D.'">'.$I22E.'</a></span>';

 else $I22F='<span class="restricted"><a href="'.$I22D.'">'.TR1236926015ATTV.'</a></span>';






$I22G=PSUBSCRIPTION_NODE_VRESTMESSAGEPOS;

if(!empty($I22G) AND isset($I22G) AND $I22G==true) $OE->$I227=$OE->$I227 . $I22A . $I22F;

else  $OE->$I227=$I22A . $I22F . $OE->$I227;



}



}else{

$I22F='<span class="restricted">'.TR1236926015ATTW.'</span>';

$OE->$I227=$I22A . $I22F . $OE->$I227;

}



}











function onPrepareContent(&$OJ,&$OK,$OL){
return '';
}












function onAfterContentSave(&$OM,$ON){
return true;
}



}