<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Role_Node_install extends WInstall {


public function install(&$O1)  {


if($O1->newInstall){

}
return true;


}








public function addExtensions(){

$I22P=new stdClass;
$I22P->namekey='role.content.plugin';
$I22P->name='Role Restriction to Joomla Articles';
$I22P->folder='content';
$I22P->type=50;
$I22P->publish=1;
$I22P->certify=1;
$I22P->destination='node|role|plugin';
$I22P->core=1;
$I22P->params='publish=0';
$I22P->description='';

if($this->insertNewExtension( $I22P )) $this->installExtension( $I22P->namekey );

$I22P=new stdClass;
$I22P->namekey='role.system.plugin';
$I22P->name='Role Restriction for Joomla Components';
$I22P->folder='system';
$I22P->type=50;
$I22P->publish=1;
$I22P->certify=1;
$I22P->destination='node|role|plugin';
$I22P->core=1;
$I22P->params='publish=0';
$I22P->description='';

if($this->insertNewExtension( $I22P )) $this->installExtension( $I22P->namekey );

}


}