<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Theme_Node_model extends WModel {


function copyValidate(){

$I1Z6=WGlobals::get('trucs');

$I1Z7=$I1Z6['x']['foldername'];

$I1Z7=strtolower($I1Z7);

$I1Z7=str_replace(' ', '_', $I1Z7);

$I1Z8=WClass::get('theme.helper');
$I1Z8->unPremium( $this->tmid );



$this->core=0;

$this->premium=0;


$this->created=time();
$this->alias='Clone of: ' . $this->alias . '  : ' . $I1Z7;

$this->folder=$I1Z7;
$this->premium=1;

return true;


}





function copyExtra(){

$I1Z9=WGlobals::getEID();
$I1Z8=WClass::get('theme.helper');

$I1ZA=$I1Z8->destfolder( $this->type );


$I1ZB=$I1Z8->getCol( $I1Z9, 'folder' ) ;

$I1ZC=$I1ZA. DS . $this->folder;


$I1ZD=WClass::get('apps.files');

$I1ZD->createFolder( 'theme', $I1ZC, 'user' );



$I1ZE=WGet::folder();
$I1ZE->copy( JOOBI_DS_THEME.$I1ZA.DS.$I1ZB, JOOBI_DS_THEME.$I1ZC,'', true );

$I1ZF=WCache::get();
$I1ZF->resetCache( 'Theme' );



return true;

}





function deleteValidate($O1=null){

$this->_x=$this->load( $O1 );

return true;

}




function deleteExtra($O2=0){

$I1ZE=WGet::folder();

$I1Z8=WClass::get('theme.helper');


if(!empty($this->_x->type)){
$I1ZA=$I1Z8->destfolder( $this->_x->type );
$I1ZC=$I1ZA.DS.$this->_x->folder;
if(!empty($I1ZC)){
$I1ZE->delete( JOOBI_DS_JOOBI . 'user' . DS . 'theme' .DS. $I1ZC );
}
if(!empty($this->_x->type) && !empty($this->_x->premium)){
$I1ZG=WModel::get( 'theme' );
$I1ZG->whereE( 'type', $this->_x->type );
$I1ZG->whereE( 'wid', $this->_x->wid );
$I1ZG->whereE( 'core', 1 );
$I1ZG->setVal( 'premium', 1 );
$I1ZG->update();
}
}

$I1ZF=WCache::get();
$I1ZF->resetCache( 'Theme' );


return true;

}

}