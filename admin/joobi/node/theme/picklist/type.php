<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Theme_Type_picklist extends WPicklist {

function create(){

$I1V6=WGlobals::get('wid');

$I1V7=WModel::get('theme');



$I1V7->select('type');

if($I1V6 !=0)

$I1V7->whereE('wid', $I1V6);

$I1V7->setDistinct();

$I1V7->orderBy('type');

$I1V7->setLimit('50');



$I1V8=$I1V7->load('lra');



if(empty($I1V8) OR sizeof($I1V8)<=1 ) return false;



if(!empty($I1V8)){

foreach($I1V8 as $I1V9){

switch ($I1V9){

case 49:

$I1VA='Application ( node )';
break;


case 106:

$I1VA=TR1206732400OWZW;
break;
case 107:
$I1VA=TR1206961936HCWP;break;
case 108:
$I1VA=TR1206961869IGNP;break;
case 105:
$I1VA='Tag ( tag )';break;


case 1:

$I1VA='Joobi Site ( site )';
break;

case 2:

$I1VA='Joobi Admin ( admin )';
break;

default:

$I1VA=''; 
}
$this->addElement( $I1V9 , $I1VA);

}
}


return true;

}
}