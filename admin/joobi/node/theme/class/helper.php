<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Theme_Helper_class extends WClasses {












public function destfolder($O1=49){


if(empty($O1)) $I1VB='node';
else {
$I1VC=WType::get( 'theme.typefolder' );
$I1VB=$I1VC->getName( $O1 );
if(empty($I1VB)) $I1VB='node';
}
return $I1VB;

}



public function getCol($O2,$O3){

if(empty($O2) || empty($O3)) return null;



static $I1VD=array();



if(empty($I1VD[$O2])){

$I1VE=WModel::get('theme');

$I1VE->whereE('tmid', $O2);

$I1VD[$O2]=$I1VE->load( 'o' );

}

return $I1VD[$O2]->$O3;

}














public function getFiles($O4,$O5=''){


if(!is_string($O5)) return false;

$I1VF=$this->getCol($O4,'type');

$I1VG=$this->getCol($O4,'folder');
$I1VH=$this->getCol($O4,'core');

$I1VB=$this->destfolder( $I1VF );


$I1VI=JOOBI_DS_THEME.$I1VB.DS.$I1VG. ( !empty($O5) ? DS.$O5 : '' );
$I1VJ=WGet::file();$I1VK=WGet::folder();

if(!$I1VK->exist(JOOBI_DS_THEME.$I1VB.DS.$I1VG)) return '';

$I1VL='controller=theme&task=copythemefile&eid='.$O4.'&type='.$I1VF . ( !empty( $O5 ) ? '&filetype='.$O5 : '' );
$I1VM='<img src="'.JOOBI_URL_JOOBI_IMAGES.'toolbar/16/copy.png" title="Copy" /> ';


if(!$I1VK->exist($I1VI)) return '';



$I1VN=$I1VK->files( $I1VI );                
$I1VO=array();



if(!empty($I1VN)){

sort($I1VN);
foreach( $I1VN as $I1VP){

if($I1VP=='index.html' ) continue;


$I1VQ=explode( '.', $I1VP );
$I1VR=array_pop( $I1VQ );

if(!in_array( $I1VR, array( 'php', 'css', 'js', 'html')) ) continue;

$I1VS=new stdClass;
$I1VS->filename=$I1VP;

$I1VT=WPage::routeURL('controller=theme&task=showfile&eid='.$O4.'&filetype='.$O5.'&file='.base64_encode(serialize($I1VP)).'&titleheader='.$I1VP);
$I1VU='<a href="'. $I1VT .'">';
$I1VU .=$I1VP;
$I1VU .="</a>";

if($I1VH==0){
$I1VL .="&filename=".base64_encode(serialize($I1VP)).'&titleheader='.$I1VP;
$I1VV=WPage::linkPopUp($I1VL );

$I1VW=WPage::routeURL('controller=theme&task=editfile&eid='.$O4.'&filetype='.$O5.'&file='.base64_encode(serialize($I1VP)).'&titleheader='.$I1VP);
$I1VX='<img src="'.JOOBI_URL_JOOBI_IMAGES.'toolbar/16/edit.png" title="Edit" /> ';

$I1VS->filename=WPage::createPopUpLink( $I1VV, $I1VM, 600, 250 );

$I1VS->filename .='&nbsp;&nbsp;&nbsp;<a href="'. $I1VW .'">';

$I1VS->filename .=$I1VX;
$I1VS->filename .="</a>";

$I1VS->filename .='&nbsp;&nbsp;&nbsp;';
$I1VS->filename .=$I1VU;
}else{
$I1VS->filename=$I1VU;
}

$I1VO[]=$I1VS;

}
}


return $I1VO;


}




public function overwriteThemeFile($O6=false){

$I1VY=WGlobals::get('titleheader');
$I1VZ=WGlobals::get('trucs');
$I1W0=$I1VZ['x']['file'];
$I1W1=unserialize(base64_decode($I1W0));
$I1W2=$I1VZ['x']['eid'];
$I1W3=$I1VZ['x']['filetype'];
$I1W4=$I1VZ['x']['content'];

WGlobals::set('eid', $I1W2 );
WGlobals::set('file', $I1W1 );
WGlobals::set('filetype', $I1W3 );


$I1VJ=WGet::file();

$I1VF=$this->getCol($I1W2,'type');
$I1VG=$this->getCol($I1W2,'folder');
$I1VB=$this->destfolder($I1VF);
if($I1W3=='main')  $I1W5=JOOBI_DS_THEME.$I1VB.DS.$I1VG.DS.$I1W1;
else $I1W5=JOOBI_DS_THEME.$I1VB.DS.$I1VG.DS.$I1W3.DS.$I1W1;

$I1VJ->write($I1W5, $I1W4, 'overwrite');

$I1W6=WMessage::get();
$I1W6->userS('1298294183ETWL',array('$file'=>$I1W1));

if($O6){
WPage::redirect( 'controller=theme&task=editfile&eid='.$I1W2.'&filetype='.$I1W3.'&file='.$I1W0.'&titleheader='. $I1VY);
}else{
WPage::redirect( 'controller=theme&task=show&eid='.$I1W2 );
}
return true;

}





function setPremium($O7){
$I1VE=WModel::get('theme');
if(empty($O7)) return false;
$I1VF=$this->getCol($O7, 'type');

$I1VE->setVal('premium', 1);
$I1VE->whereE( 'tmid', $O7 );
$I1VE->whereE( 'type', $I1VF );
$I1VE->update();

return true;
}
function unPremium($O8){

if(empty($O8)) return false;
$I1VF=$this->getCol($O8, 'type');

$I1VE=WModel::get('theme');

$I1W7=$this->getCol($O8, 'wid');

$I1VE->setVal('premium', 0);
$I1VE->whereE( 'wid', $I1W7 );
$I1VE->whereE( 'type', $I1VF );
$I1VE->update();
return true;
}






public function getFileContent($O9='',$OA='main',$OB='index.html'){

$I1VF=$this->getCol($O9,'type');
$I1VG=$this->getCol($O9,'folder');
$I1VB=$this->destfolder($I1VF);

if($OA=='main'){
$I1W5=JOOBI_DS_THEME.$I1VB.DS.$I1VG.DS.'index.html';
}else{
$I1W5=JOOBI_DS_THEME.$I1VB.DS.$I1VG. ( !empty($OA) ? DS.$OA : '' ) .DS.$OB;
}
$I1VJ=WGet::file();
$I1W8=$I1VJ->size($I1W5);
$I1W9=$I1VJ->read($I1W5);    
if($I1W8 <=1048576){
$I1W4=$I1W9;
}else{
return false;
}return $I1W4;

}


}