<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Theme_Theme_js_files_view extends Output_Listings_class {
function prepareQuery(){

$I1TV=WGlobals::getEID();



$I1TW=WClass::get('theme.helper');

$I1TX=$I1TW->getFiles($I1TV, 'js');



if(!empty($I1TX)) $this->addData( $I1TX );

else return false;



return true;

}}