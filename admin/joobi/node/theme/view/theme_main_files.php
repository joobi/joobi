<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Theme_Theme_main_files_view extends Output_Listings_class {
function prepareQuery(){

$I1TS=WGlobals::getEID();



$I1TT=WClass::get('theme.helper');

$I1TU=$I1TT->getFiles( $I1TS, '' );



if(!empty($I1TU)) $this->addData( $I1TU );

else return false;



return true;

}
}