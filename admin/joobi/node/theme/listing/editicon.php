<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'listing.butedit' , JOOBI_LIB_HTML );
class Theme_Editicon_listing extends WListings_butedit {
function create(){


if($this->getValue('core')){
WView::addCSSLegend( TR1206732372QTKM , 'jpng-16-show' );
$this->content='<span class="jpng-16-show" alt="Show" title="View" border="0"></span>';
return true;
}else{
return parent::create();
}

}}