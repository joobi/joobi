<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;













class Joomla_Quickicons_module extends WModule {




	public function create() {

		$I3SD = new stdClass;
		$I3SD->wid = WExtension::get( 'joomla.node', 'wid' );
		$I3SE = new stdClass;

		$I3SF = WView::getHTML( 'joomla_quickicons' , $I3SD, $I3SE );
		if ( !empty($I3SF) ) $this->content = '<div class="dashboard-panel">' . $I3SF->make() . '</div>';

	}
}