<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Joomla_rendermodule_controller extends WController {

	function rendermodule() {


		$I3SH = JRequest::getVar('code', '', null, 'string');
		$I3SI = JFactory::getConfig();
		$I3SJ = $I3SI->get('config.secret');
		if ( $I3SH != $I3SJ ) exit;

		$I3SK = JRequest::getInt('protect');
		$I3SL = time();

		if( empty($I3SK) || ( $I3SL > $I3SK + 5 ) || $I3SL < $I3SK ) exit;

		$I3SM = JRequest::getInt('id');
		if( empty($I3SM) ) exit;

		$I3SN = JFactory::getDBO();
	 	$I3SN->setQuery( 'SELECT * FROM #__modules WHERE `id`='.$I3SM.' LIMIT 1' );
	 	$I3SO = $I3SN->loadObject();

	 	if ( empty($I3SO) ) exit;

		$I3SO->user  	= substr( $I3SO->module, 0, 4 ) == 'mod_' ?  0 : 1;
		$I3SO->name = $I3SO->user ? $I3SO->title : substr( $I3SO->module, 4 );
		$I3SO->style = null;
		$I3SO->module = preg_replace( '/[^A-Z0-9_\.-]/i', '', $I3SO->module );
		$I3SP = array();
		$I3SQ =& JFactory::getLanguage();
		$I3SQ->load( $I3SO->module );
		echo JModuleHelper::renderModule( $I3SO, $I3SP );
		exit;




		return true;

	}
}