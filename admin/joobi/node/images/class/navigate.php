<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;







class Images_navigate_class extends WClasses {

var $_navType;






var $basePath=DS;




var $uri='';





var $basePathName='';





var $dispFolders=true;




var $dispFiles=false;








var $constraints=array();





var $_message;

var $_arrowImg='>';
var $_dirImg='D ';
var $_backImg='<<';

function __construct($O1){
$this->_navType=$O1;
WPage::addCSSScript('table.'.JOOBI_BOX_CLASS.' td.navkey{
padding-top: 0.6em;
text-align: right;
vertical-align: middle;
width: 7em;
color: #666;
border-bottom: 0.1em solid #e9e9e9;
border-right: 0.1em solid #e9e9e9;}');
$this->_arrowImg='<img src="'.JOOBI_URL_JOOBI_IMAGES.'toolbar/16/fleche.png" alt=">">';
$this->_dirImg='<img src="'.JOOBI_URL_JOOBI_IMAGES.'app/blue/16/category.png" alt="D "> ';
$this->_backImg='<img src="'.JOOBI_URL_JOOBI_IMAGES.'toolbar/16/back.png" alt="<<">';
$this->_message=WMessage::get();
}






function setVariables($O2,$O3=DS,$O4=''){
$this->basePath=$O3;
WGlobals::set('basePath',$O3, 'session' );
$this->basePathName=$O4;
$this->uri=$O2;
}








function pathAndContent($O5=true,$O6=true){
if($this->uri==''){
$this->_message->codeE('You need at least to define the $uri variable.');
$this->_message->codeE('This can be done by the setVariables function.');
return '';
}


$I4J='';
$I4K=TR1209123607OCSQ;
if($O5==true)
$I4J.='<fieldset><legend>'.$I4K.'</legend>';$I4J.=$this->path($O6);
$I4J.=$this->content($O6);
if($O5==true)
$I4J.='</fieldset>';
return $I4J;
}








function path($O7=true){

$I4L=$this->l1l_D_31y($this->basePath,$this->basePath,$this->basePathName).' ';

$I4M=urldecode(WGlobals::get('linkedpath'));
WGlobals::set('linkedpath',$I4M, 'session' );
if($I4M==1) $I4M='';

if(!empty($I4M)){
$I4N=explode(DS,$I4M);
foreach($I4N as $I4O){
if(!empty($I4O)){
$I4P=substr($I4M,0,strpos($I4M,$I4O)+strlen($I4O));
$I4L.=$this->_arrowImg.' '.$this->l1l_D_31y($I4P).' ';
}
}
}

if($this->_navType=='insertImages'){
$I4Q=$this->l1m_E_686($this->basePath.$I4M);
$I4R=$this->l1n_F_3rf($I4M,$I4Q);
WGlobals::set('imagesPath',$I4R, 'session' );
WGlobals::set('imagesSecure',$I4Q, 'session' );
}

if($O7==true){
$I4K=TR1209034856HUVS;
return '<table><tr><td class="navkey">'.$I4K.'</td><td>'.$I4L.'</td></tr></table>';
}
else
return $I4L;

}









function l1m_E_686($O8){
$I4S=strpos($O8,'safe');
if($I4S===false) return 2;
else return 1;
}










function l1n_F_3rf($O9,$OA){
$I4L='';
$I4T=$this->basePath.$O9;
$I4L=$this->l1o_G_30i($I4T,'safe');
if($I4L=='') $I4L=$this->l1o_G_30i($I4T,'media');
if($I4L=='') $I4L='1';
return str_replace(DS,'|',$I4L);
}

function l1o_G_30i($OB,$OC){
$I4U=array();
$I4V=explode(DS,$OB);
$I4W=false;
$I4X=0;
foreach($I4V as $I4Y){
if($I4W) $I4U[$I4X++]=$I4Y;
if($I4Y==$OC) $I4W=true;
}
if(!$I4W) return '';
return implode( '/',$I4U);
}










function content($OD=true){

$I4M=urldecode(WGlobals::get('linkedpath'));
WGlobals::set('linkedpath',$I4M, 'session' );
if($I4M==1) $I4M='';

$I4Z=$this->basePath.$I4M;

$I50=scandir($I4Z);
$I51=array(); $I52=0;
$I53=array(); $I54=0;
$I55='';
$I56='';

$I55.=$this->l1p_H_5ym($I4M);
foreach($I50 as $I4O){
if($this->_navType=='insertImages'){
if((is_dir($I4Z.DS.$I4O))&&($I4O[0]!='.')&&($this->l1q_I_7mx($I4O,$I4Z))
&& $I4O!='thumbnails'){
$I55.=$this->l1l_D_31y($I4M.DS.$I4O,'',$this->_dirImg.$I4O).'<br/>';
}
}
else {
if((is_dir($I4Z.DS.$I4O))&&($I4O[0]!='.')&&($this->l1q_I_7mx($I4O,$I4Z))){
$I55.=$this->l1l_D_31y($I4M.DS.$I4O,'',$this->_dirImg.$I4O).'<br/>';
}
}
if((!is_dir($I4Z.DS.$I4O)) && ($I4O[0]!='.')){
$I56.=$I4O.'<br/>';
}

}

$I57='';
if(($this->dispFolders)&&($this->dispFiles))
$I57=$I55.$I56;
elseif($this->dispFolders)
$I57=$I55;
elseif($this->dispFiles)
$I57=$I56;

if($OD==true){
$I4K=TR1206961882TDHH;
return '<table><tr><td class="navkey">'.$I4K.'</td><td>'.$I57.'</td></tr></table>';
}
else
return $I57;

}









function l1l_D_31y($OE,$OF='',$OG=''){
$I4M='';
if($OE==$OF) $I4M=1;
else $I4M=substr($OE,strlen($OF),strlen($OE)-strlen($OF));

$I58=WPage::routeURL($this->uri,'smart','popup');
$I59='<a href="'.$I58.'&linkedpath='.urlencode($I4M).'">';
$I5A='';

if($OG==''){
$I51=explode(DS,$OE);
$I5B=sizeof($I51);

if(!empty($I51[$I5B-1])) $I5A=$I51[$I5B-1];
elseif(!empty($I51[$I5B-2])) $I5A=$I51[$I5B-2];
}
else $I5A=$OG;

return $I59.$I5A.'</a>';
}







function l1p_H_5ym($OH){
if(!empty($OH)){
$I51=explode(DS,$OH);
$I5B=sizeof($I51);

if(!empty($I51[$I5B-1])){
$I59=substr($OH,0,strlen($OH)-strlen($I51[$I5B-1])-1);
return $this->l1l_D_31y($I59,'',$this->_backImg).'<br/>';
}
elseif(!empty($I51[$I5B-2])){
$I59=substr($OH,0,strlen($OH)-strlen($I51[$I5B-2]-1));
return $this->l1l_D_31y($I59,'',$this->_backImg).'<br/>';
}
else
return $this->l1l_D_31y('','',$this->_backImg).'<br/>';
}
else return '';
}











function l1q_I_7mx($OI,$OJ){
$I5C=$this->constraints;
if(isset($I5C[$OJ]) && $I5C[$OJ]!=null) $I5D=$I5C[$OJ];
else return true;
$I4J=false;
foreach($I5D as $I5E){
if($OI==$I5E) $I4J=true;
}
return $I4J;
}
}