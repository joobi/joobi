<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;









class Images_Resize_class extends WClasses{

    public $source=null;

        public $height=0;
    public $width=0;
    public $mime=null;

    public $max_width=200;
    public $max_height=200;
    public $thumb_width=50;
    public $thumb_height=50;
    public $dest='';
    public $type='';
    public $size=null;












    public function resizeImageDimentions(&$O1,&$O2,$O3=30,$O4=30){

    $this->max_width=$O3;
    $this->max_height=$O4;

    $this->calcImageSize( $O1, $O2 );

    $O1=$this->thumb_width;
        $O2=$this->thumb_height;

    }






    public function resizeImage(){

        switch ( strtolower($this->type)){
            case 'gif':
                $I5F=imagecreatefromgif( $this->source );
                break;
            case 'jpg':
            case 'jpeg':
                $I5F=imagecreatefromjpeg( $this->source );
                break;
            case 'png':
                $I5F=imagecreatefrompng( $this->source );
                break;
            default:
            return false;
            break;
        }
        $this->calcImageSize( $this->GetWidth(), $this->GetHeight());

                if(empty($this->thumb_width) || $this->thumb_width < 1 ) $this->thumb_width=50;
        if(empty($this->thumb_height) || $this->thumb_height < 1 ) $this->thumb_height=50;
        $I5G=imagecreatetruecolor((int)$this->thumb_width, (int)$this->thumb_height );


if( strtolower($this->type)=='png' ){ imagealphablending($I5G, false);
imagealphablending($I5G, false);
imagesavealpha($I5G,true);
$I5H=imagecolorallocatealpha($I5G, 255, 255, 255, 127);
imagefilledrectangle($I5G, 0, 0, $this->thumb_width, $this->thumb_height, $I5H);
}elseif( strtolower($this->type)=='gif' ){ $I5I=imagecolorallocate ($I5G, 255, 255, 255);
       imagefilledrectangle($I5G, 0, 0, $this->thumb_width, $this->thumb_height, $I5I );
}
$I5J=imagecopyresampled( $I5G, $I5F, 0, 0, 0, 0, $this->thumb_width, $this->thumb_height, $this->GetWidth(), $this->GetHeight());
        
if(!is_writable( dirname($this->dest))){
$I5K=WMessage::get();
$I5L=dirname($this->dest);
$I5K->userW('1212843259ITHW',array('$FOLDER'=>$I5L));
return false;
}
switch ( strtolower($this->type)){
            case 'gif':
                return ((imagegif( $I5G, $this->dest )==true ) ? true: false );
                break;
            case 'jpg':
            case 'jpeg':
                return ((imagejpeg( $I5G, $this->dest, 100 )==true ) ? true: false );
                break;
            case 'png':
               return ((imagepng( $I5G, $this->dest )==true ) ? true: false );
               break;
        }

     }



        private function GetWidth($O5=false){
$this->l1r_J_1mr($O5=false);
return $this->width;
    }
        private function GetHeight($O6=false){
$this->l1r_J_1mr($O6=false);
return $this->height;
    }

    private function l1r_J_1mr($O7=false){
static $I5M=null;

    if(!isset($I5M) || $O7){
    $I5M=@getimagesize($this->source);
$this->width=$I5M[0];
$this->height=$I5M[1];
$this->mime=$I5M['mime'];
    }
    }


        private function resizeHeight($O8,$O9,$OA=0){

        if($OA==0 ) $OA=$O9 / $this->max_height;
        $I5N=$O9 / $OA;
        return array( 'ratio'=>$OA ,'newHeight'=>intval($I5N));    }







    private function resizeWidth($OB,$OC,$OD=0){

        if($OD==0 ) $OD=$OB / $this->max_width;
        $I5O=$OB / $OD;
        return array('newWidth'=>intval($I5O),'ratio'=>$OD );    }








    private function calcImageSize($OE,$OF){

if($this->max_width < 1 || $this->max_height < 1 ) return false;

  if($OE > $this->max_width){

            $I5P=$this->resizeWidth( $OE, $OF );
$OE=$I5P['newWidth'];
            $I5Q=$this->resizeHeight( $OE, $OF, $I5P['ratio'] );
$OF=$I5Q['newHeight'];

        }
          if($OF > $this->max_height){

            $I5Q=$this->resizeHeight( $OE, $OF );
$OF=$I5Q['newHeight'];
            $I5P=$this->resizeWidth( $OE, $OF, $I5Q['ratio'] );
$OE=$I5P['newWidth'];

        }
        $this->thumb_width=$OE;
        $this->thumb_height=$OF;
    }
}