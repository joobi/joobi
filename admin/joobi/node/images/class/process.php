<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Images_Process_class extends WClasses {


public function process(&$O1){
if(!function_exists('gd_info')) return true;
$this->imagePath=$O1->_completePath . DS . $O1->name . '.' . $O1->type;

if(function_exists('gd_info')){

$I48=( is_array($O1->_maxTHeight)) ? $O1->_maxTHeight[0] : $O1->_maxTHeight;
$I48=( $I48 ) ? $I48 : 15;$I49=( is_array($O1->_maxTWidth)) ? $O1->_maxTWidth[0] : $O1->_maxTWidth;
$I49=( $I49 ) ? $I49 : 15;
$I4A=WClass::get( 'images.resize' );
$I4A->source=$O1->_tmp_name;
$I4A->type=$O1->type;
$I4B=$this->l1k_C_2jz( $O1->_tmp_name );

if($O1->_maxWidth > 0 || $O1->_maxHeight > 0){

if($I4B[0] > $O1->_maxWidth || $I4B[1] > $O1->_maxHeight){

$I4A->max_width=$O1->_maxWidth;
$I4A->max_height=$O1->_maxHeight;
$I4A->width=$I4B[0];
$I4A->height=$I4B[1];

$I4A->dest=$this->imagePath;

if($I4A->resizeImage()){
$I4B=$this->l1k_C_2jz( $this->imagePath );
$O1->width=@$I4B[0];
$O1->height=@$I4B[1];

$I4C=$O1->_maxHeight / $I48;
$this->twidth=$I4B[0] / $I4C;
$this->theight=$I4B[1] / $I4C;
$O1->_resized=true;
$I4D=filesize( $this->imagePath );
if(!empty($I4D)) $O1->size=$I4D;
}else{
$O1->width=@$I4B[0];
$O1->height=@$I4B[1];
$O1->_resized=false;
}
}else{
$O1->width=@$I4B[0];
$O1->height=@$I4B[1];
$O1->_resized=false;
}}else{
$I4B=$this->l1k_C_2jz( $this->imagePath, $O1->_tmp_name );
$O1->width=@$I4B[0];
$O1->height=@$I4B[1];

$I4C=$O1->_maxHeight / $I48;
if(!empty($I4C)){
$this->twidth=$I4B[0] / $I4C;
$this->theight=$I4B[1] / $I4C;
}
}
if(!empty($O1->_watermark)){
$I4E=WClass::get( 'images.watermark' );
$I4E->processWatermark( $I4A, $O1->width, $O1->height );

}

if($O1->thumbnail){

$I4A->max_width=$I48;
$I4A->max_height=$I49;
if(empty( $I4A->width )) $I4A->width=$I4B[0];
if(empty( $I4A->height )) $I4A->height=$I4B[1];

$I4F=WGet::folder();
$I4G=$O1->_completePath . DS . 'thumbnails';
$I4F->create( $I4G, '', true );

$I4A->dest=$I4G . DS . $O1->name . '.' . $O1->type;
if($I4A->resizeImage()){
$I4B=$this->l1k_C_2jz( $I4A->dest );
$O1->twidth=@$I4B[0];
$O1->theight=@$I4B[1];
}else{
$O1->twidth=0;
$O1->theight=0;
}
}

if($O1->_resized ) $O1->_completePath=$O1->_completePath . DS . 'oversize';

return true;

}else{
$I4H=WMessage::get();
$I4H->historyE('1369749782AJAF');
}
return true;

}







private function l1k_C_2jz($O2,$O3=''){
if(file_exists( $O2 )){
return @getimagesize( $O2 );
}
if(!empty($O3) && file_exists( $O3 )){
return @getimagesize( $O3 );
}
$I4H=WMessage::get();
$I4I=$O2;
$I4H->adminE( 'The image file ' . $I4I . ' does not exist.' );
return false;

}

}