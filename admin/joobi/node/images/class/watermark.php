<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;








class Images_Watermark_class extends WClasses {

private $imageInfoO=null;
private $imageWidth=null;
private $imageHeight=null;






public function processWatermark(&$O1,$O2,$O3){

if(empty($O1)) return false;
$this->imageInfoO=&$O1;
$this->imageWidth=$O2;
$this->imageHeight=$O3;

if(!defined( 'PIMAGES_NODE_WATERMARKUSEIMAGE')) WPref::get( 'images.node' );

if(PIMAGES_NODE_WATERMARKUSEIMAGE){
$this->watermarkImage();
}
if(PIMAGES_NODE_WATERMARKUSETEXT){
$this->watermarkText();
}
}

    private function watermarkText(){
    static $I38=null;

    if(!isset($I38)){
    $I39=PIMAGES_NODE_WATERMARKTEXTFONTFILE;
    if(empty( $I39 )){
    $I38=JOOBI_DS_MEDIA . 'fonts' . DS . 'monofont.ttf';
    }else{
        $I3A=PIMAGES_NODE_WATERMARKTEXTFONTFILE;
    $I3A=JOOBI_DS_ROOT . trim( str_replace( array( '/', '\\' ), DS, $I3A ), DS );
    $I3B=WGet::file();
    if(!$I3B->exist( $I3A )){
    $I3C=WMessage::get();
    $I3C->userE('1338404981IUWW');
    $I38=JOOBI_DS_MEDIA . 'fonts' . DS . 'monofont.ttf';
    }else{
    $I38=$I3A;
    }    }    }
    if(empty($I38)) return false;

$I3D=$this->imageCreate( $this->imageInfoO->source, $this->imageInfoO->type );
if(empty($I3D)) return false;

    $I3E=$this->renderTextOnImage( $I3D, $I38 );

   if(!$I3E ) return false;

switch ( strtolower($this->imageInfoO->type)){
            case 'gif':
                return ((imagegif( $I3D, $this->imageInfoO->source )==true ) ? true: false );
                break;
            case 'jpg':
            case 'jpeg':
                return ((imagejpeg( $I3D, $this->imageInfoO->source, 100 )==true ) ? true: false );
                break;
            case 'png':
               return ((imagepng( $I3D, $this->imageInfoO->source )==true ) ? true: false );
               break;
        }
        return false;
}


    private function watermarkImage(){
    static $I3F=null;
    static $I3G=null;
    static $I3H=null;
    static $I3I=null;

    if(!isset($I3F)){

        $I3A=PIMAGES_NODE_WATERMARKIMAGEFILE;    $I3A=JOOBI_DS_ROOT . trim( str_replace( array( '/', '\\' ), DS, $I3A ), DS );
    $I3B=WGet::file();
    if(!$I3B->exist( $I3A )){
    $I3F=true;
    $I3C=WMessage::get();
    $I3C->userE('1338338300MXQW');
    }else{
    $I3F=$I3A;

    $I3J=explode( '.', $I3A );
    $I3G=$this->imageCreate( $I3F, array_pop($I3J));
        if(empty($I3G)) $I3F=true;
        $I3H=imagesx( $I3G );
$I3I=imagesy( $I3G );

    }    }
    if($I3F===true || empty($I3F)) return false;

        $I3D=$this->imageCreate( $this->imageInfoO->source, $this->imageInfoO->type );
        if(empty($I3D)) return false;

        $this->calculatePosition( PIMAGES_NODE_WATERMARKIMAGEPOSITION, $I3H, $I3I );
        $I3E=imagecopymerge( $I3D, $I3G, $this->destinationX, $this->destinationY, 0, 0, $I3H, $I3I, 100 );
    if(!$I3E ) return false;

switch ( strtolower($this->imageInfoO->type)){
            case 'gif':
                return ((imagegif( $I3D, $this->imageInfoO->source )==true ) ? true: false );
                break;
            case 'jpg':
            case 'jpeg':
                return ((imagejpeg( $I3D, $this->imageInfoO->source, 100 )==true ) ? true: false );
                break;
            case 'png':
               return ((imagepng( $I3D, $this->imageInfoO->source )==true ) ? true: false );
               break;
        }
    return false;

}


    private function calculatePosition($O4,$O5,$O6){
    $I3K=5;
    $I3L=5;

    switch( $O4){
    case 1:
    $this->destinationX=$I3K;
    $this->destinationY=$I3L;
    break;
    case 2:
    $this->destinationX=round(($this->imageWidth / 2) - ($O5 / 2));
    $this->destinationY=$I3L;
    break;
    case 3:
    $this->destinationX=$this->imageWidth - $O5 - $I3K;
    $this->destinationY=$I3L;
    break;
    case 4:
    $this->destinationX=$I3K;
    $this->destinationY=round(($this->imageHeight / 2) - ($O6 / 2));
    break;
    case 5:
    $this->destinationX=round(($this->imageWidth / 2) - ($O5 / 2));
    $this->destinationY=round(($this->imageHeight / 2) - ($O6 / 2));
    break;
    case 6:
    $this->destinationX=$this->imageWidth - $O5 - $I3K;
    $this->destinationY=round(($this->imageHeight / 2) - ($O6 / 2));
    break;
    case 7:
    $this->destinationX=$I3K;
    $this->destinationY=$this->imageHeight - $O6 - $I3L;
    break;
    case 8:
    $this->destinationX=round(($this->imageWidth / 2) - ($O5 / 2));
    $this->destinationY=$this->imageHeight - $O6 - $I3L;
    break;
    case 9:
    default:
    $this->destinationX=$this->imageWidth - $O5 - $I3K;
    $this->destinationY=$this->imageHeight - $O6 - $I3L;
    break;
    }
}


private function renderTextOnImage(&$O7,$O8){
    $I3K=5;

    $I3M=PIMAGES_NODE_WATERMARKTEXTVALUE;
    $I3N=PIMAGES_NODE_WATERMARKTEXTSIZE;
    $I3O=PIMAGES_NODE_WATERMARKTEXTCOLOR;
    $I3P=PIMAGES_NODE_WATERMARKTEXTOPACITY;
    $I3Q=PIMAGES_NODE_WATERMARKTEXTROTATION;

  $I3R=imagesx( $O7 );
    $I3S=imagesy( $O7 );

    $I3T=$this->imagettfbbox_fixed( $I3N, $I3Q, $O8, $I3M );

    $I3U=min( $I3T[ 0 ], $I3T[ 2 ], $I3T[ 4 ], $I3T[ 6 ] ) - $I3K;
    $I3V=max( $I3T[ 0 ], $I3T[ 2 ], $I3T[ 4 ], $I3T[ 6 ] ) + $I3K;
    $I3W=min( $I3T[ 1 ], $I3T[ 3 ], $I3T[ 5 ], $I3T[ 7 ] ) - $I3K;
    $I3X=max( $I3T[ 1 ], $I3T[ 3 ], $I3T[ 5 ], $I3T[ 7 ] ) + $I3K;

    $I3Y=abs( $I3V - $I3U );
    $I3Z=abs( $I3X - $I3W );

    switch ( PIMAGES_NODE_WATERMARKTEXTPOSITION){
      case 1:
        $I40=-$I3W;
        $I41=-$I3U;
        break;
      case 2:
        $I40=-$I3W;
        $I41=$I3R / 2 - $I3Y / 2 - $I3U;
        break;
      case 3:
        $I40=-$I3W;
        $I41=$I3R - $I3V;
        break;
      case 4:
        $I40=$I3S / 2 - $I3Z / 2 - $I3W;
        $I41=-$I3U;
        break;
      case 5:
        $I40=$I3S / 2 - $I3Z / 2 - $I3W;
        $I41=$I3R / 2 - $I3Y / 2 - $I3U;
        break;
      case 6:
        $I40=$I3S / 2 - $I3Z / 2 - $I3W;
        $I41=$I3R - $I3V;
        break;
      case 7:
        $I40=$I3S - $I3X;
        $I41=-$I3U;
        break;
      case 8:
        $I40=$I3S - $I3X;
        $I41=$I3R / 2 - $I3Y / 2 - $I3U;
        break;
      case 9;
        $I40=$I3S - $I3X;
        $I41=$I3R - $I3V;
        break;
    }

    $I42=imagecolorallocatealpha(
      $O7,
      hexdec( substr( $I3O, 0, 2 )),
      hexdec( substr( $I3O, 2, 2 )),
      hexdec( substr( $I3O, 4, 2 )),
      127 * ( 100 - $I3P ) / 100
    );


    return imagettftext( $O7, $I3N, $I3Q, $I41, $I40, $I42, $O8, $I3M );

  }
  private function imagettfbbox_fixed($O9,$OA,$OB,$OC){
    $I3T=imagettfbbox( $O9, 0, $OB, $OC );
    $I43=deg2rad( $OA );
    $I44=cos( $I43 );
    $I45=sin( $I43 );
    $I46=array( );
    for( $I47=0; $I47 < 7; $I47 +=2 )
    {
      $I46[ $I47 + 0 ]=round( $I3T[ $I47 + 0 ] * $I44 + $I3T[ $I47 + 1 ] * $I45 );
      $I46[ $I47 + 1 ]=round( $I3T[ $I47 + 1 ] * $I44 - $I3T[ $I47 + 0 ] * $I45 );
    }
    return $I46;
  }


private function imageCreate($OD,$OE){

       switch ( strtolower($OE)){
            case 'gif':
                $I3D=@imagecreatefromgif( $OD );
                break;
            case 'jpg':
            case 'jpeg':
                $I3D=@imagecreatefromjpeg( $OD );
                break;
            case 'png':
                $I3D=@imagecreatefrompng( $OD );
                break;
            default:
            return false;
            break;
        }
return $I3D;

}
}