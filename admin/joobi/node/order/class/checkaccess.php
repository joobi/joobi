<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;













class Order_Checkaccess_class extends WClasses {
	


	function hasOrderAccess() {
				if ( !IS_ADMIN ) {
			$IP1 = WRole::get();
			$IP2 = $IP1->hasRole( 'storemanager' );

									if ( !$IP2 ) {

				$IP3 = WUser::get('uid');
				if ( !empty($IP3) ) {
					$IP4 = WClass::get('vendor.helper',null,'class',false);
					$IP5 = $IP4->getVendorID( $IP3 );
				}
				if ( empty( $IP5 ) || empty( $IP3 ) ) {
					$IP6 = WMessage::get();
					$IP6->exitNow( 'Unauthorized access 134' );
				} else {
										$IP7 = WGlobals::getEID();
					
					$IP8 = WModel::get( 'order' );
					$IP8->whereE( 'oid', $IP7 );
					$IP8->whereE( 'vendid', $IP5 );
					$IP9 = $IP8->exist();
					if ( !$IP9 ) {
												$IPA = WModel::get( 'order.products' );
						$IPA->makeLJ( 'order', 'oid' );
												$IPA->whereE( 'oid', $IP7, 1 );
						$IPA->whereE( 'vendid', $IP5 );
						$IP9 = $IPA->exist();						
						
						if ( !$IP9 ) {
							$IP6 = WMessage::get();
							$IP6->exitNow( 'Unauthorized access 135' );
						}					}	
				}				
			}			
		}		
		return true;
	}	

}