<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Order_Helper_class extends WClasses {




















	function getNamekey($O1,$O2,$O3) {



		switch($O2) {

			case 'web':

				$IK1 = 'W';

			break;

			case 'phone':

				$IK1 = 'P';

			break;

			case 'refund':

				$IK1 = 'R';

			break;

			case 'mail':

				$IK1 = 'M';

			break;

			case 'free':

				$IK1 = 'F';

			break;

			default:

				$IK1 = 'X';

			break;

		}




		

		$IK2 = $O1->genNamekey( '', '250', $IK1 );

		$IK2 .= strtoupper( base_convert( $O3,10,36) );




		return $IK2;



	}












	function getOrderNamekey($O4) {

		static $IK3=array();



		if( !isset( $IK3[$O4] ) )

		{

			static $IK4 = null;

			if( !isset( $IK4 ) ) $IK4 = WModel::get( 'order' );

			$IK4->whereE( 'oid', $O4 );

			$IK3[$O4] = $IK4->load( 'lr', 'namekey' );

		}


		return $IK3[$O4];

	}
















	function storeToHistory($O5,$O6,$O7,$O8=0){

		
		if( empty($O5) || empty($O6) ) {

			$IK5 = WMessage::get();

			$IK5->userW('1262855060IIEP');

			return true;

		}


		
		if( empty($O7) || ( $O7 < 0 ) ) $O7 = WUser::get( 'uid' );



		static $IK6=null;

		if( !isset($IK6) ) $IK6 = WModel::get( 'order.history' );

		$IK6->setVal( 'oid', $O5 );

		$IK6->setVal( 'ostatusid', $O6 );

		$IK6->setVal( 'uid', $O7 );

		$IK6->setVal( 'payid', $O8 );

		$IK6->setVal( 'created', time() );

		$IK6->insert();



		return true;

	}


	












	function getOrderDetails($O9,$OA=null,$OB=false,$OC=false) {

			if ( empty($O9) || !is_numeric($O9) || ( !empty( $OA ) && !is_string($OA) && !is_array($OA) ) ) {
				$IK5 = WMessage::get();
 					$IK5->adminW( 'Please check order parameters' );

				return false;
			}
			static $IK4 = null;
			if ( empty( $IK4 ) ) $IK4 = WModel::get( 'order' );
			if ( !empty( $OA ) )$IK4->select( $OA );
			$IK4->whereE( 'oid', $O9 );

			$IK7 = $IK4->load( 'o' );

			if ( $OB ) $IK7->details = $this->getOrderProducts( $O9 );
			if ( $OC ) $IK7->payment = $this->getOrderPaymentDetails( $O9 );

			return $IK7;
	}


	











	function getOrderProducts($OD,$OE=null,$OF=null,$OG=true) {

			if ( empty($OD) || !is_numeric($OD) || ( !empty($OF) && !is_string($OF) && !is_array($OF) ) ) {
				$IK5 = WMessage::get();
 					$IK5->adminW( 'Please check order products parameters' );

				return false;
			}
			static $IK8 = null;
			if ( empty( $IK8 ) ) $IK8 = WModel::get( 'order.products' );

						if ( !empty( $OF ) ) {
			$IK8->select( $OF );
						if ( ( is_string($OF) && ( 'ordpid' != $OF ) ) ||
				 ( is_array($OF) && ( !in_array('ordpid', $OF ) ) ) ) $IK8->select( 'ordpid' );

			} else $IK8->select( '*' );
			$IK8->makeLJ( 'product', 'pid', 'pid', 0, 1 );
			$IK8->select( 'namekey', 1 );
			$IK8->whereE( 'oid', $OD );
						if ( !empty( $OE ) ) $IK8->whereE( 'pid', $OE );
			$IK9 = $IK8->load( 'ol' );

			$IKA = null;
						if ( $OG && !empty($IK9) ) {
								$IKB = array();
				foreach( $IK9 as $IKC ) {
				$IKB[] = $IKC->ordpid;
				}
								$IKD = $this->getOrderProductOptions( $IKB );

								foreach( $IK9 as $IKC ) {
				if (!empty($IKD[$IKC->ordpid])) $IKC->options = $IKD[$IKC->ordpid];
				$IKA[] = $IKC;
				}

			} else {
					$IKA = $IK9;
			}
			return $IKA;
	}









	function getOrderProductOptions($OH,$OI=null) {

			if ( empty($OH) || (!is_numeric($OH) && !is_array($OH))) {
				$IK5 = WMessage::get();
 					$IK5->adminW( 'Please check order product options parameters' );

				return false;
			}
			static $IKE = array();

						if (is_array($OH)) {

				static $IKF = null;
				if ( empty( $IKF ) ) $IKF = WModel::get( 'order.poptions' );
				$IKF->whereIN( 'ordpid', $OH );
				$IKF->orderBy( 'ordpid' );
				$IKE = $IKF->load( 'ol' );

			$IKG = array();
				$IKH = null;
				foreach( $IKE as $IKI ) {
					if ( !empty($IKI->ordpid) ) {
						if ($IKH != $IKI->ordpid ){
							$IKH = $IKI->ordpid;
							$IKG[$IKH] = array();
							$IKE[$IKH] = array();
						}					$IKG[$IKH][] = $IKI;
												$IKE[$IKH][] = $IKI;
					}				}
			return $IKG;
			}
			if ( !isset( $IKE[$OH] ) || empty( $IKE[ $OH ] ) ){
				static $IKF = null;
				if ( empty( $IKF ) ) $IKF = WModel::get( 'order.poptions' );

								if ( !empty( $OI ) ) $IKF->select( $OI );
				$IKF->whereE( 'ordpid', $OH );
				$IKE[ $OH ] = $IKF->load( 'ol' );
			}
			return $IKE[ $OH ];
	}


	










	function getOrderPaymentDetails($OJ,$OK=null,$OL=false) {

		if ( empty($OJ) || !is_numeric($OJ) ||	!is_bool($OL) ||
			( !empty($OK) && !is_string( $OK ) && !is_array( $OK ) ) ){
			$IK5 = WMessage::get();
			$IK5->adminW( 'Please check order payment parameters' );

			return false;
		}
		static $IKJ = array();
		if ( !isset( $IKJ[$OJ] ) || empty( $IKJ[$OJ] ) || $OL ) {
			$IKK = WModel::get( 'order.payment' );
			if ( !empty($OK) ) $IKK->select( $OK );
			$IKK->whereE( 'oid', $OJ );
			$IKJ[ $OJ ] = $IKK->load( 'o' );
		}
		return $IKJ[ $OJ ];
	}






	function getOrderColValue($OM,$ON='quantity') {
		if(empty($OM)) return '';

		static $IKL = array();
		static $IK4 = null;

		if ( isset($IKL[$OM]->$ON ) ) return $IKL[$OM]->$ON;

		if ( !isset($IK4) ) $IK4 = WModel::get('order.products');
		$IK4->whereE('oid', $OM );

		$IKL[$OM] = $IK4->load('o');

		return ( isset($IKL[$OM]->$ON ) ? $IKL[$OM]->$ON : null );

	}






	function getOrdersColumnValue($OO,$OP='created'){

		if(empty($OO)) return '';

		static $IKL = array();
		static $IK4 = null;

		if(isset($IKL[$OO][$OP])) return $IKL[$OO][$OP];
		if(!isset($IK4)) $IK4 = WModel::get('order');

		$IK4->select($OP);
		$IK4->whereE('oid', $OO);

		$IKL[$OO][$OP] = $IK4->load('lr');

		return $IKL[$OO][$OP];
	}}