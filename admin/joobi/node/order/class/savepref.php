<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;













class Order_Savepref_class extends WClasses {
	
	public function toggleDirectPaymentPref($O1) {
		
		$IKM = WModel::get( 'library.viewmenus', 'object' );
		$IKM->whereE( 'namekey', 'vendors_sales_menu' );
				
		if ( empty($O1) ) {
						$IKM->setVal( 'action', 'vendors-sales' );
		} else {
						$IKM->setVal( 'action', 'vendors-orders' );
		}		
		$IKM->update();

		$IKN = WCache::get();
		$IKN->resetCache( 'Views' );			
		
		
	}
}