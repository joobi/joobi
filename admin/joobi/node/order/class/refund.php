<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;












class Order_Refund_class extends WClasses {
	
	




	public function orderRefundgetCol($O1,$O2){
		if(empty($O1) || empty($O2)) return '';
		
		static $IO6 = array();
		static $IO7 = null;
		
		if(isset($IO6[$O1][$O2])) return $IO6[$O1][$O2];
		if(!isset($IO7)) $IO7 = WModel::get('order.refund');
		
		$IO7->select($O2);
		$IO7->whereE('refundid', $O1);
		$IO6[$O1][$O2] = $IO7->load('lr');
		
		return $IO6[$O1][$O2];
	}	
	




	public function orderRefundUpdateStatus($O3,$O4,$O5=0) {
		if( empty($O3) ) return '';
		
		$IO7 = WModel::get('order.refund');
		$IO7->status = $O4;
		$IO7->whereE( 'refundid', $O3 );
		if(!empty($O5)) $IO7->whereE( 'pid', $O5 );
		$IO7->update();
		
		return true;
	}







	public function getUpdateStatus($O6,$O7=2,$O8=0) {
		$IO8 = WMessage::get();
		
		$IO9 = $this->orderRefundgetCol($O6, 'status');
		$IOA = $this->orderRefundgetCol($O6, 'uid');
		$IOB = $this->orderRefundgetCol($O6, 'vendid');
		
				$IOC = WClass::get( 'vendor.helper' );
		$IOD = $IOC->getVendor( $IOB, 'name' );
		
				if ( !IS_ADMIN && $O7 != 4 ) {
			$IOC = WClass::get('vendor.helper',null,'class',false);				
			$IOE = $IOC->getVendorID();	
			if ( $IOE != $IOB ) return false;
		}		
				if ( $IO9 == 1 ) {
			
						$IOF = $this->orderRefundUpdateStatus($O6, $O7, $O8);
			
			if ( $IOF ) {
				if ( $O7 == 3 ) {
					$IO8->userS('1344385262ASHR');
				
					$IOG = new stdClass;
				 	$IOG->issuedby = 'Administrator';
					$IOG->type = 'Vendor';
					$IOG->refundID = $O6;
					$IOG->user = WUser::get('name', $IOA);
					$IOG->vendor = $IOD;
					$IOH = WPage::linkHome('controller=order-myrefunds'  );
					$IOG->refundLink = '<a href="'.$IOH.'">'.TR1206961903JJIS.'</a>';
					
									 	$IOI = WMail::get();
				 	$IOI->setParameters( $IOG );
				 	$IOI->sendSchedule( $IOA, 'order_refund_request_denied' );
				
				
				} elseif ( $O7 == 4 ) {
										$IOB = $this->orderRefundgetCol( $O6, 'vendid' );
					
										$IOJ = WModel::get( 'vendor' );
					$IOJ->select('uid');
					$IOJ->whereE('vendid', $IOB);
					$IOA = $IOJ->load('lr');				
					$IO8->userS('1344385262ASHS');
					
					$IOG = new stdClass;
				 	$IOG->issuedby = WUser::get('name');	
					$IOG->refundID = $O6;
					$IOG->user = Wuser::get('name');
					$IOH = WPage::linkHome('controller=order-refund&task=show&eid='.$O6 );
					$IOG->refundLink = '<a href="'.$IOH.'">'.TR1206961903JJIS.'</a>';

				 	$IOI = WMail::get();
				 	$IOI->setParameters( $IOG );
				 	$IOI->sendSchedule( $IOA, 'order_request_cancelled');
					
				}else{
					
					$IOG = new stdClass;
				 	$IOG->issuedby = 'Administrator';
					$IOG->type = 'Vendor';
					$IOG->refundID = $O6;
					$IOG->vendor = $IOD;
					$IOG->user = WUser::get('name', $IOA);
					$IOH = WPage::routeURL('controller=order-myrefunds', 'home' );
					$IOG->refundLink = '<a href="'.$IOH.'">'.TR1206961903JJIS.'</a>';
									 	$IOI = WMail::get();
				 	$IOI->setParameters( $IOG );
				 	$IOI->sendSchedule( $IOA, 'order_refund_request_approved' );
					
					$IO8->userS('1310465033JNLS');
					$IO8->userW('1310465033JNLT');				
				}							}		}else{
			$IO8->userN('1310465033JNLU');
		}		
		return true;
		
	}
	





	public function getorderRefundId($O9,$OA=0){
		if(empty($O9)) return '';
		
		$IO7 = null;
		
		if(!isset($IO7)) $IO7 = WModel::get('order.refund');
		
		$IO7->select('refundid');
		$IO7->whereE('orderid', $O9);
		if(!empty($OA)) $IO7->whereE('pid', $OA);
		$IOK = $IO7->load('lr');
		
		return $IOK;
	}
}