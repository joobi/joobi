<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Mail_class extends WClasses {

	private $nSign = "\n\r<br />";

	private $_totalPaid = false;	
	private $_offlineInstructions = '';















	public function sendEmail($O1,$O2,$O3=false,$O4=false,$O5=true) {


		$IKO = WMail::get();



		$IKP = $O1->general->uid;

		$IKQ = $O1->general->storeVendorID;
		$IKR =  WUser::get('object', $IKP );

		$IKS = WClass::get( 'vendor.helper' );
		WPref::load( 'PBASKET_NODE_DIRECTPAY' );
		if ( PBASKET_NODE_DIRECTPAY ) {
			$IKT = $IKS->getVendor( $IKQ );
						if ( empty($IKT) ) $IKS->getVendor();
		} else {
			$IKT = $IKS->getVendor();
		}

		if ( !empty($O1->general->orderID) ) $IKU = $O1->general->orderID;

		else $IKU = $O1->oid;

		$IKV = WPage::linkHome( 'controller=order&task=show&eid='. $IKU );



		$IKW = new stdClass;

		$IKW->orderID = $O1->general->namekey;

		$IKW->ordlink = '<a href="'. $IKV .'">'. $O1->general->namekey .'</a>';

		$IKW->total = WTools::format( $O1->general->totalPrice, 'money', $O1->general->currencyID ); 
		$IKW->currency = $O1->general->currencyCode;

		$IKW->orderDate = WApplication::date( JOOBI_DATE3, $O1->general->orderDate + WUser::timezone($IKP) );

		$IKW->comment = str_replace( array( "\n\r", "\r\n", "\n", "\r"), '<br />', $O1->general->comment );

		if ( $O2 == 30 ) {
			$IKW->status = TR1256629146HYRP;
		} else {
			$IKX = WType::get( 'order.orderstatus' );
			$IKW->status = $IKX->getName( $O2 );
		}
		if ( $O2 == 20 ) {
			$this->_totalPaid = true;
		}

		if ( !empty($IKR->name) ) $IKW->name = $IKR->name;

		else $IKW->name = '';

		if ( !empty($IKR->email) ) $IKW->email = $IKR->email;

		else $IKW->email = '';





		$IKY = $this->l1q_12_3i9( $O1, 'addressShippingID' );

		$IKW->shippingHTML = $IKY->html;

		$IKW->shippingTXT = $IKY->txt;


		$IKZ = $this->l1q_12_3i9( $O1, 'addressBillingID' );
		if ( empty($IKZ) ) $IKZ = $IKY;
		$IKW->billingHTML = $IKZ->html;
		$IKW->billingTXT = $IKZ->txt;

		if ( !empty($O1->general->comment) ) $IKW->userComment = $O1->general->comment;
		else $IKW->userComment = '';

		$IKW->copyright = '';


		
		if ( !empty( $IKT ) ) $IL0 = ( isset( $IKT->name ) ) ? $IKT->name : '';

		else $IL0 = ( defined( JOOBI_SITE_NAME ) ) ? JOOBI_SITE_NAME : '';



		$IKW->vendorName = $IL0;

		if ( file_exists( JOOBI_DS_MEDIA .'images'.DS.'vendors'.DS.'logo.png' ) ) {

			$IKW->vendorHTML ='<img src="'.JOOBI_URL_IMAGES.'vendors/logo.png" border="0" alt="'.$IL0.'" style="padding-right:5px;"/>';

		} else $IKW->vendorHTML = $IL0;


		$IKW->vendorTXT = $IL0;



		$IL1 = $this->l1r_13_2ym( $O1 );

		$IKW->productTXT = $IL1->txt;

	


		$IKW->productHTML = $this->l1s_14_7ia( $O1 );

		$IKW->orderHTML = $this->l1t_15_3dv( $O1 );

		$IL2 = '';
				if ( $O4 ) {
			$IL3 = WClass::get( 'payment.helper',null,'class',false );
			$IL4 = $IL3->getPayment( $O1->general->paymentID );
			$this->_offlineInstructions = $IL4->offlineemail;

			$IL2 = '<div><div style="font-weight:bold;">' . TR1375235461LLKH . ' :</div>';
			$IL2 .= '<div>' . $this->_offlineInstructions . '</div></div>';

		}
		$IKW->paymentInstructions = $IL2;




		$IKW->discount = $this->tableauphp->discount;

		$IKW->totalPrice = $this->tableauphp->total;

		$IKW->shipping = $this->tableauphp->shipping;

		$IKW->finalAmount = $this->tableauphp->final_amount;

		$IKW->payment = $this->tableauphp->payment;

				$this->l1u_16_5ez( $IKW, $O1 );


						$this->l1v_17_6fq( $IKW, $O1 );


		
		if ( $O2 == '10' ) {

			$IKW->customerName = $IKR->name;
			$IL5 = CMSAPIPage::getSpecificItemId( 'basket' );


			$IL6 = WPage::routeURL( 'controller=basket&task=reload&oid='. $IKU, 'home', false, false, $IL5 );



			if ( isset($O1->payment->name) && !empty($O1->payment->name) ) $IL7 = $O1->payment->name;



			$IL8 = $this->getOtherPaymentMethod( $O1->general->paymentID );

			$IL9 = '';

			if ( !empty($IL8) ) {

			 	$IL9 .= '<ol>';

			 	foreach( $IL8 as $ILA ) {

			 		if ( $ILA->name == $IL7 ) $IKW->paymentUsed = '<a href="'. $IL6 .'&payid='. $ILA->payid .'"> here </a>';

			 		else $IL9 .= '<li>Pay with "<a href="'. $IL6 .'&payid='. $ILA->payid .'">'. $ILA->name .'</a>"</li>';

			 	}
			 	$IL9 .= '</ol>';

			}

			$IKW->paymentLink = $IL9;


		}


		$IKO->setParameters( $IKW );



		if ($O3) $IKO->keepAlive(true);




		switch ( $O2 ) {

			case '1':					break;
			case '3':					$ILB = 'invoice_sent';
				if ($O5) $IKO->sendNow( $IKP, $ILB, true );
				break;
			case '5':	
			case '10':	
				if ( $O4 ) {
					$ILB = 'order_offline_pending';
				} else {
					$ILB = 'order_pending';
				}				if ( $O5 ) $IKO->sendNow( $IKP, $ILB, true );
				break;
			case '15': 				$ILB = 'order_change_status';
												$O3 = false;
				if ($O5) $IKO->sendNow( $IKP, $ILB, true );
				break;

			case '20':	
				$ILB = 'order_confirmed';
				if ($O5) $IKO->sendNow( $IKP, $ILB, true );

				break;
			case '25': 												$O3 = false;
				$ILB = 'order_change_status';
				if ($O5) $IKO->sendNow( $IKP, $ILB, true );
				break;
			case '30': 												$O3 = false;
				$ILB = 'order_shipped';
				if ($O5) $IKO->sendNow( $IKP, $ILB, true );
				break;

			case '50':				case '55': 			case '60': 			case '80':					$ILB = 'order_refunded';
				if ($O5) $IKO->sendNow( $IKP, $ILB, true );
				break;

			case '97':				case '98':	
			case '99':	
				$ILB = 'order_canceled';

				if ($O5) $IKO->sendNow( $IKP, $ILB, true );

				break;

			case '110': 			case '115': 				break;


			case '120': 
			case '150':	
				$ILC = WMail::get();

				$ILD = " The following order failed: $IKW->orderID .";

				$ILD .= "\r\n Payment name :  $ILA->name .";

				$ILD .= "\r\n Order ID :  $IKU .";

				$ILE = 'The following order failed';

				$ILF = true;

				break;

			case '220': 			case '230': 				break;


			case '259':	
				return true;

				break;

			default:	
				$ILD = " The following status is not defined.  Please contact Joobi team to report this problem.";

				$ILD .= " Confirmation email : status = $O2   case not defined. \n\r";

				$ILD .= " Order ID :  $IKU   .";

				$ILE = 'Status not defined';

				$ILF = true;


				break;

		}


		
		if ( !empty($ILF) ) {

			$ILC = WMail::get();

			
			if ( !defined( 'PPAYMENT_NODE_LOGERROR_EMAIL' ) ) WPref::get( 'payment.node' );

			$ILG = PPAYMENT_NODE_LOGERROR_EMAIL;

			$ILH = PPAYMENT_NODE_EMAIL_SUPPORT;



			
			if ( empty($ILG) && empty($ILH)) {

				$ILC->sendTextAdmin( $ILE, $ILD, false );

			} else {

				$IKR = new stdClass;

				$IKR->email = $ILG;

				$ILC->sendTextNow( $IKR, $ILE, $ILD, false );

				$IKR->email = $ILH;

				$ILC->sendTextNow( $IKR, $ILE, $ILD, false );

			}


		}


				$ILI = array();
		if ( $O3 && !empty($ILB) ) {
						$ILI[$IKT->vendid] = true;

						
			$ILJ = new stdClass;

			$ILJ->email = $IKT->email;

			$ILJ->name = $IL0;

			$ILK = $ILB.'_admin';
			$IKO->setParameters( $IKW );
			$IKO->sendNow( $ILJ, $ILK, true );


						if ( PBASKET_NODE_DIRECTSHIPPING && $O2 == 20 ) {
								foreach( $O1->details as $ILL ) {
					if ( !isset( $ILI[$ILL->vendid]) ) {
						$IKT = $IKS->getVendor( $ILL->vendid );
						if ( !empty($IKT) ) {
							$ILJ = new stdClass;
							$ILJ->email = $IKT->email;
							$ILJ->name = $IL0;
							$IKO->setParameters( $IKW );
							$IKO->sendNow( $ILJ, $ILK, true );
						}					}				}
			}
						WPref::load( 'PORDER_NODE_COPYSTOREMANAGER' );
			if ( PORDER_NODE_COPYSTOREMANAGER ) {
				$IKT = $IKS->getVendor();
				if ( !isset( $ILI[$IKT->vendid]) ) {
					$ILJ = new stdClass;
					$ILJ->email = $IKT->email;
					$ILJ->name = $IL0;
					$IKO->setParameters( $IKW );
					$IKO->sendNow( $ILJ, $ILK, true );
				}			}
			$IKO->keepAlive( false );


		}

		return true;


	}







	public function getOtherPaymentMethod($O6) {
		static $ILM = array();

		if ( !isset( $ILM[$O6] ) ) {
			static $ILN = null;
			if (empty($ILN)) $ILN = WModel::get( 'payment' );
						$ILN->whereE( 'payid', $O6 );
			$ILO = $ILN->load( 'lr', 'vendid' );

			$ILN->whereE( 'publish', 1 );
			$ILN->whereE( 'vendid', $ILO );
			$ILN->orderBy( 'premium', 'DESC' );
			$ILN->setLimit( 500 );
			$IL8 = $ILN->load( 'ol', array( 'payid', 'name' ) );

			$ILM[$O6] = $IL8;

		}
		return $ILM[$O6];

	}










private function l1q_12_3i9($O7,$O8='addressShippingID') {
	static $ILP=null;

	$ILQ = new stdClass;

	if ( !empty($O8) && !empty($O7->general) && !empty($O7->general->$O8) ) {	
		if ( !isset($ILP) ) $ILP = WClass::get( 'address.helper' );
		$ILQ->html = $ILP->renderAddress( $O7->general->$O8, 'html', true, false, true );
		$ILQ->txt = $ILP->renderAddress( $O7->general->$O8, "\n", true, false, true );





















	} else {

		$ILR = TR1344273942PQIY;

		$ILQ->html .=  $ILR;

		$ILQ->txt .= "\n".$ILR."\n";

	}

	return $ILQ;


}










	  private function l1r_13_2ym($O9) {

	  	$ILQ = new stdClass;

	  	
		$this->tableauphp = null;




		$this->tableauphp->entetes[] = 'Item';

		$this->tableauphp->entetes[] = 'Name';

		$this->tableauphp->entetes[] = 'Quantity';

		$this->tableauphp->entetes[] = 'Unit price';

		$this->tableauphp->entetes[] = 'Price';

		$this->tableauphp->etotal[] = 'Total';

		$this->tableauphp->etotal[] = 'Shipping';

		$this->tableauphp->etotal[] = 'Order Total';

		$this->tableauphp->etotal[] = 'Payment method' . ': ';



		foreach($this->tableauphp->entetes as $ILS) {

			$this->tableauphp->taille[] = strlen($ILS);

		}


		foreach($this->tableauphp->etotal as $ILS) {

			$this->tableauphp->ttaille[] = strlen($ILS);

		}
				


		$ILT = 0;

		$ILU = 0;



		if ( isset($O9->details) && !empty($O9->details) ) {

			foreach( $O9->details as $ILV => $ILW ) {



				$this->tableauphp->produits[$ILV][0] = $ILW->productID;

				$this->sizeUpdate(0,$this->tableauphp->produits[$ILV][0]);



				
				$ILX = null;



				if ( isset($O9->Options) && !empty($O9->Options) ) {

					foreach( $ILW->Options as $ILY ) {

						if ( !empty( $ILY ) ) {

							$ILX .= "<br /><font style=\"font-size:11px;\"> ".$ILY->name." : ".$ILY->value."</font>";

						}
					}
				}


				$this->tableauphp->produits[$ILV][1] = '<b>'.$ILW->productName.'</b> '.$ILX; 
				


				$this->sizeUpdate(1,$this->tableauphp->produits[$ILV][1]);



				$ILZ = 0;



	


				if ( false && !empty($ILW->Options) ) {

					$IM0 = 0;

					$IM1 = 0;

					foreach( $ILW->Options as $IM2) {

						if ($IM2->cumulate == 1) {

							if ($IM2->type == 2) {

								$ILZ = $ILZ +(($ILW->price * $IM2->price)/100);

							} else {

								$ILZ = $ILZ + $IM2->price;

							}
						$IM3 = true;

						
						}elseif ($IM2->cumulate == 2) {

							if ($IM2->type == 2) {

								$IM0 = $IM0 + $IM2->price;

							} else {

								$IM1 = $IM1 + $IM2->price;

							}
						}
					$this->tableauphp->produits[$ILV][2][] = $IM2->description.' : '.$IM2->value;

					$this->sizeUpdate(2,$IM2->description.' : '.$IM2->value);

					}


					$ILZ = $ILZ + ($ILW->price + $IM1);

					$ILZ = $ILZ + (($ILZ*$IM0)/100);



				} else {

					$ILZ = $ILZ + $ILW->price;

				}


				$this->tableauphp->produits[$ILV][3] = $ILW->quantity;

				$this->sizeUpdate(3,$ILW->quantity);



				$IM4 = $ILW->price;

				$this->tableauphp->produits[$ILV][5] = WTools::format( $IM4, 'money', $O9->general->currencyID ); 
				$this->sizeUpdate(5,$this->tableauphp->produits[$ILV][5]);

				if (!empty($ILW->discount)) $IM5 = ( $IM4 * $ILW->quantity) - $ILW->discount;

				else $IM5 = $IM4 * $ILW->quantity;

				$this->tableauphp->produits[$ILV][6] = WTools::format( $IM5, 'money', $O9->general->currencyID ); 
				$this->sizeUpdate(6,$this->tableauphp->produits[$ILV][6]);

				$ILT += $ILW->discount;

				$ILU = $ILU + $IM5;

			}


		}


		
		$this->tableauphp->total = WTools::format( $ILU, 'money', $O9->general->currencyID ); 
		$this->tableauphp->totaltaille['total'] = strlen($this->tableauphp->total);

		$this->tableauphp->shipping = '';

		$this->tableauphp->totaltaille['shipping'] = strlen($this->tableauphp->shipping);

		$this->tableauphp->currency = $O9->general->currencyCode;

	
		if ($ILT > 0) {

			$this->tableauphp->discount = WTools::format( $ILT, 'money', $O9->general->currencyID );

		}else {

			$this->tableauphp->discount = TR1207055519ASZY;

		}


		$this->tableauphp->totaltaille['discount'] = strlen($this->tableauphp->discount);

		$this->tableauphp->final_amount = WTools::format( $O9->general->totalPrice, 'money', $O9->general->currencyID );

		


		$this->tableauphp->totaltaille['final_amount'] = strlen($this->tableauphp->final_amount);


		$this->tableauphp->payment = !empty($O9->payment->name) ? $O9->payment->name : '';

		$this->tableauphp->totaltaille['payment'] = strlen($this->tableauphp->payment);




		$ILQ->html = $this->generateHTMLTable();



		$ILQ->txt = $this->l1w_18_1gx();

		return $ILQ;



	  }














 private function l1w_18_1gx() {

		$ILQ = '';

		
		$ILQ .= $this->newLine($this->tableauphp->entetes);



	  	


		foreach($this->tableauphp->produits as $ILV=>$ILW) {

			$ILQ .= $this->newLine($ILW,true);

		}
		
		$IM6 = max($this->tableauphp->ttaille);

		$IM7 = max($this->tableauphp->totaltaille);

		$this->tableauphp->derniere = $IM7;

		$this->tableauphp->avderniere = $IM6;

		$ILQ .= $this->totalLine($this->tableauphp->etotal[0],$this->tableauphp->total);

		$ILQ .= $this->totalLine($this->tableauphp->etotal[1],$this->tableauphp->shipping);

		$ILQ .= $this->totalLine($this->tableauphp->etotal[2],$this->tableauphp->discount);

		$ILQ .= $this->totalLine($this->tableauphp->etotal[3],$this->tableauphp->final_amount);

		$ILQ .= $this->txtLine();

	return $ILQ;

}










function totalLine($OA,$OB) {

		$IM8 = '';

		$IM8 .= "|";

		$IM9 = 0;

		$IMA = array_sum($this->tableauphp->taille) + count($this->tableauphp->taille);

		$IM6 = $IMA - $this->tableauphp->derniere - $this->tableauphp->avderniere -3;

		$IM7 = $IMA - $this->tableauphp->derniere -2;

		$IMB = false;

		$IMC = false;

		while(strlen($IM8) <  $IMA) {

			if ($IM9 == $IM7 || $IM9 == $IM6) {

				$IM8 .= '|';

				$IM9 = $IM9+1;

			}elseif ($IM7 > $IM9 && $IM9 > $IM6) {

				if ($IMB == false) {

					$IM8 .= $OA;

					$IMB = true;

					$IM9 = $IM9+strlen($OA);

				} else {

					$IM8 .= chr(32);

					$IM9 = $IM9+1;

				}

			}elseif ($IM9 > $IM7) {

				if ($IMC == false) {

					$IM8 .= $OB;

					$IMC = true;

					$IM9 = $IM9+strlen($OB);

				} else {

					$IM8 .= chr(32);

					$IM9 = $IM9+1;

				}

			} else {

	 			$IM8 .= chr(32);

	 			$IM9 = $IM9+1;

			}

	 	}
 	return $IM8."|" . $this->nSign;

}










 function txtLine($OC=false) {

 	$IM8 = '';

	if ($OC == false) {

		$IM8 .= "|";

		while(strlen($IM8) < (array_sum($this->tableauphp->taille) + count($this->tableauphp->taille) )) {

	 		$IM8 .= '-';

	 	}
	} else {

		foreach($this->tableauphp->taille as $IMD) {

			$IM8 .= "|";

			while(strlen($IM8) <  $IMD+1) {

		 		$IM8 .= '-';

		 	}
		}
	}
 	return $IM8."|" . $this->nSign;

 }



 function newLine($OD,$OE=false) {


 	$ILQ = '';

	$ILQ .= $this->txtLine();

	$ILQ .= $this->nSign;

	$IME = 1;

	$IMF = 0;

	$IMG = false;

	foreach($OD as $IMH=>$IMI) {

		$IMJ = '';

		$IMK = '';

		if (!is_array($IMI)) {

			$IMD = $this->tableauphp->taille[$IMH] - strlen($IMI);

			if ($IMD > 0) {

				$IML =round( $IMD/2);

				$IMM = $IMD - $IML;

				$IMK = '';

				while(strlen($IMK) < $IML) {

			 		$IMK .= chr(32);

			 	}
			 	while(strlen($IMJ) < $IMM) {

			 		$IMJ .= chr(32);

			 	}
			}
			$ILQ .= '|'.$IMJ.$IMI.$IMK;

		}else {

			$IML = $this->tableauphp->taille[$IMH] - strlen($IMI[0]);

			if ($IML > 0) {

				while(strlen($IMK) < $IML) {

			 		$IMK .= chr(32);

			 	}
			}

			$ILQ .= '|'.$IMJ.$IMI[0].$IMK;

			$IMF = $IMF+1;

			if (count($IMI) > $IMF) $IMG = payment.triggerstrue;

		}
	}
	if ($IMG) {

		while($IMG == true) {

			$ILQ .= "|".$this->nSign;

			$IM8 = '';

			foreach($this->tableauphp->taille as $IMH => $IMD) {

				$IM8 = '|';

				if ($IMH != 2) {

					while(strlen($IM8) <  $IMD+1) {

				 		$IM8 .= chr(32);

				 	}
				$ILQ .= $IM8;

				}else {

					$ILQ .= $IM8.$OD[2][$IMF];

					$IMF = $IMF+1;

					if (count($OD[2]) == $IMF) $IMG = false;

				}
			}
		}
		$ILQ .= "|";

	}
 	if ($OE) {

 		$IM8 = $this->txtLine();

	 	$ILQ .= $this->nSign . $IM8;

	 	return $ILQ. $this->nSign;

 	} else {

 		return $ILQ."|" . $this->nSign;

 	}

 }











function generateHTMLTable() {


	$ILQ ='';

	$IMN = true;

	foreach($this->tableauphp->produits as $ILV=>$ILW) {

		if (!$IMN) {

			$ILQ .= '</td></tr><tr><td>' ;

		} else {

			$IMN = false;

		}
		$ILQ .=	'<nobr>'.$this->tableauphp->produits[$ILV][0].'</nobr></td>' ;

		$ILQ .=	'<td style=\'background-color:white;text-align:left;\'><nobr>'.$this->tableauphp->produits[$ILV][1].' </nobr></td>' ;

		$ILQ .=	'<td style=\'background-color:white;\'><nobr>'.$this->tableauphp->produits[$ILV][5].'</nobr></td>' ;

		$ILQ .=	'<td style=\'background-color:white;\'><nobr> '.$this->tableauphp->produits[$ILV][3].'</nobr></td>';

		$ILQ .=	'<td style=\'background-color:white;\'><nobr> '.$this->tableauphp->produits[$ILV][6].'</nobr>';


	}
				


	return $ILQ;


}










	function sizeUpdate($OF,$OG) {

		$IMD = strlen($OG);

		if ($IMD > @$this->tableauphp->taille[$OF]) @$this->tableauphp->taille[$OF] = $IMD;

	}












private function l1s_14_7ia($OH) {

	$IMO = '';

	if ( !empty($OH) )

	{

		
		if ( !isset($OH->details) || empty($OH->details) ) return '';






		$IMP = $OH->general;

		$IMQ = ( isset($IMP->currencyID) && ( $IMP->currencyID > 0 ) ) ? $IMP->currencyID : 0;

		$IMR = ( isset($IMP->currencyIDRef) && ( $IMP->currencyIDRef > 0 ) ) ? $IMP->currencyIDRef : $IMQ;



		$IMS = WTools::format( 0, 'money', $IMQ );

		$IMT = WTools::format( 0, 'money', $IMR );



		
		$IMU = 'font-size:11px;color:blue;';	


		$IMV = 1;

		foreach( $OH->details as $ILW ) {

			$IMW = ( isset( $ILW->unitPrice ) ) ? WTools::format( $ILW->unitPrice, 'money', $IMQ ) : $IMS;

			$IMX = ( isset( $ILW->unitPriceRef ) && ( $ILW->unitPriceRef > 0 ) ) ? WTools::format( $ILW->unitPriceRef, 'money', $IMR ) : $IMT;

			$IMY = ( isset( $ILW->discount ) ) ? WTools::format( $ILW->discount, 'money', $IMQ ) : $IMS;

			$IMZ = ( isset( $ILW->discountRef ) && ( $ILW->discountRef > 0 ) ) ? WTools::format( $ILW->discountRef, 'money', $IMR ) : $IMT;

			$IN0 = ( isset( $ILW->price ) ) ? WTools::format( $ILW->price, 'money', $IMQ ) : $IMS;

			$IN1 = ( isset( $ILW->priceRef ) ) ? WTools::format( $ILW->priceRef, 'money', $IMR ) : $IMT;

			$IN2 = ( isset( $ILW->subTotal ) ) ? WTools::format( $ILW->subTotal, 'money', $IMQ ) : $IMS;

			$IN3 = ( isset( $ILW->subTotalRef ) && ( $ILW->subTotalRef > 0 ) ) ? WTools::format( $ILW->subTotalRef, 'money', $IMR ) : $IMT;



			
			$IN4 = '';

			if ( isset( $ILW->Options ) && is_array( $ILW->Options ) ) {

				$IN4 = '';

				foreach( $ILW->Options as $IN5 ) {


					$IN6 = ( isset( $IN5->name ) ) ? $IN5->name : '';

					$IN7 = ( isset( $IN5->value ) ) ? $IN5->value : '';

					$IN8 = ( isset( $IN5->price ) ) ? $IN5->price : 0;



					if ( isset( $IN5->type ) && ( $IN5->type == 2 ) ) {							$IN8 = ( $IN8 == round($IN8,0) ) ? round($IN8,0) : round($IN8,2);

						$IN4 .= $IN6 .' : '. $IN7 .' '. $IN8 .'%';

					} else {



						$IN9 = ( isset( $IN5->priceRef ) && ( $IN5->priceRef != 0 ) ) ? WTools::format( $IN5->priceRef, 'money', $IMR ) : WTools::format( $IN8, 'money', $IMQ );

						if ( $IMR != $IMQ ) $IN4 .= $IN6 .' : '. $IN7 .' <span style="'. $IMU .'">'. $IN9 .'</span>';							else $IN4 .= $IN6 .' : '. $IN7 .' '. $IN9;	
					}
					$IN4 .= '<br />';


				}

			}


			$INA = ( isset( $ILW->productName ) ) ? $ILW->productName : '';

			if ( isset( $ILW->Options ) && is_array( $ILW->Options ) ) $INA .= '<br /><span style="font-size:11px;">'. $IN4 .'</span>';	
			$INB = $IMW;

			if ( $IMQ != $IMR ) $INB .= '<br /><span style="'. $IMU .'">'. $IMX .'</span>';

			$INC = $IN0;

			if ( $IMQ != $IMR ) $INC .= '<br /><span style="'. $IMU .'">'. $IN1 .'</span>';



			
			$IMO .= '<tr>';


			$IMO .= '<td style="background-color:white;">'. $IMV .'</td>';

			$IMO .= '<td>'. $INA .'</td>';

			$IMO .= '<td style="background-color:white;">'. $INB .'</td>';

			$IMO .= '<td>'. (real)$ILW->quantity .'</td>';

			$IMO .= '<td style="background-color:white;">'. $INC .'</td>';

			$IMO .= '</tr>';



			$IMV += 1;

		}



	}


	return $IMO;

}


private function l1u_16_5ez(&$OI,$OJ) {

	if ( !empty($OJ->general->shipID) ) {

		$IND = WClass::get( 'shipping.helper' );
		$INE = $IND->getCarrier( $OJ->general->shipID, 'name' );
		$INF = $IND->getCarrier( $OJ->general->shipID, 'website' );

		if ( !empty($INF) ) {
			$ING = '<a ref="'.$INF.'">' . $INE . '</a>';
		} else {
			$ING = $INE;
		}				$OI->shippingCarrier = TR1344270669PCEB .' '. $ING;
	} else {
		$OI->shippingCarrier = '';
	}

	if ( !empty($OJ->general->trackingNumber) ) {
				$OI->trackingNumber = TR1344268954STDN .' '. $OJ->general->trackingNumber;
	} else {
		$OI->trackingNumber = '';
	}
}



private function l1v_17_6fq(&$OK,$OL) {

	$OK->taxNumber = '';
	$OK->taxInformation = '';
	$OK->vendorContact = '';

	$INH = WClass::get( 'vendor.helper' );
	$INI = $INH->getVendor( $OL->general->storeVendorID );
	if ( !empty($INI) )	{

		if ( !empty($INI->taxnumber) ) {
			$OK->taxNumber = $INI->taxnumber;
			$OK->taxInformation = TR1344348842PMHF . ': ' . $INI->taxnumber;
		}
				$OK->vendorContact = '';
		if ( !empty($INI->originaddress) ) $OK->vendorContact .= $INI->originaddress;
		if ( !empty($INI->originaddress2) ) $OK->vendorContact .= '<br />' . $INI->originaddress2;
		if ( !empty($INI->origincity) ) {
			$OK->vendorContact .= '<br />' . $INI->origincity . ' ';
		}		if ( !empty($INI->originzipcode) ) {
			if ( empty($INI->origincity) ) $OK->vendorContact .= '<br />';
			$OK->vendorContact .= $INI->originzipcode;
		}		if ( !empty($INI->originstate) ) $OK->vendorContact .= '<br />' . $INI->originstate;
		if ( !empty($INI->originctyid) ) {
			$INJ = WClass::get( 'countries.helper' );
			$OK->vendorContact .= '<br />' . $INJ->getData( $INI->originctyid, 'name' );
		}
	}
}










private function l1t_15_3dv($OM) {


	$IMO = '';

	if ( !empty($OM) ) {


		
		if ( !isset($OM->general) || empty($OM->general) ) return '';

		$IMP = $OM->general;

		$IMQ = ( isset($IMP->currencyID) && ( $IMP->currencyID > 0 ) ) ? $IMP->currencyID : 0;

		$IMR = ( isset($IMP->currencyIDRef) && ( $IMP->currencyIDRef > 0 ) ) ? $IMP->currencyIDRef : $IMQ;

		$INK = ( isset($IMP->currencyCode) ) ? $IMP->currencyCode : '';

		$INL = ( isset($IMP->currencyCodeRef) ) ? $IMP->currencyCodeRef : $INK;



		$IMS = WTools::format( 0, 'money', $IMQ );

		$IMT = WTools::format( 0, 'money', $IMR );

		$INM = ( isset($IMP->subTotal) && ( $IMP->subTotal > 0 ) ) ? WTools::format( $IMP->subTotal, 'money', $IMQ ) : $IMS;

		$INN = ( isset($IMP->subTotalRef) && ( $IMP->subTotalRef > 0 ) ) ? WTools::format( $IMP->subTotalRef, 'money', $IMR ) : $IMT;

		$ILT = ( isset($IMP->totalDiscount) && ( $IMP->totalDiscount > 0 ) ) ? '- '. WTools::format( $IMP->totalDiscount, 'money', $IMQ ) : $IMS;

		$INO = ( isset($IMP->totalDiscountRef) && ( $IMP->totalDiscountRef > 0 ) ) ? '- '. WTools::format( $IMP->totalDiscountRef, 'money', $IMR ) : $IMT;

		$INP = ( isset($IMP->totalPrice) && ( $IMP->totalPrice > 0 ) ) ? WTools::format( $IMP->totalPrice, 'money', $IMQ ) : $IMS;

		$INQ = ( isset($IMP->totalPriceRef) && ( $IMP->totalPriceRef > 0 ) ) ? WTools::format( $IMP->totalPriceRef, 'money', $IMR ) : $IMT;

		$IKY = ( isset($IMP->totalShipping) && ( $IMP->totalShipping > 0 ) ) ? '+ '. WTools::format( $IMP->totalShipping, 'money', $IMQ ) : $IMS;

		$INR = ( isset($IMP->totalShippingRef) && ( $IMP->totalShippingRef > 0 ) ) ? '+ '. WTools::format( $IMP->totalShippingRef, 'money', $IMR ) : $IMT;

		$INS = $IMS;

		$INT = ( isset($IMP->totalTax) && ( $IMP->totalTax > 0 ) ) ? '+ '. WTools::format( $IMP->totalTax, 'money', $IMQ ) : $IMS;

		$INU = ( isset($IMP->totalTaxRef) && ( $IMP->totalTaxRef > 0 ) ) ? '+ '. WTools::format( $IMP->totalTaxRef, 'money', $IMR ) : $IMT;

		$INV = ( isset($IMP->totalDue) && ( $IMP->totalDue > 0 ) ) ? WTools::format( $IMP->totalDue, 'money', $IMQ ) : $IMS;

		$INW = ( isset($IMP->totalDueRef) && ( $IMP->totalDueRef > 0 ) ) ? WTools::format( $IMP->totalDueRef, 'money', $IMQ ) : $IMS;

		$INX = ( isset( $IMP->bookingFeeHTML ) && ( !empty( $IMP->bookingFeeHTML ) ) ) ? $IMP->bookingFeeHTML : null;



		
		$IMU = 'style="text-align:left;font-weight:bold;"';

		$INY = 'style="text-align:right;"';

		$INZ = 'style="text-align:right;font-weight:bold;"';



		$IO0 = 'style="text-align:left;font-weight:bold;font-size:11px;"';

		$IO1 = 'style="text-align:right;font-size:11px;color:blue;"';

		$IO2 = 'style="text-align:right;font-weight:bold;font-size:11px;color:blue;"';



		$IMO .= '<table>';



		
		$IMO .= '<tr><td '. $IMU .'>'. TR1251253375NEZJ .'</td><td> : </td><td '. $INY .'>'. $INM .'</td><td '. $INY .'>'. $INK .'</td></tr>';

		if ( $IMQ != $IMR ) $IMO .= '<tr><td> </td><td> </td><td '. $IO1 .'>'. $INN .'</td><td '. $IO1 .'>'. $INL .'</td></tr>';



		if ( !empty($IMP->totalDiscount) && $IMP->totalDiscount > 0 ) {
						$IMO .= '<tr><td '. $IMU .'>'. TR1206961911NYAO .'</td><td> : </td><td '. $INY .'>'. $ILT .'</td><td '. $INY .'>'. $INK .'</td></tr>';
			if ( $IMQ != $IMR ) $IMO .= '<tr><td> </td><td> </td><td '. $IO1 .'>'. $INO .'</td><td '. $IO1 .'>'. $INL .'</td></tr>';
		}
		if ( !empty($IMP->totalShipping) && $IMP->totalShipping > 0 ) {
						$IMO .= '<tr><td '. $IMU .'>'. TR1213180320PQFZ .'</td><td> : </td><td '. $INY .'>'. $IKY .'</td><td '. $INY .'>'. $INK .'</td></tr>';
			if ( $IMQ != $IMR ) $IMO .= '<tr><td> </td><td> </td><td '. $IO1 .'>'. $INR .'</td><td '. $IO1 .'>'. $INL .'</td></tr>';
		}

		if ( !empty($IMP->totalTax) && $IMP->totalTax > 0 ) {
			
			$IMO .= '<tr><td '. $IMU .'>'. TR1206961911NYAQ .'</td><td> : </td><td '. $INY .'>'. $INT .'</td><td '. $INY .'>'. $INK .'</td></tr>';
			if ( $IMQ != $IMR ) $IMO .= '<tr><td> </td><td> </td><td '. $IO1 .'>'. $INU .'</td><td '. $IO1 .'>'. $INL .'</td></tr>';

		}

		



		
		$IMO .= '<tr><td colspan="3" style="text-align:right;"> ------------------ </td></tr>';





		if ( !empty($IMP->totalDue) && $IMP->totalDue > 0 ) {

			
			$IMO .= '<tr><td '. $IMU .'>'. TR1299678632JXEB .'</td><td> : </td><td '. $INZ .'>'. $INV .'</td><td '. $INY .'>'. $INK .'</td></tr>';

			if ( $IMQ != $IMR ) $IMO .= '<tr><td> </td><td> </td><td '. $IO2 .'>'. $INW .'</td><td '. $IO1 .'>'. $INL .'</td></tr>';



			
		


			
			$IMO .= '<tr><td colspan="3" style="text-align:right;"> ------------------ </td></tr>';



			
			$IMO .= '<tr><td '. $IMU .'>'. TR1240888717QTPY . '</td><td> : </td><td '. $INZ .'>'. $INP .'</td><td '. $INY .'>'. $INK .'</td></tr>';

			if ( $IMQ != $IMR ) $IMO .= '<tr><td> </td><td> </td><td '. $IO2 .'>'. $INQ .'</td><td '. $IO1 .'>'. $INL .'</td></tr>';

		} else {

			$IO3 = ( $this->_totalPaid ? TR1240888717QTPY : TR1299678632JXEB );


			
			$IMO .= '<tr><td '. $IMU .'>'. $IO3 .'</td><td> : </td><td '. $INZ .'>'. $INP .'</td><td '. $INY .'>'. $INK .'</td></tr>';

			if ( $IMQ != $IMR ) $IMO .= '<tr><td> </td><td> </td><td '. $IO2 .'>'. $INQ .'</td><td '. $IO1 .'>'. $INL .'</td></tr>';

		}




		$IMO .= '<tr><br/><br/></tr>';
		WPref::load( 'PCURRENCY_NODE_MULTICUR' );
		if ( PCURRENCY_NODE_MULTICUR ) {
			$IMO .= '<tr><td '. $IMU .'>'. TR1329422724BSFL .'</td><td> : </td><td colspan="2" '. $INY .'>'. $IMP->currencyTitle .' ( '. $INK .' )</td></tr>';
			if ( $INL != $INK ) $IMO .= '<tr><td '. $IMU .'>'. TR1344645090QGQN .'</td><td> : </td><td colspan="2" '. $INY .'>'. $IMP->currencyTitleRef .' ( '. $INL .' )</td></tr>';

		} else {
			$IMO .= '<tr><td '. $IMU .'>'. TR1206961948HYTE .'</td><td> : </td><td colspan="2" '. $INY .'>'. $IMP->currencyTitle .' ( '. $INK .' )</td></tr>';
		}


		


		if ( !empty($OM->payment) ) {

			$IO4 = $OM->payment;


			if ( !empty($IO4->name) ) {






				$IO5 = $IO4->name;

			}else $IO5 = $IO4->namekey;



			$IMO .= '<tr><td '. $IMU .'>'. TR1251425145MBYP .'</td><td> : </td>';

			if ( !empty($IO5) ) $IMO .= '<td colspan="2" style="font-weight:bold;text-align:center;"> '. $IO5 .'</td></tr>';

			else $IMO .= '<td colspan="2"> </td></tr>';




		}


		$IMO .= '</table>';

	}

	return $IMO;


}
}