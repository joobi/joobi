<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;













class Order_Instance_object extends WClasses {

	private $_oid = null;
	var $uid = 0;		var $curid = 0;		var $productsA = array();		var $_productKeysA = array(); 	var $orderObj = null;






	public function setBuyer($O1) {
		$this->uid = $O1;
	}

	public function set($O2,$O3) {
		$this->$O2 = $O2;
	}









	public function addProduct($O4) {

		 $IA5 = array();		    $IA6 = array(); 

		if( empty( $O4 ) ) {
			$IA7 = WMessage::get();
			$IA7->codeE( 'An empty productObject parameter passed for function addProduct in object order.instance' );
			return false;
		}
						$IA8 = 'p'. $O4->pid;
		$IA8 .=  '_'. $O4->curid .'-'. $O4->price;

		$this->productsA[ $IA8 ] = $O4;
		$this->_productKeysA[] = $IA8;



		return true;
	}






	public function finish($O5=0) {


				if ( !empty($O5) ) {
			$this->_oid = $O5;
		}
				$IA9 = $this->l1a_M_4yz();
		if( !$IA9 ) {
			$IA7 = WMessage::get();
			$IA7->codeE( 'False return from function _VALIDATEORDER().' );
			return false;
		}
				$IA9 = $this->l1b_N_65c();
		if( !$IA9 ) {
			$IA7 = WMessage::get();
			$IA7->codeE( 'False return from function _SAVEORDER().' );
			return false;
		}
		return $IA9;
	}






	private function l1a_M_4yz() {

				$IAA = ( isset( $this->_productKeysA ) ) ? $this->_productKeysA : null;
		$IA5 = ( isset( $this->productsA ) ) ? $this->productsA : null;


								$IAB = new stdClass;
		$IAB->oid = !empty( $this->_oid ) ? $this->_oid : null;
		$IAB->uid = !empty( $this->uid ) ? $this->uid : 0;
		$IAB->payid = !empty( $this->payid ) ? $this->payid : 0;							$IAB->ostatusid = !empty( $this->orderStatus ) ? $this->orderStatus : 5;			$IAB->curid = !empty( $this->curid ) ? $this->curid : 0;					
				if (!empty( $this->migid )) $IAB->migid = $this->migid ;
		if (!empty( $this->migrefid )) $IAB->migrefid = $this->migrefid ;
		if (!empty( $this->type )) $IAB->type = $this->type ;
		
				$IAC = 0;
		$IAD = 0;


				if( !empty( $IAA ) || !empty( $IA5 ) ) {
			$IAB->Products = array();
			foreach( $IAA as $IAE ) {


				$IAF = $IA5[ $IAE ];

				$IAG = $IAF->baseprice;
				$IAH = $IAF->unitprice;
				$IAI = $IAF->price;
				$IAJ = $IAF->curid;
				$IAK = $IAF->subtotal;

												if( $IAB->curid != $IAJ ) {
					$IAL = $this->l1c_O_t3( $IAF->baseprice, $IAF->curid, $IAB->curid );
					$IAM = $this->l1c_O_t3( $IAF->unitprice, $IAF->curid, $IAB->curid );
					$IAN = $this->l1c_O_t3( $IAF->price, $IAF->curid, $IAB->curid );
					$IAO = $this->l1c_O_t3( $IAF->subtotal, $IAF->curid, $IAB->curid );

					$IAF->baseprice = $IAL;
					$IAF->unitprice = $IAM;
					$IAF->price = $IAN;
					$IAF->curid = $IAB->curid;
					$IAF->subtotal = $IAO;
				}
				$IAF->basepriceref = $IAG;
				$IAF->unitpriceref = $IAH;
				$IAF->priceref = $IAI;
				$IAF->curidref = $IAJ;
				$IAF->subtotalref = $IAK;

	

								if ( !empty($IAF->Options) ) {
					foreach( $IAF->Options as $IAP => $IAQ ) {
						$IAF->Options[$IAP]->priceref = $this->l1c_O_t3( $IAQ->price, $IAF->curid, $IAB->curid );
					}				}


								$IAC += $IAF->subtotal;					$IAD += $IAF->price;

				$IAB->Products[] = $IAF;



			}
		}
						$IAB->subtotal = $IAC;
		$IAB->totalprice = $IAD;
		$IAB->totalpriceet = $IAD;

				if($IAB->migid == 2)
		{
			$IAB->subtotal = $this->totalprice;
		    $IAB->totalprice = $this->totalprice;
		    $IAB->totalpriceet = $this->totalprice;
		    $IAB->created =  $this->created;
		    $IAB->modified =  $this->created;
		    		    $IAB->namekey =  $this->namekey;
		}
        
		$IAB->origtotalprice = $IAD;				$IAB->origtotalpriceet = $IAD;				$IAB->publish = 1;

				if(!count($IA5) == 0)
				$IAB->productnum = count($IA5);


		$this->orderObj = $IAB;



		return true;
	}





	























































	private function l1b_N_65c() {
		$IAB = ( isset( $this->orderObj ) ) ? $this->orderObj : null;
		if( empty( $IAB ) ) return false;
		if( empty($this->_oid) ) $this->l1d_P_2nz(); 		else if( !empty( $IAB->Products ) ) $this->l1e_Q_3rd(); 
		$IAR = WClass::get('order.helper',null,'class',false);
		$IAR->storeToHistory( $this->_oid, $IAB->ostatusid, 0, $IAB->payid );

		return $this->_oid;

	}
	



	private function l1d_P_2nz() {

		$IAB = ( isset( $this->orderObj ) ) ? $this->orderObj : null;
		if( empty( $IAB ) ) return false;

						static $IAS = null;
		if( empty( $IAS ) ) $IAS = WModel::get( 'order' );

				$IAT = (array)$IAB;
		foreach( $IAT as $IAU => $IAV ) {
			$IAS->$IAU = $IAV;
					}


				$IAS->returnId( true );
		$IAS->save();

				if ( !empty($IAS->oid) ) $this->_oid = $IAS->oid;

	}
	



	private function l1e_Q_3rd() {
		if( empty( $this->_oid ) ) return false;


		
		$IAB = ( isset( $this->orderObj ) ) ? $this->orderObj : null;
		if( empty( $IAB ) ) return false;

								$IAW = array();
		$IAX = array();
		$IAY = ( is_array( $IAB->Products ) ) ? $IAB->Products : (array)$IAB->Products;
		$IAZ = false;

		static $IB0 = null;
		if( empty( $IB0 ) ) $IB0 = WModel::get( 'order.products' );

		static $IB1 = null;
		if( empty( $IB1 ) ) $IB1 = WModel::get( 'order.poptions' );

		foreach( $IAY as $IAE => $IAF ) {

			$IB0->ordpid = null;

			$IB2 = new stdClass;
			$IB2->oid = $this->_oid;

			$IB3 = (array)$IAF;
			$IB3['oid'] = $this->_oid;

			foreach( $IB3 as $IB4 => $IB5 ) {
					if( !$IAZ ) $IAW[] = $IB4;
					if ( $IB4 != 'Poptions' ) $IB2->$IB4 = $IB5;

					if ( $IB4 != 'Poptions' ) $IB0->$IB4 = $IB5;

			}
			$IAX[] = $IB2;
			$IAZ = true;

			$IB0->setIgnore();
			$IB0->returnId();

			if ( !empty($IAF->Poptions) ) {

				foreach( $IAF->Poptions as $IAQ ) {
					if ( empty( $IAQ ) ) continue;
										$IB1->opid = null;
					$IB1->ordpid = $IB0->ordpid;
					foreach( $IAQ as $IB6 => $IB7 ) {
						$IB1->$IB6 = $IB7;
					}
					$IB1->setIgnore();
				}
			}

		}


		

		$IB0->save();

		return true;
	}

	






	private function l1c_O_t3($O6,$O7=1,$O8=1) {

		static $IB8 = array();

		$IAE = $O7 .'-'. $O8 .'_'. $O6;
		if( !isset( $IB8[$IAE] ) ) {
			static $IB9 = null;
			if( empty( $IB9 ) ) $IB9 = WClass::get( 'currency.convert', null, 'class', false );
			if( $IB9 ) {
				$IBA = null;
				$IBA->currencyFrom = $O7;						$IBA->currencyTo = $O8;							$IBA->amount = $O6;				
				$IB8[$IAE] = $IB9->convertTo( $IBA );
			}		}
		return ( isset( $IB8[$IAE] ) ) ? $IB8[$IAE] : 0;
	}

}