<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;













class Order_Product_object extends WClasses {

	var $price = 0;
	var $curid = 0;
	var $quantity = 1;

	private $_tableObj = null; 	private $_productObj = null;







	function complete() {

				if ( !empty( $this->_tableObj ) ) {

				$I9R = !empty( $this->quantity ) ? $this->quantity : 1;

				
					if ( !empty($this->_tableObj->Poptions) ) {

						foreach( $this->_tableObj->Poptions as $I9S ) {
							$this->price = $this->price + ($I9R * $I9S->price);
						}
					}
					$I9T = $this->_tableObj;


								$I9U = new stdClass;
				foreach( $I9T as $I9V => $I9W ) {
					$I9U->$I9V = !empty( $this->$I9V ) ? $this->$I9V : $I9W;
				}
								$I9U->quantity = !empty( $this->quantity ) ? $this->quantity : 1;
				$I9U->subtotal = $I9U->price;
				$I9U->unitprice = ( $I9U->price / $I9U->quantity );
				$I9U->baseprice = $I9U->unitprice;

				$this->_productObj = $I9U;

		} else {
			$I9X = WMessage::get();
			$I9X->codeE( 'Failed to get from property _TABLEOBJ in function COMPLETE().' );
		}
		return (!empty( $this->_productObj ) ? $this->_productObj : false);

	}







	public function setAuctionFee($O1) {

				$I9Y = WPref::load( 'PAUCTION_NODE_ENABLEFEE' );

		if ( $I9Y && !empty($this->_tableObj) ) {

			if ( WExtension::exist( 'subscription.node' ) ) {

				$I9Z = WObject::get( 'subscription.check' );
				$I9Z->restriction( 'auction_fee' );

				if ( !$I9Z->restrictionExist() ) {
					$IA0 = false;
				} else {
										if ( $I9Z->getStatus( false ) ) {
						return true;
					}				}			}

			$I9T = $this->_tableObj;

			$IA1 = WClass::get( 'auction.fee' );
			$IA1->setAuctionPrice( $I9T->price, $I9T->curid );

			$IA2 = new stdClass;
			$IA2->pid = $I9T->pid;
			$IA2->type = 1;				$IA2->optnamekey = 'auction_fee';
			$IA2->optname = TR1369750762MHAH;
			$IA2->groupkey = 'fee';
			$IA2->optvaluename = TR1369750762MHAI;				$IA2->price = $IA1->getFee();
			$IA2->cumulative = 1;

			$I9T->Poptions = array();
			$I9T->Poptions[] = $IA2;

			$this->_tableObj = $I9T;

		}
		return true;

	}









	function get($O2) {
		static $IA3 = array();

				if( empty( $O2 ) ) {
			$I9X = WMessage::get();
			$I9X->code( 'Empty item Product ID parameter set in function get().' );
			return false;
		}
						if( !isset( $IA3[ $O2 ] ) ) {

			static $IA4 = null;
			if( empty( $IA4 ) ) $IA4 = WModel::get( 'product' );
			$IA4->makeLJ( 'producttrans', 'pid' );
			$IA4->makeLJ( 'product.price', 'priceid' );
			$IA4->makeLJ( 'product.type', 'prodtypid' );
			$IA4->whereLanguage( 1 );
			$IA4->select( array( 'pid', 'price', 'curid', 'prodtypid', 'priceid', 'vendid', 'weight', 'unitid' ) );
			$IA4->select( 'name', 1 );
			$IA4->select( 'type', 2, 'pricetype' );
			$IA4->select( 'type', 3 );
			$IA4->whereE( 'pid', $O2 );
			$IA4->setLimit( 1 );
			$IA3[ $O2 ] = $IA4->load( 'o' );

		}
		$this->_tableObj = !empty( $IA3[ $O2 ] ) ? $IA3[ $O2 ] : false;
		return true;

	}
}