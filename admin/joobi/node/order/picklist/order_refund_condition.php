<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Order_Order_refund_condition_picklist extends WPicklist {
	

	function create() {

	

		$this->addElement( 0, TR1338224678JOTP );

		$this->addElement( 1, TR1338224678JOTQ );

		$this->addElement( 2, TR1338224678JOTR );

		$this->addElement( 3, TR1338224678JOTS );

		$this->addElement( 4, TR1338224678JOTT );

		$this->addElement( 5, TR1338224678JOTU );

		

		return true;

	

	}}