<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

 defined('_JEXEC') or die;











class Order_Formcurrency_picklist extends WPicklist {








function create(){

	static $IJT = null;

	static $IJU = null;

	if( !isset($IJT) ) $IJT = WModel::get( 'currency' );

	$IJT->whereE( 'accepted', 1 );

    $IJT->whereE( 'publish', 1 );

	$IJT->orderBy( 'ordering' );
	$IJT->setLimit( 300 );

	$IJV = $IJT->load( 'ol', array( 'curid', 'title', 'code', 'symbol') );



	if( !empty($IJV) ) {

		foreach( $IJV as $IJW ) {

			$this->addElement( $IJW->curid, '('. $IJW->symbol .') '. $IJW->title );

		}
	}
	return true;

}}