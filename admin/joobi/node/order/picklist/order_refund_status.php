<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Order_Order_refund_status_picklist extends WPicklist {
	function create() {


		if ( $this->onlyOneValue() ) {
			if ($this->defaultValue == 1 ) $this->addElement( 1, TR1206732372QTKP );
			if ($this->defaultValue == 2 ) $this->addElement( 2, TR1246518570RHDZ );
			if ($this->defaultValue == 3 ) $this->addElement( 3, TR1338493441EEFF );
			if ($this->defaultValue == 4 ) $this->addElement( 4, TR1304526544DLEW );
			return true;
		}
		$this->addElement( 0, TR1251298684SHUD );
		$this->addElement( 1, TR1206732372QTKP );
		$this->addElement( 2, TR1246518570RHDZ );
		$this->addElement( 3, TR1338493441EEFF );
		$this->addElement( 4, TR1304526544DLEW );


		return true;


	}

}