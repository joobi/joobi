<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Order_Paymenttype_picklist extends WPicklist {
function create() {



$this->addElement( '0', TR1206732405TIXC );

$this->addElement( '1', TR1344448391FDMC );

$this->addElement( '2', TR1344448391FDMD );



return true;

}}