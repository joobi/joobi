<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Order_Orderstatus_picklist extends WPicklist {














	function create() {


		if ( $this->onlyOneValue() ) {



			if ($this->defaultValue == 1 ) $this->addElement( 1, TR1338493444QPAH ) ;

			if ($this->defaultValue == 3 ) $this->addElement( 3, TR1338493444QPAI ) ;

			if ($this->defaultValue == 5 ) $this->addElement( 5, TR1338493444QPAJ ) ;

			if ($this->defaultValue == 10 ) $this->addElement( 10, TR1206732372QTKP ) ;

			if ($this->defaultValue == 15 ) $this->addElement( 15, TR1338493444QPAK ) ;

			if ($this->defaultValue == 20 ) $this->addElement( 20, TR1206732376KOVN ) ;

			if ($this->defaultValue == 25 ) $this->addElement( 25, TR1206961923SUFO ) ;


			if ($this->defaultValue == 50 ) $this->addElement( 50, TR1240893783FCXE ) ;

			if ($this->defaultValue == 55 ) $this->addElement( 55, TR1338493444QPAN ) ;

			if ($this->defaultValue == 60 ) $this->addElement( 60, TR1206961905BHAW ) ;

			if ($this->defaultValue == 80 ) $this->addElement( 80, TR1338493444QPAO ) ;

			if ($this->defaultValue == 99 ) $this->addElement( 99, TR1304526544DLEW ) ;

			if ($this->defaultValue == 98 ) $this->addElement( 98, TR1338493444QPAQ ) ;

			if ($this->defaultValue == 97 ) $this->addElement( 97, TR1338493444QPAR ) ;

			if ($this->defaultValue == 110 ) $this->addElement( 110, TR1338493444QPAS ) ;

			if ($this->defaultValue == 115 ) $this->addElement( 115, TR1235461988ERWP ) ;

			if ($this->defaultValue == 120 ) $this->addElement( 120, TR1338493441EEFF ) ;

			if ($this->defaultValue == 150 ) $this->addElement( 150, TR1242282449PIPX ) ;

			if ($this->defaultValue == 220 ) $this->addElement( 220, TR1338493444QPAV ) ;

			if ($this->defaultValue == 230 ) $this->addElement( 230, TR1338493444QPAW ) ;



			return true;



		}




		$this->addElement( 0, TR1251298684SHUD ) ;

		$this->addElement( -1, TR1338493444QPAG ) ;

		$this->addElement( 1, TR1338493444QPAH ) ;

		$this->addElement( 3, TR1338493444QPAI ) ;

		$this->addElement( 5, TR1338493444QPAJ ) ;

		$this->addElement( 10, TR1206732372QTKP ) ;

		$this->addElement( 15, TR1338493444QPAK ) ;

		$this->addElement( -2, TR1338493444QPAL ) ;

		$this->addElement( 20, TR1206732376KOVN ) ;

		$this->addElement( 25, TR1206961923SUFO ) ;


		$this->addElement( -3, TR1338493444QPAM ) ;

		$this->addElement( 50, TR1240893783FCXE ) ;

		$this->addElement( 55, TR1338493444QPAN ) ;

		$this->addElement( 60, TR1206961905BHAW ) ;

		$this->addElement( 80, TR1338493444QPAO ) ;

		$this->addElement( -4, TR1338493444QPAP ) ;

		$this->addElement( 99, TR1304526544DLEW ) ;

		$this->addElement( 98, TR1338493444QPAQ ) ;

		$this->addElement( 97, TR1338493444QPAR ) ;

		$this->addElement( 110, TR1338493444QPAS ) ;

		$this->addElement( 115, TR1235461988ERWP ) ;

		$this->addElement( -5, TR1338493444QPAT ) ;

		$this->addElement( 120, TR1338493441EEFF ) ;

		$this->addElement( 150, TR1242282449PIPX ) ;

		$this->addElement( -6, TR1338493444QPAU ) ;

		$this->addElement( 220, TR1338493444QPAV ) ;

		$this->addElement( 230, TR1338493444QPAW ) ;



		return true;



	}

}