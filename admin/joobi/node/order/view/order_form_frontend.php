<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Order_form_frontend_view extends Output_Forms_class {
protected function prepareView() {



	$I9P = $this->getValue( 'coupid' );



	
	if ( empty( $I9P ) ) $this->removeElements( array( 'ordercoupontabfe', 'ordercouponnamefe', 'ordercoupondescfe', 'ordercouponvaluefe' ) );

	

	$I9Q = WPref::load( 'PORDER_NODE_ALLOWRETURN' );

	if ( !$I9Q ) $this->removeMenus( 'orders_form_frontend_request_refund' );

	

	

	return true;

}}