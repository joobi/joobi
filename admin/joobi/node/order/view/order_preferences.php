<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Order_preferences_view extends Output_Forms_class {
protected function prepareView() {



		
		$I9M = WExtension::exist( 'vendors.node' );

		if ( !$I9M ) $this->removeElements( array( 'order_preferences_vendors_directpay', 'order_preferences_basket_splitpayprimary', 'order_preferences_vendors_directshipping', 'order_preferences_order_node_copystoremanager' ) );



		$I9N = WExtension::exist( 'affiliate.node' );

		if ( !$I9N || WGlobals::checkCandy(50,true) ) $this->removeElements( array( 'order_preferences_basket_splitpay', 'order_preferences_basket_splitpayprimary' ) );





		if ( !$I9M && !$I9N ) $this->removeElements( array( 'order_preferences_basket_splitpay' ) );





		WPref::load( 'PBASKET_NODE_DIRECTPAY' );

		if ( PBASKET_NODE_DIRECTPAY ) {

			 $this->removeElements( array( 'order_preferences_basket_splitpay', 'order_preferences_basket_splitpayprimary' ) );

		}


		$I9O = WPref::load( 'PCATALOG_NODE_USETAX' );

		if ( !$I9O ) {	
			$this->removeElements( array( 'order_preferences_vendors_vendortax' ) );

		}


		return true;



	}}