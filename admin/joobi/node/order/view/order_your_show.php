<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Order_your_show_view extends Output_Forms_class {
function prepareView() {



	$I9L = WPref::load( 'PORDER_NODE_ALLOWRETURN' );

	if ( !$I9L ) $this->removeMenus( 'orders_form_frontend_request_refund' );



	return true;

}}