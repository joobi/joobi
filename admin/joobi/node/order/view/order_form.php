<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Order_form_view extends Output_Forms_class {
protected function prepareView() {



	$I9I = $this->getValue( 'coupid' );

	$I9J = $this->getValue( 'shipid' );



	
	if ( empty( $I9I ) ) $this->removeElements( array( 'ordercoupontab', 'ordercouponcode', 'ordercouponname', 'ordercoupondesc', 'ordercouponvalue' ) );

	

	if ( empty( $I9J ) ) {

		$I9K = array( 'orders_form_shipping_address', 'orders_form_shipping_carrier', 'orderbeshipfldset', 'orderbeshiptrckno', 'orderbeshipcrier', 'orderbeshiprtenme', 'orderbeshippkgfee', 'orderbeshipshipfee',

			 'orderbeshipcrncy', 'orderbeshiphidpid', 'fpftyz1sm' );

		$this->removeElements( $I9K );

	} 
	

	return true;

	

}}