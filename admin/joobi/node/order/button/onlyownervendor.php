<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Onlyownervendor_button extends WButtons_external {
function create() {


		$IW4 = WClass::get('vendor.helper',null,'class',false);				
	$IW5 = $IW4->getVendorID();		
	
	if ( empty($IW5) ) return false;
	
	$IW6 = $this->getValue( 'vendid', 'order.refund' );
	
	if ( $IW5 != $IW6 ) return false;
	
	return true;



}}