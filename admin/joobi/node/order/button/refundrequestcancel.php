<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Refundrequestcancel_button extends WButtons_external {
function create() {
	
	$IVZ = WGlobals::get('eid');
	
	$IW0 = WClass::get( 'order.refund' );
	$IW1 = $IW0->getorderRefundId( $IVZ );
	$IW2 = $IW0->orderRefundgetCol( $IW1, 'status' );

	$IW3 = $this->getValue( 'ostatusid');
	if ( $IW3 != 20 ) return false;

	if ( !empty($IW1) && $IW2 == 1 ) {
				$this->setIcon( 'cancel' );
		$this->setTitle( TR1310465041HAEP );
		$this->setAddress( 'controller=order-refund&task=cancelrequest&orderid='.$IVZ );
		return true;
	}	
	if (empty($IW1)) {
		$this->setAddress( 'controller=order-refund&task=request&orderid='.$IVZ );
		return true;
	}	
	if (!empty($IW1) && $IW2 > 1) return false;

	return true;
	
}}