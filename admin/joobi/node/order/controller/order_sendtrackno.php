<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_sendtrackno_controller extends WController {
function create() {



	
	$IUN = WGlobals::get( 'trucs' );

	$IUO = WModel::getID( 'order' );

	$IUP = ( !empty( $IUN[$IUO][ 'oid' ] ) ) ? $IUN[$IUO][ 'oid' ] : 0;
	$IUQ = ( !empty( $IUN[$IUO][ 'trackno' ] ) ) ? $IUN[$IUO][ 'trackno' ] : '';
	
	if ( empty($IUQ) ) {
		$IUR = WMessage::get();
		$IUR->historyE('1342235472NFCF');
	}	

	

	
	if ( !empty( $IUP ) ) {
		

		
		$IUS = WModel::get('order');

		$IUS->whereE( 'oid', $IUP );

		$IUT =$IUS->load( 'o', array( 'trackno', 'shipid', 'uid', 'namekey', 'vendid' ) );

	
		if ( !IS_ADMIN ) {
						$IUU = WRole::get();
			$IUV = $IUU->hasRole( 'storemanager' );
			$IUW = Wuser::get( 'uid' );
			if ( !$IUV ) {
				$IUX = $IUU->hasRole( 'vendor' );
				$IUY = WClass::get('vendor.helper',null,'class',false);				
				$this->vendid = $IUY->getVendorID( $IUW );	
				
				if ( $this->vendid != $IUT->vendid ) {
					$IUR = WMessage::get();
					$IUR->exitNow( 'Not allow to do that.' );
				}				
			}		}		
		

		$IUR = WMessage::get();

		if ( !empty( $IUT->shipid ) ) {		


					$IUS->whereE( 'oid', $IUP );
			$IUS->setVal( 'trackno', $IUQ );
			$IUS->update();
			$IUR->userS('1342235472NFCG');
			

			
			$IUZ = new stdClass;
			$IV0 = WClass::get( 'shipping.helper' );

			$IUZ->carrier = $IV0->getCarrier( $IUT->shipid, 'name' );

			$IUZ->orderID = $IUT->namekey;

			$IUZ->customer = WUser::get( 'name', $IUT->uid );

			$IUZ->trackno = $IUQ;

			$IUZ->sitename = '<a href="'.  JOOBI_SITE .'">'. JOOBI_SITE_NAME .'</a>';

	

			
			$IV1 = WMail::get();

			$IV1->setParameters( $IUZ );

			$IV1->sendNow( $IUT->uid, 'shipping_tracking_number' );

			
		} else {
			
			$IUR->historyE('1342235472NFCH');
		}
		
		

	} else {

		$IUR = WMessage::get();

		$IUR->adminW( 'Problem of retrieving order id in controller order_sendtrackno.' );

	}


	WPage::redirect( 'controller=order&task=edit&eid='. $IUP );

	return true;

}}