<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_edit_controller extends WController {


function edit() {



	$IST = WClass::get( 'order.checkaccess' );

	$IST->hasOrderAccess();

	

	parent::edit();

	

	
	
	$ISU = WGlobals::get( 'option' );

	if ( !empty($ISU) && ( $ISU == 'com_jsubscription' ) ) $this->l1x_19_1u5( '0' );

	else {

		$ISV = WGlobals::getEID();

		$ISW = $this->l1y_1A_3mp( $ISV );

	

		if ( $ISW ) $this->l1x_19_1u5( '1' );

		else $this->l1x_19_1u5( '0' );

	}




	return true;

}












function l1y_1A_3mp($O1) {

	static $ISX = null;

	if ( empty($ISX) ) $ISX = WModel::get( 'order' );

	$ISX->whereE( 'oid', $O1 );

	$ISY = $ISX->load( 'lr', 'shipid' );

	

	$ISZ = ( !empty($ISY) ) ? true : false;	

	return $ISZ;

}












function l1x_19_1u5($O2) {

	WView::updateElement( 'orderbeshipfldset', 'form', $O2, 'publish' );

	WView::updateElement( 'orderbeshiptrckno', 'form', $O2, 'publish' );

	WView::updateElement( 'orderbeshipcrier', 'form', $O2, 'publish' );

	WView::updateElement( 'orderbeshiprtenme', 'form', $O2, 'publish' );

	WView::updateElement( 'orderbeshippkgfee', 'form', $O2, 'publish' );

	WView::updateElement( 'orderbeshipshipfee', 'form', $O2, 'publish' );

	WView::updateElement( 'orderbeshipcrncy', 'form', $O2, 'publish' );

	WView::updateElement( 'orderbeshipsend', 'form', $O2, 'publish' );

	WView::updateElement( 'orderbeshiphidpid', 'form', $O2, 'publish' );

	WView::updateElement( 'fpftyz1sm', 'menu', $O2, 'publish' );

	

	return true;

}}