<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_create_infos_controller extends WController {
function infos() {



	
	$IVP = WGlobals::get( 'trucs', array(), null, 'array' );

	$IVQ = !empty( $IVP['x']['email'] ) ? $IVP['x']['email'] : '';

	$IVR = !empty( $IVP['x']['name'] ) ? $IVP['x']['name'] : '';

	if (empty($IVR)) $IVR = $IVQ;



	if (!empty($IVQ)) {

		$IVS = WUser::credential();

		$IVT = $IVS->ghostAccount( $IVQ, $IVR );

	} else {

		$IVU = WController::getFormValue( 'uid', 'users', null, true );
		$IVT = current($IVU);


	}


	if (empty($IVT)) {

		$IVV = WMessage::get();

		$IVV->userN('1308543350DBTR');

		WPage::redirect('controller=order-create');

		return false;

	}




	if ( !defined('CURRENCY_USED') ) {
		$IVW = WClass::get('currency.format',null,'class',false);
		$IVW->set();
	}

	
	$IVX = WObject::get( 'order.instance' );


	$IVX->setBuyer( $IVT );
	$IVX->orderStatus = 1;
	$IVX->curid = CURRENCY_USED;

	$IVY = $IVX->finish();



	WPage::redirect('controller=order&task=showinfo&eid='.$IVY);



	return true;

}}