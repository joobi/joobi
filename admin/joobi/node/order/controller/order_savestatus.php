<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_savestatus_controller extends WController {




function savestatus() {


	if ( !IS_ADMIN ) {

		$IT0 = WClass::get( 'order.checkaccess' );

		$IT0->hasOrderAccess();

	}


	$this->model = WModel::get('order');

	$this->model->getFormData();



	$IT1 = $this->sid;

	if ( empty($this->model->$IT1) ) return true;

	$IT2 = $this->model->$IT1;



	if ( isset( $this->model->oid ) ) $IT3 = !empty( $this->model->oid ) ? $this->model->oid : 0;

	elseif ( isset( $IT2[ 'oid' ] ) ) $IT3 = !empty( $IT2[ 'oid' ] ) ? $IT2[ 'oid' ] : 0;

	else $IT3 = 0;



	if ( isset( $this->model->ostatusid ) ) $IT4 = !empty($this->model->ostatusid) ? $this->model->ostatusid : 5;

	elseif ( isset( $IT2[ 'ostatusid' ] ) ) $IT4 = !empty( $IT2[ 'ostatusid' ] ) ? $IT2[ 'ostatusid' ] : 5;

	else $IT4 = 5;



	if ( isset( $this->model->x['notify'] ) ) $IT5 = $this->model->x['notify'];

	elseif ( isset( $IT2[ 'x' ][ 'notify' ] ) ) $IT5 = $IT2[ 'x' ][ 'notify' ];

	else $IT5 = 1;



	if ( isset( $this->model->x['reason'] ) ) $IT6 = $this->model->x['reason'];

	elseif ( isset( $IT2[ 'x' ][ 'reason' ] ) ) $IT6 = $IT2[ 'x' ][ 'reason' ];

	else $IT6 ='';



	$IT7 = WModel::get( 'order' );

	$IT7->whereE( 'oid', $IT3 );

	$IT8 = $IT7->load( 'lr', 'ostatusid' );



	$IT9 = WClass::get('payment.triggers',null,'class',false);

	$ITA = $IT9->allowedStatusChange( $IT4, $IT8 );



	if ( !$ITA ) {

		$ITB = WMessage::get();

		$ITB->userW('1242199264RCSO');

		return true;

	}




	
	
	$ITC = $IT9->excecuteTrigger( $IT3, $IT4, '', (bool)$IT5, $IT6 );



	$ITB = WMessage::get();

	$ITD = WType::get( 'order.status' );

	$ITE = '' . $ITD->getName( $IT4 );

	$ITB->userS('1260242462IOXE',array('$ORDERSTATUS'=>$ITE));



	
	$IT7 = WModel::get( 'order' );

	$IT7->setVal( 'ostatusid', $IT4 );

	$IT7->whereE( 'oid', $IT3 );

	$ITF = $IT7->update();



	$ITG = WGlobals::get( 'is_popup', false, 'joobi' );


	if ( $ITG ) WPage::redirect( 'previous' );

	else {
		if ( IS_ADMIN ) WPage::redirect( 'controller=order' );
		else WPage::redirect( 'controller=vendors-orders' );
	}


	return true;



}}