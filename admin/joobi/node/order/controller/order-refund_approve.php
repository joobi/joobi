<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_refund_approve_controller extends WController {
function approve() {

	$IRE = WClass::get('order.refund');

	

	$IRF = WModel::get('order.refund','sid');

	$IRG = WGlobals::get('refundid_'.$IRF);

	$IRH = WGlobals::get('eid');

	

	if ( !IS_ADMIN ) {

		$IRI = WGlobals::get('trucs');

		$IRJ = $IRI['s']['mid'];

		$IRH = $IRI[$IRJ]['refundid'];

	}

	

	if ( !empty($IRH) ) {

		$IRE->getUpdateStatus( $IRH, 2 );

		$this->setView('order_refund_show');

	} else {

		if (!empty($IRG)) {

			foreach($IRG as $IRK) {

				$IRE->getUpdateStatus( $IRK, 2 );

			}
		}
	}


	return true;

}}