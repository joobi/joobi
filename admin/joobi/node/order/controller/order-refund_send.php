<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_refund_send_controller extends WController {
function send() {

	$IQP = WGlobals::getEID();

	$IQQ = WGlobals::get('controller');

	$IQR = WModel::get( 'order.refund' );

	$IQR->select( 'orderid' );

	$IQR->select( 'uid' );

	$IQR->whereE( 'refundid', $IQP );

	$IQS = $IQR->load('o');



	$IQT = $IQS->uid;

	$IQU = $IQS->orderid;



	if (!empty($IQS)) {

		$IQT = $IQS->uid;

		
		WPage::redirect( 'controller='.$IQQ.'&task=email&uid='. $IQT .'&orderid='. $IQU );

	} else {



	}
	return true;

}}