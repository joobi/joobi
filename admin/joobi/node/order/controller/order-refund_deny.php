<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_refund_deny_controller extends WController {
function deny() {

	$IPZ = WClass::get('order.refund');

	

	$IQ0 = WModel::get('order.refund','sid');

	$IQ1 = WGlobals::get('refundid_'.$IQ0);

	$IQ2 = WGlobals::get('eid');

	

	if (!IS_ADMIN) {

		$IQ3 = WGlobals::get('trucs');

		$IQ4 = $IQ3['s']['mid'];

		$IQ2 = $IQ3[$IQ4]['refundid'];

	}

	

	if (!empty($IQ2)) {

		$IPZ->getUpdateStatus($IQ2, 3);

		$this->setView('order_refund_show');

	} else {

		if (!empty($IQ1)) {

			foreach($IQ1 as $IQ5) {

				$IPZ->getUpdateStatus($IQ5, 3);

			}
		}
	}


	return true;

}}