<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_products_addtoorderproducts_controller extends WController {

function addtoorderproducts() {



	
	$ITW = WGlobals::get('oid');

	$ITX = WGlobals::get('pid');


	$ITY = WObject::get( 'order.instance' );



	
	$ITZ = WObject::get( 'order.product' );

	$ITZ->get( $ITX );



	$IU0 = $ITZ->complete();



	if ( !defined('CURRENCY_USED') ) {

		$IU1 = WClass::get( 'currency.format' );

		$IU1->set();

	}

	
	$ITY->curid = CURRENCY_USED ;

	$ITY->curiddisp = CURRENCY_USED;



	$ITY->addProduct( $IU0 );

	$IU2 = $ITY->finish( $ITW );


	if ( $IU2 ) {

		
		$IU3 = WModel::get('order');

		$IU3->updatePlus('totalprice', $IU0->price);

		$IU3->updatePlus('totalpriceet', $IU0->price);

		$IU3->updatePlus('totalpriceref', $IU0->price);

		$IU3->updatePlus('origtotalpriceet', $IU0->price);

		$IU3->updatePlus('origtotalprice', $IU0->price);

		$IU3->updatePlus('subtotal', $IU0->price);

		$IU3->updatePlus('subtotalref', $IU0->price);

		$IU2 = $IU3->update();

	}


	$IU4 = WMessage::get();

	if ( !empty($IU2) ) {

		$IU5 = $IU0->name;

		$IU4->userS('1308548838EKPV',array('$NAME'=>$IU5));

	} else {

		$IU4->userE('1308543350DBTS');

	}


	return true;



}}