<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_members_show_controller extends WController {
function show() {

	if ( !IS_ADMIN ) {

		$IUD = WGlobals::getEID();

	

		if ( empty( $IUD ) ) {

			$IUE = WMessage::get();

			$IUE->exitNow( 'Unauthorized access 137' );

		}


		$IUF = WUser::get('uid');

			

 		
		
		static $IUG = array();

 		if ( !isset( $IUG[ $IUF ] ) ) {

 			$IUH = WRole::get();

			$IUG[ $IUF ] = $IUH->hasRole( 'storemanager' );

 		}
 		if ( $IUG[ $IUF ] ) return true;



		static $IUI = null;

	 	
		if ( !isset( $IUI[ $IUF ] ) && !empty( $IUF ) ) {

			$IUJ = WClass::get('vendor.helper',null,'class',false);

			$IUI[ $IUF ] = $IUJ->getVendorID( $IUF );

		}
		

		
		if ( !empty( $IUI[ $IUF ] ) ) {

			$IUK = WModel::get( 'order' );

			$IUK->makeLJ( 'order.products', 'oid' );

			$IUK->whereE( 'uid', $IUD );

			$IUK->whereE( 'vendid', $IUI[ $IUF ], 1 );

			$IUL = $IUK->load( 'lr', 'oid' );

			

			$IUM = !empty( $IUL ) ? true : false;

		} else $IUM = false;

			

		if ( !$IUM ) {

			$IUE = WMessage::get();

			$IUE->exitNow( 'Unauthorized access 138' );

		}
	}
}}