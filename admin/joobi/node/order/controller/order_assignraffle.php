<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Order_assignraffle_controller extends WController {




	function assignraffle() {

		
		$IS9 = WGlobals::get( 'oid' );

		$ISA = WGlobals::getEID();



		
		$ISB = WModel::get( 'order' );

		$ISB->whereE( 'oid', $IS9 );

		$ISC = $ISB->load( 'lr', 'uid' );



		
		$ISD = WUser::get( 'uid' );



		
		if( ( !empty($IS9) && !empty($ISD) ) && ( !empty($ISC) && $ISD == $ISC ) ) {

			$ISE = WClass::get( 'product.raffle' );



			
			$ISF = $ISE->getRaffleID( $ISA );



			
			$ISG = $ISE->numAvailableRaffles( $ISF );



			if( empty($ISG) || ( $ISG < 0 ) ) {

				$ISH = WMessage::get();

				$ISH->userN('1254450087AHPI');

				return true;

			}


			
			static $ISI=null;

			if( !isset($ISI) ) $ISI = WModel::get( 'product.rafflemembers' );

			$ISI->select( 'rafno', 0, null, 'count' );

			$ISI->whereE( 'rafid', $ISF );

			$ISI->whereE( 'oid', $IS9 );

			$ISI->whereE( 'uid', $ISD );

			$ISI->groupBy( 'rafid' );

			$ISJ = $ISI->load( 'lr' );



			
			static $ISK=null;

			if( !isset($ISK) ) $ISK = WModel::get( 'order.products' );

			$ISK->whereE( 'pid', $ISA );

			$ISK->whereE( 'oid', $IS9 );

			$ISL = (int)$ISK->load( 'lr', 'quantity' );



			if( !empty($ISJ) && !empty($ISL) ) {

				if( $ISJ < $ISL ) {
					
				} else {

					$ISH = WMessage::get();

					$ISH->userN('1254414492PMFA');

				}
			} else {
				
			}
		} else {

			$ISH = WMessage::get();

			$ISH->userW('1254412730IGSM');

			$ISH->adminW( 'Only the owner of this order with the correct user id can access this page.' );

		}


		return true;

	}}