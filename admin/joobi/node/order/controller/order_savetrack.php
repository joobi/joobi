<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_savetrack_controller extends WController {


function savetrack() {
	

	$IPO = WModel::getID( 'order' );

	$IPP = WGlobals::get( 'trucs' );

	$IPQ = ( isset($IPP[$IPO]['oid']) ) ? $IPP[$IPO]['oid'] : 0;

	$IPR = ( isset($IPP[$IPO]['trackno']) ) ? $IPP[$IPO]['trackno'] : '';

	
	

	if ( !empty($IPQ) && !empty($IPR) ) {
		
	 		 	if ( !IS_ADMIN ) {
	 		
	 		$IPS = WUser::get('uid');
			$IPT = WClass::get('vendor.helper',null,'class',false);
			$IPU = $IPT->getVendorID( $IPS );	
	 		
	 		$IPV = WModel::get( 'order' );
	 		$IPV->whereE( 'oid', $IPQ );
	 		$IPV->whereE( 'vendid', $IPU );
	 		$IPW = $IPV->exist();
	 		if ( empty($IPW) ) {
	 			$IPX = WMessage::get();
				$IPX->exitNow('You are not authorized... 204');
	 		}	 	}		

		
		$IPV = WModel::get( 'order' );

		$IPV->setVal( 'trackno', $IPR );

		$IPV->whereE( 'oid', $IPQ );

		$IPY = $IPV->update();

	

		if ( $IPY ) {

			$IPX = WMessage::get();

			$IPX->adminS( 'Tracking Number successfully saved.' );

		} else {

			$IPX = WMessage::get();

			$IPX->adminE( 'Failed in saving tracking number. Please try again, trace controller order_savetrace' );

		}
	}




	return true;



}}