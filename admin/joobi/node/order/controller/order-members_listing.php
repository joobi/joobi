<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_members_listing_controller extends WController {
function listing() {



	if ( !IS_ADMIN ) {

		
		$IUA = WClass::get( 'item.restriction', null, 'class', false);

 		if ( $IUA ) $IUB = $IUA->filterRestriction();

		else $IUB = false;

		

		if ( !$IUB ) {

			$IUC = WMessage::get();

			$IUC->exitNow( 'Unauthorized access 136' );

		}
	}
			

	return true;

}}