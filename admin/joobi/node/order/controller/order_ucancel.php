<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;


class Order_ucancel_controller extends WController {




function ucancel() {
	

	$IRL = time();

	$IRM = WGlobals::get('oid');

	$IRN = WGlobals::get('uid');

	if( empty($IRN) )$IRN = WUser::get('uid');

	if ( empty($IRM) || empty($IRN) ) return true;

	

	
	$IRO = WModel::get('order');

	$IRO->whereE( 'oid', $IRM );

	$IRO->whereE( 'uid', $IRN );

	$IRO->whereE( 'ostatusid', 5 );

	$IRP = $IRO->load('o');	

	

	
	if ( isset( $IRP ) ) {

		if ( $IRP->coupid > 0 ) {

			$IRQ = WClass::get( 'basket.reload' );

			$IRQ->reloadCoupon( $IRP );

		}
	}

	else $IRP = new stdClass;

	

	
	$IRO = WModel::get( 'order' );	

	$IRO->whereE( 'oid', $IRM );

	$IRO->whereE( 'uid', $IRN );

	$IRO->setVal( 'ostatusid', 99 );

	$IRR = $IRO->update();

	

	
	if ( $IRR ) 

	{

		$IRS = WMessage::get();

		$IRS->userS('1233642055HTTO');

		

			

	}


	if( IS_ADMIN ) WPage::redirect( 'controller=order' );



	return true;

}}