<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_members_unaddress_controller extends WController {




function unaddress()

{

	
	$ISM = WGlobals::get( 'id' );

	$ISN = WGlobals::get( 'id2' );

	$ISO = WGlobals::get( 'id3' );



	if ( !empty($ISM) )

	{

		
		$ISP = WModel::get( 'address' );

		$ISP->whereE( 'adid', $ISM );

		$ISQ = $ISP->load( 'r', 'publish' );

	

		
		

		if ( empty($ISQ) || ( $ISQ == 0 ) ) $ISP->setVal( 'publish', 1 );

		else $ISP->setVal( 'publish', 0 );

		$ISP->whereE( 'adid', $ISM );

		$ISP->update();

	}


	WPage::redirect( 'controller=order-members&task=show&eid='. $ISN .'&titleheader='. $ISO );

	return true;

}}