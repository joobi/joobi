<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_applypref_controller extends WController {
function applypref() {



	parent::savepref();



	if ( isset( $this->generalPreferences['basket.node']['directpay'] ) ) {

		

		$IQV = WClass::get( 'order.savepref' );

		$IQV->toggleDirectPaymentPref( $this->generalPreferences['basket.node']['directpay'] );		

		

	}


	WPage::redirect( 'controller=order&task=preferences' );



	return true;



}
}