<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Order_selectraffle_controller extends WController {




function selectraffle()

{

	$IR0 = WGlobals::get( 'oid' );

	$IR1 = WGlobals::get( 'eid' );

	$IR2 = WGlobals::get( 'rafid' );

	$IR3 = WGlobals::get( 'rafno' );


	$IR4 = WUser::get( 'uid' );

	

	
	$IR5 = WClass::get( 'product.raffle' );

	

	
	$IR6 = $IR5->assignRaffle( $IR2, $IR3, $IR4, $IR0 );



	if( $IR6 )

	{

		$IR7 = WModel::get( 'order.products' );

		$IR7->whereE( 'pid', $IR1 );

		$IR7->whereE( 'oid', $IR0 );

		$IR8 = $IR7->load( 'lr', 'ordpid' );	

	

		
		$IR9 = WModel::get( 'order.poptions' );

		$IR9->setVal( 'oid', $IR0 );

		$IR9->setVal( 'opid', $IR3 );

		$IR9->setVal( 'ordpid', $IR8 );

		$IR9->setVal( 'optname', 'Raffle #' );

		$IR9->setVal( 'optnamekey', 'raffle' );

		$IR9->setVal( 'optvaluename', ''.$IR3 );

		$IR9->setVal( 'type', 3 );

		$IR9->setVal( 'pid', $IR1 );

		$IR9->setVal( 'cumulative', 0 );

		$IR9->insert();

	}


	
	
	$IRA = $IR5->getRaffle( $IR2, $IR3 );

	$IR1 = $IRA->pid;

	$IRB = '<table border="0">';

	$IRB .= '<tr><td> '. TR1254292157HDGC .': '. WApplication::date( JOOBI_DATE5, $IRA->datepurchased ) .'</td></tr>';

	

	$IRB .= '<tr>';

	

	$IRB .= '<td> '. TR1254123961COUV .' # '. $IR3 .'</td>';



	$IRB .= '</tr>';

	$IRB .= '</table>';

	

	
	$IRC = new stdClass;

	$IRC->raffleno = $IR3;

	$IRC->customerName = WUser::get( 'name', $IR4 );

	$IRC->customerEmail = WUser::get( 'email', $IR4 );

	$IRD = WPage::routeURL( 'controller=product-rafflemembers&task=listing&eid='. $IR1, 'admin' );

	$IRC->textlink = '<a href="'. $IRD .'">here</a>';

	$IRC->sitename = JOOBI_SITE_NAME;

	$IRC->raffleHTML = $IRB;



	
	$IR5->sendRaffleEmail( 'customer_raffle_ticketno', $IR4, $IRC, true, true, false );



	

		


	WPage::redirect( 'controller=order&task=assignraffle&oid='. $IR0 .'&eid='. $IR1 );

	return true;

}}