<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Order_viewhistory_controller extends WController {






function viewhistory()

{

	
	$IVK = WGlobals::getEID();

	if( empty($IVK) )

	{

		$IVL = WModel::getID( 'order' );

		$IVM = 'oid_'. $IVL;

		$IVK = WGlobals::get( $IVM );

		if( is_array($IVK) ) $IVK = $IVK[0];

	}


	
	$IVN = WGlobals::get( 'titleheader' );

	if( empty($IVN) )

	{

		$IVO = WClass::get('order.helper',null,'class',false);

		$IVN = $IVO->getOrderNamekey( $IVK );

	}


	
	WPage::redirect( 'controller=order-history&task=listing&eid='. $IVK .'&titleheader='. $IVN );

	return true;

}}