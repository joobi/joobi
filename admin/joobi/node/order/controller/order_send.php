<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_send_controller extends WController {
function send() {

	$IRT = WGlobals::getEID();

	$IRU = WGlobals::get('controller');

	$IRV = WModel::get( 'order' );

	$IRV->select( 'namekey' );

	$IRV->select( 'uid' );

	$IRV->whereE( 'oid', $IRT );

	$IRW = $IRV->load('o');



	if (!empty($IRW)) {

		$IRX = $IRW->uid;

		$IRY = $IRW->namekey;

		WPage::redirect( 'controller='.$IRU.'&task=email&uid='. $IRX .'&title='. $IRY .'&oid='. $IRT );



	}
	return true;

}}