<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_savepref_controller extends WController {
function savepref() {



	parent::savepref();



	if ( isset( $this->generalPreferences['basket.node']['directpay'] ) ) {

		
		$ISR = WClass::get( 'order.savepref' );
		$ISR->toggleDirectPaymentPref( $this->generalPreferences['basket.node']['directpay'] );		
		

	}


	return true;



}
}