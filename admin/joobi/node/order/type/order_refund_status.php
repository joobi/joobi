<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Order_Order_refund_status_type extends WTypes {
var $order_refund_status = array( 

	1=>'Pending', 

	2=>'Approved', 

	3=>'Denied', 

	4=>'Cancelled' 

);
}