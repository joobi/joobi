<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
 defined('_JEXEC') or die;









class Order_Orderstatus_type extends WTypes {
	
	





var $orderstatus = array(

		'0'	=> 'All Status',

		'-1'	=> '--Waiting Transactions',
		'1'	=>	'Invoice Created',
		'3'	=>	'Invoice Sent',

		'5'	=> 'Wait for payment',

		'10'	=> 'Pending',

		'15'	=> 'In-Progress',

		'-2'	=> '--Confirmed Transactions',

		'20'	=> 'Completed',

		'25'	=> 'Processed',


		'-3'	=> 'Refunded Transactions',

		'50'	=> 'Refunded',
		'55'	=> 'Partial Refund',

		'60'	=> 'Refund requested',

		'80'	=> 'Charged back',

		'-4'	=> '--Canceled Transactions',

		'99'	=> 'Cancelled',

		'98'	=> 'Cancelled-Reversal',

		'97'	=> 'Automatically Cancelled',

		'110'	=> 'Voided',

		'115'	=> 'Expired',

		'-5'	=> '--Error Transactions',

		'120'	=> 'Denied',

		'150'	=> 'Failed',
		'-6'	=> '--Other Transactions',
		'220'	=> 'Fraud',
		'230'	=> 'Dispute Filed'



	);
}