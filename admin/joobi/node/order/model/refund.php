<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Refund_model extends WModel {
	
function validate() {
	

	$this->namekey = $this->genNamekey();
	$this->namekey = strtoupper( $this->namekey );
	

	$I12B = WClass::get('order.helper');	
	$I12C = $this->orderid;
	$this->curid = $I12B->getOrderColValue($I12C, 'curid');

	$this->price = $I12B->getOrderColValue($I12C, 'price');

	$this->discount = $I12B->getOrderColValue($I12C, 'discount');

	$this->bookingfee = $I12B->getOrderColValue($I12C, 'bookingfee');

	$this->tax = $I12B->getOrderColValue($I12C, 'tax');

	$this->baseprice = $I12B->getOrderColValue($I12C, 'baseprice');

	$this->subtotal = $I12B->getOrderColValue($I12C, 'subtotal');

	$this->quantity = $I12B->getOrderColValue($I12C, 'quantity');

	$this->vendid = $I12B->getOrderColValue($I12C, 'vendid');

	$this->pid = $I12B->getOrderColValue($I12C, 'pid');
	
	return true;

}
function extra() {
	
	$I12D = WMessage::get();
	$I12B = WClass::get('order.helper');	
	$I12E = WClass::get('order.refund');
	
	$I12F = $I12E->getorderRefundId($this->orderid);
	$I12G = new stdClass;
 	$I12G->issuedby =  Wuser::get('name');
	$I12G->type = 'Vendor';
	$I12G->user = Wuser::get('name');
	$I12G->refundID = $I12F;
	$I12H = WPage::linkHome('controller=order-refund&task=show&eid='.$I12F );
	$I12G->refundLink = '<a href="'.$I12H.'">'.TR1206961903JJIS.'</a>';
	
		$I12I = WModel::get('order');
	$I12I->select('uid');
	$I12I->whereE('oid', $this->orderid );
	$I12J = $I12I->load('lr');

	
 	$I12K = WMail::get();
 	$I12K->setParameters( $I12G );
 	$I12K->sendNow( $I12J, 'order_refund_request_acknowledgment' );
 	
	$I12D->userS('1310701767BHIY');
 	
		if ( !defined('PORDER_NODE_NOTIFYSTOREMANAGERONREFUND') ) WPref::get('order.node');
	
	if ( PORDER_NODE_NOTIFYSTOREMANAGERONREFUND ) {
		
				if ( !empty($this->vendid) ) {
			
						$I12L = WClass::get( 'vendor.helper', null, 'class', false );
			$I12J = $I12L->getVendor( $this->vendid, 'uid' );			
			
			$I12K->setParameters( $I12G );
		 	$I12K->sendNow( $I12J, 'order_refund_request' );
			
		}
			 	
	}	
	return true;
}
}