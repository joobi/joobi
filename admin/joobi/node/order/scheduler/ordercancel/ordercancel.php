<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Order_Ordercancel_scheduler extends Scheduler_Parent_class {












	function process() {


				WPref::get( 'order.node' );

		$IBX = PORDER_NODE_DELAYCANCEL * 86400 ;
		if ( $IBX < 1 ) return true;

		$IBY = $this->getLastRun();


				$IBZ = $this->getCurrentRun() - $IBX;
				$IC0 = $IBY - $IBX;
		if ( $IC0<0 ) $IC0 = 0;
		if ( $IBZ < $IC0 ) return true;

		$IC1 = WModel::get('order');
		if ( PORDER_NODE_NOTCUSTCANCEL ) { 
						$IC1->whereE('ostatusid', 5 );
			$IC1->where('created','>=', $IC0 );
			$IC1->where('created','<', $IBZ );
			$IC1->orderby('uid' );
			$IC1->setLimit( 5000 );

			$IC2 = $IC1->load('ol', array( 'uid', 'oid', 'namekey' ) );

						$this->lastProcess = ( ( count($IC2) < 5000 ) ? true : false );

						if( empty( $IC2 ) ) {
					$IC3 =  WApplication::date( JOOBI_DATE2, $IC0 + WUser::timezone() );
					$IC4 = WApplication::date( JOOBI_DATE2, $IBZ + WUser::timezone() );
					$this->addReport(str_replace(array('$beginDate','$endDate'),array($IC3,$IC4),TR1233642055HTTP));
				return true;
			}
		}


		$IC1->whereE('ostatusid', 5 );
		$IC1->where('created','>=', $IC0 );
		$IC1->where('created','<', $IBZ );
		$IC1->setVal( 'ostatusid', 97 );
		$IC1->update();


		return true;


	}

}