<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Orderpending_scheduler extends Scheduler_Parent_class {












	function process() {



		
		WPref::get( 'order.node' );



		$IBB = PORDER_NODE_DELAYPENDING * 60;

		if ( $IBB < 1 ) return true;



		$IBC = $this->getLastRun();



		
		$IBD = $this->getCurrentRun() - $IBB;

		
		$IBE = $IBC - $IBB;

		if ( $IBE<0 ) $IBE = 0;

		if ( $IBD < $IBE ) return true;



		$IBF = WModel::get('order');

		$IBF->makeLJ( 'users', 'uid' );

		$IBF->select( 'name', 1 );

		
		$IBF->whereIN('ostatusid', array( 5, 150 ) );	
		$IBF->where('created','>=', $IBE );

		$IBF->where('created','<', $IBD );

		$IBF->orderby('uid' );
		$IBF->setLimit( 500 );	
		$IBG = $IBF->load('ol', array( 'uid', 'oid', 'namekey', 'payid' ) );


				$this->lastProcess = ( ( count($IBG) < 5000 ) ? true : false );


		
		if ( empty( $IBG ) ) {

				$IBH =  WApplication::date( JOOBI_DATE2, $IBE + WUser::timezone() );

				$IBI = WApplication::date( JOOBI_DATE2, $IBD + WUser::timezone() );

				$this->addReport(str_replace(array('$beginDate','$endDate'),array($IBH,$IBI),TR1233642055HTTP) );

			return true;

		}


		$IBJ = WMail::get();

		$IBJ->keepAlive();



		$IBK = $this->l1f_R_204();

		$IBL = CMSAPIPage::getSpecificItemId( 'basket' );

		$IBM = WClass::get( 'order.mail' );


		foreach( $IBG as $IBN ) {

			
			$IBO = WPage::routeURL( 'controller=order&task=show&eid='. $IBN->oid, 'home', false, false, $IBL );


			$IBP = new stdClass;

			$IBP->orderID = $IBN->namekey;

			$IBP->ordlink = '<a href="'. $IBO .'">'. $IBN->namekey .'</a>';

			$IBP->customerName = $IBN->name;


						$IBQ = $IBM->getOtherPaymentMethod( $IBN->payid );


			if ( !empty($IBQ) ) {


				$IBR = WPage::routeURL( 'controller=basket&task=reload&oid='. $IBN->oid, 'home', false, false, $IBL );


				$IBS = '<ul>';

				foreach( $IBQ as $IBT ) {

					if ( $IBT->payid == $IBN->payid ) {

						$IBP->paymentUsed = '<a href="'. $IBR .'&payid='. $IBT->payid .'">'. TR1206961903JJIS .'('. $IBT->name .')</a>';

					} else {

			 			$IBS .= '<li>Pay with "<a href="'. $IBR .'&payid='. $IBT->payid .'">'. $IBT->name .'</a>"</li>';

			 		}
				}
				$IBS .= '</ul>';

				$IBP->paymentLink = $IBS;

			}


			$IBU = WClass::get('vendor.helper',null,'class',false);

			$IBV = $IBU->getVendor();

			$IBP->vendorName = $IBV->name;



			$IBJ->setParameters( $IBP );


			$IBJ->sendNow( $IBN->uid, 'order_pending', $this->report );



		}


		$IBJ->keepAlive(false);

		return true;



	}




















private function l1f_R_204() {

	$IBW = WExtension::get( 'jmarket.application', 'wid', 'application' );



	if ( !empty($IBW) ) return 'com_jmarket';

	else {

		$IBW = WExtension::get( 'jstore.application', 'wid', 'application' );



		if ( !empty($IBW) ) return 'com_jstore';

		else {

			$IBW = WExtension::get( 'jsubscription.application', 'wid', 'application' );



			if ( !empty($IBW) ) return 'com_jsubscription';

			else {
				$IBW = WExtension::get( 'jauction.application', 'wid', 'application' );

				if ( !empty($IBW) ) return 'com_jauction';
				else {
					$IBW = WExtension::get( 'jcatalog.application', 'wid', 'application' );
					if ( !empty($IBW) ) return 'com_jcatalog';
					else return 'com_jcenter';

				}			}		}
	}

}
}