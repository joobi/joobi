<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Conversion_scheduler extends Scheduler_Parent_class {
	
		var $fromDateTime = 0;
    var $toDateTime = 0;
    var $currentDateTime = 0;
    var $pidA = array();
    var $lsidA = array();
    var $fromDateA = array();
    var $toDateA = array();
    var $convRateIDA = array();
    var $startDateA = array();
        var $convA = array();
    var $oneIntervalTS = 86400;     var $fromDateS = 'fromDate';
    var $toDateS = 'toDate';
	var $totalS = 'total';
    var $uidS = 'uid';
 



	public function process() 
	{
		$IC5 = mktime(0,0,0,date("m"),date("d"),date("Y"));
		$IC6 = date("Y-m-d", $IC5);
		WMessage::log( 'Began Processing Conversion Data for Date : '.$IC6.' , Timestamp : '.$IC5, 'orders-conversion' );
		
                $this->l1g_S_4ig();
                $this->l1h_T_5se();
                $this->l1i_U_43g();
        
        $IC5 = mktime(0,0,0,date("m"),date("d"),date("Y"));
        WMessage::log( 'Completed Processing Conversion Data for Date : '.$IC6.' , Timestamp : '.$IC5, 'orders-conversion' );

	} 

	




	private function l1g_S_4ig() 
	{
	    $IC7 = 0;

	            $IC8 = $this->l1j_V_6aw();
        
        if(!empty($IC8))
        {
         foreach($IC8 as $IC9)
         {
         	        	$ICA = new stdClass;
        	$ICB = array();
        	$ICC = $IC9->period ;
        	
        	$ICA->convRateID = $IC9->conv_rate_id ;
        	$ICA->pid = $IC9->pid ;
        	$ICA->lsid = $IC9->lsid ;
        	$ICA->period = $IC9->period ;
        	$ICA->startDate = $IC9->startdate ;
        	
        	if($IC9->startdate == 0)
        	{
        		        		        		                $ICD = $this->l1k_W_2js($IC9->lsid) ;
                
                                $ICD = strtotime(date("d-m-Y", $ICD));
            }
        	else
        	{
        		        		$ICD = $IC9->startdate;
        	}
        	
        	        	$ICE = strtotime(date("d-m-Y", time())) - $this->oneIntervalTS;
        	
        	        	$ICF = (int)(($ICE - $ICD) / $this->oneIntervalTS ) ;
        	$ICA->noOfDays = $ICF;
        	
        	        	for( $ICG=0; $ICG<$ICF; $ICG++ )
        	 {
        	 	$ICH = $ICE - ($ICC*$this->oneIntervalTS) - ($ICG*$this->oneIntervalTS);
        	 	$ICI = $ICE - ($ICG*$this->oneIntervalTS) ;
        	 	$ICB[$ICG]=array($this->fromDateS => $ICH, $this->toDateS => $ICI);
        	 }
        	 
        	        	$ICA->dateRange = $ICB;
        	
        	        	$this->convA[$IC7] = $ICA ; 
        	
        	$IC7++;
         }         }
        else
        {
        	WMessage::log( 'There was no Conversion Rate Row Fetched', 'orders-conversion' );
        }
                 		
	} 




	private function l1j_V_6aw() 
	{
		$ICJ=WModel::get('order.conversionrate');	
        
        $ICJ->select( 'lsid' );
        $ICJ->select( 'pid' );
        $ICJ->select( 'period' );
        $ICJ->select( 'conv_rate_id' );
        $ICJ->select( 'startdate' );
        
        return $ICJ->load('ol');
	}
	




	private function l1k_W_2js($O1) 
	{
		$ICK=WModel::get('list');	
        $ICK->whereE('lsid',$O1);
        
        return $ICK->load('lr', 'created');
	}	
	





	private function l1h_T_5se() 
	{
		$ICL = $this->convA ;
		
		if(!empty($ICL))
		{
		 foreach($ICL as $ICA)
		 {
		  		  	
		  $ICM = $ICA->pid;
		  $ICN = $ICA->lsid;
		  $ICF = $ICA->noOfDays;

		  $ICO = array();
		  
		  for( $ICP=0; $ICP<$ICF; $ICP++ )
		  {
		    		    		  	
		    $ICH = $ICA->dateRange[$ICP][$this->fromDateS];
		    $ICI = $ICA->dateRange[$ICP][$this->toDateS];
		  
		    $ICQ = $this->l1l_X_67r($ICM,$ICN,$ICH,$ICI);

		    if(!empty($ICQ))
		    {
		      foreach($ICQ as $ICR )
		      {
		    			    	$ICO[$ICH][$ICR->uid] = $ICR->total ; 
		      } 		      
		    }		    
		    unset ( $ICQ );
		  
		  } 		  
		   $ICA->resultData = $ICO;
		  
		 } 		 
		} 		else
		{
			WMessage::log( 'There were no Conversion Objects in the Array', 'orders-conversion' );
		}
				
	} 	
	




	private function l1l_X_67r($O2,$O3,$O4,$O5)
	{
		$ICS=WModel::get('order.items');
                $ICS->select( 'price', 0, $this->totalS, 'sum' );
        $ICS->select( $this->uidS,1  );
        
        $ICS->makeLJ('order','oid');
        $ICS->makeLJ('contacts.tagsassigned','uid','uid',1,2);
        $ICS->makeLJ('list','lsid','lsid',2,3);
        	
        $ICS->whereE('pid', $O2 ,0);
        $ICS->whereE('lsid', $O3 ,3);
        $ICS->where('created', '>', $O4,1 );
        $ICS->where('created', '<', $O5,1 );
        
        $ICS->groupBy('uid', 1);
        
        return $ICS->load('ol');
	}	

	



	private function l1i_U_43g() 
	{
		            $ICT = 2;
          
          $ICU = $this->convA ;
          
          if(!empty($ICU))
          {
           foreach($ICU as $ICA)
           {
          	           	$ICV = $ICA->convRateID;
          	
          	$ICO = $ICA->resultData;
          	
          	if(!empty($ICO))
          	{
          	  foreach($ICO as $ICW => $ICX)
          	  {
          	  	           		 $ICY = $ICW;
          		
          		 if(!empty($ICX))
          		 { 
          		  foreach($ICX as $ICZ => $ID0)
          		  {
          		  	          		  	$this->l1m_Y_1wv($ICT,$ICV,$ICY,$ICZ,$ID0);
          		  }           		 }
          	  }           	}
          	else
          	{
          		WMessage::log( 'There was no Data to be inserted for Conversion Rate ID :'.$ICV, 'orders-conversion' );
          	}
          	$this->l1n_Z_2jg($ICV);
          	
            }           }           else
          {
          	WMessage::log( 'There were no Conversion Objects in the Array', 'orders-conversion' );
          	
          }
 		
	} 	
	



	private function l1m_Y_1wv($O6,$O7,$O8,$O9,$OA)
	{
		        	
          $ID1=WModel::get('order.conversionratedata');
          $ID1->setVal( 'created', $O8 );
          $ID1->setVal( 'conv_rate_id', $O7 );
          $ID1->setVal( 'curid', $O6 );
          $ID1->setVal( 'total', $OA );
          $ID1->setVal( 'uid', $O9 );
          $ID1->insert();
		
	}	
	
	




	private function l1n_Z_2jg($OB) 
	{
		  $ID2 = strtotime(date("d-m-Y", time()));
		
          $ICJ=WModel::get('order.conversionrate');
          $ICJ->setVal( 'startdate', $ID2 );
	      $ICJ->whereE( 'conv_rate_id', $OB );
	      $ICJ->update();	
		
	}	




	private function l1o_10_3bi() 
	{
				
		$ID3 = true;
		
			    if($this->startDateA[$ID4] != 0)
        {
            $ID5 = date("Y-m-d", $ID6[$ID4]);
        	$ID7 = date("Y-m-d", $ID8);

        	        	        	if($ID5 == $ID7)
        	{
        	  	echo '<br>Already fetched for this date '.$ID7;
        	  	$ID3 = false;
        	}
        	else if($ID6[$ID4] < $ID8)
        	{
        	  	echo '<br>Already fetched for this date too '.$ID7;
        	  	$ID3 = false;
        	}
        	  
         }         
         return $ID3;
        
	} 	
} 