<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Conversionrate_scheduler extends Scheduler_Parent_class {

			var $fromDateTime = 0;
    var $toDateTime = 0;
    var $currentDateTime = 0;
    var $pidA = array();
    var $lsidA = array();
    var $fromDateA = array();
    var $toDateA = array();
    var $convRateIDA = array();
    var $startDateA = array();
        var $convA = array();
    var $oneIntervalTS = 86400;     var $fromDateS = 'fromDate';
    var $toDateS = 'toDate';
	var $totalS = 'total';
    var $uidS = 'uid';
 



	public function process() 
	{
		$ID9 = mktime(0,0,0,date("m"),date("d"),date("Y"));
		$IDA = date("Y-m-d", $ID9);
		WMessage::log( 'Began Processing Conversion Data for Date : '.$IDA.' , Timestamp : '.$ID9, 'orders-conversion' );
		
                $this->l1g_S_4ig();
                $this->l1h_T_5se();
                $this->l1i_U_43g();
        
        $ID9 = mktime(0,0,0,date("m"),date("d"),date("Y"));
        WMessage::log( 'Completed Processing Conversion Data for Date : '.$IDA.' , Timestamp : '.$ID9, 'orders-conversion' );

	} 

	




	private function l1g_S_4ig() 
	{
	    $IDB = 0;

	            $IDC = $this->l1j_V_6aw();
        
        if(!empty($IDC))
        {
         foreach($IDC as $IDD)
         {
         	        	$IDE = new stdClass;
        	$IDF = array();
        	$IDG = $IDD->period ;
        	
        	$IDE->convRateID = $IDD->conv_rate_id ;
        	$IDE->period = $IDD->period ;
        	$IDE->startDate = $IDD->startdate ;
        	
        	        	if($IDD->prodtypid != 0)
        	{
        		$IDE->prodI = $IDD->prodtypid ;
        		$IDE->prodS = 'type';
        	}
        	else
        	{
        		$IDE->prodI = $IDD->pid ;
        		$IDE->prodS = '';     
        	}

        	            if($IDD->lstypeid != 0)
        	{
        		$IDE->listI = $IDD->lstypeid ;
        		$IDE->listS = 'type';
        		$IDH = $IDD->lstypeid ;
        		$IDI = 'type';          		
        	}
        	else
        	{
        		$IDE->listI = $IDD->lsid ;
        		$IDE->listS = '';        		
        		$IDH = $IDD->lsid ;
        		$IDI = '';
        	}
        	
        	if($IDD->startdate == 0)
        	{
        		        		        		                $IDJ = $this->l1k_W_2js($IDH,$IDI) ;
                
                                $IDK = strtotime(date("d-m-Y", $IDJ));
            }
        	else
        	{
        		        		$IDK = $IDD->startdate;
        	}
        	
        	        	$IDL = strtotime(date("d-m-Y", time())) - $this->oneIntervalTS;
        	
        	        	$IDM = (int)(($IDL - $IDK) / $this->oneIntervalTS ) ;
        	
        	if($IDG > 5 && $IDM > 100) $IDM = 100 ;
        	if($IDG > 10 && $IDM > 30) $IDM = 30 ;
        	if($IDG > 40 && $IDM > 20) $IDM = 20 ;
        	if($IDG > 80 && $IDM > 10) $IDM = 10 ;
        	
        	        	
        	$IDE->noOfDays = $IDM;
        	
        	        	for( $IDN=0; $IDN<$IDM; $IDN++ )
        	 {
        	 	        	 	        	 	$IDO = $IDK + ($IDN*$this->oneIntervalTS) - ($IDG*$this->oneIntervalTS);
        	 	$IDP = $IDK + ($IDN*$this->oneIntervalTS) ;
        	 	$IDF[$IDN]=array($this->fromDateS => $IDO, $this->toDateS => $IDP);
        	 }
        	 
        	        	$IDE->dateRange = $IDF;
        	
        	        	$this->convA[$IDB] = $IDE ; 
        	
        	$IDB++;
         }         }
        else
        {
        	WMessage::log( 'There was no Conversion Rate Row Fetched', 'orders-conversion' );
        }
                         		
	} 




	private function l1j_V_6aw() 
	{
		$IDQ=WModel::get('order.conversionrate');	
        
        $IDQ->select( 'lsid' );
        $IDQ->select( 'pid' );
        $IDQ->select( 'lstypeid' );
        $IDQ->select( 'prodtypid' );
        $IDQ->select( 'period' );
        $IDQ->select( 'conv_rate_id' );
        $IDQ->select( 'startdate' );
        
        return $IDQ->load('ol');
	}
	




	private function l1k_W_2js($O1,$O2) 
	{
		if($O2 == 'type')
		{
		  $IDR=WModel::get('list.type');
		  		  $IDR->makeLJ('list','lstypeid');		
		  $IDR->whereE('lstypeid',$O1,0);			
		  return $IDR->load('lr', 'created',1);
		}
		else
		{
		  $IDR=WModel::get('list');

		  if($O1 != 0)
		  {
		  	$IDR->whereE('lsid',$O1);
		  }
		  
		  
		  return $IDR->load('lr', 'created');			
		}
        
	}	
	





	private function l1h_T_5se() 
	{
		$IDS = $this->convA ;
		
		if(!empty($IDS))
		{
		 foreach($IDS as $IDE)
		 {
		  		  	
		  $IDT = $IDE->prodI;
		  $IDH = $IDE->listI;
		  $IDU = $IDE->prodS;
		  $IDI = $IDE->listS;
		  $IDM = $IDE->noOfDays;

		  
		  $IDV = array();
		  
		  for( $IDW=0; $IDW<$IDM; $IDW++ )
		  {
		    		    		  	
		    $IDO = $IDE->dateRange[$IDW][$this->fromDateS];
		    $IDP = $IDE->dateRange[$IDW][$this->toDateS];
		  
		    $IDX = $this->l1l_X_67r($IDT,$IDH,$IDU,$IDI,$IDO,$IDP);

		    if(!empty($IDX))
		    {
		      foreach($IDX as $IDY )
		      {
		    			    	$IDV[$IDP][$IDY->uid] = $IDY->total ; 
		      } 		      
		    }		    
		    unset ( $IDX );
		  
		  } 		  
		   $IDE->resultData = $IDV;
		  
		 } 		 
		} 		else
		{
			WMessage::log( 'There were no Conversion Objects in the Array', 'orders-conversion' );
		}
						
	} 	
	




	private function l1l_X_67r($O3,$O4,$O5,$O6,$O7,$O8)
	{
		$IDZ=WModel::get('order.items');
                $IDZ->select( 'price', 0, $this->totalS, 'sum' );
        $IDZ->select( $this->uidS,1  );
        
        $IDZ->makeLJ('order','oid');
        $IDZ->makeLJ('contacts.tagsassigned','uid','uid',1,2);
        $IDZ->makeLJ('list','lsid','lsid',2,3);

        if($O5 == 'type')
        {
          $IDZ->whereE('prodtypid', $O3 ,0);
        }
        else
        {
        	if($O3 != 0)
        	{
              $IDZ->whereE('pid', $O3 ,0);
        	}	
        }
        
	    if($O6 == 'type')
        {
          $IDZ->whereE('lstypeid', $O4 ,3);
        }
        else
        {
        	if($O4 != 0)
        	{
              $IDZ->whereE('lsid', $O4 ,3);
        	}
        }        
        

        $IDZ->where('created', '>', $O7,1 );
        $IDZ->where('created', '<', $O8,1 );
        
        $IDZ->groupBy('uid', 1);
        
        return $IDZ->load('ol');
	}	

	



	private function l1i_U_43g() 
	{
		            $IE0 = 2;
          
          $IE1 = $this->convA ;
          
          if(!empty($IE1))
          {
           foreach($IE1 as $IDE)
           {
          	          	$IDV = array();
           	
           	$IE2 = $IDE->convRateID;
          	
          	$IDV = $IDE->resultData;
          	          	
          	          	$IE3 = end($IDE->dateRange);
          	$IE4 = $IE3['toDate'];
          	
          	if(!empty($IDV))
          	{
          	  foreach($IDV as $IE5 => $IE6)
          	  {
          	  	           		 $IE7 = $IE5;
          		
          		 if(!empty($IE6))
          		 { 
          		  foreach($IE6 as $IE8 => $IE9)
          		  {
          		  	          		  	$this->l1m_Y_1wv($IE0,$IE2,$IE7,$IE8,$IE9);
          		  }           		 }
          	  }           	}
          	else
          	{
          		WMessage::log( 'There was no Data to be inserted for Conversion Rate ID :'.$IE2, 'orders-conversion' );
          	}
          	$this->l1n_Z_2jg($IE2,$IE4);
          	
          	unset ( $IDV );

          	
            }           }           else
          {
          	WMessage::log( 'There were no Conversion Objects in the Array', 'orders-conversion' );
          	
          }
 		
	} 	
	



	private function l1m_Y_1wv($O9,$OA,$OB,$OC,$OD)
	{
		        	
          $IEA=WModel::get('order.conversionratedata');
          $IEA->setVal( 'created', $OB );
          $IEA->setVal( 'conv_rate_id', $OA );
          $IEA->setVal( 'curid', $O9 );
          $IEA->setVal( 'total', $OD );
          $IEA->setVal( 'uid', $OC );
          $IEA->insert();
		
	}	
	
	




	private function l1n_Z_2jg($OE,$OF) 
	{
		  		
          $IDQ=WModel::get('order.conversionrate');
          $IDQ->setVal( 'startdate', $OF );
	      $IDQ->whereE( 'conv_rate_id', $OE );
	      $IDQ->update();	
		
	}	




	private function l1o_10_3bi() 
	{
				
		$IEB = true;
		
			    if($this->startDateA[$IEC] != 0)
        {
            $IED = date("Y-m-d", $IEE[$IEC]);
        	$IEF = date("Y-m-d", $IEG);

        	        	        	if($IED == $IEF)
        	{
        	  	echo '<br>Already fetched for this date '.$IEF;
        	  	$IEB = false;
        	}
        	else if($IEE[$IEC] < $IEG)
        	{
        	  	echo '<br>Already fetched for this date too '.$IEF;
        	  	$IEB = false;
        	}
        	  
         }         
         return $IEB;
        
	} 	
	
	
}