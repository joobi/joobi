<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Order_Ordshiprate_form extends WForms_standard {




function create() {

	$IEU = WModel::getID( 'shipping.rate' );

	$IEV = 'curid_'. $IEU;

	$IEW = $this->data->$IEV;

	$IEX = $this->value;



	$this->content = '<span style="font-weight:bold;">'. WTools::format( $IEX, 'money', $IEW) .'</span>';

	return true;

}}