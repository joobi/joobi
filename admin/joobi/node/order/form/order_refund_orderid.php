<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'main.form.smplhiddenvar' , JOOBI_DS_NODE );
class Order_Order_refund_orderid_form extends WForm_smplhiddenvar {
function create() {

	$IFM = WGlobals::get('orderid');



	$this->value = $IFM;

	$this->element->hidden = 1;



	return parent::create();

}}