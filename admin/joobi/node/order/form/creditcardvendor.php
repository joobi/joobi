<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Creditcardvendor_form extends WForms_standard {


	function create() {

	

		$IIN = $this->getValue( 'card' );

		

		if ( empty($IIN) ) return false;

		

		

		$IIO = $this->getValue( 'oid' );

		$IIP = WModel::get( 'orders.creditcard' );

		$IIP->select( '*' );

		$IIP->makeLJ( 'payment.creditcard', 'ctypid' );

		$IIP->select( 'name', 1, 'cardname' );

		$IIP->whereE( 'oid', $IIO );

		$IIQ = $IIP->load( 'o' );

		if ( !empty( $IIQ ) ) {

			$this->content = $this->l1p_11_64h( $IIQ );

		}
		

		return true;
		

	}
	



	private function l1p_11_64h($O1) {

		

			

			$IIR = WModel::get( 'payment.creditcard' );

			$IIR->select( 'name', 0, 'cardname' );

			$IIR->select( 'namekey' );

			$IIR->select( '*', 1 ); 

			$IIR->makeLJ( 'files', 'filid', 'filid', 0, 1 );

			$IIR->whereE( 'ctypid', $O1->ctypid );

			$IIS = $IIR->load( 'o' );			

			$IIT = JOOBI_URL_MEDIA . str_replace( '|', '/', $IIS->path ) . '/' . $IIS->name . '.' . $IIS->type;

			$IIU = '<img style="width:31px; height:18px;" align="middle" alt="'.$O1->cardname.'" src="'.$IIT.'"> ';

			

			$IIV = $O1->card;





			$IIW = '<table class="creditCard">';

			$IIW .= '<tr><td>' . TR1313471769LRSN . '</td><td style="vertical-align: middle;">: ' . $IIU . $O1->cardname . '</td></tr>';

			$IIW .= '<tr><td>' . TR1351516230NWRA . '</td><td>: ' . $IIV . '</td></tr>';

			$IIW .= '<tr><td>' . TR1351516230NWRB . '</td><td>: ' . $O1->expdate . '</td></tr>';

			$IIW .= '</table>';

			

			return $IIW;

		

	}
	

	

	
}