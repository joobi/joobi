<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;









class Order_Couponvaluebe_form extends WForms_standard {


function create() {
	

	
	static $IJB=null;

	if( !isset($IJB) ) $IJB = $this->modelID;

	$IJC = 'curid_'. $IJB;

	$IJD = $this->data->$IJC; 
	$IJC = 'curidref_'. $IJB;

	$IJE = $this->data->$IJC; 
	$IJC = 'coupvalueref_'. $IJB;

	$IJF = $this->data->$IJC; 
	

	
	$this->content = WTools::format( $this->value, 'money', $IJD );

	

	
	if( !empty($IJE) && !empty($IJF) ) $this->content .= ' (<span style="color:blue;">'. WTools::format( $IJF, 'money', $IJE ) .'</span>)';

	return true;
	

}
}