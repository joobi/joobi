<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Joomlarequired_form extends WForms_standard {
function create() {



	if ( !defined('PBASKET_NODE_GHOSTACCOUNT') ) WPref::get( 'basket.node' );



	if ( !PBASKET_NODE_GHOSTACCOUNT ) return false;



	if ( !defined('PUSERS_NODE_FRAMEWORK_FE') ) WPref::get( 'users.node', false, true, false );

	$IJ9 = WAddon::get( 'users.'. PUSERS_NODE_FRAMEWORK_FE );

	$IJA = $IJ9->checkConfirmationRequired();



	if ( !$IJA ) return false;



	$this->content = '<span style="color:red;">' . TR1379525428MVRR . '</span>';



	return true;


}
}