<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;






class Order_Curidrefoi_form extends WForms_standard {






function create()

{

	static $IEL = null;	

	if(empty($IEL))$IEL = WModel::get( 'currency' );

	if(!empty($IEL))

	{

		$IEL->whereE( 'curid', $this->value );

		$IEM = $IEL->load('lr', 'title');

	}
	if (!empty($IEM))

	{ 

		$this->content = $IEM;

	}

	else

	{ 

		$IEN = 'title_'. WModel::get( 'currency', 'sid');

		$this->content = $this->data->$IEN;

	}
	return true;

}







}