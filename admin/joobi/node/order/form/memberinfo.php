<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Memberinfo_form extends WForms_standard {







function create() {	



	$IF2 = $this->getValue( 'uid', 'users' );

	if ( empty( $IF2 ) ) return false;

	

	if ( WGlobals::checkCandy(50) ) {

		$IF3 = WPage::linkPopUp( 'controller=order-members&task=show&eid='. $IF2 .'&titleheader='. $this->value );

		$this->content = WPage::createPopUpLink( $IF3, $this->value, 1000, 600 );
		

	} else $this->content = $this->value;

	return true;

}}