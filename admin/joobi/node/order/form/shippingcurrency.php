<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;

class Order_Shippingcurrency_form extends WForms_standard {




function create()

{

	static $II0=null;

	if( !isset($II0) ) $II0 = WClass::get( 'currency.format' );

	

	$II1 = $this->value;

	$II2 = $II0->load( $II1 );

	if( !empty($II2) ) $II3 = $II2->title .' ('. $II2->symbol .')';

	else $II3 = '';

	

	$this->content = $II3;

	return true;

}}