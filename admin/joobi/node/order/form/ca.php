<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Ca_form extends WForms_standard {


	function create() {

		static $IIB = null;

		if (empty($IIB)) $IIB = WModel::get( 'payment.type', 'sid');

	

		

		$IIC = $this->getValue( 'paytype', 'payment.type' );

		$IID = $this->getValue( 'website', 'payment.type' );

		$IIE = $this->getValue( 'name', 'payment.type' );

		$IIF = $this->value;

		

		if ( !empty( $IID ) ) {

			$IIE = '<a href="'. $IID .'" target="_blank">'. $IIF .' ('. $IIE .')</a>';

		}
	

		$this->content = $IIE;

		return true;

	}}