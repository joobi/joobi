<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;






class Order_Totalpricepref_form extends WForms_standard {
	

	function create() {

		$IFF = 'curidref_'. $this->modelID;

		static $IFG = null;

		

		if (!empty($this->data->$IFF))

		{

			if(empty($IFG)) $IFG = WModel::get( 'currency');

			if(!empty($IFG))

			{

				$IFG->whereE( 'curid', $this->data->$IFF );

				$IFH = $IFG->load( 'r', 'title' );

			}
		}
		

		if(!empty($this->value) && $this->value > 0)

		{

			if(!empty($this->data->$IFF))$this->content = '<b><span style="color:#FF0000;">'. WTools::format($this->value, 'money', $this->data->$IFF) .'</span></b> - '. $IFH;

			else $this->content = null;

		}
	

		return true;

	}









}