<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Creditcardfe_form extends WForms_standard {


	function show() {

		

		$IHM = $this->getValue( 'uid' );

		$IHN = WUser::get('uid');

		

		if ( $IHM != $IHN ) return false;

		

		$IHO = $this->getValue( 'paytype' );

		$IHP = WModel::get( 'payment.type' );

		$IHP->whereE( 'paytype', $IHO );

		$IHQ =  $IHP->load( 'lr', 'namekey' );


		
		if ( !empty( $IHQ) && $IHQ != 'creditcard' ) return false;

		

		$IHR = $this->getValue( 'oid' );

		$IHS = WModel::get( 'orders.creditcard' );

		$IHS->select( '*' );

		$IHS->makeLJ( 'payment.creditcard', 'ctypid' );

		$IHS->select( 'name', 1, 'cardname' );

		$IHS->whereE( 'oid', $IHR );

		$IHT = $IHS->load( 'o' );


		if ( empty($IHT) ) return false;
		

		$IHU = '';

		for ( $IHV=0; $IHV < $IHT->digits - 4; $IHV++ ) $IHU .= '*';

		$IHU .= $IHT->card;

		

		
		$IHW = '<tr> <td valign="top" class="key">Card</td> ';

		$IHW .= '<td valign="top"> <strong><em>' .$IHT->cardname . '</em></strong>  ' .$IHU .'</td></tr>';

		$IHW .= '<tr> <td valign="top" class="key">Exp Date</td> ';

		$IHW .= '<td valign="top">' .$IHT->expdate .'</td></tr>';

		

		

		
		$IHX = $this->getValue( 'ostatusid' );

		if ( $IHX != 20 ) {

			$IHY = WPage::routeURL( 'controller=creditcard&task=checkout&oid='.$IHR );

			$IHW .= '<tr><td colspan="2" align="center"><a title="Update" href="'. $IHY .'"><img style="border: medium none;" src="'.JOOBI_URL_THEME_JOOBI . 'images/toolbar/16/updatestore.png" title="Update" alt="Update">Update</a></td></tr> ';

		}
		

		$this->content = $IHW;

		return true;

	}
	
}