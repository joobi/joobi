<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'form.textonly' , JOOBI_LIB_HTML );
class Order_Ordercoupondescbe_form extends WForm_textonly {
function create() {

	$IIX = $this->getValue( 'coupid' );

	if ( empty( $IIX ) ) return false;

	else return parent::create();

}}