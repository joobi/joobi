<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Orderprice_form extends WForms_standard {
function show() {



	$IET = $this->getValue( 'curid' );

	if ( $this->value > 0 ) $this->content = WTools::format( $this->value, 'money', $IET );

	else $this->content = TR1206961944PEUR;

	return true;

}}