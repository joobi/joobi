<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Billingaddress_form extends WForms_standard {
function create() {
	if ( empty($this->value) ) return false;

	
	$IIA = WClass::get( 'address.helper' );
	$this->content = $IIA->renderAddress( $this->value );

	return true;

}}