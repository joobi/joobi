<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Totalprice_form extends WForms_standard {








function show() {



	$IFN = $this->getValue( 'subtotal', 'order' );

	$IFO = $this->getValue( 'subtotalref', 'order' );

	$IFP = $this->getValue( 'totaldiscount', 'order' );

	$IFQ = $this->getValue( 'totaldiscountref', 'order' );

	$IFR = $this->getValue( 'totalprice', 'order' );

	$IFS = $this->getValue( 'totalpriceref', 'order' );

	$IFT = $this->getValue( 'curid', 'order' );

	$IFU = $this->getValue( 'curidref', 'order' );

	$IFV = $this->getValue( 'curiddisp', 'order' );

	$IFW = $this->getValue( 'shiprateid', 'order' );

	$IFX = $this->getValue( 'shipcurid', 'order' );

	$IFY = $this->getValue( 'totalshipping', 'order' );

	$IFZ = $this->getValue( 'totalshippingref', 'order' );

	$IG0 = $this->getValue( 'shippingtax', 'order' );

	$IG1 = $this->getValue( 'paymentfee', 'order', 0 );
	$IG2 = $this->getValue( 'totaltax', 'order' );

	$IG3 = $this->getValue( 'totaltaxref', 'order' );

	$IG4 = $this->getValue( 'totalbookingfee', 'order' );

	$IG5 = $this->getValue( 'totalbookingfeeref', 'order' );

	$IG6 = $this->getValue( 'totaldue', 'order' );

	$IG7 = $this->getValue( 'totaldueref', 'order' );

	$IG8 = $this->getValue( 'bookingfeehtml', 'order' );



	$IG9 = $this->getValue( 'title', 'currency' );

	$IGA = $this->getValue( 'symbol', 'currency' );

	$IGB = $this->getValue( 'code', 'currency' );

	$IGC = $this->getValue( 'fee', 'order.payment' );



		if ( $IG1 > 0 ) $IGD = WTools::format( $IG1, 'money', $IFT );
	$IGE = WTools::format( $IFN, 'money', $IFT );

	$IGF = WTools::format( $IFP, 'money', $IFT );

	$IGG = WTools::format( $IGC, 'money', $IFT );

	$IGH = WTools::format( $IFR, 'money', $IFT );

	$IGI = WTools::format( $IFY, 'money', $IFT );

	$IGJ = WTools::format( $IG0, 'money', $IFT );
	$IGK = WTools::format( $IG2, 'money', $IFT );

	$IG4 = WTools::format( $IG4, 'money', $IFT );

	$IG6 = WTools::format( $IG6, 'money', $IFT );

	$IGL = WTools::format( $IFO, 'money', $IFU );

	$IGM = WTools::format( $IFQ, 'money', $IFU );

	$IGN = WTools::format( $IFS, 'money', $IFU );

	$IGO = WTools::format( $IFZ, 'money', $IFU );

	$IGP = WTools::format( $IG3, 'money', $IFT );

	$IGQ = WTools::format( $IG5, 'money', $IFT );

	$IGR = WTools::format( $IG7, 'money', $IFT );



	static $IGS=null;

	if ( !isset($IGS) ) $IGS = WClass::get( 'currency.helper' );



	
	if ( !isset($IGS) ) $IGS = WClass::get( 'currency.helper' );

	$IGT = !empty($IFU) ? $IGS->getValue( $IFU, array( 'title', 'code' ) ) : null;

	$IGU = isset($IGT->title) ? $IGT->title : '';

	$IGV = isset($IGT->code) ? $IGT->code : '';



	
	$IFV = !empty($IFV) ? $IFV : 0;

	$IGW = !empty($IFV) ? $IGS->getValue( $IFV, array( 'title', 'code' ) ) : null;

	$IGX = isset($IGW->title) ? $IGW->title : $IGU;

	$IGY = isset($IGW->code)? $IGW->code : '';



	
	$IGZ = TR1251253375NEZJ;

	$IH0 = TR1307528241QOXY;
	$IH1 = TR1315401256ERGV;

	$IH2 = TR1206961911NYAO;

	$IH3 = TR1220793699CLXD;

	$IH4 = TR1206961912MJPF;

	$IH5 = TR1329422724BSFL;

	$IH6 = TR1329422724BSFM;

	$IH7 = TR1206961911NYAQ;

	$IH8 = TR1299678632JXEB;

	$IH9 = TR1240888717QTPY;

	$IHA = TR1299678632JXED;



	
	static $IHB=null;

	if ( !isset($IHB) ) {

		
		$IHC = 'style="text-align:left;"';

		$IHD = 'style="text-align:right;"';

		$IHE = 'style="text-align:right;color:blue;font-size:11px;"';

		$IHF = 'style="color:blue;font-size:11px;"';



		
		$IHB = '<table width="100%">';



		
		$IHB .= '<tr><td '. $IHC .'>'. $IGZ .'</td><td>:</td>';

		$IHB .= '<td '. $IHD .'>'. $IGE .'</td><td></td><td>'. $IGB .'</td></tr>';



		
		if ( !empty($IHG) && ( !empty($IFU) && ( $IFT != $IFU ) ) ) $IHB .= '<td colspan="4" '. $IHE .'>'. $IGL .'</td><td '. $IHF .'>'. $IGV .'</td></tr>';



		if ( $IFY > 0  ) {
						$IHB .= '<tr><td '. $IHC .'>'. $IH0 .'</td><td>:</td>';

			$IHB .= '<td '. $IHD .'> + '. $IGI .'</td><td></td><td>'. $IGB .'</td></tr>';

		}

		
		if ( !empty($IHH) && ( !empty($IFU) && ( $IFT != $IFU ) ) ) $IHB .= '<td colspan="4" '. $IHE .'> + '. $IGO .'</td><td '. $IHF .'>'. $IGV .'</td></tr>';



		
		if ( $IFP > 0 ) {
			$IHB .= '<tr><td '. $IHC .'>'. $IH2 .'</td><td>:</td>';
			$IHB .= '<td '. $IHD .'> - '. $IGF .'</td><td></td><td>'. $IGB .'</td></tr>';
		}

		
		if ( !empty($IHI) && ( !empty($IFU) && ( $IFT != $IFU ) ) ) $IHB .= '<td colspan="4" '. $IHE .'> - '. $IGM .'</td><td '. $IHF .'>'. $IGV .'</td></tr>';



				if ( $IG1 > 0 ) {
						$IHB .= '<tr><td colspan="3" '. $IHD .'> --------------- </td><td></td><td></td></tr>';
			$IG1 = 0;
			$IHB .= '<tr><td '. $IHC .'>'. TR1369750670TGSB .'</td><td>:</td>';
			$IHB .= '<td '. $IHD .'>' . $IGD .'</td><td></td><td>'. $IGB .'</td></tr>';
		}

		
		if ( $IG2 > 0 ) {
						$IHB .= '<tr><td colspan="3" '. $IHD .'> --------------- </td><td></td><td></td></tr>';
			$IHJ = ( $this->getValue( 'pricetaxtype' ) == -1 ) ? ' + ' : '';
			if ( !defined('PCATALOG_NODE_USETAX') ) WUser::pref('catalog.node');
			if ( $IG0 > 0 && PCATALOG_NODE_USETAX && PCATALOG_NODE_SHOWTAX ) {
								$IHB .= '<tr><td '. $IHC .'>'. $IH1 .'</td><td>:</td>';
				$IHB .= '<td '. $IHD .'>'. $IHJ. $IGJ .'</td><td></td><td>'. $IGB .'</td></tr>';
			}
			$IHB .= '<tr><td '. $IHC .'>'. $IH7 .'</td><td>:</td>';
			$IHB .= '<td '. $IHD .'>' . $IHJ . $IGK .'</td><td></td><td>'. $IGB .'</td></tr>';

		}

		
		if ( !empty($IHK) && ( !empty($IFU) && ( $IFT != $IFU ) ) ) $IHB .= '<td colspan="4" '. $IHE .'> + '. $IGP .'</td><td '. $IHF .'>'. $IGV .'</td></tr>';



		
		if ( $IGC > 0 ) $IHB .= '<tr><td '. $IHC .'>'. $IH3 .'</td><td>:</td><td '. $IHD .'>'. $IGG .'</td><td></td><td>'. $IGB .'</td></tr>';



		
		$IHB .= '<tr><td colspan="3" '. $IHD .'> --------------- </td><td></td><td></td></tr>';





		if ( !empty( $IG6 ) && !empty( $IG8 ) ) {

			
			$IHB .= '<tr><td '. $IHC .'>'. $IH8 .'</td><td>:</td>';

			$IHB .= '<td '. $IHD .'><b>'. $IG6 .'</b></td><td></td><td>'. $IGB .'</td></tr>';



			
			if ( !empty($IG7) && ( !empty($IFU) && ( $IFT != $IFU ) ) ) $IHB .= '<td colspan="4" '. $IHE .'>'. $IGR .'</td><td '. $IHF .'>'. $IGV .'</td></tr>';



			
			$IHB .= '<tr><td '. $IHC .'>'. $IHA .'</td><td>:</td>';

			$IHB .= '<td '. $IHD .'>'. $IG8 .'</td><td></td><td></td></tr>';



			
			$IHB .= '<tr><td colspan="3" '. $IHD .'> --------------- </td><td></td><td></td></tr>';



			
			$IHB .= '<tr><td '. $IHC .'>'. $IH9 .'</td><td>:</td>';

			$IHB .= '<td '. $IHD .'><b>'. $IGH .'</b></td><td></td><td>'. $IGB .'</td></tr>';



			
			if ( !empty($IHL) && ( !empty($IFU) && ( $IFT != $IFU ) ) ) $IHB .= '<td colspan="4" '. $IHE .'>'. $IGN .'</td><td '. $IHF .'>'. $IGV .'</td></tr>';



		} else {

			
			$IHB .= '<tr><td '. $IHC .'>'. $IH4 .'</td><td>:</td>';

			$IHB .= '<td '. $IHD .'><b>'. $IGH .'</b></td><td></td><td>'. $IGB .'</td></tr>';



			
			if ( !empty($IHL) && ( !empty($IFU) && ( $IFT != $IFU ) ) ) $IHB .= '<td colspan="4" '. $IHE .'>'. $IGN .'</td><td '. $IHF .'>'. $IGV .'</td></tr>';



		}


		$IHB .= '<tr><td colspan="3" '. $IHD .'> </td><td></td><td></td></tr>';
		
		$IHB .= '<tr><td '. $IHC .'>'. $IH5 .'</td><td>:</td><td '. $IHD .'>'. $IG9 .'</td><td></td><td>( '. $IGB .' )</td></tr>';




		
		if ( !empty($IGX) && !empty($IGY) && $IFT != $IFV ) $IHB .= '<tr><td '. $IHC .'>'. $IH6 .'</td><td>:</td><td '. $IHD .'>'. $IGX .'</td><td></td><td>( '. $IGY .' )</td></tr>';



		$IHB .= '</table>';

	}




	$this->content = $IHB;

	return true;

}}