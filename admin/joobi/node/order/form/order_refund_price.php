<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Order_refund_price_form extends WForms_standard {
function show() {

	$IFB = WClass::get('order.helper');

	$IFC = WGlobals::get('orderid');

	

	
	$IFD = $IFB->getOrderColValue($IFC, 'price');

	


	

	$IFE = $IFB->getOrderColValue($IFC, 'curid');

	if ( $IFD > 0 ) $this->value = WTools::format( $IFD, 'money', $IFE );

	else $this->value = TR1206961944PEUR;

	

	return parent::show();

}}