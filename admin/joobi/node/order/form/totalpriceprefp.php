<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Order_Totalpriceprefp_form extends WForms_standard {
	function show()

	{

		$IEY = 'curidref_'. $this->modelID;

		static $IEZ = null;

		

		if (!empty($this->data->$IEY))

		{

			if(empty($IEZ)) $IEZ = WModel::get( 'currency');

			if(!empty($IEZ))

			{

				$IEZ->whereE( 'curid', $this->data->$IEY );

				$IF0 = $IEZ->load( 'r', 'title' );

			}
		}
		

		if(!empty($this->value) && $this->value > 0)

		{

			if(!empty($this->data->$IEY))$this->content = '<b><span style="color:#FF0000;">'. WTools::format($this->value, 'money', $this->data->$IEY) .'</span></b> - '. $IF0;

			else $this->content = null;

		}
	

		return true;

	}}