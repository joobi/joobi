<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;

class Order_Ordshippackfee_form extends WForms_standard {




function create()

{

	$IFI = WModel::getID( 'shipping.rate' );

	$IFJ = 'curid_'. $IFI;

	$IFK = $this->data->$IFJ;

	$IFL = $this->value;



	$this->content = WTools::format( $IFL, 'money', $IFK);

	return true;

}}