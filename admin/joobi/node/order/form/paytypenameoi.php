<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Order_Paytypenameoi_form extends WForms_standard {


	function create() {

		static $IF4 = null;

		if(empty($IF4)) $IF4 = WModel::get( 'payment.type', 'sid');

	

		
		$IF5 = $this->getValue( 'paytype', 'payment.type' );
		$IF6 = $this->getValue( 'website', 'payment.type' );
		$IF7 = $this->getValue( 'name', 'payment.type' );

		$IF8 = $this->value;

		
		if ( !empty( $IF6 ) ){
			$IF7 = '<a href="'. $IF6 .'" target="_blank">'. $IF8 .' ('. $IF7 .')</a>';
		}
	

		$this->content = $IF7;

		return true;

	}



}