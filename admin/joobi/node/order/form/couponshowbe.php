<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Couponshowbe_form extends WForms_standard {




function create() {

	$IF9 = $this->getValue( 'coupid' );

	if ( empty( $IF9 ) ) return false;



	if ( WGlobals::checkCandy(25) ) {

		$IFA = WPage::linkPopUp('controller=coupons&task=show&eid='. $IF9 .'&titleheader='. $this->value );

		$this->content = WPage::createPopUpLink( $IFA, $this->value, 1000, 600 );

	} else $this->content = $this->value;

	return true;

}}