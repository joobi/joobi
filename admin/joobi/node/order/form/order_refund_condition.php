<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Order_refund_condition_form extends WForms_standard {
function show() {

	if ($this->value == 1) $this->content = 'Unopenend';

	elseif ($this->value == 2) $this->content = 'Open box, no damages';

	elseif ($this->value == 3) $this->content = 'Open box, item is damaged';

	elseif ($this->value == 4) $this->content = 'Open box, only box damaged';

	else $this->content = 'Electronic(No Damage)';

	

	return true;

}}