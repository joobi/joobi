<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Address_listing extends WListings_standard {
function create() {


	$I106 = $this->getValue( 'adid' );
	if ( !empty($I106) ) {
				$I107 = WClass::get( 'address.helper' );

		$I108 = WGlobals::get( 'trucs' );
		$I109 = !empty($I108['x']['jdoctype']) ? $I108['x']['jdoctype'] : 200;
		if ( 240 == $I109 ) {				$this->content = $I107->renderAddress( $I106, ' - ', true, false );
		} else {				$this->content = $I107->renderAddress( $I106 );
		}
	}
	return true;

}
}