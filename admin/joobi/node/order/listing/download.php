<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Download_listing extends WListings_standard {












function create() {



	$IW7 = $this->getValue( 'ostatusid', 'order' );

	$IW8 = $this->getValue( 'unitprice', 'order.products' );



	if ( $IW7 == '20' || $IW8==0 ) {

		static $IW9 = null;

		if ( !isset($IW9) ) $IW9 = WModel::get('order.products', 'sid');

		$IWA = $this->getValue( 'pid', 'order.products' );

		$IWB = $this->getValue( 'useraffle', 'order.products' );

		$IWC = $this->getValue( 'name', 'order.products' );

		$IWD = WGlobals::get('eid');



		

		$IWE = $this->getValue( 'downloadable' );


		$this->content = '';

		if ( !empty($IWE) ) { 
			static $IWF = array();



			if ( !isset($IWF[$IWA]) ) {

				$IWG = WModel::get('product.downloads' );

				$IWG->makeLJ( 'files', 'filid' );

				$IWG->select( array ( 'pid' ) );

				$IWG->select( 'name', 1, 'fileurl' );

				$IWG->select( 'type', 1, 'filetype' );

				$IWG->whereE('pid', $IWA );

				$IWF[$IWA] = $IWG->load('ol');

			}


			$IWH = count($IWF[$IWA]);



			if ( $IWH < 2 ) {

				
				$IWI = WPage::routeURL('controller=order&task=download&oid=' . $IWD . '&eid=' . $IWA, '', false, false, true, null, true );

			} else {

				$IWJ = WTools::randomString( 100, false );

				WGlobals::setSession( 'order', 'secretKey', md5( $IWJ . JOOBI_SITE_TOKEN . $IWA ) );

				
				$IWI = WPage::routeURL( 'controller=order&task=downloads&eid='. $IWA . '&secretkey=' . $IWJ, '', false, false, true, null, true );
			}

			$this->content .= '<a class="fontOrange" href="' . $IWI . '">' . TR1206961905BHAV . '</a><br>';



		}










		$IWK = WPref::load( 'PORDER_NODE_ALLOWRETURN' );

		if ( $IWK ) {
			$IWL = WClass::get('order.helper');
			$IWM = WClass::get('order.refund');

			if ( !defined('PCATALOG_NODE_TERMSREFUNDALLOWED') ) WPref::get('catalog.node');
			$IWN = PCATALOG_NODE_TERMSREFUNDPERIOD;
			$IWO = PCATALOG_NODE_TERMSREFUNDALLOWED;
							$IWP = $IWM->getorderRefundId($IWD);
			$IWQ = $IWM->orderRefundgetCol( $IWP, 'status' );

			if (!empty($IWP) && $IWQ == 1) {
				$IWI = WPage::routeURL('controller=order-refund&task=cancelrequest&orderid='.$IWD.'&pid='.$this->getValue('pid'));
				$this->content .= '<br />(<a href ='.$IWI.'>'.TR1341596467IZZE.'</a>)';
				return true;
			}
			if ( empty($IWP) ) {
				if ($IWO == 0) {
					$IWR = $IWL->getOrdersColumnValue($IWD, 'created');
					$IWS = 86400;										$IWT = $IWS * $IWN;
					if ( time() - $IWR <= $IWT ) {
												
						$IWI = WPage::routeURL('controller=order-refund&task=request&orderid='.$IWD.'&pid='.$this->getValue('pid'));
						$this->content .= '<br />(<a href='.$IWI.'>'.TR1341517674ESOS.'</a>)';
					}				}			}
		}





	}else {

		$this->content = TR1227580874PDTG;

	}


	return true;


}
}