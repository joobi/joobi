<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Order_Archive_listing extends WListings_standard {




function create() {

	$this->content = '';

	$I10A = 'oid_'.WModel::get('order','sid');

	if($this->value == -1){

		$this->content .= '<a href="'.WPage::routeURL('controller=orders&task=unarchive&eid='.$this->data->$I10A).'">';

		$this->content .= ' <img src="'. JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/unarchive.png" border="0" alt="'. TR1206961905BHBA .'" />';

		$this->content .= '</a>';

	}else{

		$this->content .= '<a href="'.WPage::routeURL('controller=orders&task=archive&eid='.$this->data->$I10A).'">';

		$this->content .= ' <img src="'. JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/archive.png" border="0" alt="'. TR1206961905BHBB .'" />';

		$this->content .= '</a>';

	}

return true;
}





}