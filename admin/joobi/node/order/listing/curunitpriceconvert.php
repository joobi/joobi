<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Order_Curunitpriceconvert_listing extends WListings_standard {


function create()

{

	if( $this->value > 0 )

	{

	    $IZK = $this->getValue( 'curid', 'order.products' );

	    $IZL = $this->getValue( 'curid', 'order' );

	    $IZM = $this->getValue( 'vendid' );

	    $this->content = WTools::format($this->value, 'money', $IZK);



	    if( $IZK != $IZL )

	    {

	        
	        static $IZN=null;

	        if(empty($IZN)) $IZN = WClass::get('vendor.helper',null,'class',false);

	        $IZO = $IZN->getCurrency($IZM);



	        static $IZP = null;

	        if(empty($IZP)) $IZP = WClass::get( 'currency.convert',null,'class',false);

	        if( $IZK == $IZO ) $IZQ = false;

	        else $IZQ = true;

	        $IZR = $IZP->currencyConvert( $IZK, $IZL, (real)$this->value, $IZQ );



	        if( is_numeric($IZR) ) $this->content .= '<br>(<span style="color:blue;">'. WTools::format( $IZR, 'money', $IZL ) .'</span>)';

	    }
	}

	else $this->content = TR1206961944PEUR;



    return true;

}













}