<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Order_refund_status_vendor_listing extends WListings_standard {
function create() {
	if ($this->value == 1) $this->content = '<span style="color:#FFA500;">'.TR1206732372QTKP.'</span>';
	elseif ($this->value == 2) $this->content = '<span style="color:#008000;">'.TR1246518570RHDZ.'</span>';
	elseif ($this->value == 3) $this->content = '<span style="color:#FF0000;">'.TR1338493441EEFF.'</span>';
	else $this->content = '<span style="color:#0000FF;">'.TR1304526544DLEW.'</span>';

	return true;
}

}