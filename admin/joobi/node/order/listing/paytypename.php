<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Paytypename_listing extends WListings_standard {
function create() {

	
	$IYL = $this->getValue( 'website' );

	$IYM = $this->value;

	

	if ( empty( $IYL ) ) {

		$this->content = $IYM;

	} else {

		$this->content = '<a href="'. $IYL .'" target="_blank">'. $IYM .'</a>';

	}
	

	return true;

}}