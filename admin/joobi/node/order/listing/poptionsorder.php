<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Order_Poptionsorder_listing extends WListings_standard {




function create() {
	
	$IY1 = WGlobals::get( 'trucs' );
	$IY2 = !empty($IY1['x']['jdoctype']) ? $IY1['x']['jdoctype'] : 200;
	if ( 240 == $IY2 ) {			$IY3 = '"';
		$IY4 = '';
		$IY5 = ' / ';
		$IY6 = '"';
	} else {			$IY3 = '<ul>';
		$IY4 = '<li>';
		$IY5 = '</li>';
		$IY6 = '</ul>';
	}	

	$IY7 = $this->value;

	$IY8 = $this->getValue( 'ordpid' );

	$IY9 = $this->getValue( 'pid' );

	$IYA = $this->getValue( 'curid', 'order.products' );

	$IYB = $this->getValue( 'curid', 'order' );

	$IYC = $this->getValue( 'vendid' );



	
				$IYD = WModel::get( 'order.poptions' );			$IYD->whereE( 'ordpid', $IY8 );
		$IYE = $IYD->load( 'ol' );
		
		if ( !empty($IYE) ) {
			$IYF = $IY3;
			foreach( $IYE as $IYG ) {
				$IYH = false;
				$IYI = null;
				$IYF .= $IY4;
				$IYF .= $IYG->optname . ': ';
				
									$IYJ = '';
					if ( $IYG->type == 1 || $IYG->type == 4 ) {							if( $IYG->type != 4 ) $IYF .= $IYG->optvaluename;
					
						$IYF .= ' ' . WTools::format( $IYG->price, 'money', $IYB );
						
					} else {						
						$IYF .= $IYG->optvaluename;

						if ( $IYG->price!=0 ) {
							$IYK = WTools::format( $IYG->price, 'decimal', $IYB );
							$IYK = ( $IYK == round($IYK,0) ) ? round($IYK,0) : round($IYK,2);
							$IYJ .= ' ( '.$IYK.'% )';
						}
						$IYF .= ' '. $IYJ;
					}				
				$IYF .= $IY5;
			}			$IYF .= $IY6;
	
			$this->content = $IYF;
			
		}		
		




	return true;

}}