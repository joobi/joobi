<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Prodtotalshowbe_listing extends WListings_standard {












function create()

{

	if ( empty($this->value) || ( $this->value <= 0 ) ) $IXM = TR1206961944PEUR; 
	else

	{

		
		$IXN = $this->getValue( 'subtotalref' );

		$IXO = $this->getValue( 'curid' );

		if (empty($IXO)) {

			if ( !defined('CURRENCY_USED') ) {

				$IXP = WClass::get('currency.format', null,'class',false);

				$IXP->set();

			}
		 $IXO = CURRENCY_USED;

		 }

		$IXQ = $this->getValue( 'curidref' );

		

		
		$IXR = WTools::format( $this->value, 'moneyCode', $IXO );

		$IXS = WTools::format( $IXN, 'moneyCode', $IXQ );

		

		
		$IXM = '<span style="font-weight:bold;">'. $IXR .'</span>';

		

		if ( $IXO != $IXQ )

		{

			
			$IXT = 'style="color:blue;font-size:11px;font-weight:bold;"';

		

			
			if ( !empty($IXN) ) $IXM .= '<br><span '. $IXT .'>( '. $IXS .' )</span>';

		}
	}


	
	$this->content = $IXM;

	return true;
	

}
}