<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;






class Order_Tax_listing extends WListings_standard {


function create()

{

	$IXL = $this->getValue( 'curid' );

	$this->content = WTools::format($this->value, 'money', $IXL);

	return true;

}

}