<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Ordercommission_listing extends WListings_standard {










	function create() {



		$I10B = $this->getValue( 'price' );

		$I10C = $this->getValue( 'percent' );

		$I10D = $this->getValue( 'curid' );

		$I10E = $this->getValue( 'curidref' );



		
		$I10F = new stdClass;

		$I10F->base = $I10D;

		$I10F->ref = $I10E;

		$I10F->alt = $I10E;



		
		$I10G = new stdClass;

		$I10G->base = '';

		$I10G->alt = 'color:blue';



		static $I10H = null;

		if ( empty( $I10H ) ) $I10H = WClass::get( 'partner.listing' );

		
		$this->content = $I10H->genDisplay( $this->value, $I10F, $I10G );

		$I10I = '';

		

		if ( !empty( $I10B ) && (real)$I10B != 0 ) $I10I = WTools::format( $I10B , 'money', $I10E );

		
		if ( !empty( $I10C ) && (real)$I10C != 0 ) {

			if ( !empty( $I10I ) ) $I10I .= ' + ';

			$I10I .= round( $I10C, 2) . '%';

		}


		if ( !empty( $I10I ) ) {

			$this->content = '<span style="color:red;">( '. $I10I .' )</span>' . $this->content ;	

		}


		return true;

	}	
}