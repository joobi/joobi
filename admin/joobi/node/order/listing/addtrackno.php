<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Addtrackno_listing extends WListings_standard {
function create() {

	$IXC = $this->getValue( 'oid' );

	$IXD = $this->value;	

	$IXE = $this->getValue('ostatusid');	

	$IXF = $this->getValue('shipstatus');	


	if ( !empty($IXD) ) {

			if ( $IXF == 1 && $IXE >= 10 ) {

	        	$IXG = WPage::linkPopUp('controller=order-shipping&task=show&eid='. $IXC );

				$this->content = WPage::createPopUpLink( $IXG, TR1256629147ISKV, 1000, 600 );
				

	        } elseif ( $IXF > 1 ) {
	        	$this->content = '<span style="color:green;">'. TR1342550586DLAK .'</span>';
	        	
				$IXH = $this->getValue( 'trackno' );
				
				if ( !empty($IXH) ) {
					$this->content .= '<br /><span style="color:gray;">' . TR1342550586DLAL . ': <span style="color:green;font-weight:bold;">'.$IXH.'</span></span>';	
				} else {
					$this->content .= '<br /><span style="color:orange;">' . TR1342550586DLAM . '</span>';
				}	        	
	        }
        	

	}
       

        else $this->content = '';

        return true;

}}