<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Order_Poptionsorderfe_listing extends WListings_standard {


function create() {
	

	$I11F = $this->value;

	$I11G = $this->getValue( 'ordpid' );

	$I11H = $this->getValue( 'pid' );

	$I11I = $this->getValue( 'curid', 'order' );
	
	
	if( !empty($I11F) && !empty($I11G) && !empty($I11H) ) {
		

		


			





		$I11J = WModel::get( 'order.poptions' );			$I11J->whereE( 'ordpid', $I11G );
		$I11K = $I11J->load( 'ol' );
		
		if ( !empty($I11K) ) {
			$I11L = '<ul>';
			foreach( $I11K as $I11M ) {
				$I11N = false;
				$I11O = null;
				$I11L .= '<li>';
				$I11L .= $I11M->optname . ': ';
					$I11P = '';
					if ( $I11M->type == 1 || $I11M->type == 4 ) {							if( $I11M->type != 4 ) $I11L .= $I11M->optvaluename;
					
						$I11L .= ' ' . WTools::format( $I11M->price, 'money', $I11I );
						
					} else {						
						$I11L .= $I11M->optvaluename;
	

						if ( $I11M->price!=0 ) {
							$I11Q = WTools::format( $I11M->price, 'decimal', $I11I );
							$I11Q = ( $I11Q == round($I11Q,0) ) ? round($I11Q,0) : round($I11Q,2);
							$I11P .= ' ( '.$I11Q.'% )';
						}
						$I11L .= ' '. $I11P;						}				$I11L .= '</li>';
			}			$I11L .= '</ul>';
	
			$this->content = $I11L;
			
		}			
			
			
			
			


	


	
	}	

	return true;

}}