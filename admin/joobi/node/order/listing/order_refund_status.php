<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Order_Order_refund_status_listing extends WListings_standard {
function create() {

	if ($this->value == 1) $this->content = '<span style="color:orange">'.TR1206732372QTKP.'</span>';

	elseif ($this->value == 2) $this->content = '<span style="color:green">'.TR1246518570RHDZ.'</span>';

	elseif ($this->value == 3) $this->content = '<span style="color:red">'.TR1338493441EEFF.'</span>';

	else $this->content = '<span style="color:blue">'.TR1304526544DLEW.'</span>';
	
	if(!IS_ADMIN){
		if($this->value == 1){
			$IZY = WPage::routeURL('controller=order-refund&task=cancelrequest&orderid='.$this->getValue('orderid').'&pid='.$this->getValue('pid'));
			$this->content .= '<br />(<a href ='.$IZY.'>'.TR1310465041HAEQ.'</a>)';
		}	}


	return true;

}}