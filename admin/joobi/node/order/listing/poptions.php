<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Order_Poptions_listing extends WListings_standard {










function create() {

	static $I114 = array();

	$I115 = $this->value;

	$I116 = $this->getValue( 'ordpid' );

	$I117 = $this->getValue( 'pid' );



	if(!isset($I114[$I117][$I116])) {

		static $I118 = null;

		if( empty($I118) ) $I118 = WClass::get( 'basket.options' );

		$I114[$I117][$I116] = $I118->ordDetail( $I115, $I116, $I117 );

	}


	if(!empty($I114[$I117][$I116])) $this->content = $I114[$I117][$I116];

	else $this->content = '';

	return true;

}



}