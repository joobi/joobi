<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'listing.selectone' , JOOBI_LIB_HTML );
class Order_Status_listing extends WListings_selectone {


function create() {



	$I11R = parent::create();

	$I11S = $this->content;



	if ( !WGlobals::checkCandy(25) ) {

		$this->content = $I11S;

		return true;

	}


	switch ( $this->value ) {

		
		case 5:

			$this->content = $I11S;

			$I11T = $this->getValue('oid');





			$I11U = CMSAPIPage::getSpecificItemId( 'basket' );



			$I11V = WPage::routeURL( 'controller=basket&task=reload&oid='. $I11T, '', false, false, $I11U );

			$this->content .= '<br /><a href="'.$I11V.'"><span style="color:rgb(255,0,0);text-decoration:underline;">' . TR1233642055HTTN . '</span></a>';



			$I11W = TR1206961912MJOX;



			
			$I11X = 'onclick="return confirm(\''.str_replace(array('$ACTION'),array($I11W),TR1233626551NWXV).'\')"';



			$I11V = WPage::link( 'controller=order&task=ucancel&oid='. $I11T );

			$this->content .= '<br /><a href="'.$I11V.'" '. $I11X .'><span style="text-decoration:underline;">' . TR1206732393CXVV . '</span></a>';

			break;

		case 20:

		case 30:

			$I11T = $this->getValue('oid');



			$I11V = WPage::link( 'controller=order&task=show&eid='. $this->getValue( 'oid' ) );

			if ( !empty($I11S) ) $this->content = '<span style="color:green;">' . $I11S . '';



			$I11Y = $this->getValue( 'shipstatus' );

			if ( !empty( $I11Y ) ) {

				if ( $I11Y == 30 ) {



					$this->content .= '<br /><span style="color:green;">' . TR1256629146HYRP . '</span>';

					
					$I11Z = $this->getValue( 'trackno' );



					if ( !empty($I11Z) ) {

						$this->content .= '<br /><span style="color:gray;">' . TR1342550586DLAL . ': <span style="font-weight:bold;color:green;">'.$I11Z.'</span></span>';

					} else {

						$this->content .= '<br /><span style="color:orange;">' . TR1342550586DLAM . '</span>';

					}
				}


			}


			$I120 = $this->getValue( 'downloadable' );

			if ( $I120 ) $this->content .= '<br /><a href="'.$I11V.'"><span class="fontOrange">'. TR1206961905BHAV .'</span></a>';



			


			$I121 = WPref::load( 'PORDER_NODE_ALLOWRETURN' );

			if ( $I121 ) {
												$I122 = WClass::get('order.refund');
				$I123 = $I122->getorderRefundId( $I11T );
				$I11R = $I122->orderRefundgetCol($I123, 'status');

				if (!empty($I123) && $I11R == 1) {
					$I11V = WPage::link( 'controller=order&task=show&eid=' . $I11T );
					$this->content .= '<br /><a href ='.$I11V.'><span class="fontGray">'.TR1341596467IZZE.'</span></a>';
					return true;
				}
				if ( empty($I123) ) {
					if (!defined('PCATALOG_NODE_TERMSREFUNDALLOWED') ) WPref::get('catalog.node');
					$I124 = PCATALOG_NODE_TERMSREFUNDPERIOD;
					$I125 = PCATALOG_NODE_TERMSREFUNDALLOWED;	
					if ( !empty($I125) ) {
						$I126 = $this->getValue('created');
						$I127 = 86400;												$I128 = $I127 * $I124;

						if (time() - $I126 <= $I128 ) {
							$I11V = WPage::link( 'controller=order&task=show&eid=' . $I11T );
							$this->content .= '<br /><a href='.$I11V.'><span class="fontGray">'.TR1341517674ESOS.'</span></a>';
						}
					}				}			}

		break;


		default:

			$this->content = $I11S;

		break;



	}


	return true;

}}