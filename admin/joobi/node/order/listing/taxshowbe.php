<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Order_Taxshowbe_listing extends WListings_standard {




function create()

{

	if ( empty($this->value) || ( $this->value <= 0 ) ) $I10J = TR1206732410ICCJ; 
	else

	{

		
		$I10K = $this->getValue( 'taxref' );

		$I10L = $this->getValue( 'curid' );

		$I10M = $this->getValue( 'curidref' );

		

		
		$I10N = WTools::format( $this->value, 'money', $I10L );

		$I10O = WTools::format( $I10K, 'money', $I10M );

		

		
		$I10J = '+ '. $I10N;

		

		if ( $I10L != $I10M )

		{

			
			$I10P = 'style="color:blue;font-size:11px;"';

		

			
			if ( !empty($I10K ) ) $I10J .= '<br><span '. $I10P .'>( + '. $I10O .' )</span>';

		}
	}


	
	$this->content = $I10J;

	return true;

}}