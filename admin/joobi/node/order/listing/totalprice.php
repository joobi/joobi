<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'main.listing.moneycurrency' , JOOBI_DS_NODE );
class Order_Totalprice_listing extends WListings_moneycurrency {


function create() {




	$I104 = $this->getValue( 'pricetype' );



	$this->value = $this->getValue( 'price' );

	if ( $I104 >= 0 ) {
		$I105 = WPref::load( 'PCATALOG_NODE_TAXEDITEDPRICE' );
		if ( $I105 != 1 ) {
			$this->value = $this->value + $this->getValue( 'tax' );
		}	}


	return parent::create();



}
}