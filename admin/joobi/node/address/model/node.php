<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_Node_model extends WModel {







	function addValidate() {



		if ( empty($this->uid) ) $this->uid = WUser::get('uid');
		$this->vendid = WGlobals::get('vendid');
		if(empty($this->vendid)){
			if ( WExtension::exist( 'vendor.node' ) ) {
				$I9E = WClass::get('vendor.helper',null,'class',false);
				if ( !empty( $this->uid ) ) $this->vendid = $I9E->getVendorID( $this->uid );
			}		}
		$this->setChild( 'address.members', 'uid', $this->uid );

		$this->setChild( 'address.members', 'ordering', 99 );



		return true;

	}





	function editValidate() {

				$this->longitude = 0;
		$this->latitude = 0;
		$this->found = 0;
		$this->lastcheck = 0;
		$this->mapservice = 0;

		return true;

	}




	function validate() {

				if ( !empty($this->premium) ) {
			if ( empty($this->uid) ) $this->uid = WUser::get('uid');
			$I9F = WModel::get( 'address' );
			$I9F->setVal( 'premium', 0 );
			$I9F->whereE( 'uid', $this->uid );
			$I9F->whereE( 'premium', 1 );
			$I9F->update();
		}

		$I9G = WClass::get( 'address.location' );
		$this->location = $I9G->createAddressLocation( $this );

		return true;

	}






	function deleteValidate($O1=null) {
		$this->_x = $this->load( $O1 );
		return true;
	}






	function deleteExtra($O2=0) {


		if ( !empty($this->_x->premium) ) {
									$this->whereE( 'uid', $this->_x->uid );
			$this->orderBy( 'modified', 'DESC' );
			$I9H = $this->load( 'lr', 'adid' );

			if ( !empty($I9H) ) {
				$this->whereE( 'adid', $I9H );
				$this->setVal( 'premium', 1 );
				$this->update();
			}		}

		return true;

	}
}