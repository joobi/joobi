<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_map_show_controller extends WController {

function show() {



	$I7K = WGlobals::get( 'adid' );

	if ( empty($I7K) ) return false;


	$this->dontDisplayView();

	if ( version_compare( phpversion(), '5.3', '<' ) ) {
		$I7L = WMessage::get();
		$I7L->userN('1373209094YQB');
		return true;
	}


	$I7M = WGlobals::get( 'width' );

	$I7N = WGlobals::get( 'height' );

	$I7O = WGlobals::get( 'streetView' );
	$I7P = WGlobals::get( 'heightstreet' );


	$I7Q = WClass::get( 'address.map' );

	$I7Q->setAddressID( $I7K );

	if ( !empty($I7M) ) $I7Q->setWidth( $I7M );

	if ( !empty($I7N) ) $I7Q->setHeight( $I7N );

	if ( !empty($I7O) ) $I7Q->showStreetView();
	if ( !empty($I7P) ) $I7Q->setStreetHeight( $I7P );
	$this->content = $I7Q->renderMap();





	return true;



}}