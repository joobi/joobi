<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_vendor_selectaddress_controller extends WController {
function selectaddress() {



	$I7R= WGlobals::get('adid');

	$I7S= WGlobals::get('vendid');



	if (!empty($I7S)) {

		$I7T = WModel::get( 'vendor' );

		$I7T->whereE('vendid', $I7S);

		$I7T->setVal('adid', $I7R);

		$I7T->update();

	}
	

	WPage::redirect('controller=address-vendor&task=listing&vendid='.$I7S);

	

	return true;



}}