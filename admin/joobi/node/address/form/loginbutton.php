<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


WLoadFile( 'main.form.submit' , JOOBI_DS_NODE );
class Address_Loginbutton_form extends WForm_submit {
	public function create() {
		$I4N = parent::create();

		$this->content = '<span class="btnL"><span class="btnR"><span class="btnC">' . $this->content . '</span></span></span>';

		$this->content = '<div class="bttnContinue">' . $this->content . '</div>';
		return $I4N;

	}}