<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_Checkshipout_button extends WButtons_external {


function create() {



	
	$this->setIcon( 'next' );



	
	if ( WGlobals::checkCandy(25) ) {

		$I7Y = WClass::get('basket.previous',null,'class',false);

		$I7Z = $I7Y->getBasket();



		if ( $I7Z->hasShipping ) {



			
			$this->setAction( 'shipping' );

			return true;



		}
	}


	
	$this->setAction( 'checkout' );

	return true;

}}