<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;







class Address_Header_class extends WClasses {







	function display($O1,$O2='none',$O3=''){
		$I5E = '<H2>'.TR1206961913QQBR.'</h2>';

				$I5F = WGlobals::get( 'option' );
		if( !empty($I5F) ) $I5F = substr( $I5F, 4 );

				if( empty($I5F) ) $I5F = 'jstore';

		if($O2 == 'none'){
			$I5G = $this->getName($O1, $O2);
			return $I5E.'<a href="'.WPage::routeURL('controller='.$O1, '', 'default', '', true, $I5F).'">'.$I5G.'</a>&nbsp;<img src="'.JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/fleche.png" alt="arrow" />';
		}else{
			$I5G = $this->getName($O1);
			$I5H = $this->getName($O1, $O2);
			$I5I = '<a href="'.WPage::routeURL('controller='.$O1, '', 'default', '', true, $I5F).'">'.$I5G.'</a>&nbsp;<img src="'.JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/fleche.png" alt="arrow" />&nbsp;<a href="#">'.$I5H.' '.$O3.'</a>';
			return $I5E . $I5I;
		}
	}
	function getName($O4,$O5='none'){
		if(isset($O4)){
			$I5J = WModel::get('controller','object');
			$I5J->whereE('app',$O4);
			$I5J->whereE('publish',1);
			$I5J->whereE('admin',0);
			if($O5 != 'none'){
				$I5J->whereE('task',$O5);
			}else{
				$I5J->whereE('premium',1);
			}
			$I5K = $I5J->load('o','yid');
			if(isset($I5K)){
				$I5L = WModel::get('viewtrans');
				$I5L->whereE('yid',$I5K->yid);
								$I5L->whereLanguage( 1 );
				$I5E = $I5L->load('o','name');
				if(isset($I5E)){
					return $I5E->name;
				}
			}		}	}}