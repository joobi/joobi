<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_Helper_class extends WClasses {












public function getAddress($O1,$O2='*') {

	static $I4Y = array();


	if ( !isset( $I4Y[$O1] ) ) {

		static $I4Z = null;

		if ( empty( $I4Z ) ) $I4Z = WModel::get( 'address' );
		$I4Z->makeLJ( 'countries', 'ctyid');
		$I4Z->select( 'name', 1, 'country' );

		$I4Z->select( 'isocode2', 1, 'countryISO2' );
		$I4Z->select( 'isocode3', 1, 'countryISO3' );
		$I4Z->whereE( 'adid', $O1 );

		$I4Y[$O1] = $I4Z->load( 'o', $O2 );

	}


	return $I4Y[$O1];

}





public function getDefaultAddressID($O3=null) {
	static $I50 = array();

	if ( empty($O3) ) $O3 = WUser::get('uid');
	if ( empty($O3) ) return false;


	if ( !isset( $I50[$O3] ) ) {
		$I4Z = WModel::get( 'address' );
		$I4Z->whereE( 'premium', 1 );
		$I4Z->whereE( 'uid', $O3 );
		$I50[$O3] = $I4Z->load( 'lr', 'adid' );
	}
	return $I50[$O3];

}










	public function renderAddress($O4,$O5='html',$O6=true,$O7=true,$O8=false,$O9=true) {

		if ( is_numeric($O4) ) $I51 = $this->getAddress( $O4 );
		else $I51 = $O4;

		if ( empty($I51) ) return '';

		$I52 = '';
		if ( $O5 == 'html' ) {
			$I53 = '<br />';
		} else {
			$I53 = $O5;
		}
		if ( !empty($O8) ) {
			if ( $O8 === true ) $I52 .= $I51->name;
			else $I52 .= $O8;
		}
		$I54 = '';
		if ( !empty($I51->address1) ) $I54 .= $I51->address1;
		if ( !empty($I51->address2) ) $I54 .= ( !empty($I54)?$I53:'' ) . $I51->address2;
		if ( !empty($I51->address3) ) $I54 .= ( !empty($I54)?$I53:'' ) . $I51->address3;
		if ( !empty($I54) ) $I52 .= ( !empty($I52)?$I53:'' ) . $I54;

		$I55 = '';
		if ( !empty($I51->city) ) $I55 .= $I51->city;
		if ( !empty($I51->state) ) $I55 .= ' ' . $I51->state;
		if ( !empty($I51->zipcode) ) $I55 .=  ' ' . $I51->zipcode;
		$I55 = trim($I55);
		if ( !empty($I55) ) $I52 .= ( !empty($I52)?$I53:'' ) . $I55;

		if ( !empty($I51->ctyid) || !empty($I51->country) ) {
			$I56 = '';
			if ( $O7 && !empty($I51->ctyid) ) {
				$I57 = WClass::get( 'countries.helper', null, 'class', false );
				if ( !empty($I57) ) {
					$I56 .= $I57->getCountryFlag( $I51->ctyid );
				}			}			if ( empty( $I51->country ) && !empty($I51->ctyid) ) {
								if (empty($I57) ) $I57 = WClass::get( 'countries.helper', null, 'class', false );
				if ( !empty($I57) ) $I51->country = $I57->getData( $I51->ctyid, 'name' );
			}			if ( !empty($I51->country) ) {
				if ( !empty($I52) ) $I52 .= $I53;
				$I52 .= $I56;
				$I52 .= $I51->country;
			}		}
		if ( $O6 && !empty($I51->phone) ) {
			if ( !empty($I52) ) $I52 .= $I53 . TR1242282412EESC .':' . $I51->phone;
		}
		if ( $O9 && WGlobals::checkCandy(50) && !empty($I51->adid) ) {
			$I58 = TR1373210357FEOT;
			$I59 = '';
			$I5A = 700;
			$I5B = 600;
			$I5C = 0;
			if ( !empty($I51->address1) ) {
				$I59 .= '&streetView=1';
				$I5A = 700;
				$I5B = 500;
				$I5C = 250;
			}			$I5D = WPage::linkPopUp( 'controller=address-map&adid=' . $I51->adid . '&width='.$I5A.'&&height='.$I5B. $I59 );
			$I52 .= $I53 . WPage::createPopUpLink( $I5D, $I58, $I5A+25, $I5B+($I5C?$I5C+50:0)+25 );

		}
				if ( $O5=='html' && !empty($I52) ) {
			$I52 = '<p style="line-height:1.4em;">' . $I52 . '</p>';
		}
		return $I52;
	}

}