<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_Location_class extends WClasses {






public function createAddressLocation($O1) {

	if ( empty($O1) ) return false;

		$O1->location = '';
	if ( !empty( $O1->address1 ) ) $O1->location .= $O1->address1;
	if ( !empty( $O1->address2 ) ) $O1->location .= ( !empty($O1->location)?',':'' ) . $O1->address2;
	if ( !empty( $O1->address3 ) ) $O1->location .= ( !empty($O1->location)?',':'' ) . $O1->address3;

		$I7E = '';
	if ( !empty( $O1->city ) ) $I7E .= $O1->city;
	if ( !empty( $O1->state ) ) $I7E .= ( !empty($I7E)?',':'' ) . $O1->state;
	if ( !empty( $O1->zipcode ) ) $I7E .= ( !empty($I7E)?' ':'' ) . $O1->zipcode;
	if ( !empty( $I7E ) ) $O1->location .= ( !empty($O1->location)?',':'' ) . $I7E;

		if ( !empty( $O1->ctyid ) ) {
		$I7F = WClass::get( 'countries.helper', null, 'class', false );
		if ( !empty($I7F) ) {
			$I7G = $I7F->getData( $O1->ctyid );
			if ( !empty( $I7G ) ) {
				$O1->location .= ( !empty($O1->location)?',':'' ) . $I7G->name;
			}		}
	}
	return $O1->location;

}

}