<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_Map_class extends WClasses {

	private $_adid = array();

	private $_locationA = array();

	private $_coordinatesA = array();


		private $_addressO = array();

	private $_mapID = 'map';

	private $_width = 0;
	private $_height = 0;
	private $_heightStreet = 0;
	private $_widthStreet = 0;

	private $_mapType = '';

	private $_enableClustering = false;

	private $_showStreetView = true;

	private $_mainMap = '';
	private $_streetViewMap = '';


	private $_addressGeomapC = null;

	private $_mapInstance = null;










public function setAddressID($O1) {

	$this->_adid[] = $O1;

}





public function setAddress($O2) {
	$this->_locationA[] = $O2;
}







public function setCoordinates($O3,$O4,$O5='') {
	$I62 = new stdClass;
	$I62->longitude = $O3;
	$I62->latitude = $O4;
	$I62->location = $O5;

	$this->_coordinatesA[] = $I62;
}








public function setClustering($O6) {
	$this->_enableClustering = true;
	$this->_addressO = $O6;

}







public function setWidth($O7) {
	$this->_width = $O7;
}





public function setHeight($O8) {
	$this->_height = $O8;
}





public function setStreetHeight($O9) {
}







public function showStreetView($OA=true,$OB=0,$OC=0) {
	$this->_showStreetView = $OA;
	if ( !empty($OC) ) $this->_heightStreet = $OC;
	if ( !empty($OB) ) $this->_widthStreet = $OB;
}





public function renderMap() {

	$this->prepareMap();

	$I63 = $this->_mainMap;
	$I63 .= $this->_streetViewMap;
	return $I63;

}





public function renderMapOnly() {
	$this->prepareMap();
	return $this->_mainMap;
}




public function renderStreetView() {
	return $this->_streetViewMap;
}

private function l1c_E_65d() {

	static $I64 = 0;
	if ( version_compare( phpversion(), '5.3', '<' ) ) {
		$I65 = WMessage::get();
		$I65->adminE( 'PHP 5.3 is required for Geo Location module to function!' );
		return false;
	}
	WLoadFile( 'PHPGoogleMaps.Core.Autoloader', JOOBI_DS_INC );		$I66 = new SplClassLoader( 'PHPGoogleMaps', '..' );
	$I66->register();

	$I67 = array();
	$I68 = WGlobals::get( 'HTTPS', 'off', 'server' );
	if ( 'on' == $I68 ) $I67['use_https'] = true;

	$I69 = WLanguage::get( WUser::get('lgid'), 'code' );
	if ( 'en' != $I69 ) $I67['language'] = $I69;

	$I6A = WClass::get( 'main.credentials' );
	$I6B = $I6A->loadFromID( WPref::load( 'PADDRESS_NODE_GOOGLEKEY'), 'passcode' );
	if ( !empty($I6B) ) $I67['api_key'] = $I6B;

	$I64++;
	$this->_mapID = 'map_' . $I64;
	$I67['map_id'] = $this->_mapID;

	$this->_mapInstance = new \PHPGoogleMaps\Map( $I67 );

	return true;

}







public function prepareMap() {

		if ( !$this->l1d_F_5vt() ) {
				return false;
	}
	if ( empty($this->_mapInstance) ) {
		if ( !$this->l1c_E_65d() ) return false;
	}
	$I6C = array();
	$I6D = -200;
	$I6E = 200;
	$I6F = -200;
	$I6G = 200;
	$I6H = false;

	foreach( $this->_addressO as $I6I ) {
		if ( empty($I6I) ) continue;

		if ( $I6I->longitude < -180 || $I6I->longitude > 180 || $I6I->latitude < -180 || $I6I->latitude > 180 ) continue;

		$I6H = true;

		if ( $I6I->longitude > $I6D ) $I6D = $I6I->longitude;
		if ( $I6I->longitude < $I6E ) $I6E = $I6I->longitude;
		if ( $I6I->latitude > $I6F ) $I6F = $I6I->latitude;
		if ( $I6I->latitude < $I6G ) $I6G = $I6I->latitude;

		$I6J = $this->l1e_G_72y( $I6I );
		$this->_mapInstance->addObject( $I6J );

		$I6C[] = $I6J;

	}
	if ( ! $I6H ) return false;

	$this->_mapInstance->enableCompressedOutput();

		if ( count( $I6C ) <2 ) {
				$this->_mapInstance->setCenter( $I6C[0]->getPosition() );
		$this->_mapInstance->disableAutoEncompass();
		$this->_mapInstance->setZoom( 15 );
		if ( $this->_showStreetView ) $this->_mapInstance->enableStreetView( array( 'position'=>$I6C[0]->getPosition(), 'addressControl' => false, 'enableCloseButton' => true ), $this->_mapID . '_street' );

	} else {
				$this->_mapInstance->enableAutoEncompass();

		$I6K = ( $I6D + $I6E ) / 2;
		$I6L = ( $I6F + $I6G ) / 2;
		$this->_mapInstance->setCenterCoords( $I6L, $I6K );

	}

	if ( !empty( $this->_width ) ) $this->_mapInstance->setWidth( $this->_width );
	if ( !empty( $this->_height ) ) $this->_mapInstance->setHeight( $this->_height );
	if ( !empty( $this->_mapType ) )  $this->_mapInstance->setMapType( $this->_mapType );

		if ( $this->_enableClustering ) {


		$I6M = array(
			'maxZoom' => 10,
			'gridSize' => null
		);


	}

	WPage::addJS( $this->_mapInstance->getMapJS() );

	$this->_mainMap = $this->_mapInstance->getMap();



		if ( $this->_showStreetView && count($this->_addressO)<2 ) {
		$I6N = $this->_mapID . '_street';
		$I6O = ( !empty($this->_heightStreet) ? $this->_heightStreet : 300 );
		$I6P = ( !empty($this->_widthStreet) ? $this->_widthStreet : $this->_width );

		$I6Q = '#' .  $I6N  . '{width:'.$I6P.'px;height:'.$I6O.'px;clear:both;}';
		WPage::addCSSScript( $I6Q );
				$this->_streetViewMap = '<div id="' . $I6N . '"></div>';
	}


	$I6R = $this->_mapInstance->getHeaderJS();
	foreach( $I6R as $I6S ) WPage::addScript( $I6S );


	return true;

}

	private function l1e_G_72y($OD) {

		if ( empty($OD->location) ) $OD->location = '';

		$I6T = trim( !empty($OD->name) ? $OD->name : '' );
		if ( empty($I6T) || $I6T=='default' ) {
			$I6T = str_replace( array(',' ), ', ', $OD->location );
			$OD->name = $I6T;
		}		$I6U = WClass::get( 'address.helper' );
		$I6V = trim( $I6U->renderAddress( $OD, 'html', true, true, true, false ));
		if ( empty($I6V) || $I6V=='default' ) $I6V = $I6T;

		$I6W = array(
		'title'	=> $I6T,
		'content'	=> $I6V
		);


		$I6X = new PHPGoogleMaps\Core\LatLng( $OD->latitude, $OD->longitude, $OD->location);

		$I6Y = \PHPGoogleMaps\Overlay\Marker::createFromPosition( $I6X, $I6W );
		if ( !empty($OD->icon) ) {
		}
		return $I6Y;

	}





	public function getCoordinates($OE='',$OF=0,$OG=true) {

		if ( empty($OE) && empty($OF) ) return false;

		$I6Z = $this->l1f_H_4wv( $OE, $OF );
		if ( $I6Z === false || ( empty( $I6Z->longitude ) || $I6Z->longitude == 0
		|| empty( $I6Z->latitude ) || $I6Z->latitude == 0 )  ) {

			if ( empty($this->_mapInstance) ) {
				if ( !$this->l1c_E_65d() ) return false;
			}
												if ( empty($OE) && !empty($I6Z->location) ) $OE = $I6Z->location;
			if ( empty($OE) ) return false;
			$I70 = \PHPGoogleMaps\Service\Geocoder::geocode( $OE );

			if ( $I70 instanceof \PHPGoogleMaps\Service\GeocodeResult ) {
				
				$I6Z = $I70->getLatLng();
				$I6Z->latitude = $I6Z->lat;
				$I6Z->longitude = $I6Z->lng;
				unset($I6Z->lat);
				unset($I6Z->lng);

      		        		if ( empty( $OF ) ) {
        			$OF = $this->l1g_I_1ai( $OE, $I6Z->longitude, $I6Z->latitude );
        		} else {
        			$this->l1h_J_1tv( $OF, $I6Z->longitude, $I6Z->latitude, $OE );
        		}
        		$I6Z->adid = $OF;
			} else {
								WMessage::log( 'The location: ' . @$I6Z->location . ' could not be found by Google Map. Error: ' . @$I6Z->error, 'GoogleGeoLocationFailed' );
				if ( $OG ) {
					$I65 = WMessage::get();
					$I65->userN('1373210359RPSF');
				}
				$I6Z = new stdClass;
				$I6Z->latitude = 0;
				$I6Z->longitude = 0;
				$I6Z->location = $OE;

				return $I6Z;
			}
		}

		if ( empty( $I6Z ) ) return false;


		return $I6Z;

	}






private function l1d_F_5vt() {

		if ( !empty($this->_adid) ) {
		$I6U = WClass::get( 'address.helper' );
		foreach( $this->_adid as $I71 ) {
			$this->_addressO[$I71] = $I6U->getAddress( $I71 );
		}	}
	if ( !empty( $this->_locationA ) ) {

		$I6U = WClass::get( 'address.helper' );
		foreach( $this->_locationA as $I72 ) {
			$I73 = $this->getCoordinates( $I72 );
			if ( empty($I73) || empty($I73->adid) ) continue;
			$this->_addressO[$I73->adid] = $I6U->getAddress( $I73->adid );
		}
	}
		if ( !empty($this->_coordinatesA) ) {
		static $I74=0;
		$I74++;

		foreach( $this->_coordinatesA as $I75 ) {
			$I76 = 'key'.$I74;
			$this->_addressO[$I76] = $I75;
		}	}

	if ( empty($this->_addressO) ) return false;
			if ( !$this->l1i_K_3xy() ) return false;
	return true;

}












    private function l1g_I_1ai($OH,$OI,$OJ) {

    	if ( empty($OH) || empty($OI) || empty($OJ) ) return false;

    	$OH = str_replace( array(' ,', ', ' ), ',', $OH );

    	$I77 = WModel::get( 'address' );
    	$I77->setVal( 'location', $OH );
    	$I77->setVal( 'longitude', $OI );
    	$I77->setVal( 'latitude', $OJ );
	    $I77->setVal( 'found', 1 );
	    $I77->setVal( 'lastcheck', time() );
	    $I77->setVal( 'mapservice', 1 );
    	$I77->insertIgnore();

    	$I78 = $I77->lastId();
    	return ( !empty($I78) ? $I78 : null );

    }








    private function l1f_H_4wv($OK,$OL=0) {
    	static $I79 = array();
                $OK = str_replace( array(' ,', ', ' ), ',', $OK );

        if ( !empty($I79[$OK]) ) return $I79[$OK];

        $I77 = WModel::get( 'address' );
        $I77->select( 'longitude' );	        $I77->select( 'latitude' );	        $I77->select( 'location' );
        $I77->select( 'adid' );
        if ( !empty($OL) ) {
        	$I77->whereE( 'adid', $OL );
        } else {
        	$I77->whereE( 'location', $OK );
        }        $I7A = $I77->load( 'o' );
        if ( empty($I7A) ) return false;

        $I79[$OK] = $I7A;

        return $I7A;

    }









    private function l1h_J_1tv($OM,$ON,$OO,$OP) {

    	if ( empty($OM) || empty($ON) || empty($OO) ) return false;

    	$I77 = WModel::get( 'address' );
    	$I77->whereE( 'adid', $OM );

	    $I77->setVal( 'location', $OP );
    	$I77->setVal( 'longitude', $ON );
    	$I77->setVal( 'latitude', $OO );
	    $I77->setVal( 'found', 1 );
	    $I77->setVal( 'lastcheck', time() );
	    $I77->setVal( 'mapservice', 1 );
	    $I77->update();

    	return true;

    }
private function l1i_K_3xy() {

	if ( empty($this->_addressO) ) return false;
	foreach( $this->_addressO as $I78 => $I7B ) {

		if ( empty($I7B) ) continue;

		if ( !empty( $I7B->longitude ) && $I7B->longitude != 0
		&& !empty( $I7B->latitude ) && $I7B->latitude!= 0 ) {
			continue;
		}
		

						if ( !empty( $I7B->lastcheck ) && time() > ( $I7B->lastcheck + 7776000 ) ) {
						unset( $this->_addressO[$I78] );
		}
				if ( empty($I7B->location) ) {
			$I7C = WClass::get( 'address.location' );
			$I7B->location = $I7C->createAddressLocation( $I7B );
			$this->_addressO[$I78]->location = $I7B->location;
		}

				$I7D = ( !empty($I7B->adid) ? $I7B->adid : 0 );
		$I73 = $this->getCoordinates( $I7B->location, $I7D );

				if ( empty($I7B->latitude) || $I7B->latitude == 0 || $I7B->latitude < -200 || $I7B->latitude > 200 ) $I7B->latitude = $I73->latitude;
		if ( empty($I7B->longitude) || $I7B->longitude == 0 || $I7B->longitude < -200 || $I7B->longitude > 200 ) $I7B->longitude = $I73->longitude;

		if ( empty($I73) ) {
						unset( $this->_addressO[$I78] );
		}
	}
	if ( empty($this->_addressO) ) return false;

    return true;

}

}