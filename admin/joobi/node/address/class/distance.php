<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_Distance_class extends WClasses {


public $radLat; public $radLon; 
public $degLat;	public $degLon; 
const EARTHS_RADIUS_KM = 6371.01;
const EARTHS_RADIUS_MI = 3958.762079;

protected static $MIN_LAT; protected static $MAX_LAT; protected static $MIN_LON; protected static $MAX_LON; 
public function __construct() {
	self::$MIN_LAT = deg2rad(-90); 	self::$MAX_LAT = deg2rad(90); 	self::$MIN_LON = deg2rad(-180); 	self::$MAX_LON = deg2rad(180); }












public function getMixMaxLongLatFromLocationRadius($O1,$O2,$O3='km') {

	if ( empty($O1) || empty($O2) ) return false;

		$I5M = WClass::get( 'address.map' );
	$I5N = $I5M->getCoordinates( $O1, 0, false );
		if ( empty($I5N) ) return false;

	return $this->getMixMaxLongLatFromCoordinateRadius( $I5N->longitude, $I5N->latitude, $O2, $O3 );

}











public function getMixMaxLongLatFromCoordinateRadius($O4,$O5,$O6,$O7='km') {

	if ( empty($O4) || empty($O5) || empty($O6) || $O4==0 || $O5==0 ) return false;

		if ( 'mi' == $O7 || 'miles' == $O7 ) {
		$O6 = Address_Distance_class::MilesToKilometers( $O6 );
		$O7='km';
	}
		$I5O = Address_Distance_class::fromDegrees( $O5, $O4 );
	if ( empty($I5O) ) return false;

	$I5P = $I5O->boundingCoordinates( $O6, $O7 );
	$I5Q = new stdClass;
	$I5Q->minCoordinate = new stdClass;
	$I5Q->minCoordinate->longitude = $I5P[0]->degLon;
	$I5Q->minCoordinate->latitude = $I5P[0]->degLat;
	$I5Q->maxCoordinate = new stdClass;
	$I5Q->maxCoordinate->longitude = $I5P[1]->degLon;
	$I5Q->maxCoordinate->latitude = $I5P[1]->degLat;

	return $I5Q;

}





public static function fromDegrees($O8,$O9) {
	$I5R = new Address_Distance_class();
	$I5R->radLat = deg2rad($O8);
	$I5R->radLon = deg2rad($O9);
	$I5R->degLat = $O8;
	$I5R->degLon = $O9;
	$I5S = $I5R->checkBounds();

	if ( false === $I5S ) return false;

	return $I5R;

}





public static function fromRadians($OA,$OB) {

	$I5R = new Address_Distance_class();
	$I5R->radLat = $OA;
	$I5R->radLon = $OB;
	$I5R->degLat = rad2deg($OA);
	$I5R->degLon = rad2deg($OB);
	$I5S = $I5R->checkBounds();

	if ( false === $I5S ) return false;

	return $I5R;
}
protected function checkBounds() {

	if ( $this->radLat < self::$MIN_LAT || $this->radLat > self::$MAX_LAT ||
	$this->radLon < self::$MIN_LON || $this->radLon > self::$MAX_LON ) {
		$I5T = WMessage::get();
		$I5T->codeE( 'Invalid Argument in checkBounds' );
		return false;
	}

}









public function distanceTo($OC,$OD) {

	$I5U = $this->getEarthsRadius($OD);

	return acos(sin($this->radLat) * sin($OC->radLat) +
	cos($this->radLat) * cos($OC->radLat) *
	cos($this->radLon - $OC->radLon)) * $I5U;

}



public function getLatitudeInDegrees() {
	return $this->degLat;
}



public function getLongitudeInDegrees() {
	return $this->degLon;
}



public function getLatitudeInRadians() {
	return $this->radLat;
}



public function getLongitudeInRadians() {
	return $this->radLon;
}
public function __toString() {
	return "(" . $this->degLat . ", " . $this->degLong . ") = (" .
	$this->radLat . " rad, " . $this->radLon . " rad";
}

































public function boundingCoordinates($OE,$OF='km') {

	$I5U = $this->getEarthsRadius($OF);
	if ($I5U < 0 || $OE < 0) throw new Exception('Arguments must be greater than 0.');

		$I5V = $OE / $I5U;

	$I5W = $this->radLat - $I5V;
	$I5X = $this->radLat + $I5V;

	$I5Y = 0;
	$I5Z = 0;
	if ($I5W > self::$MIN_LAT && $I5X < self::$MAX_LAT) {
	$I60 = asin(sin($I5V) /
	cos($this->radLat));
	$I5Y = $this->radLon - $I60;
	if ($I5Y < self::$MIN_LON) $I5Y += 2 * pi();
	$I5Z = $this->radLon + $I60;
	if ($I5Z > self::$MAX_LON) $I5Z -= 2 * pi();
	} else {
		$I5W = max($I5W, self::$MIN_LAT);
	$I5X = min($I5X, self::$MAX_LAT);
	$I5Y = self::$MIN_LON;
	$I5Z = self::$MAX_LON;
	}

	return array(
		Address_Distance_class::fromRadians($I5W, $I5Y),
		Address_Distance_class::fromRadians($I5X, $I5Z)
	);

}











public static function MilesToKilometers($OG) {
	return $OG * 1.6093439999999999;
}
public static function KilometersToMiles($OH) {
	return $OH * 0.621371192237334;
}
protected function getEarthsRadius($OI) {
	$I61 = $OI;
	if ( $I61 == 'miles' || $I61 == 'mi' ) return $I5U = self::EARTHS_RADIUS_MI;
	elseif ($I61 == 'kilometers' || $I61 == 'km') return $I5U = self::EARTHS_RADIUS_KM;

	else throw new Exception('You must supply a valid unit of measurement');
}
}