<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_Address_user_registration_view extends Output_Forms_class {
function prepareView() {



	$I4C = WUser::get( 'uid' );

	if ( !empty($I4C) ) {

		$this->removeElements( array( 'address_user_registration_name', 'address_user_registration_username', 'address_user_registration_email', 'address_user_registration_password', 'address_user_registration_confirmpassword' ) );

	}




	$this->l1b_D_2ad();



	return true;



}


















private function l1b_D_2ad() {	


	$I4D = WClass::get( 'basket.tail' , null, 'class', false );

	if ( $I4D ) $I4E = $I4D->display( 2 );	
	else {

		$I4F = WMessage::get();


		$I4F->codeE( 'Class basket_tail does not exist, trace function _setProcessTail' );

		return false;

	}


	$this->setValue( 'processtail', $I4E );

	return true;

}}