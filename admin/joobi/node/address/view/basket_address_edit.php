<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_Basket_address_edit_view extends Output_Forms_class {
protected function prepareView() {



		$I49 =  WPref::load( 'PBASKET_NODE_ZONEUSESTATES' );



		if ( empty($I49) ) {

			$this->removeElements( array('basket_edit_address_stateid_picklist' ) );

		} else {

			$this->removeElements( array('basket_edit_address_state_input' ) );

		}


	return true;



}}