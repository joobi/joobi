<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Address_Address_list_change_basket_view extends Output_Listings_class {
protected function prepareView() {



	$I4A = WGlobals::get('selectedid');

	$I4B = WGlobals::get('basketaddtype');



	if ( !empty($I4A) ) WGlobals::setSession( 'checkoutEditAddress', 'selectedid', $I4A );

	else WGlobals::set( 'selectedid', WGlobals::getSession( 'checkoutEditAddress', 'selectedid' ) );



	if ( !empty($I4B) ) WGlobals::setSession( 'checkoutEditAddress', 'basketaddtype', $I4B );

	else WGlobals::set( 'basketaddtype', WGlobals::getSession( 'checkoutEditAddress', 'basketaddtype' ) );



	return true;



}}