<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Coupons_Helper_class extends WClasses {






	public function loadCoupon($O1) {
		static $IM9 = array();

		if ( empty($O1) ) return false;

		if ( !isset( $IM9[$O1] ) ) {
			$IMA = WModel::get( 'coupons' );
			if ( is_numeric($O1) ) $IMA->whereE( 'coupid', $O1 );
			else  $IMA->whereE( 'namekey', $O1 );

			$IMB = $IMA->load( 'o' );
			WTools::getParams( $IMB );

			$IM9[$O1] = $IMB;
		}
		return $IM9[$O1];

	}
}