<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;

class Coupons_Ownership_class extends WClasses {






	public function isOwner($O1) {

		if ( empty($O1) ) return false;

		$IMC = WClass::get( 'vendor.helper' );
		$IMD = $IMC->getVendorID();

		$IME = WModel::get( 'coupons' );
		if ( is_array($O1) ) $IME->whereIn( 'coupid', $O1 );
		else $IME->whereE( 'coupid', $O1 );
		$IME->whereE( 'vendid', $IMD );
		return $IME->exist();


	}
}