<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Generate_class extends WClasses {

	private $_couponO = null;

	private $_voucherO = null;

	private $_location = '';	








public function generateHTML($O1) {

	$this->l1d_16_zm( $O1 );
	if ( empty($this->_couponO->themeid) ) return false;

	$ILH = $this->l1e_17_6tt();


	return $ILH;

}









public function generatePDF($O2,$O3=null) {

	if ( empty($O2) ) return false;

	if ( !empty($O3) ) $this->_voucherO = $O3;

		$ILI = WExtension::exist( 'tcpdf.includes' );
	if ( empty($ILI) ) {
		$ILJ = WMessage::get();
		$ILJ->userE('1359430178PKKA');
		return false;
	} else {

		WLoadFile('tcpdf.tcpdf',JOOBI_DS_INC );

	}
	$this->l1d_16_zm( $O2 );
	if ( empty($this->_couponO->themeid) ) return false;
	$ILK = $this->l1e_17_6tt();

		$ILL = preg_replace( '#[^a-z0-9\.\_]#i', '_' , strtolower( $this->_couponO->namekey ) );
	$this->_location = JOOBI_DS_MEDIA . 'coupons' . DS . $this->_couponO->vendid . DS . $ILL . '.pdf';

	$this->l1f_18_48e( $this->_location, $ILK );

		$this->l1g_19_63n( $ILL );


	return $ILL;

}





	private function l1e_17_6tt() {

		
				$ILM = new stdClass;
		$ILM->code = $this->_couponO->namekey;
		$ILM->name = $this->_couponO->name;
		$ILM->description = $this->_couponO->description;
		$ILM->expirationDate = WApplication::date( JOOBI_DATE3, $this->_couponO->publishend );
		$ILM->expirationText = TR1236926618OAPP;
		$ILM->valideDate = WApplication::date( JOOBI_DATE3, $this->_couponO->publishstart );
		$ILM->valideText = TR1369750699CNMY;
		$ILM->maxUseText = TR1229653442FEQY;
		$ILM->maxUseValue = $this->_couponO->numperuser;

		$ILM->userID = $this->_couponO->uid;
		$ILM->orderID = $this->_couponO->oid;

		if ( !empty($this->_couponO->vendid) ) {
						$ILN = WClass::get( 'vendor.helper' );
			$ILO = $ILN->getVendor( $this->_couponO->vendid );
		}				if ( !empty( $ILO ) ) {
			$ILM->vendorName = $ILO->name;
			$ILM->vendorPhone = $ILO->phone;
			$ILP = $ILO->originaddress;
			$ILP .= '<br>' . $ILO->originaddress2;
			$ILP .= '<br>' . $ILO->origincity . ', ' . $ILO->originstate . ' ' . $ILO->originzipcode;
			$ILM->vendorAddress = $ILP;
			if ( !empty( $ILO->filid ) ) {
				$ILQ = WClass::get( 'files.helper' );
				$ILR = JOOBI_URL_MEDIA . $ILQ->getURL( $ILO->filid );
			} else {
				$ILR = '';
			}
			$ILM->vendorAvatarLink = $ILR;
		}

		switch( $this->_couponO->type ) {
			case 2:					$ILM->discountText = TR1206961911NYAO;
				$ILM->discountValue = $this->_couponO->value . ' %';
				break;
			case 3:					$ILM->discountText = TR1359430278ARKQ;
				$ILM->discountValue = WTools::format( $this->_couponO->price, 'money', $this->_couponO->curid );
				break;
			case 1:					$ILM->discountText = TR1240389053CNDF;
				$ILM->discountValue = WTools::format( $this->_couponO->price, 'money', $this->_couponO->curid );
				break;
			case 7:					$ILM->discountText = '';
				$ILM->discountValue = '';

				$ILI = WExtension::exist( 'qrcode.includes' );

				if ( empty($ILI) ) {
					$ILJ = WMessage::get();
					$ILJ->userE('1359477057BKAQ');
					return false;
				} else {
					if ( WPref::load( 'PCOUPONS_NODE_USEQRCODE') ) {
						WLoadFile('qrcode.qrcode', JOOBI_DS_INC );
						if ( class_exists('Inc_Qrcode_include') ) {
														$ILS = WPage::routeURL( 'controller=coupons&task=show&eid=' . $this->_couponO->coupid, 'home' );
							$ILT = WPref::load( 'PCOUPONS_NODE_QRCODESIZE' );
							$ILU = $this->_couponO->namekey;
							$ILV = JOOBI_DS_MEDIA . 'qrcode' . DS . $this->_couponO->vendid;

			 				$ILW = new Inc_Qrcode_include( $ILS, 'jpg', $ILT, $ILU, $ILV );
			     			$ILX = $ILW->saveImage();

			     			$ILY = WGet::file();
			     			if ( $ILY->exist( $ILV . DS . $ILU . '.jpg' ) ) {
								$ILZ = JOOBI_URL_MEDIA . 'qrcode/' . $this->_couponO->vendid .'/'. $ILU . '.jpg';
				     			$ILM->qrcode = '<img style="width: '.$ILT.'px; height: '.$ILT.'px;" alt="QR Code" src="'. $ILZ . '">';
			     			} else {
				     			$ILM->qrcode = '';
			     			}						}					}
				}				break;

		}
		$IM0 = WPage::theme( $this->_couponO->themeid, '' );
		$IM0->file = 'default.php';
		$IM0->load();
		$IM0->setParameters( $ILM );
		$ILH = $IM0->display();

		return $ILH;

	}






	private function l1d_16_zm($O4) {

		$IM1 = WClass::get( 'coupons.helper' );
		$this->_couponO = $IM1->loadCoupon( $O4 );

	}







	private function l1g_19_63n(&$O5) {

		$IM2 = WGet::file();
	    $ILT = $IM2->size( $this->_location );
		$IM3 = WModel::get( 'files' );
		$IM3->saveOnlyData( $this->_couponO->namekey . '.pdf',  'coupons' . DS .  $this->_couponO->vendid );
		$IM3->alias = 'Voucher: ' . $this->_couponO->namekey;
		$IM3->mime = 'application/pdf';
		$IM3->size = $ILT;
		$IM3->secure = 0;
		$IM3->md5 = '';
		$IM3->core = 0;
		$IM3->storage = 0;
		$IM3->returnId();


		$IM3->save();

		if ( empty( $IM3->filid ) ) return false;

		if ( $IM3->name != $O5 ) {
						$IM2 = WGet::file();
			$IM4 = JOOBI_DS_MEDIA . 'coupons' . DS . $this->_couponO->vendid . DS . $IM3->name . '.pdf';
	    	$IM2->move( $this->_location, $IM4, '', true );

	    	$O5 = $IM3->name;

		}
				$IM5 = WModel::get( 'coupons' );
		$IM5->whereE( 'coupid', $this->_couponO->coupid );
		$IM5->setVal( 'filid', $IM3->filid );
		$IM6 = $IM5->update();



		return $IM6;

	}







	private function l1f_18_48e($O6,$O7) {	
		$IM7 = 'LETTER';	
	    $IM8 = new TCPDF( PDF_PAGE_ORIENTATION, PDF_UNIT, $IM7, true, 'UTF-8', false );

	    $IM8->SetCreator( PDF_CREATOR );
	    $IM8->SetAuthor( JOOBI_SITE_NAME );
	    $IM8->SetTitle( $this->_couponO->name );
	    $IM8->SetSubject( $this->_couponO->description );






		$IM8->AliasNbPages();
	    $IM8->AddPage();

	    $IM8->writeHTML( $O7, true );		    $IM8->lastPage();

	    	    	    $IM2 = WGet::file();
	    if ( !$IM2->exist() ) $IM2->write( $O6, '' );


	    $IM8->Output( $O6 , 'F' );

	    return true;


	}

}