<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Coupons_Template_list_picklist extends WPicklist {
function create() {



	$ILE = WModel::get( 'coupons' );

	$ILE->whereE( 'theme', 9 );

	$ILE->select( array( 'coupid', 'alias' ) );

	$ILE->orderBy( 'name' );

	$ILF = $ILE->load( 'ol' );



	$this->addElement( '0', ' - ' . TR1358529659NPBB . ' - ' );



	if ( !empty( $ILF ) ) {

		foreach( $ILF as $ILG ) {



			$this->addElement( $ILG->coupid, $ILG->alias );



		}




	}


	return true;

}}