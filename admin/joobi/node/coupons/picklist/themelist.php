<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;





class Coupons_Themelist_picklist extends WPicklist {


	function create() {



		$ILB = WModel::get( 'theme' );







		$ILB->select( 'alias' );



		$ILB->whereE( 'publish', 1 );

		$ILB->whereE( 'type', 107 );

		$ILB->select( 'alias', 0 );

		$ILB->orderBy( 'premium', 'DESC' );

		$ILC = $ILB->load( 'ol', 'tmid' );



		$this->addElement( '0', ' - ' . TR1359430278ARKP . ' - ' );



		if ( empty( $ILC ) ) return true;



		
		foreach( $ILC as $ILD ) {



			$this->addElement( $ILD->tmid, $ILD->alias );



		}




		return true;



	}}