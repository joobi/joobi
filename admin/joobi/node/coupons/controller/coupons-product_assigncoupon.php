<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_product_assigncoupon_controller extends WController {




function assigncoupon()

{

	$INO = WGlobals::get( 'coupid' );

	$INP = WGlobals::get( 'catid' );

	$INQ = WGlobals::getEID( true );



	if ( !empty( $INO ) && !empty( $INQ ) )

	{

		foreach( $INQ as $INR )

		{

			static $INS=null;

			static $INT=null;



			
			if (empty($INS)) $INS = WModel::get( 'coupons.products' );

			$INS->whereE( 'pid', $INR);

			$INS->whereE( 'coupid', $INO);

			$INU = $INS->load( 'o' );



			if ( empty( $INU ) )

			{

				
				if (empty($INS))$INS = WModel::get( 'coupons.products' );

				$INS->setVal( 'pid', $INR );

				$INS->setVal( 'coupid', $INO );

				$INS->insert();



				






				

			}

			else

			{

				
				if (empty($INS)) $INS = WModel::get( 'coupons.products' );

				$INS->whereE( 'pid', $INR );

				$INS->whereE( 'coupid', $INO );

				$INS->delete();



				




			}
		

			$this->l1h_1A_66z( $INO );

		}
	}


	$INV = WGlobals::get( 'titleheader' );

	if ( $INP == null ) $INP = 0;



	
	WPage::redirect( 'controller=coupons-product&task=listing&catid='. $INP .'&coupid='. $INO . '&titleheader=' . $INV );

	return true;

}








function l1h_1A_66z($O1) {

	$INW = WClass::get( 'item.helper' );



	
	$INX = new stdClass;

	$INX->tablename = 'coupons.products';

	$INX->column = 'pid';

	$INX->groupBy = 'coupid';

	$INX->filterCol1 = 'coupid';

	$INX->filterVal1 = $O1;

	$INY = $INW->noOfItem( $INX );



	
	if ( empty($INY) ) $INY = 0;

	if ( is_numeric($INY) )

	{		

		if ( !isset($INZ) ) $INZ = WModel::get( 'coupons' );

		$INZ->setVal( 'product', $INY );

		$INZ->whereE('coupid', $O1);

		$INZ->update();

	}


	return true;

}}