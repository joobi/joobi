<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;






class Coupons_send_controller extends WController {


function send() {

	$INH = WGlobals::get( 'trucs' );



	if( isset($INH['x']['user']) )

	{

		$INI = WMail::get();

		$INJ = $INH['x']['subject'];

		$INK = $INH['x']['content'];

		

		if( $INH['x']['user'] == 0 )

		{

			$INL = WModel::get( 'users' );

			$INM = $INL->load( 'lra', 'uid' );

			

			if( !empty($INM) )

			{

				foreach( $INM as $INN )

				{

					$INI->sendTextNow( $INN, $INJ, $INK );

				}
			}
		}

		else

		{

			foreach( $INH['x']['user'] as $INN )

			{

				$INI->sendTextNow( $INN, $INJ, $INK );

			}
		}
	}
	

	WPage::redirect( 'controller=coupons' );

	return true;

}


}