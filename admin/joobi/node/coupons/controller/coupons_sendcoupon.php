<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Coupons_sendcoupon_controller extends WController {


function sendcoupon() {
	

	$IN3 = WGlobals::getEID();

	

	static $IN4=null;

	if(empty($IN4)) $IN4 = WModel::get( 'coupons' );

	$IN4->whereE( 'coupid', $IN3 );

	$IN5 = $IN4->load( 'o' );



	if ( !defined('CURRENCY_USED') ) {

		$IN6 = WClass::get('currency.format',null,'class',false);

		$IN6->set();

	}


	if( !empty($IN5))

	{

		$IN7 = TR1240389053CNDD;

	

		$IN8 = 'Coupon Info<br>';

		$IN8 .= TR1206732392OZVB .' : '. $IN5->name .'<br>';

		$IN8 .= TR1206961912MJQB .' : '. $IN5->namekey .'<br>';

		$IN8 .= TR1206732392OZVD .' : ' ;

		if($IN5->type == 2)

		{

			$IN8 .= TR1240389053CNDE .'<br>';

			$IN8 .= TR1206961878BMAO .' : '. (real)$IN5->value .'%<br>';

		}

		else 

		{	

			if( $IN5->type == 1) $IN8 .=  TR1240389053CNDF .'<br>';

			else $IN8 .= TR1240389053CNDG .'<br>';

			$IN8 .= TR1206961878BMAO .' : '. WTools::format( $IN5->value, 'money', CURRENCY_USED) .'<br>';

		}
		if($IN5->publishend > 0) $IN8 .= TR1240389053CNDH .' : '. WApplication::date( JOOBI_DATE5, $IN5->publishend) .'<br>';

		else $IN8 .= TR1240389053CNDH .' : '. TR1237967968ABUO .'<br>';

				

		WPage::redirect( 'controller=coupons&task=sendform&coupid='. $IN3 .'&titleheader='. $IN5->name .'&subject='. $IN7 .'&content='. $IN8 );

	}

	else

	{

		WPage::redirect( 'controller=coupons&task=sendform&coupid='. $IN3 );

	}
	return true;

}















































































}