<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_user_assigncoupon_controller extends WController {
function assigncoupon()

{

	$IO6 = WGlobals::get( 'coupid' );

	$IO7 = WGlobals::getEID( true );



	if ( !empty( $IO6 ) && !empty( $IO7 ) )

	{

		foreach( $IO7 as $IO8 )

		{

			static $IO9=null;

			static $IOA=null;



			
			if (empty($IO9)) $IO9 = WModel::get( 'coupons.user' );

			$IO9->whereE( 'uid', $IO8);

			$IO9->whereE( 'coupid', $IO6);

			$IOB = $IO9->load( 'o' );



			if ( empty( $IOB ) )

			{

				
				if (empty($IO9)) $IO9 = WModel::get( 'coupons.user' );

				$IO9->setVal( 'uid', $IO8 );

				$IO9->setVal( 'coupid', $IO6 );

				$IO9->insert();



				




			}

			else

			{

				
				if (empty($IO9)) $IO9 = WModel::get( 'coupons.user' );

				$IO9->whereE( 'uid', $IO8 );

				$IO9->whereE( 'coupid', $IO6 );

				$IO9->delete();



				




			}
		}
		

		$this->l1i_1B_1fh( $IO6 );

	}

	else

	{

		$IOC = WMessage::get();

		$IOC->userE('1237800152RRQF');

	}


	$IOD = WGlobals::get( 'titleheader' );

	
	WPage::redirect( 'controller=coupons-user&task=listing&coupid='. $IO6 . '&titleheader=' . $IOD );

	return true;

}








function l1i_1B_1fh($O1) {

	$IOE = WClass::get( 'item.helper' );



	
	$IOF = new stdClass;

	$IOF->tablename = 'coupons.user';

	$IOF->column = 'uid';

	$IOF->groupBy = 'coupid';

	$IOF->filterCol1 = 'coupid';

	$IOF->filterVal1 = $O1;

	$IOG = $IOE->noOfItem( $IOF );



	
	if ( empty($IOG) ) $IOG = 0;

	if ( is_numeric($IOG) )

	{		

		if ( !isset($IOH) ) $IOH = WModel::get( 'coupons' );

		$IOH->setVal( 'user', $IOG );

		$IOH->whereE('coupid', $O1);

		$IOH->update();

	}


	return true;

}}