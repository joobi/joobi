<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_save_controller extends WController {


	function save() {


				if ( !IS_ADMIN ) {
			if ( !WPref::load( 'PVENDORS_NODE_ALLOWCOUPON' ) ) return false;
		}
		return parent::save();


	}
}