<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Node_model extends WModel {

	private $_noMessage = false;	

	function addValidate() {



		if ( empty($this->namekey) ) {

			$this->namekey = $this->genNamekey( '', 50, 'CP' );

		}


		$this->namekey = strtoupper(  $this->namekey );

		$this->namekey = str_replace( ' ', '', $this->namekey );




		$IQF = WModel::get('coupons');

		$IQF->whereE( 'namekey', trim($this->namekey) );

		if ( !empty($this->coupid) ) $IQF->where( 'coupid', '!=', $this->coupid );

		$IQG = $IQF->load('lr', 'coupid');



		$IQH = WMessage::get();

		if ( !empty($IQG) ) {

			$IQH->historyW('1360250260RNXV');

			return true;

		}


		if ( empty($this->type) ) $this->type = 0;



		
		if ( $this->type == 1 ) {

			if ( $this->nombre != 1 ) {

				$this->nombre = 1;

				$IQH->userN('1229653441KEYY');

			}
		} else {

			if ( $this->nombre < 0 ) $this->nombre = '-1';

		}


		$IQI = $this->name;

		if ( 7 != $this->type && $this->allcategory_product < 1 ) {

			$IQH->userW('1357789160SJWT',array('$NAME'=>$IQI));

		}




		if ( !$this->_noMessage && ( IS_ADMIN && WGlobals::checkCandy(50) ) && ( $this->alluser < 1 ) ) {
			$IQH->userW('1241752386FJGL',array('$NAME'=>$IQI));

		}




		return true;



	}












	function validate() {





		if ( empty($this->description) ) $this->description = $this->name;

		if ( empty($this->comment) ) $this->comment = $this->description;

		if ( empty($this->alias) ) $this->alias = $this->name;





		
		$this->validateDate( 'publishstart', time() );

		$this->validateDate( 'publishend' );

				if ( !empty($this->publishend) && ( $this->publishend > 0 ) && ( $this->publishstart > $this->publishend ) ) {
			$IQH = WMessage::get();
			$IQH->userN('1298350794SKCG');
		}


		if ( !empty( $IQJ ) && !empty( $IQI ) ) {
			$IQJ = $this->namekey;
			$IQI = $this->name;
			$IQH = WMessage::get();
			$IQH->userS('1336526155SAPD',array('$NAME'=>$IQI,'$CODE'=>$IQJ));

		}
		$this->skipMessage();





		
		if ( !IS_ADMIN ) {

			
			$IQK = WClass::get( 'vendor.helper' );

			$this->vendid = $IQK->getVendorID();

		}




		return true;



	}






	function editValidate() {


		if ( !empty($this->type) && $this->type == 2 ) {
						if ( !empty($this->value) && $this->value > 100 ) {
				$this->value = 100;
			}		}
		return true;



	}




	function copyValidate() {



		$this->namekey = $this->namekey . '-' . base_convert( time() , 10, 36 );

		$this->_noMessage = true;
				$this->theme = 1;


		return true;


	}

}