<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */

defined('_JEXEC') or die;











class Coupons_Expiredateoi_form extends WForms_standard {




function show() {

	static $IKT = null;

	if (!isset($IKT)) $IKT = time();



	if($this->value > 0) {

		if ( $IKT < $this->value ) $this->content = WApplication::date( JOOBI_DATE8, $this->value);

		else $this->content = '<span style="color:red;">'. WApplication::date( JOOBI_DATE8, $this->value) .'</span>';

	}

	else

	{

		$this->content = TR1237967968ABUO;

	}
		

	return true;

}}