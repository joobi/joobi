<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;














class Coupons_basketfield_form extends WForms_standard {
	




	function create() {

		$this->content =  '<input id="input_coupon'.$this->idLabel.'" class="'.$this->element->classes.'" type="text" value="" size="15" name="trucs[0][x][couponinput]"/>';
		$this->content .= '<div id="'.$this->idLabel.'_div" class="coupondiv">';
		$this->content .= WView::tooltip($this->element->description, $this->element->name, TR1208359284QWMZ, ''.$this->idLabel.'_legend');
		$this->content .= '</div>';

		return true;
	}
}