<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Value_form extends WForms_standard {
function show() {

	
	$IKU = $this->getValue( 'type' );



	
	if ( $IKU==7 ) return false;

	elseif ( $IKU == 2 ) $IKV = 2;

	else $IKV = 1;



	
	if ( !defined('CURRENCY_USED') ) 

	{

		$IKW = WClass::get('currency.format',null,'class',false);

		$IKW->set();

	}
		

	
	$IKX = WClass::get('currency.format',null,'class',false);

	$this->content = $IKX->displayAmount((real)$this->value, CURRENCY_USED, $IKV );

	return true;

}}