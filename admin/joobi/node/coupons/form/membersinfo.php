<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Membersinfo_form extends WForms_standard {



function show() {

	$IKY = WModel::getID('users');

	$IKZ = 'uid_'. $IKY;

	$IL0 = $this->data->$IKZ;



	if (empty($IL0) ) return true;

	if ( WGlobals::checkCandy(50) ) {

		$IL1 = WPage::linkPopUp( 'controller=order-users&task=show&eid='. $IL0 .'&titleheader='. $this->value );


		$this->content = WPage::createPopUpLink( $IL1, $this->value, 900, 600 );


	}
	else $this->content = $this->value;

	return true;

}}