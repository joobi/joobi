<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;






class Coupons_Nombre_form extends WForms_standard {


	function create() {


		if($this->nombre == '-1')

		{

			$this->nombre = TR1206961954EAMU;

		}

		elseif($this->nombre === '0')

		{

			$this->nombre = TR1206732410ICCJ;

		}

		else

		{

			$this->nombre = $this->nombre.' '.TR1210092398RSBS;

		}
		return true;
	}
}