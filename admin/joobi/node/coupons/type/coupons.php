<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Coupons_Coupons_type extends WTypes {
var $coupons = array(

	'2' => 'Discount Coupon',	
	'3' => 'Rebate Offer',	
	'1' => 'Gift Card',	
	'7' => 'Voucher'

);
}