<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Customvendid_filter {


	function create() {



		if ( IS_ADMIN ) return false;

		else {

			$IL4 = WUser::get('uid');



			
			
			static $IL5 = array();

	 		if ( !isset( $IL5[ $IL4 ] ) ) {

	 			$IL6 = WRole::get();

				$IL5[ $IL4 ] = $IL6->hasRole( 'storemanager' );

	 		}


			if ( !empty($IL5[ $IL4 ]) ) {

				$IL7 = WGlobals::getEID();

				return !empty( $IL7 ) ? $IL7 : false;

			}


			static $IL8 = null;

			
			if ( !isset( $IL8[ $IL4 ] ) && !empty( $IL4 ) ) {

				$IL9 = WClass::get('vendor.helper',null,'class',false);

				$IL8[ $IL4 ] = $IL9->getVendorID( $IL4 );

			}


	 		if ( empty( $IL8[ $IL4 ] ) || empty( $IL4  ) ) {

				$ILA = WMessage::get();

				$ILA->exitNow( 'Unauthorized access 330' );

			} else return $IL8[ $IL4 ];

		}


	}

}