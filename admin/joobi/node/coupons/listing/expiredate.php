<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











WLoadFile( 'listing.datetime' , JOOBI_LIB_HTML );

class Coupons_Expiredate_listing extends WListings_datetime {


function create() {

	static $IP8 = null;


	if ( !isset($IP8) ) $IP8 = time();



	if ( $this->value > 0 ) {


		$IP9 = ( $IP8 < $this->value ) ? true : false;

		parent::create();

		if ( $IP9 ) $this->content = (string)$this->value;

		else $this->content = '<span style="color:red;">'.  (string)$this->value .'</span>';


	} else {


		$this->content = TR1237967968ABUO;


	}


	return true;

}}