<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;











class Coupons_Currency_listing extends WListings_standard {


function create() {



	
	$IPU = $this->getValue( 'curid' );



	
	if( empty($IPU) )

	{

		$IPV = $this->getValue( 'vendid' );



		
		if( !empty($IPV) )

		{

			static $IPW=null;

			if(empty($IPW)) $IPW = WClass::get('vendor.helper',null,'class',false);

			$IPU = $IPW->getCurrency( $IPV );

		}


		
		if( empty($IPU) )

		{

			if ( !defined('CURRENCY_USED') )

			{

		            $IPX = WClass::get('currency.format',null,'class',false);

		            $IPX->set();

		        }


		        $IPU = CURRENCY_USED;

		}
	}


	
	$IPY = WClass::get('currency.format',null,'class',false);

	$this->content = $IPY->displayAmount($this->value, $IPU, 1);

	return true;

}



















}