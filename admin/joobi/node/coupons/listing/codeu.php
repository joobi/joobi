<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Codeu_listing extends WListings_standard {
	function create() {

		$this->value = strtoupper($this->value);

		return parent::create();

	}}