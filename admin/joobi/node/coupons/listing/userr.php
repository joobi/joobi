<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Userr_listing extends WListings_standard {


function create() {

	$IQ9 = $this->getValue( 'alluser' );



	if ( $IQ9 == 0 ) {

		$this->content = TR1206961902CIFI .'(<font color=red>'. $this->value .'</font>)';

	} else {

		static $IQA=null;

		if (empty($IQA)) $IQA = $this->mapList['rolid'];



		static $IQB=array();

		if ( !isset($IQB[$this->data->$IQA]) ) {

			static $IQC=null;

			if (empty($IQC)) $IQC = WModel::get( 'roletrans' );

			$IQC->whereE( 'rolid', $this->data->$IQA );

			$IQB[$this->data->$IQA] = $IQC->load( 'r', 'name');

		}
		$IQD = TR1251253346BUAT;

		$this->content = '<a title="'.$IQD.'">'. $IQB[$this->data->$IQA] .'</a>';

	}
	return true;

}}