<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Value_listing extends WListings_standard {


	function create() {

		
		$IQ0 = $this->getValue( 'type' );

	

		
		if ( $IQ0 == 2 ) {

			$IQ1 = 2;	

			$IQ2 = $this->value;

		} else {

			$IQ1 = 1;

			$IQ2 = $this->getValue( 'price' );

		}


		$IQ3 = $this->getValue( 'curid' );

		
		$IQ4 = WClass::get('currency.format',null,'class',false);

		$this->content = $IQ4->displayAmount( (real)$IQ2, $IQ3, $IQ1 );

		

		return true;

		

	}}