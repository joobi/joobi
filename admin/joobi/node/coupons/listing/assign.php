<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Assign_listing extends WListings_standard {
function create() {		

	$IPN = WGlobals::get('coupid');

	$IPO = WGlobals::get('titleheader');

	$IPP = $this->getValue( 'pid' );

	$IPQ = WGlobals::get('catid');

	if ( $IPQ == null ) $IPQ = 0;



	$IPR = WPage::routeURL('controller=coupons-product&task=assigncoupon&catid='. $IPQ .'&eid='. $IPP .'&coupid='. $IPN .'&titleheader='. $IPO);

	

	if ( ( $this->value > 0 ) && ( $this->value == $IPN ) ) { 

		$IPS = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/yes.png';

		$IPT = '<img src="'. $IPS .'"/>';

	} else {

		$IPS = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/cancel.png';

		$IPT = '<img src="'. $IPS .'"/>';

	}
	

	$this->content = '<a href="'. $IPR .'">'. $IPT .'</a>';

	return true;

}}