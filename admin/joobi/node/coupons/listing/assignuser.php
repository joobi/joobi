<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Assignuser_listing extends WListings_standard {




function create()

{

	$IPB = $this->value;

	$IPC = WGlobals::get( 'titleheader' );

	$IPD =  WGlobals::get( 'coupid' );

	$IPE = $this->getValue( 'coupid' );

	

	$IPF = WPage::routeURL( 'controller=coupons-user&task=assigncoupon&eid='. $IPB .'&coupid='. $IPD .'&titleheader='. $IPC );

	

	if ( ( $IPE > 0 ) && ( $IPE == $IPD ) ) { 

		$IPG = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/yes.png';

		$IPH = '<img src="'. $IPG .'"/>';

	} else {

		$IPG = JOOBI_URL_JOOBI_IMAGES . 'toolbar/16/cancel.png';

		$IPH = '<img src="'. $IPG .'"/>';

	}
	

	$this->content = '<a href="'. $IPF .'">'. $IPH .'</a>';

	return true;

}}