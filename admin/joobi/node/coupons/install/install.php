<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;




class Coupons_Node_install extends WInstall {


	public function install(&$O1) {


				if ( $this->newInstall ) {

									$IMF = WGet::folder();
			$IMG = JOOBI_DS_NODE . 'coupons' . DS . 'install' . DS . 'default';
			$IMH = JOOBI_DS_THEME . 'coupon' . DS . 'default';
			if ( $IMF->exist($IMG) ) {
				$IMF->move( $IMG, $IMH );
			}
		}
	}

}