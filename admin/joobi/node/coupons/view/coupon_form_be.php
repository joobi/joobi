<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Coupon_form_be_view extends Output_Forms_class {
protected function prepareView() {



	$IKO = $this->getValue('type');

	if ( empty($IKO) ) return true;



	
	if ( 2 == $IKO ) $this->removeElements( array( 'coupon_form_price', 'coupon_form_currency') );

	elseif ( 7 == $IKO ) $this->removeElements( array( 'coupon_form_price', 'coupon_form_currency', 'coupon_form_percentage', 'coupon_form_cumulative', 'coupon_form_be_coupons_allcategory_product' ) );

	else  $this->removeElements( 'coupon_form_percentage', 'coupon_form_cumulative' );



	return parent::prepareView();



}}