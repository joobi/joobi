<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;


class Coupons_Coupon_form_view extends Output_Forms_class {
protected function prepareView() {



	$IKP = $this->getValue('type');

	if ( empty($IKP) ) return true;



	
	if ( 2 == $IKP ) $this->removeElements( array( 'coupon_form_price', 'coupon_form_currency') );	
	else  $this->removeElements( 'coupon_form_percentage', 'coupon_form_cumulative' ); 	
	

	




	return parent::prepareView();



}}