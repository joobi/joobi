<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;














class Category_Node_model extends WModel {

var $_returnId=true;

var $_deleteLeaf=false;
var $_parent=null; var $_lft=null;var $_rgt=null;

var $_recalculate=false; 
var $_under=null;var $_keepAttributesOnDelete=true;





function addValidate(){

$I17F=$this->getParam('parentmap','parent');
if(!empty($this->$I17F )){
$this->_parent=$this->$I17F;
}
return true;
}





function editValidate(){
$I17G=$this->getPK();
$I17F=$this->getParam('parentmap','parent');
$this->_parent=$this->$I17F;

if(empty( $this->$I17G)){

$this->whereE($I17G, $this->$I17G);
$I17H=$this->load('o', array($I17F,'lft','rgt'));

if(empty($this->$I17F)){
$this->$I17F=$I17H->$I17F;
}elseif($I17H->$I17F !=$this->$I17F){
if($this->$I17F==$this->$I17G){
$I17I=WMessage::get();
$I17I->userW('1213020899EZVY');
return false;
}
$this->whereE($I17G, $this->$I17F);
$I17J=$this->load('o', array('lft', 'rgt',$I17F));

if(( $I17J->lft > $I17H->lft) && ($I17J->rgt < $I17H->rgt)){
$I17I=WMessage::get();
$I17I->userW('1220361707FSCI');
return false;










}
$this->_recalculate=true;
}}return true;

}







function addExtra(){
            
            $I17K=$this->getParam('parentmap','parent');
            
            if(empty($this->$I17K)) return true;

      $I17L=$this->getParam('modelParent',0);

      if(empty($I17L)){
            if(!$this->getParam('skipdepth',false)) $this->select('depth');
$I17M=$this->load( $this->$I17K, array( $I17K , 'lft', 'rgt'));
if(empty($I17M)){
      $I17N=WMessage::get();
      $I17N->codeE('Could not load the parent information with the name '.$I17K.' for the save of a category.');
return false;
}
      }else{
            $I17O=WModel::get($I17L);
      if(!$this->getParam('skipdepth',false)) $this->select('depth');
      $I17M=$I17O->load($this->$I17K, array( $I17K, 'lft', 'rgt'));

      $I17O->whereE( $this->getPK(), $I17M->$I17K );
      $I17O->updatePlus('rgt', 2);
      $I17O->update();

      }if(empty($I17M)){
      $I17N=WMessage::get();
      $I17N->codeE('Could not load the parent model with the name '.$I17K.' for the save of a category.');
      return false;
}
            if(!empty($I17L)){
      $this->whereE($I17K, $I17M->$I17K);
      }
            $this->where('rgt','>=', $I17M->rgt);
      $this->updatePlus('rgt', 2);
      $this->returnId(false);
      $this->update();

      if(!empty($I17L)){
      $this->whereE($I17K, $I17M->$I17K);
      }
      $this->where('lft','>=', $I17M->rgt);
      $this->updatePlus('lft', 2);
      $this->returnId(false);
      $this->update();


                  $this->lft=$I17M->rgt;
      $this->rgt=$I17M->rgt+1;

      if(!$this->getParam('skipdepth',false)) $this->depth=$I17M->depth+1;
            $this->$I17K=$this->_parent;

      $I17P=$this->getPK();
      $this->whereE($I17P, $this->$I17P);
      $this->setLimit(1);
      $this->update();

      WGlobals::set( 'catdepth'.$this->getModelID(), null, 'session' );

      return true;

}






function editExtra($O1=null){
if($this->_recalculate) $this->redoTree();

return true;
}











 function deleteValidate($O2=null){

if(empty($O2)){
return true;
}
if($O2==1 ) return false;

$I17F=$this->getParam('parentmap','parent');
$I17Q=$this->getPK();
$this->whereE( $I17Q, $O2 );
$I17R=$this->load( 'o', array('lft','rgt',$I17F));

if(empty($I17R)) return true;

$this->_lft=$I17R->lft;
$this->_rgt=$I17R->rgt;
$this->_parent=$I17R->$I17F;

$I17S=true;
if(!$this->_deleteLeaf){
if( $I17R->rgt - $I17R->lft !=1){
$I17N=WMessage::get();

$I17T=$this->getTranslatedName();
$I17N->userW('1235560364LCKV',array('$NAME'=>$I17T,'$NAME'=>$I17T));

$this->whereE( $I17F, $O2 );
$this->setVal( $I17F, $this->_parent );
$this->setIgnore();
$I17S=$I17S && $this->update();
$this->setIgnore( false );

$this->where('rgt','<',$this->_rgt);
$this->where('lft','>',$this->_lft);
$this->updatePlus('rgt',-1);
$this->updatePlus('lft', -1);

if(!$this->getParam('skipdepth',false)) $this->updatePlus('depth',-1);
$I17S=$I17S && $this->update();
}}else{



}
$this->where('rgt','>',$this->_rgt);
$this->updatePlus('rgt',-2);
$I17S=$I17S && $this->update();

$this->where( 'lft', '>', $this->_rgt );
$this->updatePlus( 'lft', -2 );
$I17S=$I17S && $this->update();
return $I17S;
 }




 function deleteExtra($O3=0){
return true;
  }






 

















































function getChildNode($O4='1',$O5=false){
static $I17U=array();

if(empty($O4)) return false;
$this->whereE( $this->getPK() , $O4 );
$I17V=$this->load('o', array( 'lft', 'rgt'));
if(empty($I17V)) return true;
if(($I17V->lft +1)==$I17V->rgt){
return $O5 ? array( $O4 ) : array();
}
$I17W=$I17V->lft .'.'.$I17V->rgt . '.' . $this->_infos->sid;
if(!isset($I17U[$I17W])){

if($O5){
$this->where('lft', '>=', $I17V->lft);
$this->where('rgt', '<=', $I17V->rgt);
}else{
$this->where('lft', '>', $I17V->lft);
   $this->where('rgt', '<', $I17V->rgt);
}$I17U[$I17W]=$this->load('lra', $this->getPK());
}
return $I17U[$I17W];

}







  function getAllParents($O6='1'){

  $I17X=WModel::get( $this->getModelID());   $I17X->makeLJ($this->getModelID(),'lft','lft',0,1,'>='); 
      $I17Y=WModel::get( $this->getModelID(), 'namekey' );
$I17Z=$I17Y . 'trans';
$I180=WModel::get( $I17Z, 'sid', null, false );
if(!empty($I180)){
$I17L=$I17Z;
}else{
$I17L=$this->getParam( 'prtname', '' );
}
$I17Q=$this->getPK();
  if(!empty( $I17L )){
    $I17X->makeLJ($I17L,$I17Q,$I17Q,1,2);  $I17X->whereLanguage(2);
    $I17X->select('name',2);
  }else{
  $I17X->select( 'name', 1 );
  }
  $I17X->select( 'namekey', 1 );
  $I17X->where('rgt','>=','rgt',1,0);
  $I17X->whereE( $I17Q, $O6, 0 );
  $I17X->where( 'rgt', '!=', 0 , 0 );

  $I17X->orderBy('lft','ASC',1);
$I17X->setDistinct();
$I17X->select($I17Q,1);
  $I181=$I17X->load('ol');
  return $I181;
  }









  public function getItemDefaultCategory($O7){
  return 0;
  }






function redoTree(){

$I182=WModel::get($this->getModelID());
$I182->orderBy('lft','ASC');
$I17Q=$I182->getPK();
$I17F=$I182->getParam('parentmap','parent');

if(!$I182->getParam('skipdepth',false)) $I182->select('depth');
$I182->select(array($I17F,$I17Q,'lft','rgt'));
$I182->setLimit( 10000000 );
$I183=$I182->load('ol');

$this->_mesCats=array();

foreach($I183 as $I184){
$this->_mesCats[$I184->$I17F][]=$I184;
if(empty($I184->$I17F)) $I185=$I184;
}
$I186=null;
$I186->lft=1;
$I186->depth=0;
$I186=$this->l1x_27_7oc( $I185, $I186, $I17Q, $I17F );

}








private function l1x_27_7oc($O8,$O9,$OA,$OB){
$I187=$O9->lft;
$I188=$O9->depth;
if(!empty($this->_mesCats[$O8->$OA])){
$O9->depth ++;
foreach($this->_mesCats[$O8->$OA] as $I189){
$O9->lft ++;
$O9=$this->l1x_27_7oc($I189,$O9,$OA,$OB);
}
$O9->depth --;
}

$O9->lft++;

if($O9->lft !=$O8->rgt OR $I187 !=$O8->lft OR (!$this->getParam('skipdepth',false) AND $I188!=$O8->depth)){
$this->setVal('lft',$I187);
$this->setVal('rgt',$O9->lft);

if(!$this->getParam('skipdepth',false)) $this->setVal('depth',$I188);
$this->whereE($OA,$O8->$OA);
$this->setLimit(1);
$this->update();
}

return $O9;
}
}