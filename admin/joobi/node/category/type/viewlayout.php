<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;



class Category_Viewlayout_type extends WTypes {


var $viewlayout=array(

1=> 'List View',

2=> 'Grid View - Normal',

3=> 'Grid View - Big',

4=> 'Grid View - Small'

  );
}