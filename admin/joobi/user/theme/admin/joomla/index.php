<?php 
/** @copyright Copyright (c) 2007-2013 Joobi Limited. All rights reserved.
* @link http://www.joobi.co
* @license GNU GPLv3 */
defined('_JEXEC') or die;
?>

<?php 
$isPopUp = WGlobals::get( 'is_popup', false, 'joobi' );
$showHeader = ( $isPopUp ) ? false : true;
$viewBodyClass = $this->getContent('viewBodyClass');
WPage::addCSS( '#element-box{padding-top:0;}' );//overwrite of Joomla BE template to remove padding at the top
?>
<div class="clr"></div>
<div id="pageWrapper">
	<?php if( $showHeader ): 
		$tabS = $this->getContent('tabs');	
		$devModuleHTML = WGlobals::get( 'devModuleHTML', '', 'joobi' );
		if ( !empty($tabS) || !empty($devModuleHTML) ) :
	?>
	<div id="pageHeader">
		<div id="pHLeft">
			<div id="pHRight">
				<div id="pHCenter">
					<div id="pHPosLeft">
						<div id="Logo"></div>
					</div>
					<div id="pHPosRight">
					<?php if( $tabS ) : echo $tabS; endif; ?>
					</div>
					<?php echo WGlobals::get( 'devModuleHTML', '', 'joobi' ); ?>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	</div>
	<?php 
		else :
		echo '<div class="topSpacing"></div>';
		endif;
		endif;
	?>


	<div class="viewBody<?php  echo $viewBodyClass; ?>">
		
		<?php if($breadS = $this->getContent('breadcrumbs') ) : ?>
		<div id="pathwayLeft">
			<div id="pathwayRight">
				<div id="pathwayCenter">
					<?php  echo $breadS; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php if( $beforeHeaderS = $this->getContent('beforeHeader') ) echo $beforeHeaderS; ?>
		<?php if( $messageS = $this->message() ) : ?>
		<div id="message">
		<?php  echo $messageS; ?>
		</div>
		<?php endif; ?>
		<?php if( $headerMenuS = $this->getContent('headerMenu') ) : ?>
		<div id="toolbarBox">
			<div id="toolbarBoxLeft">
				<div id="toolbarBoxRight">
					<div id="toolbarBoxCenter">
					<?php echo $headerMenuS; ?>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php if( $infoS = $this->getContent('information') ) : echo $infoS; endif; ?>		
		<?php if( $wizS = $this->getContent('wizard') ) : echo $wizS; endif; ?>
		<div id="helpArea"></div>
		<?php if( $applicationS = $this->getContent('application') ) echo $applicationS; 
		?>
		<?php if( $bottomMenuS = $this->getContent('bottomMenu') ) : ?>
		<div class="curveBottomLeft">
			<div class="curveBottomRight">
				<div class="curveBottomCenter">	
				<?php echo $bottomMenuS; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<?php if( $legendS = $this->getContent('legend') ) : ?>
		<div class="curveFootLeft">
			<div class="curveFootRight">
				<div class="curveFootCenter">
				<?php echo $legendS; ?>
				</div>
			</div>
		</div>
		<?php endif; ?>
		<div class="clr"></div>
		<?php if( $menuS = $this->getContent('footer')) echo $menuS; ?>
		<?php if( $debugS = $this->getContent('debugTrace')) : echo $debugS; endif; ?>
		<div class="clr"></div>
	</div>
<?php
if( 
//__START__
!defined('PLIBRARY_NODE_PLEX') || !PLIBRARY_NODE_PLEX &&
//__END__
$debugS = $this->getContent('debugTrace') ) : echo $debugS; endif; 
?>
	
</div>